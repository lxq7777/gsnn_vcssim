 /*                                                                      
 Copyright 2018-2020 Nuclei System Technology, Inc.                
                                                                         
 Licensed under the Apache License, Version 2.0 (the "License");         
 you may not use this file except in compliance with the License.        
 You may obtain a copy of the License at                                 
                                                                         
     http://www.apache.org/licenses/LICENSE-2.0                          
                                                                         
  Unless required by applicable law or agreed to in writing, software    
 distributed under the License is distributed on an "AS IS" BASIS,       
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and     
 limitations under the License.                                          
 */                                                                      
                                                                         
                                                                         
                                                                         
//=====================================================================
//
// Designer   : Seven Lai
//
// Description:
//  User extend peirpheral bus and the connected devices 
//
// ====================================================================

`include "e203_defines.v"


module e203_subsys_extperips(
  input                          extppi_icb_cmd_valid,
  output                         extppi_icb_cmd_ready,
  input  [`E203_ADDR_SIZE-1:0]   extppi_icb_cmd_addr, 
  input                          extppi_icb_cmd_read, 
  input  [`E203_XLEN-1:0]        extppi_icb_cmd_wdata,
  input  [`E203_XLEN/8-1:0]      extppi_icb_cmd_wmask,
  //
  output                         extppi_icb_rsp_valid,
  input                          extppi_icb_rsp_ready,
  output                         extppi_icb_rsp_err,
  output [`E203_XLEN-1:0]        extppi_icb_rsp_rdata,

  input  clk,
  input  bus_rst_n,
  input  rst_n
  );

  wire                     vga_icb_cmd_valid;
  wire                     vga_icb_cmd_ready;
  wire [32-1:0]            vga_icb_cmd_addr; 
  wire                     vga_icb_cmd_read; 
  wire [32-1:0]            vga_icb_cmd_wdata;
  wire [4 -1:0]            vga_icb_cmd_wmask;

  wire                     vga_icb_rsp_valid;
  wire                     vga_icb_rsp_ready;
  wire [32-1:0]            vga_icb_rsp_rdata;
  wire                     vga_icb_rsp_err;

  wire                     exttest_icb_cmd_valid;
  wire                     exttest_icb_cmd_ready;
  wire [32-1:0]            exttest_icb_cmd_addr; 
  wire                     exttest_icb_cmd_read; 
  wire [32-1:0]            exttest_icb_cmd_wdata;
  wire [4 -1:0]            exttest_icb_cmd_wmask;

  wire                     exttest_icb_rsp_valid;
  wire                     exttest_icb_rsp_ready;
  wire [32-1:0]            exttest_icb_rsp_rdata;
  wire                     exttest_icb_rsp_err;

  // The total address range for the EXTPPI is from/to
  //  **************0x1200 0000 -- 0x19FF FFFF
  // There are several slaves for EXTPPI bus, including:
  //  * TEST                : 0x1200 0000 -- 0x12FF FFFF
  //  * VGA                 : 0x1300 0000 -- 0x13FF FFFF
  //  * Preserved GSNN      : 0x1400 0000 -- 0x14FF FFFF
  //  * Preserved 3         : 0x1500 0000 -- 0x150F FFFF
  //  * Preserved 4         : 0x1510 0000 -- 0x151F FFFF
  //  * Preserved 5         : 0x1520 0000 -- 0x152F FFFF
  //  * Preserved 6         : 0x1530 0000 -- 0x153F FFFF
  //  * Preserved 7         : 0x1540 0000 -- 0x154F FFFF
  //  * Preserved 8         : 0x1550 0000 -- 0x155F FFFF
  //  * Preserved 9         : 0x1560 0000 -- 0x156F FFFF
  //  * Preserved 10        : 0x1570 0000 -- 0x157F FFFF
  //  * Preserved 11        : 0x1580 0000 -- 0x158F FFFF
  //  * Preserved 12        : 0x1590 0000 -- 0x159F FFFF
  //  * Preserved 13        : 0x15A0 0000 -- 0x15AF FFFF
  //  * Preserved 14        : 0x15B0 0000 -- 0x15BF FFFF
  //  * Preserved 15        : 0x15C0 0000 -- 0x15CF FFFF


  sirv_icb1to16_bus # (
  .ICB_FIFO_DP        (2),// We add a ping-pong buffer here to cut down the timing path
  .ICB_FIFO_CUT_READY (1),// We configure it to cut down the back-pressure ready signal

  .AW                   (32),
  .DW                   (`E203_XLEN),
  .SPLT_FIFO_OUTS_NUM   (1),// The peirpherals only allow 1 oustanding
  .SPLT_FIFO_CUT_READY  (1),// The peirpherals always cut ready
  //  * EXTTEST         : 0x1200 0000 -- 0x12FF FFFF
  .O0_BASE_ADDR       (32'h1200_0000),       
  .O0_BASE_REGION_LSB (24),
  //  * VGA             : 0x1300 0000 -- 0x13FF FFFF
  .O1_BASE_ADDR       (32'h1300_0000),       
  .O1_BASE_REGION_LSB (24),
  //  * Preserved 2 : 0x1400 0000 -- 0x14FF FFFF
  .O2_BASE_ADDR       (32'h1400_0000),       
  .O2_BASE_REGION_LSB (24),
  //  * Preserved 3 : 0x1500 0000 -- 0x150F FFFF
  .O3_BASE_ADDR       (32'h1500_0000),       
  .O3_BASE_REGION_LSB (20),
  //  * Preserved 4 : 0x1510 0000 -- 0x151F FFFF
  .O4_BASE_ADDR       (32'h1510_0000),       
  .O4_BASE_REGION_LSB (20),
  //  * Preserved 5 : 0x1520 0000 -- 0x152F FFFF
  .O5_BASE_ADDR       (32'h1520_0000),       
  .O5_BASE_REGION_LSB (20),
  //  * Preserved 6 : 0x1530 0000 -- 0x153F FFFF
  .O6_BASE_ADDR       (32'h1530_0000),       
  .O6_BASE_REGION_LSB (20),
  //  * Preserved 7 : 0x1540 0000 -- 0x154F FFFF
  .O7_BASE_ADDR       (32'h1540_0000),       
  .O7_BASE_REGION_LSB (20),
  //  * Preserved 8 : 0x1550 0000 -- 0x155F FFFF
  .O8_BASE_ADDR       (32'h1550_0000),       
  .O8_BASE_REGION_LSB (20),
  //  * Preserved 9 : 0x1560 0000 -- 0x156F FFFF
  .O9_BASE_ADDR       (32'h1560_0000),       
  .O9_BASE_REGION_LSB (20),
  //  * Preserved 10 : 0x1570 0000 -- 0x157F FFFF
  .O10_BASE_ADDR       (32'h1570_0000),       
  .O10_BASE_REGION_LSB (20),
  //  * Preserved 11 : 0x1580 0000 -- 0x158F FFFF
  .O11_BASE_ADDR       (32'h1580_0000),       
  .O11_BASE_REGION_LSB (20),
  //  * Preserved 12 : 0x1590 0000 -- 0x159F FFFF
  .O12_BASE_ADDR       (32'h1590_0000),       
  .O12_BASE_REGION_LSB (20),
  //  * Preserved 13 : 0x15A0 0000 -- 0x15AF FFFF
  .O13_BASE_ADDR       (32'h15A0_0000),       
  .O13_BASE_REGION_LSB (20),
  //  * Preserved 14 : 0x15B0 0000 -- 0x15BF FFFF
  .O14_BASE_ADDR       (32'h15B0_0000),       
  .O14_BASE_REGION_LSB (20),
  //  * Preserved 15 : 0x15C0 0000 -- 0x15CF FFFF
  .O15_BASE_ADDR       (32'h15C0_0000),       
  .O15_BASE_REGION_LSB (20)

  )u_sirv_extppi_fab(

    .i_icb_cmd_valid  (extppi_icb_cmd_valid),
    .i_icb_cmd_ready  (extppi_icb_cmd_ready),
    .i_icb_cmd_addr   (extppi_icb_cmd_addr ),
    .i_icb_cmd_read   (extppi_icb_cmd_read ),
    .i_icb_cmd_wdata  (extppi_icb_cmd_wdata),
    .i_icb_cmd_wmask  (extppi_icb_cmd_wmask),
    .i_icb_cmd_lock   (1'b0),
    .i_icb_cmd_excl   (1'b0 ),
    .i_icb_cmd_size   (2'b0 ),
    .i_icb_cmd_burst  (2'b0 ),
    .i_icb_cmd_beat   (2'b0 ),
    
    .i_icb_rsp_valid  (extppi_icb_rsp_valid),
    .i_icb_rsp_ready  (extppi_icb_rsp_ready),
    .i_icb_rsp_err    (extppi_icb_rsp_err  ),
    .i_icb_rsp_excl_ok(),
    .i_icb_rsp_rdata  (extppi_icb_rsp_rdata),
    
  //  * TEST    
    .o0_icb_enable     (1'b1),

    .o0_icb_cmd_valid  (exttest_icb_cmd_valid),
    .o0_icb_cmd_ready  (exttest_icb_cmd_ready),
    .o0_icb_cmd_addr   (exttest_icb_cmd_addr),
    .o0_icb_cmd_read   (exttest_icb_cmd_read),
    .o0_icb_cmd_wdata  (exttest_icb_cmd_wdata),
    .o0_icb_cmd_wmask  (exttest_icb_cmd_wmask),
    .o0_icb_cmd_lock   (),
    .o0_icb_cmd_excl   (),
    .o0_icb_cmd_size   (),
    .o0_icb_cmd_burst  (),
    .o0_icb_cmd_beat   (),
    
    .o0_icb_rsp_valid  (exttest_icb_rsp_valid),
    .o0_icb_rsp_ready  (exttest_icb_rsp_ready),
    .o0_icb_rsp_err    (exttest_icb_rsp_err),
    .o0_icb_rsp_excl_ok(1'b0),
    .o0_icb_rsp_rdata  (exttest_icb_rsp_rdata),

  //  * VGA
    .o1_icb_enable     (1'b1),

    .o1_icb_cmd_valid  (vga_icb_cmd_valid),
    .o1_icb_cmd_ready  (vga_icb_cmd_ready),
    .o1_icb_cmd_addr   (vga_icb_cmd_addr),
    .o1_icb_cmd_read   (vga_icb_cmd_read),
    .o1_icb_cmd_wdata  (vga_icb_cmd_wdata),
    .o1_icb_cmd_wmask  (vga_icb_cmd_wmask),
    .o1_icb_cmd_lock   (),
    .o1_icb_cmd_excl   (),
    .o1_icb_cmd_size   (),
    .o1_icb_cmd_burst  (),
    .o1_icb_cmd_beat   (),
    
    .o1_icb_rsp_valid  (vga_icb_rsp_valid),
    .o1_icb_rsp_ready  (vga_icb_rsp_ready),
    .o1_icb_rsp_err    (vga_icb_rsp_err),
    .o1_icb_rsp_excl_ok(1'b0),
    .o1_icb_rsp_rdata  (vga_icb_rsp_rdata),


  //  * Preserved 2      
    .o2_icb_enable     (1'b0),

    .o2_icb_cmd_valid  (),
    .o2_icb_cmd_ready  (1'b0),
    .o2_icb_cmd_addr   (),
    .o2_icb_cmd_read   (),
    .o2_icb_cmd_wdata  (),
    .o2_icb_cmd_wmask  (),
    .o2_icb_cmd_lock   (),
    .o2_icb_cmd_excl   (),
    .o2_icb_cmd_size   (),
    .o2_icb_cmd_burst  (),
    .o2_icb_cmd_beat   (),
    
    .o2_icb_rsp_valid  (1'b0),
    .o2_icb_rsp_ready  (),
    .o2_icb_rsp_err    (1'b0),
    .o2_icb_rsp_excl_ok(1'b0),
    .o2_icb_rsp_rdata  (32'b0),

  //  * Preserved 3     
    .o3_icb_enable     (1'b0),

    .o3_icb_cmd_valid  (),
    .o3_icb_cmd_ready  (1'b0),
    .o3_icb_cmd_addr   (),
    .o3_icb_cmd_read   (),
    .o3_icb_cmd_wdata  (),
    .o3_icb_cmd_wmask  (),
    .o3_icb_cmd_lock   (),
    .o3_icb_cmd_excl   (),
    .o3_icb_cmd_size   (),
    .o3_icb_cmd_burst  (),
    .o3_icb_cmd_beat   (),
    
    .o3_icb_rsp_valid  (1'b0),
    .o3_icb_rsp_ready  (),
    .o3_icb_rsp_err    (1'b0),
    .o3_icb_rsp_excl_ok(1'b0),
    .o3_icb_rsp_rdata  (32'b0),

  //  * Preserved 4     
    .o4_icb_enable     (1'b0),

    .o4_icb_cmd_valid  (),
    .o4_icb_cmd_ready  (1'b0),
    .o4_icb_cmd_addr   (),
    .o4_icb_cmd_read   (),
    .o4_icb_cmd_wdata  (),
    .o4_icb_cmd_wmask  (),
    .o4_icb_cmd_lock   (),
    .o4_icb_cmd_excl   (),
    .o4_icb_cmd_size   (),
    .o4_icb_cmd_burst  (),
    .o4_icb_cmd_beat   (),
    
    .o4_icb_rsp_valid  (1'b0),
    .o4_icb_rsp_ready  (),
    .o4_icb_rsp_err    (1'b0),
    .o4_icb_rsp_excl_ok(1'b0),
    .o4_icb_rsp_rdata  (32'b0),


  //  * Preserved 5      
    .o5_icb_enable     (1'b0),

    .o5_icb_cmd_valid  (),
    .o5_icb_cmd_ready  (1'b0),
    .o5_icb_cmd_addr   (),
    .o5_icb_cmd_read   (),
    .o5_icb_cmd_wdata  (),
    .o5_icb_cmd_wmask  (),
    .o5_icb_cmd_lock   (),
    .o5_icb_cmd_excl   (),
    .o5_icb_cmd_size   (),
    .o5_icb_cmd_burst  (),
    .o5_icb_cmd_beat   (),
    
    .o5_icb_rsp_valid  (1'b0),
    .o5_icb_rsp_ready  (),
    .o5_icb_rsp_err    (1'b0),
    .o5_icb_rsp_excl_ok(1'b0),
    .o5_icb_rsp_rdata  (32'b0),

  //  * Preserved 6     
    .o6_icb_enable     (1'b0),

    .o6_icb_cmd_valid  (),
    .o6_icb_cmd_ready  (1'b0),
    .o6_icb_cmd_addr   (),
    .o6_icb_cmd_read   (),
    .o6_icb_cmd_wdata  (),
    .o6_icb_cmd_wmask  (),
    .o6_icb_cmd_lock   (),
    .o6_icb_cmd_excl   (),
    .o6_icb_cmd_size   (),
    .o6_icb_cmd_burst  (),
    .o6_icb_cmd_beat   (),
    
    .o6_icb_rsp_valid  (1'b0),
    .o6_icb_rsp_ready  (),
    .o6_icb_rsp_err    (1'b0),
    .o6_icb_rsp_excl_ok(1'b0),
    .o6_icb_rsp_rdata  (32'b0),

  //  * Preserved 7    
    .o7_icb_enable     (1'b0),

    .o7_icb_cmd_valid  (),
    .o7_icb_cmd_ready  (1'b0),
    .o7_icb_cmd_addr   (),
    .o7_icb_cmd_read   (),
    .o7_icb_cmd_wdata  (),
    .o7_icb_cmd_wmask  (),
    .o7_icb_cmd_lock   (),
    .o7_icb_cmd_excl   (),
    .o7_icb_cmd_size   (),
    .o7_icb_cmd_burst  (),
    .o7_icb_cmd_beat   (),
    
    .o7_icb_rsp_valid  (1'b0),
    .o7_icb_rsp_ready  (),
    .o7_icb_rsp_err    (1'b0),
    .o7_icb_rsp_excl_ok(1'b0),
    .o7_icb_rsp_rdata  (32'b0),

  //  * Preserved 8    
    .o8_icb_enable     (1'b0),

    .o8_icb_cmd_valid  (),
    .o8_icb_cmd_ready  (1'b0),
    .o8_icb_cmd_addr   (),
    .o8_icb_cmd_read   (),
    .o8_icb_cmd_wdata  (),
    .o8_icb_cmd_wmask  (),
    .o8_icb_cmd_lock   (),
    .o8_icb_cmd_excl   (),
    .o8_icb_cmd_size   (),
    .o8_icb_cmd_burst  (),
    .o8_icb_cmd_beat   (),
    
    .o8_icb_rsp_valid  (1'b0),
    .o8_icb_rsp_ready  (),
    .o8_icb_rsp_err    (1'b0),
    .o8_icb_rsp_excl_ok(1'b0),
    .o8_icb_rsp_rdata  (32'b0),

  //  * Preserved 9   
    .o9_icb_enable     (1'b0),

    .o9_icb_cmd_valid  (),
    .o9_icb_cmd_ready  (1'b0),
    .o9_icb_cmd_addr   (),
    .o9_icb_cmd_read   (),
    .o9_icb_cmd_wdata  (),
    .o9_icb_cmd_wmask  (),
    .o9_icb_cmd_lock   (),
    .o9_icb_cmd_excl   (),
    .o9_icb_cmd_size   (),
    .o9_icb_cmd_burst  (),
    .o9_icb_cmd_beat   (),
    
    .o9_icb_rsp_valid  (1'b0),
    .o9_icb_rsp_ready  (),
    .o9_icb_rsp_err    (1'b0),
    .o9_icb_rsp_excl_ok(1'b0),
    .o9_icb_rsp_rdata  (32'b0),

  //  * Preserved 10  
    .o10_icb_enable     (1'b0),

    .o10_icb_cmd_valid  (),
    .o10_icb_cmd_ready  (1'b0),
    .o10_icb_cmd_addr   (),
    .o10_icb_cmd_read   (),
    .o10_icb_cmd_wdata  (),
    .o10_icb_cmd_wmask  (),
    .o10_icb_cmd_lock   (),
    .o10_icb_cmd_excl   (),
    .o10_icb_cmd_size   (),
    .o10_icb_cmd_burst  (),
    .o10_icb_cmd_beat   (),
    
    .o10_icb_rsp_valid  (1'b0),
    .o10_icb_rsp_ready  (),
    .o10_icb_rsp_err    (1'b0),
    .o10_icb_rsp_excl_ok(1'b0),
    .o10_icb_rsp_rdata  (32'b0),

  //  * Preserved 11
    .o11_icb_enable     (1'b0),

    .o11_icb_cmd_valid  (),
    .o11_icb_cmd_ready  (1'b0),
    .o11_icb_cmd_addr   (),
    .o11_icb_cmd_read   (),
    .o11_icb_cmd_wdata  (),
    .o11_icb_cmd_wmask  (),
    .o11_icb_cmd_lock   (),
    .o11_icb_cmd_excl   (),
    .o11_icb_cmd_size   (),
    .o11_icb_cmd_burst  (),
    .o11_icb_cmd_beat   (),
    
    .o11_icb_rsp_valid  (1'b0),
    .o11_icb_rsp_ready  (),
    .o11_icb_rsp_err    (1'b0),
    .o11_icb_rsp_excl_ok(1'b0),
    .o11_icb_rsp_rdata  (32'b0),     

  //  * Preserved 12
    .o12_icb_enable     (1'b0),

    .o12_icb_cmd_valid  (),
    .o12_icb_cmd_ready  (1'b0),
    .o12_icb_cmd_addr   (),
    .o12_icb_cmd_read   (),
    .o12_icb_cmd_wdata  (),
    .o12_icb_cmd_wmask  (),
    .o12_icb_cmd_lock   (),
    .o12_icb_cmd_excl   (),
    .o12_icb_cmd_size   (),
    .o12_icb_cmd_burst  (),
    .o12_icb_cmd_beat   (),
    
    .o12_icb_rsp_valid  (1'b0),
    .o12_icb_rsp_ready  (),
    .o12_icb_rsp_err    (1'b0),
    .o12_icb_rsp_excl_ok(1'b0),
    .o12_icb_rsp_rdata  (32'b0),

  //  * Preserved 13
    .o13_icb_enable     (1'b0),

    .o13_icb_cmd_valid  (),
    .o13_icb_cmd_ready  (1'b0),
    .o13_icb_cmd_addr   (),
    .o13_icb_cmd_read   (),
    .o13_icb_cmd_wdata  (),
    .o13_icb_cmd_wmask  (),
    .o13_icb_cmd_lock   (),
    .o13_icb_cmd_excl   (),
    .o13_icb_cmd_size   (),
    .o13_icb_cmd_burst  (),
    .o13_icb_cmd_beat   (),
    
    .o13_icb_rsp_valid  (1'b0),
    .o13_icb_rsp_ready  (),
    .o13_icb_rsp_err    (1'b0),
    .o13_icb_rsp_excl_ok(1'b0),
    .o13_icb_rsp_rdata  (32'b0),

   //  * Preserved 14  
    .o14_icb_enable     (1'b0),

    .o14_icb_cmd_valid  (),
    .o14_icb_cmd_ready  (1'b0),
    .o14_icb_cmd_addr   (),
    .o14_icb_cmd_read   (),
    .o14_icb_cmd_wdata  (),
    .o14_icb_cmd_wmask  (),
    .o14_icb_cmd_lock   (),
    .o14_icb_cmd_excl   (),
    .o14_icb_cmd_size   (),
    .o14_icb_cmd_burst  (),
    .o14_icb_cmd_beat   (),
    
    .o14_icb_rsp_valid  (1'b0),
    .o14_icb_rsp_ready  (),
    .o14_icb_rsp_err    (1'b0),
    .o14_icb_rsp_excl_ok(1'b0 ),
    .o14_icb_rsp_rdata  (32'b0),


   //  * Preserved 15     
    .o15_icb_enable     (1'b0),

    .o15_icb_cmd_valid  (),
    .o15_icb_cmd_ready  (1'b0),
    .o15_icb_cmd_addr   (),
    .o15_icb_cmd_read   (),
    .o15_icb_cmd_wdata  (),
    .o15_icb_cmd_wmask  (),
    .o15_icb_cmd_lock   (),
    .o15_icb_cmd_excl   (),
    .o15_icb_cmd_size   (),
    .o15_icb_cmd_burst  (),
    .o15_icb_cmd_beat   (),
    
    .o15_icb_rsp_valid  (1'b0),
    .o15_icb_rsp_ready  (),
    .o15_icb_rsp_err    (1'b0),
    .o15_icb_rsp_excl_ok(1'b0),
    .o15_icb_rsp_rdata  (32'b0),

    .clk           (clk  ),
    .rst_n         (bus_rst_n) 
  );


sirv_icb_slv_test module_test(
    .i_icb_cmd_valid (exttest_icb_cmd_valid),
    .i_icb_cmd_ready (exttest_icb_cmd_ready),
    .i_icb_cmd_addr  (exttest_icb_cmd_addr),
    .i_icb_cmd_read  (exttest_icb_cmd_read),
    .i_icb_cmd_wdata (exttest_icb_cmd_wdata),
    .i_icb_cmd_wmask (exttest_icb_cmd_wmask),
    
    .i_icb_rsp_valid (exttest_icb_rsp_valid),
    .i_icb_rsp_ready (exttest_icb_rsp_ready),
    .i_icb_rsp_rdata (exttest_icb_rsp_rdata),
    .i_icb_rsp_err   (exttest_icb_rsp_err)
);

sirv_icb_slv_test vga_out(
    .i_icb_cmd_valid (vga_icb_cmd_valid),
    .i_icb_cmd_ready (vga_icb_cmd_ready),
    .i_icb_cmd_addr  (vga_icb_cmd_addr),
    .i_icb_cmd_read  (vga_icb_cmd_read),
    .i_icb_cmd_wdata (vga_icb_cmd_wdata),
    .i_icb_cmd_wmask (vga_icb_cmd_wmask),
    
    .i_icb_rsp_valid (vga_icb_rsp_valid),
    .i_icb_rsp_ready (vga_icb_rsp_ready),
    .i_icb_rsp_rdata (vga_icb_rsp_rdata),
    .i_icb_rsp_err   (vga_icb_rsp_err)
);


endmodule
