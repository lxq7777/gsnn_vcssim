 /*                                                                      
 Copyright 2018-2020  Beijing University of Posts and Telecommunications, Inc.                
                                                                         
 Licensed under the Apache License, Version 2.0 (the "License");         
 you may not use this file except in compliance with the License.        
 You may obtain a copy of the License at                                 
                                                                         
     http://www.apache.org/licenses/LICENSE-2.0                          
                                                                         
  Unless required by applicable law or agreed to in writing, software    
 distributed under the License is distributed on an "AS IS" BASIS,       
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and     
 limitations under the License.                                          
 */                                                                      
                                                                         
                                                                         
                                                                         
//=====================================================================
//
// Designer   : Seven Lai
//
// Description:
//  EPMU module
//
// ====================================================================

`include "e203_defines.v"

module sirv_icb_epmu (
  input                          i_icb_cmd_valid,
  output                         i_icb_cmd_ready,
  input  [`E203_ADDR_SIZE-1:0]   i_icb_cmd_addr, 
  input                          i_icb_cmd_read, 
  input  [`E203_XLEN-1:0]        i_icb_cmd_wdata,
  input  [`E203_XLEN/8-1:0]      i_icb_cmd_wmask,
  //
  output                         i_icb_rsp_valid,
  input                          i_icb_rsp_ready,
  output                         i_icb_rsp_err,
  output [`E203_XLEN-1:0]        i_icb_rsp_rdata,

  output [11:0]                  epmu_ept_search_frame_out,
  output                         epmu_ept_search_frame_out_ena,
  input  [3:0]                   epmu_ept_search_frame_page_in,

  input  clk,
  input  bus_rst_n,
  input  rst_n
);

wire icb_cmd_hsked = i_icb_cmd_valid & i_icb_cmd_ready;

wire write_EPMUPSS_r = (i_icb_cmd_addr[1:0] == 2'b00) & (~i_icb_cmd_read) & (icb_cmd_hsked);

assign epmu_ept_search_frame_out_ena = write_EPMUPSS_r;
assign epmu_ept_search_frame_out = i_icb_cmd_wdata[11:0];

wire [31:0] EPMUPSS_next = {{16{1'b0}}, epmu_ept_search_frame_page_in, i_icb_cmd_wdata[11:0]};
wire EPMUPSS_ena = write_EPMUPSS_r;
wire [31:0] EPMUPSS_r;
sirv_gnrl_dfflr #(32) EPMUPSS_dfflr (EPMUPSS_ena, EPMUPSS_next , EPMUPSS_r, clk, rst_n);

assign i_icb_cmd_ready = i_icb_rsp_ready;
assign i_icb_rsp_valid = i_icb_cmd_valid;
assign i_icb_rsp_err = 1'b0;
assign i_icb_rsp_rdata = {`E203_XLEN{icb_cmd_hsked & i_icb_cmd_read}} & EPMUPSS_r;

endmodule