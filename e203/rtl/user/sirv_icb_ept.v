 /*                                                                      
 Copyright 2018-2020 Nuclei System Technology, Inc.                
                                                                         
 Licensed under the Apache License, Version 2.0 (the "License");         
 you may not use this file except in compliance with the License.        
 You may obtain a copy of the License at                                 
                                                                         
     http://www.apache.org/licenses/LICENSE-2.0                          
                                                                         
  Unless required by applicable law or agreed to in writing, software    
 distributed under the License is distributed on an "AS IS" BASIS,       
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and     
 limitations under the License.                                          
 */                                                                      
                                                                         
                                                                         
                                                                         
//=====================================================================
//
// Designer   : Seven Lai
//
// Description:
//  Extend peri
//
// ====================================================================


`include "e203_defines.v"

module sirv_icb_ept  (
  input                          ept_icb_master_cmd_valid,
  output                         ept_icb_master_cmd_ready,
  input  [`E203_ADDR_SIZE-1:0]   ept_icb_master_cmd_addr, 
  input                          ept_icb_master_cmd_read, 
  input  [`E203_XLEN-1:0]        ept_icb_master_cmd_wdata,
  input  [`E203_XLEN/8-1:0]      ept_icb_master_cmd_wmask,
  //
  output                         ept_icb_master_rsp_valid,
  input                          ept_icb_master_rsp_ready,
  output                         ept_icb_master_rsp_err,
  output [`E203_XLEN-1:0]        ept_icb_master_rsp_rdata,

  output                         ept_icb_slave_cmd_valid,
  input                          ept_icb_slave_cmd_ready,
  output  [`E203_ADDR_SIZE-1:0]  ept_icb_slave_cmd_addr, 
  output                         ept_icb_slave_cmd_read, 
  output  [`E203_XLEN-1:0]       ept_icb_slave_cmd_wdata,
  output  [`E203_XLEN/8-1:0]     ept_icb_slave_cmd_wmask,
  //
  input                          ept_icb_slave_rsp_valid,
  output                         ept_icb_slave_rsp_ready,
  input                          ept_icb_slave_rsp_err,
  input [`E203_XLEN-1:0]         ept_icb_slave_rsp_rdata,

  input [11:0]                   epmu_ept_search_frame_in,
  input                          epmu_ept_search_frame_in_ena,
  output [3:0]                   epmu_ept_search_frame_page_out,

  input  clk,
  input  bus_rst_n,
  input  rst_n
);


wire [11:0] shft_reg_page_0 = 32'h0000_0000;
wire [11:0] shft_reg_page_1;
wire [11:0] shft_reg_page_2;
wire [11:0] shft_reg_page_3;
wire [11:0] shft_reg_page_4;
wire [11:0] shft_reg_page_5;
wire [11:0] shft_reg_page_6;
wire [11:0] shft_reg_page_7;
wire [11:0] shft_reg_page_8;
wire [11:0] shft_reg_page_9;
wire [11:0] shft_reg_page_a;
wire [11:0] shft_reg_page_b;
wire [11:0] shft_reg_page_c;
wire [11:0] shft_reg_page_d;
wire [11:0] shft_reg_page_e;
wire [11:0] shft_reg_page_f;

wire frame_not_found;

wire shft_ena = epmu_ept_search_frame_in_ena & frame_not_found;
wire [11:0] shft_reg_sft_page_in = epmu_ept_search_frame_in;

sirv_gnrl_dfflr #(12) ept_shft_dfflr_1 (shft_ena, shft_reg_sft_page_in  , shft_reg_page_1, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_2 (shft_ena, shft_reg_page_1       , shft_reg_page_2, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_3 (shft_ena, shft_reg_page_2       , shft_reg_page_3, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_4 (shft_ena, shft_reg_page_3       , shft_reg_page_4, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_5 (shft_ena, shft_reg_page_4       , shft_reg_page_5, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_6 (shft_ena, shft_reg_page_5       , shft_reg_page_6, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_7 (shft_ena, shft_reg_page_6       , shft_reg_page_7, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_8 (shft_ena, shft_reg_page_7       , shft_reg_page_8, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_9 (shft_ena, shft_reg_page_8       , shft_reg_page_9, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_a (shft_ena, shft_reg_page_9       , shft_reg_page_a, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_b (shft_ena, shft_reg_page_a       , shft_reg_page_b, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_c (shft_ena, shft_reg_page_b       , shft_reg_page_c, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_d (shft_ena, shft_reg_page_c       , shft_reg_page_d, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_e (shft_ena, shft_reg_page_d       , shft_reg_page_e, clk, rst_n);
sirv_gnrl_dfflr #(12) ept_shft_dfflr_f (shft_ena, shft_reg_page_e       , shft_reg_page_f, clk, rst_n);

wire [4:0] search_frame_page_out_rlt = (epmu_ept_search_frame_in == shft_reg_page_0) ? 5'b0_0000 :
                                       (epmu_ept_search_frame_in == shft_reg_page_1) ? 5'b0_0001 :
                                       (epmu_ept_search_frame_in == shft_reg_page_2) ? 5'b0_0010 :
                                       (epmu_ept_search_frame_in == shft_reg_page_3) ? 5'b0_0011 :
                                       (epmu_ept_search_frame_in == shft_reg_page_4) ? 5'b0_0100 :
                                       (epmu_ept_search_frame_in == shft_reg_page_5) ? 5'b0_0101 :
                                       (epmu_ept_search_frame_in == shft_reg_page_6) ? 5'b0_0110 :
                                       (epmu_ept_search_frame_in == shft_reg_page_7) ? 5'b0_0111 :
                                       (epmu_ept_search_frame_in == shft_reg_page_8) ? 5'b0_1000 :
                                       (epmu_ept_search_frame_in == shft_reg_page_9) ? 5'b0_1001 :
                                       (epmu_ept_search_frame_in == shft_reg_page_a) ? 5'b0_1010 :
                                       (epmu_ept_search_frame_in == shft_reg_page_b) ? 5'b0_1011 :
                                       (epmu_ept_search_frame_in == shft_reg_page_c) ? 5'b0_1100 :
                                       (epmu_ept_search_frame_in == shft_reg_page_d) ? 5'b0_1101 :
                                       (epmu_ept_search_frame_in == shft_reg_page_e) ? 5'b0_1110 :
                                       (epmu_ept_search_frame_in == shft_reg_page_f) ? 5'b0_1111 :
                                       5'b1_0000;

assign frame_not_found = search_frame_page_out_rlt[4] & epmu_ept_search_frame_in_ena;
assign epmu_ept_search_frame_page_out = ({4{~search_frame_page_out_rlt[4]}} & search_frame_page_out_rlt[3:0]) | 
                                        ({4{ search_frame_page_out_rlt[4]}} & 4'b0001);


// address convert part
wire [3:0] trans_page_idx = ept_icb_master_cmd_addr[23:20];

wire [11:0] trans_frame_idx = ({12{(trans_page_idx == 4'b0000)}} & shft_reg_page_0) |
                       ({12{(trans_page_idx == 4'b0001)}} & shft_reg_page_1) |                                                                               
                       ({12{(trans_page_idx == 4'b0010)}} & shft_reg_page_2) |                                                                                
                       ({12{(trans_page_idx == 4'b0011)}} & shft_reg_page_3) |                                                                                
                       ({12{(trans_page_idx == 4'b0100)}} & shft_reg_page_4) |                                                                                
                       ({12{(trans_page_idx == 4'b0101)}} & shft_reg_page_5) |                                                                                
                       ({12{(trans_page_idx == 4'b0110)}} & shft_reg_page_6) |                                                                                
                       ({12{(trans_page_idx == 4'b0111)}} & shft_reg_page_7) |                                                                                
                       ({12{(trans_page_idx == 4'b1000)}} & shft_reg_page_8) |                                                                                
                       ({12{(trans_page_idx == 4'b1001)}} & shft_reg_page_9) |                                                                                
                       ({12{(trans_page_idx == 4'b1010)}} & shft_reg_page_a) |                                                                                
                       ({12{(trans_page_idx == 4'b1011)}} & shft_reg_page_b) |                                                                                
                       ({12{(trans_page_idx == 4'b1100)}} & shft_reg_page_c) |                                                                                
                       ({12{(trans_page_idx == 4'b1101)}} & shft_reg_page_d) |                                                                                
                       ({12{(trans_page_idx == 4'b1110)}} & shft_reg_page_e) |                                                                                
                       ({12{(trans_page_idx == 4'b1111)}} & shft_reg_page_f) ;

assign ept_icb_slave_cmd_addr   = {trans_frame_idx,ept_icb_master_cmd_addr[19:0]};
assign ept_icb_slave_cmd_valid  = ept_icb_master_cmd_valid;
assign ept_icb_slave_cmd_read   = ept_icb_master_cmd_read;
assign ept_icb_slave_cmd_wdata  = ept_icb_master_cmd_wdata;
assign ept_icb_slave_cmd_wmask  = ept_icb_master_cmd_wmask;
assign ept_icb_master_cmd_ready = ept_icb_slave_cmd_ready;

assign ept_icb_master_rsp_valid = ept_icb_slave_rsp_valid;
assign ept_icb_master_rsp_err   = ept_icb_slave_rsp_err;
assign ept_icb_master_rsp_rdata = ept_icb_slave_rsp_rdata;
assign ept_icb_slave_rsp_ready  = ept_icb_master_rsp_ready;


endmodule