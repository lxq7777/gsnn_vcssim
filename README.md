# AI ACCLERATE PROJECT
## Introduction
This project implement a simulation with both e203 (a light riscv core) and gsnn (ai acclerate module)
## Run step
please make sure `vcs` `verdi` `vlogan` `vhdlan` already in SYSTEM PATH

1. One step for all
`make all` for whole project run.

2. Separate steps
- `make compile` : using `vlogan` and `vhdlan` to analyse module in verilod and entity in vhdl.

**project have two logical libraries *unisim* for xilinx vhdl support libraries, *work* for gsnn and e203 and testbench**

- `make elaborate` : for elaborating the previous analysed files, this step will check syntax errors.
- `make simulate` : run simulation defined in tb_top.v, make sure testcase(bin for verilog) path is corrected!
- `make wave` : run verdi to show waveform, default using `script/signal.rc` file for wave configuration.

*current branch origin:master*
*refresh time:1700544540*