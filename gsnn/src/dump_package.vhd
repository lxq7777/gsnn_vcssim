LIBRARY ieee;     
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE std.textio.ALL;

PACKAGE dump_package IS

  PROCEDURE parse_string(L : INOUT LINE; V : OUT STRING; ok : INOUT BOOLEAN);
  PROCEDURE parse_hex(L    : INOUT LINE; V : OUT STD_LOGIC_VECTOR; ok : INOUT BOOLEAN);

  PROCEDURE dump_string(L       : INOUT LINE; V : IN STRING; B : IN STRING            := ""; A : IN STRING := "");
  PROCEDURE dump_cycle(L        : INOUT LINE; B : IN STRING                           := ""; A : IN STRING := ""; D : IN INTEGER := 0);
  PROCEDURE dump_cycle_ps(L        : INOUT LINE; B : IN STRING                           := ""; A : IN STRING := ""; D : IN INTEGER := 0);
  PROCEDURE dump_hex_integer(L      : INOUT LINE; V : IN INTEGER; B : IN STRING           := ""; A : IN STRING := "");
  PROCEDURE dump_integer(L      : INOUT LINE; V : IN INTEGER; B : IN STRING           := ""; A : IN STRING := "");
  PROCEDURE dump_bit(L          : INOUT LINE; V : IN STD_LOGIC; B : IN STRING         := ""; A : IN STRING := "");
  PROCEDURE dump_bit(L          : INOUT LINE; V : IN STD_LOGIC_VECTOR; B : IN STRING  := ""; A : IN STRING := "");
  PROCEDURE dump_hex(L          : INOUT LINE; V : IN STD_LOGIC_VECTOR; B : IN STRING  := ""; A : IN STRING := "");

END PACKAGE dump_package;

PACKAGE BODY dump_package IS
  PROCEDURE parse_string(
    L  : INOUT LINE;
    V  : OUT   STRING;
    ok : INOUT BOOLEAN
    ) IS
    VARIABLE c : CHARACTER;
    VARIABLE i : INTEGER;
  BEGIN
    FOR i IN V'range LOOP
      V(i) := ' ';
    END LOOP;

    LOOP
      read(L, c, ok);
      IF NOT ok THEN
        RETURN;
      END IF;
      EXIT WHEN c /= ' ';
    END LOOP;

    i := 1;
    LOOP
      V(i) := c;
      i    := i + 1;

      read(L, c, ok);
      EXIT WHEN c = ' ' OR NOT ok;
    END LOOP;
  END PROCEDURE parse_string;

  PROCEDURE parse_hex(
    L  : INOUT LINE;
    V  : OUT   STD_LOGIC_VECTOR;
    ok : INOUT BOOLEAN
    ) IS
    VARIABLE c   : CHARACTER;
    VARIABLE n   : INTEGER;
    VARIABLE i   : INTEGER;
    VARIABLE buf : STRING(1 TO 1024);
    VARIABLE dbg : LINE;
  BEGIN

    FOR i IN V'range LOOP
      V(i) := '0';
    END LOOP;

    LOOP
      read(L, c, ok);
      IF NOT ok THEN
        RETURN;
      END IF;
      EXIT WHEN c /= ' ';
    END LOOP;

    n := 0;
    LOOP
      n      := n + 1;
      buf(n) := c;

      read(L, c, ok);
      EXIT WHEN c = ' ' OR NOT ok;
    END LOOP;

    i := V'low;
    FOR j IN n DOWNTO 1 LOOP
      c := buf(j);
      CASE c IS
        WHEN '0'     => V(3+i DOWNTO i) := "0000";
        WHEN '1'     => V(3+i DOWNTO i) := "0001";
        WHEN '2'     => V(3+i DOWNTO i) := "0010";
        WHEN '3'     => V(3+i DOWNTO i) := "0011";
        WHEN '4'     => V(3+i DOWNTO i) := "0100";
        WHEN '5'     => V(3+i DOWNTO i) := "0101";
        WHEN '6'     => V(3+i DOWNTO i) := "0110";
        WHEN '7'     => V(3+i DOWNTO i) := "0111";
        WHEN '8'     => V(3+i DOWNTO i) := "1000";
        WHEN '9'     => V(3+i DOWNTO i) := "1001";
        WHEN 'a'|'A' => V(3+i DOWNTO i) := "1010";
        WHEN 'b'|'B' => V(3+i DOWNTO i) := "1011";
        WHEN 'c'|'C' => V(3+i DOWNTO i) := "1100";
        WHEN 'd'|'D' => V(3+i DOWNTO i) := "1101";
        WHEN 'e'|'E' => V(3+i DOWNTO i) := "1110";
        WHEN 'f'|'F' => V(3+i DOWNTO i) := "1111";
        WHEN 'x'|'X' => V(3+i DOWNTO i) := "XXXX";
        WHEN 'u'|'U' => V(3+i DOWNTO i) := "UUUU";
        WHEN 'z'|'Z' => V(3+i DOWNTO i) := "ZZZZ";
        WHEN 'w'|'W' => V(3+i DOWNTO i) := "WWWW";
        WHEN 'h'|'H' => V(3+i DOWNTO i) := "HHHH";
        WHEN 'l'|'L' => V(3+i DOWNTO i) := "LLLL";
        WHEN '-'     => V(3+i DOWNTO i) := "----";
        WHEN OTHERS  => write(dbg, STRING'("unexpected '")); write(dbg, CHARACTER'(c)); write(dbg, STRING'("'")); writeline(output, dbg); ASSERT false REPORT "unexpected hex character" SEVERITY failure;
      END CASE;
      i := i+4;
    END LOOP;
  END PROCEDURE parse_hex;

  PROCEDURE dump_string(
    L : INOUT LINE;
    V : IN    STRING;
    B : IN    STRING := "";
    A : IN    STRING := ""
    ) IS
  BEGIN
    write(L, B);
    write(L, V);
    write(L, A);
  END PROCEDURE dump_string;

  PROCEDURE dump_cycle(
    L : INOUT LINE;
    B : IN    STRING  := "";
    A : IN    STRING  := "";
    D : IN    INTEGER := 0
    ) IS
    VARIABLE T : INTEGER;
  BEGIN
    T := (now /1 ns) + D;
    write(L, B);
    write(L, T);
    write(L, A);
  END PROCEDURE dump_cycle;

  PROCEDURE dump_cycle_ps(
    L : INOUT LINE;
    B : IN    STRING  := "";
    A : IN    STRING  := "";
    D : IN    INTEGER := 0
    ) IS
    VARIABLE T : INTEGER;
  BEGIN
    T := (now /1 ps) + D;
    write(L, B);
    write(L, T);
    write(L, A);
  END PROCEDURE dump_cycle_ps;

  PROCEDURE dump_integer(
    L : INOUT LINE;
    V : IN    INTEGER;
    B : IN    STRING := "";
    A : IN    STRING := ""
    ) IS
  BEGIN
    write(L, B);
    write(L, V);
    write(L, A);
  END PROCEDURE dump_integer;

  PROCEDURE dump_hex_integer(
    L : INOUT LINE;
    V : IN    INTEGER;
    B : IN    STRING := "";
    A : IN    STRING := ""
    ) IS
    TYPE      array_integer IS ARRAY( natural RANGE<>) OF integer;
    VARIABLE  digits: array_integer(0 TO 15);
    VARIABLE  R : integer;
    VARIABLE  found : boolean;
    VARIABLE  num : integer;
  BEGIN
    write(L, B);
    write(L, string'("0x"));

    num := V;
    FOR i IN 0 TO 15 LOOP
      R := num mod 16;
      digits(i) := R;
      num := (num - R)/16;
    END LOOP;

    found := false;
    FOR i IN 15 DOWNTO 0 LOOP
      IF i = 0 OR found OR digits(i) /= 0 THEN
        found := true;

        CASE digits(i) IS
          WHEN 0  => write(L, string'("0"));
          WHEN 1  => write(L, string'("1"));
          WHEN 2  => write(L, string'("2"));
          WHEN 3  => write(L, string'("3"));
          WHEN 4  => write(L, string'("4"));
          WHEN 5  => write(L, string'("5"));
          WHEN 6  => write(L, string'("6"));
          WHEN 7  => write(L, string'("7"));
          WHEN 8  => write(L, string'("8"));
          WHEN 9  => write(L, string'("9"));
          WHEN 10  => write(L, string'("a"));
          WHEN 11  => write(L, string'("b"));
          WHEN 12  => write(L, string'("c"));
          WHEN 13  => write(L, string'("d"));
          WHEN 14  => write(L, string'("e"));
          WHEN 15  => write(L, string'("f"));
          WHEN OTHERS  => write(L, string'("?"));
        END CASE;
      END IF;
    END LOOP;

    write(L, A);
  END PROCEDURE dump_hex_integer;

  PROCEDURE dump_bit(
    L : INOUT LINE;
    V : IN    STD_LOGIC;
    B : IN    STRING := "";
    A : IN    STRING := ""
    ) IS
  BEGIN
    write(L, B);
    CASE V IS
      WHEN '0'    => write(L, STRING'("0"));
      WHEN '1'    => write(L, STRING'("1"));
      WHEN 'X'    => write(L, STRING'("X"));
      WHEN 'U'    => write(L, STRING'("U"));
      WHEN 'Z'    => write(L, STRING'("Z"));
      WHEN 'W'    => write(L, STRING'("W"));
      WHEN 'L'    => write(L, STRING'("L"));
      WHEN 'H'    => write(L, STRING'("H"));
      WHEN '-'    => write(L, STRING'("-"));
      WHEN OTHERS => write(L, STRING'("?"));
    END CASE;
    write(L, A);
  END PROCEDURE dump_bit;

  PROCEDURE dump_bit(
    L : INOUT LINE;
    V : IN    STD_LOGIC_VECTOR;
    B : IN    STRING := "";
    A : IN    STRING := ""
    ) IS
  BEGIN
    write(L, B);
    FOR i IN V'range LOOP
      CASE V(i) IS
        WHEN '0'    => write(L, STRING'("0"));
        WHEN '1'    => write(L, STRING'("1"));
        WHEN 'X'    => write(L, STRING'("X"));
        WHEN 'U'    => write(L, STRING'("U"));
        WHEN 'Z'    => write(L, STRING'("Z"));
        WHEN 'W'    => write(L, STRING'("W"));
        WHEN 'L'    => write(L, STRING'("L"));
        WHEN 'H'    => write(L, STRING'("H"));
        WHEN '-'    => write(L, STRING'("-"));
        WHEN OTHERS => write(L, STRING'("?"));
      END CASE;
    END LOOP;
    write(L, A);
  END PROCEDURE dump_bit;

  PROCEDURE dump_hex(
    L : INOUT LINE;
    V : IN    STD_LOGIC_VECTOR;
    B : IN    STRING := "";
    A : IN    STRING := ""
    ) IS
    VARIABLE length  : INTEGER;
    VARIABLE nibbles : INTEGER;
    VARIABLE left    : INTEGER;
    VARIABLE bit4    : STD_LOGIC_VECTOR(3 DOWNTO 0);
    VARIABLE bit3    : STD_LOGIC_VECTOR(2 DOWNTO 0);
    VARIABLE bit2    : STD_LOGIC_VECTOR(1 DOWNTO 0);
    VARIABLE bit1    : STD_LOGIC_VECTOR(0 DOWNTO 0);
  BEGIN
    write(L, B);
    length := V'length;

    left := length MOD 4;
    IF left = 1 THEN
      bit1 := v(v'high DOWNTO v'high);
      CASE bit1 IS
        WHEN "0"    => write(L, STRING'("0"));
        WHEN "1"    => write(L, STRING'("1"));
        WHEN "X"    => write(L, STRING'("X"));
        WHEN "U"    => write(L, STRING'("U"));
        WHEN "Z"    => write(L, STRING'("Z"));
        WHEN "W"    => write(L, STRING'("W"));
        WHEN "L"    => write(L, STRING'("L"));
        WHEN "H"    => write(L, STRING'("H"));
        WHEN "-"    => write(L, STRING'("-"));
        WHEN OTHERS => write(L, STRING'("?"));
      END CASE;
    ELSIF left = 2 THEN
      bit2 := v(v'high DOWNTO v'high-1);
      CASE bit2 IS
        WHEN "00"   => write(L, STRING'("0"));
        WHEN "01"   => write(L, STRING'("1"));
        WHEN "10"   => write(L, STRING'("2"));
        WHEN "11"   => write(L, STRING'("3"));
        WHEN "XX"   => write(L, STRING'("X"));
        WHEN "UU"   => write(L, STRING'("U"));
        WHEN "ZZ"   => write(L, STRING'("Z"));
        WHEN "WW"   => write(L, STRING'("W"));
        WHEN "LL"   => write(L, STRING'("L"));
        WHEN "HH"   => write(L, STRING'("H"));
        WHEN "--"   => write(L, STRING'("-"));
        WHEN OTHERS => write(L, STRING'("?"));
      END CASE;
    ELSIF left = 3 THEN
      bit3 := v(v'high DOWNTO v'high-2);
      CASE bit3 IS
        WHEN "000"  => write(L, STRING'("0"));
        WHEN "001"  => write(L, STRING'("1"));
        WHEN "010"  => write(L, STRING'("2"));
        WHEN "011"  => write(L, STRING'("3"));
        WHEN "100"  => write(L, STRING'("4"));
        WHEN "101"  => write(L, STRING'("5"));
        WHEN "110"  => write(L, STRING'("6"));
        WHEN "111"  => write(L, STRING'("7"));
        WHEN "XXX"  => write(L, STRING'("X"));
        WHEN "UUU"  => write(L, STRING'("U"));
        WHEN "ZZZ"  => write(L, STRING'("Z"));
        WHEN "WWW"  => write(L, STRING'("W"));
        WHEN "LLL"  => write(L, STRING'("L"));
        WHEN "HHH"  => write(L, STRING'("H"));
        WHEN "---"  => write(L, STRING'("-"));
        WHEN OTHERS => write(L, STRING'("?"));
      END CASE;
    END IF;

    nibbles := length / 4;
    IF nibbles > 0 THEN
      FOR i IN nibbles-1 DOWNTO 0 LOOP
        bit4 := V(v'low + i*4 + 3 DOWNTO v'low + i*4);
        CASE bit4 IS
          WHEN "0000" => write(L, STRING'("0"));
          WHEN "0001" => write(L, STRING'("1"));
          WHEN "0010" => write(L, STRING'("2"));
          WHEN "0011" => write(L, STRING'("3"));
          WHEN "0100" => write(L, STRING'("4"));
          WHEN "0101" => write(L, STRING'("5"));
          WHEN "0110" => write(L, STRING'("6"));
          WHEN "0111" => write(L, STRING'("7"));
          WHEN "1000" => write(L, STRING'("8"));
          WHEN "1001" => write(L, STRING'("9"));
          WHEN "1010" => write(L, STRING'("a"));
          WHEN "1011" => write(L, STRING'("b"));
          WHEN "1100" => write(L, STRING'("c"));
          WHEN "1101" => write(L, STRING'("d"));
          WHEN "1110" => write(L, STRING'("e"));
          WHEN "1111" => write(L, STRING'("f"));
          WHEN "XXXX" => write(L, STRING'("X"));
          WHEN "UUUU" => write(L, STRING'("U"));
          WHEN "ZZZZ" => write(L, STRING'("Z"));
          WHEN "WWWW" => write(L, STRING'("W"));
          WHEN "LLLL" => write(L, STRING'("L"));
          WHEN "HHHH" => write(L, STRING'("H"));
          WHEN "----" => write(L, STRING'("-"));
          WHEN OTHERS => write(L, STRING'("?"));
        END CASE;
      END LOOP;
    END IF;

    write(L, A);
  END PROCEDURE dump_hex;

END dump_package;
