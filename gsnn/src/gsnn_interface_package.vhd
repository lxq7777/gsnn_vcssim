LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;

PACKAGE gsnn_interface_package IS

  ----------------------------------------------------------------
  -- PXI bus types
  ----------------------------------------------------------------
  SUBTYPE gsnn_id_type            IS std_logic_vector(0 DOWNTO 0);
  SUBTYPE addr_pxi_type           IS std_logic_vector(39 DOWNTO 0);
  SUBTYPE BASE_ADDR_PXI           IS natural RANGE 13 DOWNTO 0;   -- 16KB memory
  SUBTYPE DATA_SEL_ADDR_PXI       IS natural RANGE 14 DOWNTO 14;  -- insn/data = 0/1
  SUBTYPE ENGINE_SEL_ADDR_PXI     IS natural RANGE 16 DOWNTO 15;  -- engine
  SUBTYPE GLOBAL_SEL_ADDR_PXI     IS natural RANGE 17 DOWNTO 17;  -- memory/global regs=0/1
  SUBTYPE REG_SEL_ADDR_PXI        IS natural RANGE 5 DOWNTO 2;    -- 4B aligned addresses (0, 0..15)   (1, 0..15) (3, 0..15)
  SUBTYPE SET_SEL_ADDR_PXI        IS natural RANGE 7 DOWNTO 6;    -- select register sets
  SUBTYPE DBG_SEL_ADDR_PXI        IS natural RANGE 9 DOWNTO 8;    -- 0: read/write regs, 2: locals, 3: pcs

  ----------------------------------------------------------------
  -- AXI bus types
  ----------------------------------------------------------------
  SUBTYPE data_axi_type           IS std_logic_vector(127 DOWNTO 0);
  SUBTYPE wstrb_axi_type          IS std_logic_vector(15 DOWNTO 0);
  SUBTYPE addr_axi_type           IS std_logic_vector(39 DOWNTO 0);
  SUBTYPE arid_axi_type           IS std_logic_vector(0 DOWNTO 0); -- it may be tweaked later

  SUBTYPE arburst_axi_type        IS std_logic_vector(1 DOWNTO 0);
--CONSTANT FIXED_ARBURST_AXI      : arlock_axi_type := "00"; -- not supported by DDR
  CONSTANT INCR_ARBURST_AXI       : arburst_axi_type := "01";
  CONSTANT WRAP_ARBURST_AXI       : arburst_axi_type := "10";

  SUBTYPE alock_axi_type          IS std_logic_vector(1 DOWNTO 0);
  CONSTANT NORMAL_ALOCK_AXI       : alock_axi_type := "00";
  CONSTANT EXCLUSIVE_ALOCK_AXI    : alock_axi_type := "01";

  SUBTYPE resp_axi_type           IS std_logic_vector(1 DOWNTO 0);
  CONSTANT OKAY_RESP_AXI          : resp_axi_type := "00";
  CONSTANT EXOKAY_RESP_AXI        : resp_axi_type := "01";
  CONSTANT SLVERR_RESP_AXI        : resp_axi_type := "10";
  CONSTANT DECERR_RESP_AXI        : resp_axi_type := "11";

  SUBTYPE awid_axi_type           IS std_logic_vector(0 DOWNTO 0); -- it may be tweaked later

  SUBTYPE awburst_axi_type        IS std_logic_vector(1 DOWNTO 0);
--CONSTANT FIXED_AWBURST_AXI      : awburst_axi_type := "00"; -- not supported by DDR
  CONSTANT INCR_AWBURST_AXI       : awburst_axi_type := "01";
--CONSTANT WRAP_AWBURST_AXI       : awburst_axi_type := "10";  -- not suported by DDR

END gsnn_interface_package;
