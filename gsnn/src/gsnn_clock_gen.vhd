LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY gsnn_clock_gen IS
  PORT(
    clock                         : IN std_logic;
    reset                         : IN bit_type;

    axi_clock                     : OUT std_logic;
    axi_reset                     : OUT bit_type;
    axi_early_busout              : OUT bit_type;
    pxi_clock                     : OUT std_logic;
    pxi_reset                     : OUT bit_type;
    pxi_early_busout              : OUT bit_type
  );
END ENTITY gsnn_clock_gen;

ARCHITECTURE behavior OF gsnn_clock_gen IS
  
  SIGNAL  clock_buf_d       : bit2_type;
  SIGNAL  clock_buf_q       : bit2_type
-- pragma translate_off
  := "00"
-- pragma translate_on
  ; -- the initialization is for simulation only; in practice, we do not care what the clock_buf flops come up with.

  SIGNAL  reset_q           : bit_type;

  SIGNAL  reset_ctr_d       : bit6_type;
  SIGNAL  reset_ctr_q       : bit6_type;
  SIGNAL  reset_delay_d     : bit_type;
  SIGNAL  reset_delay_q     : bit_type;

  SIGNAL  axi_reset_q : bit_type;
  SIGNAL  pxi_reset_q : bit_type;
BEGIN

  reset_q           <=  reset AFTER 10 ps WHEN rising_edge(clock);

  clock_buf_d       <=  "11" WHEN reset_q = "1" AND reset = "0" ELSE std_logic_vector(unsigned(clock_buf_q)-1);
  clock_buf_q       <=  clock_buf_d WHEN rising_edge(clock);

  axi_clock         <=  clock_buf_q(0);
  pxi_clock         <=  clock_buf_q(1);

  -- 某种预测信号
  axi_early_busout  <=  "1" WHEN clock_buf_q(0) = '0' ELSE "0";
  pxi_early_busout  <=  "1" WHEN clock_buf_q = "00" ELSE "0";

--  reset_ctr_d       <= (reset_ctr_d'high DOWNTO 1 => '0')&'1' WHEN reset = "1" ELSE
--                       (OTHERS => '0')                        WHEN reset_ctr_q = (reset_ctr_q'range =>'0')  ELSE
--                       std_logic_vector(unsigned(reset_ctr_q)+1);
--
--  reset_ctr_q       <= reset_ctr_d AFTER 10 ps WHEN rising_edge(clock);
--
--  reset_delay_d     <=  "1" WHEN reset = "1" ELSE
--                        "1" WHEN reset_ctr_q /= (reset_ctr_q'range => '0') ELSE
--                        "0";
--  reset_delay_q     <=  reset_delay_d AFTER 10 ps WHEN rising_edge(clock);
--  axi_reset         <=  reset_delay_q;
--  pxi_reset         <=  reset_delay_q;
  axi_reset         <=  reset;
  pxi_reset         <=  reset;


END ARCHITECTURE behavior;
