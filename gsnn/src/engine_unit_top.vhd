-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_interface_package.ALL;
USE work.gsnn_package.ALL;

ENTITY engine_unit_top IS
  GENERIC(
    engine_id                 : engine_id_type := "00"
  );
  PORT(
    clock                 : IN std_logic;
    reset                   : IN bit_type;
    reset_mem               : IN bit_type;

    slp_in                  : IN bit_type;
    sd_in                   : IN bit_type;

    block_in                : IN bit_type;
  -- global regs
    global_0_in             : IN bit32_type;
    global_1_in             : IN bit32_type;
    global_2_in             : IN bit32_type;
    global_3_in             : IN bit32_type;
    global_4_in             : IN bit32_type;
    global_5_in             : IN bit32_type;
    global_6_in             : IN bit32_type;
    global_7_in             : IN bit32_type;
    global_8_in             : IN bit32_type;
    global_9_in             : IN bit32_type;
    global_a_in             : IN bit32_type;
    global_b_in             : IN bit32_type;
    global_c_in             : IN bit32_type;
    global_d_in             : IN bit32_type;
    global_e_in             : IN bit32_type;
    global_f_in             : IN bit32_type;

  -- set global register
    en_global_busout        : OUT bit_type;
    rt_global_busout        : OUT bit4_type;
    word_global_busout      : OUT bit32_type;

  -- special command
    en_cmd_busout           : OUT bit_type;
    insn_cmd_busout         : OUT insn_type;
    ctl_cmd_busout          : OUT bit16_type;
    ctr_cmd_busout          : OUT bit32_type;
    base0_cmd_busout        : OUT bit32_type;
    base1_cmd_busout        : OUT bit32_type;
    base2_cmd_busout        : OUT bit32_type;
    spec_cmd_busout         : OUT bit32_type;

  -- error detected
    err_align_busout        : OUT bit_type;
    err_page_busout         : OUT bit_type;

-- registers
    r0_busout               : OUT bit32_type;
    r1_busout               : OUT bit32_type;
    r2_busout               : OUT bit32_type;
    r3_busout               : OUT bit32_type;
    r4_busout               : OUT bit32_type;
    r5_busout               : OUT bit32_type;
    r6_busout               : OUT bit32_type;
    r7_busout               : OUT bit32_type;
    r8_busout               : OUT bit32_type;
    r9_busout               : OUT bit32_type;
    r10_busout              : OUT bit32_type;
    r11_busout              : OUT bit32_type;
    r12_busout              : OUT bit32_type;
    r13_busout              : OUT bit32_type;
    r14_busout              : OUT bit32_type;
    r15_busout              : OUT bit32_type;

    pc_busout               : OUT bit32_type;

    ---------------------------------------------------------------------------
    -- INSN PXI signals
    ---------------------------------------------------------------------------
    en_mem_insn_pxi_in        : IN bit_type;
    we_mem_insn_pxi_in        : IN bit_type;
    addr_mem_insn_pxi_in      : IN addr_pxi_type;
    word_mem_insn_pxi_in      : IN bit32_type;

    en_insn_pxi_busout        : OUT bit_type;
    word_insn_pxi_busout      : OUT bit32_type;
    collide_insn_pxi_busout   : OUT bit_type;

    ---------------------------------------------------------------------------
    -- DATA PXI signals
    ---------------------------------------------------------------------------
    en_mem_data_pxi_in        : IN bit_type;
    we_mem_data_pxi_in        : IN bit_type;
    addr_mem_data_pxi_in      : IN addr_pxi_type;
    word_mem_data_pxi_in      : IN bit32_type;

    en_data_pxi_busout        : OUT bit_type;
    word_data_pxi_busout      : OUT bit32_type;
    collide_data_pxi_busout   : OUT bit_type
  );
END ENTITY engine_unit_top;

ARCHITECTURE behavior OF engine_unit_top IS
  COMPONENT engine_unit IS
    GENERIC(
      engine_id               : IN engine_id_type
    );
    PORT(
      clock_engine, clock_mult                   : IN std_logic;
      reset                   : IN bit_type;

      block_in                : IN bit_type;
    -- global regs
      global_0_in             : IN bit32_type;
      global_1_in             : IN bit32_type;
      global_2_in             : IN bit32_type;
      global_3_in             : IN bit32_type;
      global_4_in             : IN bit32_type;
      global_5_in             : IN bit32_type;
      global_6_in             : IN bit32_type;
      global_7_in             : IN bit32_type;
      global_8_in             : IN bit32_type;
      global_9_in             : IN bit32_type;
      global_a_in             : IN bit32_type;
      global_b_in             : IN bit32_type;
      global_c_in             : IN bit32_type;
      global_d_in             : IN bit32_type;
      global_e_in             : IN bit32_type;
      global_f_in             : IN bit32_type;

    -- insn mem read
      en_insn_in              : IN bit_type;
      word_insn_in            : IN bit32_type;

    -- data mem read
      en_data_in              : IN bit_type;
      word_data_in            : IN bit32_type;

    -- set global register
      en_global_busout        : OUT bit_type;
      rt_global_busout        : OUT bit4_type;
      word_global_busout      : OUT bit32_type;

    -- new insn request
      en_mem_insn_busout      : OUT bit_type;
      addr_mem_insn_busout    : OUT insn_addr_type;

    -- new data request
      en_mem_data_busout      : OUT bit_type;
      addr_mem_data_busout    : OUT data_addr_type;
      we_mem_data_busout      : OUT bit_type;
      word_mem_data_busout    : OUT bit32_type;

    -- special command
      en_cmd_busout           : OUT bit_type;
      insn_cmd_busout         : OUT insn_type;
      ctl_cmd_busout          : OUT bit16_type;
      ctr_cmd_busout          : OUT bit32_type;
      base0_cmd_busout        : OUT bit32_type;
      base1_cmd_busout        : OUT bit32_type;
      base2_cmd_busout        : OUT bit32_type;
      spec_cmd_busout         : OUT bit32_type;

  -- error detected
    err_align_busout          : OUT bit_type;
    err_page_busout           : OUT bit_type;

  -- registers
      r0_busout               : OUT bit32_type;
      r1_busout               : OUT bit32_type;
      r2_busout               : OUT bit32_type;
      r3_busout               : OUT bit32_type;
      r4_busout               : OUT bit32_type;
      r5_busout               : OUT bit32_type;
      r6_busout               : OUT bit32_type;
      r7_busout               : OUT bit32_type;
      r8_busout               : OUT bit32_type;
      r9_busout               : OUT bit32_type;
      r10_busout              : OUT bit32_type;
      r11_busout              : OUT bit32_type;
      r12_busout              : OUT bit32_type;
      r13_busout              : OUT bit32_type;
      r14_busout              : OUT bit32_type;
      r15_busout              : OUT bit32_type;

      pc_busout               : OUT bit32_type
    );
  END COMPONENT engine_unit;

  COMPONENT insn_memory_unit IS
    GENERIC(
    id  : engine_id_type
    );
    PORT(
    clock                           : IN std_logic;
    reset                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- Memory control signals
    ---------------------------------------------------------------------------
    slp_in                          : IN bit_type;
    sd_in                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- PXI signals
    ---------------------------------------------------------------------------
    en_mem_pxi_in                 : IN bit_type;
    we_mem_pxi_in                 : IN bit_type;
    addr_mem_pxi_in               : IN addr_pxi_type;
    word_mem_pxi_in               : IN bit32_type;

    en_pxi_busout                 : OUT bit_type;
    word_pxi_busout               : OUT bit32_type;
    collide_pxi_busout            : OUT bit_type;

    ---------------------------------------------------------------------------
    -- engine signals
    ---------------------------------------------------------------------------
    en_mem_engine_in              : IN bit_type;
    addr_mem_engine_in            : IN insn_addr_type;

    en_engine_busout              : OUT bit_type;
    word_engine_busout            : OUT bit32_type
    );
  END COMPONENT insn_memory_unit;

  COMPONENT data_memory_unit IS
    GENERIC(
    id  : engine_id_type
    );
    PORT(
    clock                           : IN std_logic;
    reset                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- Memory control signals
    ---------------------------------------------------------------------------
    slp_in                          : IN bit_type;
    sd_in                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- PXI signals
    ---------------------------------------------------------------------------
    en_mem_pxi_in                 : IN bit_type;
    we_mem_pxi_in                 : IN bit_type;
    addr_mem_pxi_in               : IN addr_pxi_type;
    word_mem_pxi_in               : IN bit32_type;

    en_pxi_busout                 : OUT bit_type;
    word_pxi_busout               : OUT bit32_type;
    collide_pxi_busout            : OUT bit_type;

    ---------------------------------------------------------------------------
    -- engine signals
    ---------------------------------------------------------------------------
    en_mem_engine_in              : IN bit_type;
    we_mem_engine_in              : IN bit_type;
    addr_mem_engine_in            : IN data_addr_type;
    word_mem_engine_in            : IN bit32_type;

    en_engine_busout              : OUT bit_type;
    word_engine_busout            : OUT bit32_type
    );
  END COMPONENT data_memory_unit;

  SIGNAL    en_mem_insn_i         : bit_type;
  SIGNAL    addr_mem_insn_i       : insn_addr_type;

  SIGNAL    en_mem_data_i         : bit_type;
  SIGNAL    we_mem_data_i         : bit_type;
  SIGNAL    addr_mem_data_i       : data_addr_type;
  SIGNAL    word_mem_data_i       : bit32_type;

  SIGNAL    en_insn_i             : bit_type;
  SIGNAL    word_insn_i           : bit32_type;

  SIGNAL    en_data_i             : bit_type;
  SIGNAL    word_data_i           : bit32_type;
  
--  component clk_wiz_0 is
--  Port ( 
--    clk_out1 : out STD_LOGIC;
--    clk_out2 : out STD_LOGIC;
--    reset : in STD_LOGIC;
--    locked : out STD_LOGIC;
--    clk_in1 : in STD_LOGIC
--  );
--end component; 

signal clock_engine, clock_mult: std_logic;

BEGIN


--PLL: clk_wiz_0 port map(
--clk_out1 => clock_engine,
--clk_out2 => clock_mult,
--clk_in1 => clock_engine_top,
--reset => '0'
--);
  
  a_engine: engine_unit GENERIC MAP( engine_id =>  engine_id ) PORT MAP(
                    clock_engine                 =>  clock,
                    clock_mult                 =>  clock_mult,

                    reset                 =>  reset,

                    block_in              =>  block_in,

                    global_0_in           =>  global_0_in,
                    global_1_in           =>  global_1_in,
                    global_2_in           =>  global_2_in,
                    global_3_in           =>  global_3_in,
                    global_4_in           =>  global_4_in,
                    global_5_in           =>  global_5_in,
                    global_6_in           =>  global_6_in,
                    global_7_in           =>  global_7_in,
                    global_8_in           =>  global_8_in,
                    global_9_in           =>  global_9_in,
                    global_a_in           =>  global_a_in,
                    global_b_in           =>  global_b_in,
                    global_c_in           =>  global_c_in,
                    global_d_in           =>  global_d_in,
                    global_e_in           =>  global_e_in,
                    global_f_in           =>  global_f_in,

                    en_insn_in            =>  en_insn_i,
                    word_insn_in          =>  word_insn_i,

                    en_data_in            =>  en_data_i,
                    word_data_in          =>  word_data_i,

                    en_global_busout      =>  en_global_busout,
                    rt_global_busout      =>  rt_global_busout,
                    word_global_busout    =>  word_global_busout,

                    en_mem_insn_busout    =>  en_mem_insn_i,
                    addr_mem_insn_busout  =>  addr_mem_insn_i,

                    en_mem_data_busout    =>  en_mem_data_i,
                    addr_mem_data_busout  =>  addr_mem_data_i,
                    we_mem_data_busout    =>  we_mem_data_i,
                    word_mem_data_busout  =>  word_mem_data_i,

                    en_cmd_busout         =>  en_cmd_busout,
                    insn_cmd_busout       =>  insn_cmd_busout,
                    ctl_cmd_busout        =>  ctl_cmd_busout,
                    ctr_cmd_busout        =>  ctr_cmd_busout,
                    base0_cmd_busout      =>  base0_cmd_busout,
                    base1_cmd_busout      =>  base1_cmd_busout,
                    base2_cmd_busout      =>  base2_cmd_busout,
                    spec_cmd_busout       =>  spec_cmd_busout,

                    err_align_busout      =>  err_align_busout,
                    err_page_busout       =>  err_page_busout,

                    r0_busout             =>  r0_busout,
                    r1_busout             =>  r1_busout,
                    r2_busout             =>  r2_busout,
                    r3_busout             =>  r3_busout,
                    r4_busout             =>  r4_busout,
                    r5_busout             =>  r5_busout,
                    r6_busout             =>  r6_busout,
                    r7_busout             =>  r7_busout,
                    r8_busout             =>  r8_busout,
                    r9_busout             =>  r9_busout,
                    r10_busout            =>  r10_busout,
                    r11_busout            =>  r11_busout,
                    r12_busout            =>  r12_busout,
                    r13_busout            =>  r13_busout,
                    r14_busout            =>  r14_busout,
                    r15_busout            =>  r15_busout,

                    pc_busout             =>  pc_busout
                    );

  a_insn: insn_memory_unit GENERIC MAP( id =>  engine_id ) PORT MAP(
                    clock                 =>  clock,
                    reset                 =>  reset_mem,

                    slp_in                =>  slp_in,
                    sd_in                 =>  sd_in,

                    en_mem_pxi_in         =>  en_mem_insn_pxi_in,
                    we_mem_pxi_in         =>  we_mem_insn_pxi_in,
                    addr_mem_pxi_in       =>  addr_mem_insn_pxi_in,
                    word_mem_pxi_in       =>  word_mem_insn_pxi_in,

                    en_pxi_busout         =>  en_insn_pxi_busout,
                    word_pxi_busout       =>  word_insn_pxi_busout,
                    collide_pxi_busout    =>  collide_insn_pxi_busout,

                    en_mem_engine_in      =>  en_mem_insn_i,
                    addr_mem_engine_in    =>  addr_mem_insn_i,

                    en_engine_busout      =>  en_insn_i,
                    word_engine_busout    =>  word_insn_i
                    );

  a_data: data_memory_unit GENERIC MAP( id =>  engine_id ) PORT MAP(
                    clock                 =>  clock,
                    reset                 =>  reset_mem,

                    slp_in                =>  slp_in,
                    sd_in                 =>  sd_in,

                    en_mem_pxi_in         =>  en_mem_data_pxi_in,
                    we_mem_pxi_in         =>  we_mem_data_pxi_in,
                    addr_mem_pxi_in       =>  addr_mem_data_pxi_in,
                    word_mem_pxi_in       =>  word_mem_data_pxi_in,

                    en_pxi_busout         =>  en_data_pxi_busout,
                    word_pxi_busout       =>  word_data_pxi_busout,
                    collide_pxi_busout    =>  collide_data_pxi_busout,

                    en_mem_engine_in      =>  en_mem_data_i,
                    we_mem_engine_in      =>  we_mem_data_i,
                    addr_mem_engine_in    =>  addr_mem_data_i,
                    word_mem_engine_in    =>  word_mem_data_i,

                    en_engine_busout      =>  en_data_i,
                    word_engine_busout    =>  word_data_i
                    );

END ARCHITECTURE behavior;
