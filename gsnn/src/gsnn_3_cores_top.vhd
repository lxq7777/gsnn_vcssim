-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;
USE work.gsnn_interface_package.ALL;

entity gsnn_three_cores is
port(
  clock_top, clock_top1, clock_top2                           : IN std_logic;
  reset                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- Memory control signals
  ---------------------------------------------------------------------------
  slp_in                          : IN bit_type;
  sd_in                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- PXI bus signals
  ---------------------------------------------------------------------------
  pxi_clock, pxi_clock1, pxi_clock2                      : OUT std_logic;
  pxi_reset, pxi_reset1, pxi_reset2                      : OUT bit_type;

  en_pxi_req_in                  : IN bit_type;
  we_pxi_req_in                  : IN bit_type;
  addr_pxi_req_in                : IN addr_pxi_type;
  word_pxi_req_in                : IN bit32_type;
  pull_pxi_req_busout, pull_pxi_req_busout1, pull_pxi_req_busout2            : OUT bit_type;

  avail_pxi_resp_in              : IN bit_type;
  en_pxi_resp_regout,  en_pxi_resp_regout1, en_pxi_resp_regout2             : OUT bit_type;
  addr_pxi_resp_regout, addr_pxi_resp_regout1, addr_pxi_resp_regout2           : OUT addr_pxi_type;
  word_pxi_resp_regout, word_pxi_resp_regout1, word_pxi_resp_regout2           : OUT bit32_type;

  interrupt_pxi_regout,  interrupt_pxi_regout1,  interrupt_pxi_regout2            : OUT bit_type;


  ---------------------------------------------------------------------------
  -- AXI bus signals
  ---------------------------------------------------------------------------
  axi_clock, axi_clock1, axi_clock2                      : OUT std_logic;
  axi_reset, axi_reset1, axi_reset2                      : OUT bit_type;

  -- AR Channel
  arqos_axi_busout    ,  arqos_axi_busout1       ,   arqos_axi_busout2               : OUT bit4_type;
  arid_axi_busout     ,  arid_axi_busout1        ,   arid_axi_busout2              : OUT arid_axi_type;  -- currently defined as 1 bit
  araddr_axi_busout   ,  araddr_axi_busout1      ,   araddr_axi_busout2            : OUT addr_axi_type;
  arlen_axi_busout    ,  arlen_axi_busout1       ,   arlen_axi_busout2             : OUT bit4_type;      -- number of bursts
  arsize_axi_busout   ,  arsize_axi_busout1      ,   arsize_axi_busout2            : OUT bit3_type;      -- size of each burst
  arburst_axi_busout  ,  arburst_axi_busout1     ,   arburst_axi_busout2          : OUT bit2_type;      -- type of burst,  will be INCR=01
  arlock_axi_busout   ,  arlock_axi_busout1      ,   arlock_axi_busout2            : OUT alock_axi_type; -- locking, will be NORMAL=00
--arcache_axi_busout              : OUT bit4_type;      -- not used by DDR
--arprot_axi_busout               : OUT bit3_type;      -- not used by DDR
  arvalid_axi_busout, arvalid_axi_busout1, arvalid_axi_busout2              : OUT std_logic;
  arready_axi_in                  : IN  std_logic;

  -- R Channel
  rid_axi_in                      : IN  arid_axi_type;
  rdata_axi_in                    : IN  data_axi_type;
  rresp_axi_in                    : IN  resp_axi_type;
  rlast_axi_in                    : IN  std_logic;
  rvalid_axi_in                   : IN  std_logic;
  rready_axi_busout, rready_axi_busout1, rready_axi_busout2               : OUT std_logic;

  -- AW Channel
  awqos_axi_busout   ,  awqos_axi_busout1    ,   awqos_axi_busout2                : OUT bit4_type;
  awid_axi_regout    ,  awid_axi_regout1     ,   awid_axi_regout2                : OUT awid_axi_type;
  awaddr_axi_regout  ,  awaddr_axi_regout1   ,   awaddr_axi_regout2              : OUT addr_axi_type;
  awlen_axi_regout   ,  awlen_axi_regout1    ,   awlen_axi_regout2               : OUT bit4_type;
  awsize_axi_regout  ,  awsize_axi_regout1   ,   awsize_axi_regout2              : OUT bit3_type;
  awburst_axi_regout ,  awburst_axi_regout1  ,   awburst_axi_regout2             : OUT awburst_axi_type;
  awlock_axi_regout  ,  awlock_axi_regout1   ,   awlock_axi_regout2              : OUT alock_axi_type;
--awcache_axi_regout              : OUT bit4_type;        -- not implemented by DDR
--awprot_axi_regout               : OUT bit3_type;        -- not implemented by DDR
  awvalid_axi_regout, awvalid_axi_regout1, awvalid_axi_regout2              : OUT std_logic;
  awready_axi_in                  : IN  std_logic;

  -- W Channel
  wid_axi_regout     , wid_axi_regout1    , wid_axi_regout2                  : OUT awid_axi_type;
  wdata_axi_regout   , wdata_axi_regout1  , wdata_axi_regout2              : OUT data_axi_type;
  wstrb_axi_regout   , wstrb_axi_regout1  , wstrb_axi_regout2              : OUT wstrb_axi_type;
  wlast_axi_regout   , wlast_axi_regout1  , wlast_axi_regout2              : OUT std_logic;
  wvalid_axi_regout  , wvalid_axi_regout1 , wvalid_axi_regout2             : OUT std_logic;
  wready_axi_in                   : IN  std_logic;

  -- B Channel
  bid_axi_in                      : IN  awid_axi_type;
  bresp_axi_in                    : IN  resp_axi_type;
  bvalid_axi_in                   : IN  std_logic;
  bready_axi_regout, bready_axi_regout1, bready_axi_regout2               : OUT std_logic
);
end entity;

architecture behavior of gsnn_three_cores is

component gsnn_unit_top IS
  PORT(
  clock                           : IN std_logic;
  reset                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- Memory control signals
  ---------------------------------------------------------------------------
  slp_in                          : IN bit_type;
  sd_in                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- PXI bus signals
  ---------------------------------------------------------------------------
  pxi_clock                      : OUT std_logic;
  pxi_reset                      : OUT bit_type;

  en_pxi_req_in                  : IN bit_type;
  we_pxi_req_in                  : IN bit_type;
  addr_pxi_req_in                : IN addr_pxi_type;
  word_pxi_req_in                : IN bit32_type;
  pull_pxi_req_busout            : OUT bit_type;

  avail_pxi_resp_in              : IN bit_type;
  en_pxi_resp_regout             : OUT bit_type;
  addr_pxi_resp_regout           : OUT addr_pxi_type;
  word_pxi_resp_regout           : OUT bit32_type;

  interrupt_pxi_regout           : OUT bit_type;


  ---------------------------------------------------------------------------
  -- AXI bus signals
  ---------------------------------------------------------------------------
  axi_clock                      : OUT std_logic;
  axi_reset                      : OUT bit_type;

  -- AR Channel
  arqos_axi_busout                : OUT bit4_type;
  arid_axi_busout                 : OUT arid_axi_type;  -- currently defined as 1 bit
  araddr_axi_busout               : OUT addr_axi_type;
  arlen_axi_busout                : OUT bit4_type;      -- number of bursts
  arsize_axi_busout               : OUT bit3_type;      -- size of each burst
  arburst_axi_busout              : OUT bit2_type;      -- type of burst,  will be INCR=01
  arlock_axi_busout               : OUT alock_axi_type; -- locking, will be NORMAL=00
--arcache_axi_busout              : OUT bit4_type;      -- not used by DDR
--arprot_axi_busout               : OUT bit3_type;      -- not used by DDR
  arvalid_axi_busout              : OUT std_logic;
  arready_axi_in                  : IN  std_logic;

  -- R Channel
  rid_axi_in                      : IN  arid_axi_type;
  rdata_axi_in                    : IN  data_axi_type;
  rresp_axi_in                    : IN  resp_axi_type;
  rlast_axi_in                    : IN  std_logic;
  rvalid_axi_in                   : IN  std_logic;
  rready_axi_busout               : OUT std_logic;

  -- AW Channel
  awqos_axi_busout                : OUT bit4_type;
  awid_axi_regout                 : OUT awid_axi_type;
  awaddr_axi_regout               : OUT addr_axi_type;
  awlen_axi_regout                : OUT bit4_type;
  awsize_axi_regout               : OUT bit3_type;
  awburst_axi_regout              : OUT awburst_axi_type;
  awlock_axi_regout               : OUT alock_axi_type;
--awcache_axi_regout              : OUT bit4_type;        -- not implemented by DDR
--awprot_axi_regout               : OUT bit3_type;        -- not implemented by DDR
  awvalid_axi_regout              : OUT std_logic;
  awready_axi_in                  : IN  std_logic;

  -- W Channel
  wid_axi_regout                  : OUT awid_axi_type;
  wdata_axi_regout                : OUT data_axi_type;
  wstrb_axi_regout                : OUT wstrb_axi_type;
  wlast_axi_regout                : OUT std_logic;
  wvalid_axi_regout               : OUT std_logic;
  wready_axi_in                   : IN  std_logic;

  -- B Channel
  bid_axi_in                      : IN  awid_axi_type;
  bresp_axi_in                    : IN  resp_axi_type;
  bvalid_axi_in                   : IN  std_logic;
  bready_axi_regout               : OUT std_logic
  );
END component gsnn_unit_top;               


  type array_wstrb_axi_type is array (natural RANGE<>) of wstrb_axi_type;
  type array_arid_axi_type is array (natural RANGE<>) of arid_axi_type;

type array_addr_pxi_type is array (natural RANGE<>) of addr_pxi_type;
type array_addr_axi_type is array (natural RANGE<>) of addr_axi_type;
type array_bit_type1 is array (natural RANGE<>) of std_logic;

type array_bit4_type is array (natural RANGE<>) of bit4_type;
  type array_bit3_type is array (natural RANGE<>) of bit3_type;
  type array_bit2_type is array (natural RANGE<>) of bit2_type;
  
    type array_alock_axi_type is array (natural RANGE<>) of alock_axi_type;
 
    type array_data_axi_type is array (natural RANGE<>) of data_axi_type;
    type array_resp_axi_type is array (natural RANGE<>) of resp_axi_type;
  type array_awid_axi_type is array (natural RANGE<>) of awid_axi_type;
  type array_awburst_axi_type is array (natural RANGE<>) of awburst_axi_type;

  signal clockx                           : array_bit_type1(0 to 2);
  signal resetx                              : array_bit_type1(0 to 2);
   signal sd_inx                          : array_bit_type1(0 to 2);
  signal slp_inx                              : array_bit_type1(0 to 2);
  signal pxi_clockx                      :  array_bit_type1(0 to 2);
  signal pxi_resetx                      :  array_bit_type1(0 to 2);
  signal en_pxi_req_inx                  :  array_bit_type1(0 to 2);
  signal we_pxi_req_inx                  :  array_bit_type1(0 to 2);
  signal addr_pxi_req_inx                :  array_addr_pxi_type(0 to 2);
  signal word_pxi_req_inx                :  array_bit32_type(0 to 2);
  signal pull_pxi_req_busoutx            :  array_bit_type1(0 to 2);
  signal avail_pxi_resp_inx              :  array_bit_type1(0 to 2);
  signal en_pxi_resp_regoutx             :  array_bit_type1(0 to 2);
  signal addr_pxi_resp_regoutx           :  array_addr_pxi_type(0 to 2);
  signal word_pxi_resp_regoutx           :  array_bit32_type(0 to 2);
  signal interrupt_pxi_regoutx           :  array_bit_type1(0 to 2);
  signal axi_clockx                      :  array_bit_type1(0 to 2);
  signal axi_resetx                      :  array_bit_type1(0 to 2);
  signal arqos_axi_busoutx                : array_bit4_type(0 to 2);
  
  
  signal arid_axi_busoutx                 : array_arid_axi_type(0 to 2);  -- currently defined as 1 bit
  signal araddr_axi_busoutx               : array_addr_axi_type(0 to 2);
  
  
  
  signal arlen_axi_busoutx                : array_bit4_type(0 to 2);      -- number of bursts
  signal arsize_axi_busoutx               : array_bit3_type(0 to 2);      -- size of each burst
  signal arburst_axi_busoutx              : array_bit2_type(0 to 2);      -- type of burst,  will be INCR=01
  
  
  signal arlock_axi_busoutx               : array_alock_axi_type(0 to 2); -- locking, will be NORMAL=00
  signal arvalid_axi_busoutx              : array_bit_type1(0 to 2);
  signal arready_axi_inx                  : array_bit_type1(0 to 2);
  signal rid_axi_inx                      : array_arid_axi_type(0 to 2);
 

  
  signal rdata_axi_inx                    : array_data_axi_type(0 to 2);
  signal rresp_axi_inx                   : array_resp_axi_type(0 to 2);
  signal rlast_axi_inx                    : array_bit_type1(0 to 2);
  signal rvalid_axi_inx                   : array_bit_type1(0 to 2);
  
  
  signal rready_axi_busoutx               : array_bit_type1(0 to 2);
  signal awqos_axi_busoutx                : array_bit4_type(0 to 2);
  
  
  signal awid_axi_regoutx                 : array_awid_axi_type(0 to 2);
  signal awaddr_axi_regoutx               : array_addr_axi_type(0 to 2);
  signal awlen_axi_regoutx                : array_bit4_type(0 to 2);
  signal awsize_axi_regoutx               : array_bit3_type(0 to 2);
  
  
  signal awburst_axi_regoutx              : array_awburst_axi_type(0 to 2);
  
  
  
  signal awlock_axi_regoutx               : array_alock_axi_type(0 to 2);
  signal awvalid_axi_regoutx              : array_bit_type1(0 to 2);
  signal awready_axi_inx                  : array_bit_type1(0 to 2);
  
  
  signal wid_axi_regoutx                  : array_awid_axi_type(0 to 2);
  
  
  signal wdata_axi_regoutx                : array_data_axi_type(0 to 2);
  
  
  
  signal wstrb_axi_regoutx                : array_wstrb_axi_type(0 to 2);
  signal wlast_axi_regoutx                : array_bit_type1(0 to 2);
  signal wvalid_axi_regoutx               : array_bit_type1(0 to 2);
  signal wready_axi_inx                   : array_bit_type1(0 to 2);
  
  
  signal bid_axi_inx                      : array_awid_axi_type(0 to 2);
  
  
  signal bresp_axi_inx                    : array_resp_axi_type(0 to 2);
  signal bvalid_axi_inx                   : array_bit_type1(0 to 2);
  signal bready_axi_regoutx               : array_bit_type1(0 to 2);

begin                                                      
  
--  clockx(0)                               <=     clock_top;               
--  resetx(0)                               <=     reset(0);               
--   sd_inx(0)                              <=      sd_in(0);              
--  slp_inx(0)                              <=     slp_in(0);              
--  en_pxi_req_inx(0)                       <=     en_pxi_req_in(0);       
--  we_pxi_req_inx(0)                       <=     we_pxi_req_in(0);       
--  addr_pxi_req_inx(0)                     <=     addr_pxi_req_in;     
--  word_pxi_req_inx(0)                     <=     word_pxi_req_in;     
--  avail_pxi_resp_inx(0)                   <=     avail_pxi_resp_in(0);   
--  arready_axi_inx(0)                      <=     arready_axi_in;      
--  rid_axi_inx(0)                          <=     rid_axi_in;          
--  rdata_axi_inx(0)                        <=     rdata_axi_in;        
--  rresp_axi_inx(0)                        <=     rresp_axi_in;        
--  rlast_axi_inx(0)                        <=     rlast_axi_in;        
--  rvalid_axi_inx(0)                       <=     rvalid_axi_in;       
--  awready_axi_inx(0)                      <=     awready_axi_in;      
--  wready_axi_inx(0)                       <=     wready_axi_in;       
--  bid_axi_inx(0)                          <=     bid_axi_in;          
--  bresp_axi_inx(0)                        <=     bresp_axi_in;        
--  bvalid_axi_inx(0)                       <=     bvalid_axi_in;       

--clockx(1)                               <=     clock_top;               
--  resetx(1)                               <=     reset(0);               
--   sd_inx(1)                              <=      sd_in(0);              
--  slp_inx(1)                              <=     slp_in(0);              
--  en_pxi_req_inx(1)                       <=     en_pxi_req_in(0);       
--  we_pxi_req_inx(1)                       <=     we_pxi_req_in(0);       
--  addr_pxi_req_inx(1)                     <=     addr_pxi_req_in;     
--  word_pxi_req_inx(1)                     <=     word_pxi_req_in;     
--  avail_pxi_resp_inx(1)                   <=     avail_pxi_resp_in(0);   
--  arready_axi_inx(1)                      <=     arready_axi_in;      
--  rid_axi_inx(1)                          <=     rid_axi_in;          
--  rdata_axi_inx(1)                        <=     rdata_axi_in;        
--  rresp_axi_inx(1)                        <=     rresp_axi_in;        
--  rlast_axi_inx(1)                        <=     rlast_axi_in;        
--  rvalid_axi_inx(1)                       <=     rvalid_axi_in;       
--  awready_axi_inx(1)                      <=     awready_axi_in;      
--  wready_axi_inx(1)                       <=     wready_axi_in;       
--  bid_axi_inx(1)                          <=     bid_axi_in;          
--  bresp_axi_inx(1)                        <=     bresp_axi_in;        
--  bvalid_axi_inx(1)                       <=     bvalid_axi_in;   
  
--  clockx(2)                               <=     clock_top;               
--  resetx(2)                               <=     reset(0);               
--   sd_inx(2)                              <=      sd_in(0);              
--  slp_inx(2)                              <=     slp_in(0);              
--  en_pxi_req_inx(2)                       <=     en_pxi_req_in(0);       
--  we_pxi_req_inx(2)                       <=     we_pxi_req_in(0);       
--  addr_pxi_req_inx(2)                     <=     addr_pxi_req_in;     
--  word_pxi_req_inx(2)                     <=     word_pxi_req_in;     
--  avail_pxi_resp_inx(2)                   <=     avail_pxi_resp_in(0);   
--  arready_axi_inx(2)                      <=     arready_axi_in;      
--  rid_axi_inx(2)                          <=     rid_axi_in;          
--  rdata_axi_inx(2)                        <=     rdata_axi_in;        
--  rresp_axi_inx(2)                        <=     rresp_axi_in;        
--  rlast_axi_inx(2)                        <=     rlast_axi_in;        
--  rvalid_axi_inx(2)                       <=     rvalid_axi_in;       
--  awready_axi_inx(2)                      <=     awready_axi_in;      
--  wready_axi_inx(2)                       <=     wready_axi_in;       
--  bid_axi_inx(2)                          <=     bid_axi_in;          
--  bresp_axi_inx(2)                        <=     bresp_axi_in;        
--  bvalid_axi_inx(2)                       <=     bvalid_axi_in;   
  
  
  
  
  
  
                                                          
-- CORE_GEN : for CORE in natural range 0 to 2 GENERATE                                                            

-- GSNN_TOP0: gsnn_unit_top port map(                                
--  clock                                  =>                        clock_top                                                                                      ,
--  reset                                  =>                        reset                                                                                      ,
--  slp_in                                 =>                        slp_in                                                                                       ,
--  sd_in                                  =>                        sd_in                                                                                        ,
--  pxi_clock                              =>                        pxi_clock                                                                                      ,
--  pxi_reset                              =>                        pxi_reset                                                                                      ,
--  en_pxi_req_in                          =>                        en_pxi_req_in                                                                                 ,
--  we_pxi_req_in                          =>                        we_pxi_req_in                                                                                 ,
--  addr_pxi_req_in                        =>                        addr_pxi_req_in                                                                               ,
--  word_pxi_req_in                        =>                        word_pxi_req_in                                                                               ,
--  pull_pxi_req_busout                    =>                        pull_pxi_req_busout                                                                           ,
--  avail_pxi_resp_in                      =>                        avail_pxi_resp_in                                                                            ,
--  en_pxi_resp_regout                     =>                        en_pxi_resp_regout                                                                           ,
--  addr_pxi_resp_regout                   =>                        addr_pxi_resp_regout                                                                         ,
--  word_pxi_resp_regout                =>                        word_pxi_resp_regout                                                                         ,
--  interrupt_pxi_regout                   =>                        interrupt_pxi_regout                                                                         ,
--  axi_clock                              =>                        axi_clock                                                                                     ,
--  axi_reset                              =>                        axi_reset                                                                                     ,
--  arqos_axi_busout                       =>                        arqos_axi_busout                                                                             ,
--  arid_axi_busout                        =>                        arid_axi_busout                                                                              ,
--  araddr_axi_busout                      =>                        araddr_axi_busout                                                                           ,
--  arlen_axi_busout                       =>                        arlen_axi_busout                                                                             ,
--  arsize_axi_busout                      =>                        arsize_axi_busout                                                                            ,
--  arburst_axi_busout                     =>                        arburst_axi_busout                                                                           ,
--  arlock_axi_busout                      =>                        arlock_axi_busout                                                                            ,
--  arvalid_axi_busout                     =>                        arvalid_axi_busout                                                                           ,
--  arready_axi_in                         =>                        arready_axi_in                                                                               ,
--  rid_axi_in                             =>                        rid_axi_in                                                                                   ,
--  rdata_axi_in                           =>                        rdata_axi_in                                                                                 ,
--  rresp_axi_in                           =>                        rresp_axi_in                                                                                 ,
--  rlast_axi_in                           =>                        rlast_axi_in                                                                                 ,
--  rvalid_axi_in                          =>                        rvalid_axi_in                                                                                ,
--  rready_axi_busout                      =>                        rready_axi_busout                                                                            ,
--  awqos_axi_busout                       =>                        awqos_axi_busout                                                                             ,
--  awid_axi_regout                        =>                        awid_axi_regout                                                                              ,
--  awaddr_axi_regout                      =>                        awaddr_axi_regout                                                                            ,
--  awlen_axi_regout                       =>                        awlen_axi_regout                                                                             ,
--  awsize_axi_regout                      =>                        awsize_axi_regout                                                                            ,
--  awburst_axi_regout                     =>                        awburst_axi_regout                                                                           ,
--  awlock_axi_regout                      =>                        awlock_axi_regout                                                                            ,
--  awvalid_axi_regout                     =>                        awvalid_axi_regout                                                                           ,
--  awready_axi_in                         =>                        awready_axi_in                                                                               ,
--  wid_axi_regout                         =>                        wid_axi_regout                                                                               ,
--  wdata_axi_regout                       =>                        wdata_axi_regout                                                                             ,
--  wstrb_axi_regout                       =>                        wstrb_axi_regout                                                                             ,
--  wlast_axi_regout                       =>                        wlast_axi_regout                                                                             ,
--  wvalid_axi_regout                      =>                        wvalid_axi_regout                                                                           ,
--  wready_axi_in                          =>                        wready_axi_in                                                                                ,
--  bid_axi_in                             =>                        bid_axi_in                                                                                   ,
--  bresp_axi_in                           =>                        bresp_axi_in                                                                                 ,
--  bvalid_axi_in                          =>                        bvalid_axi_in                                                                                ,
--  bready_axi_regout                      =>                        bready_axi_regout                                                                            
--  );
  
--  GSNN_TOP1: gsnn_unit_top port map(                                
--  clock                                  =>                        clock_top1                                                                                      ,
--  reset                                  =>                        reset                                                                                      ,
--  slp_in                                 =>                        slp_in                                                                                       ,
--  sd_in                                  =>                        sd_in                                                                                        ,
--  pxi_clock                              =>                        pxi_clock1                                                                                      ,
--  pxi_reset                              =>                        pxi_reset1                                                                                      ,
--  en_pxi_req_in                          =>                        en_pxi_req_in                                                                                 ,
--  we_pxi_req_in                          =>                        we_pxi_req_in                                                                                 ,
--  addr_pxi_req_in                        =>                        addr_pxi_req_in                                                                               ,
--  word_pxi_req_in                        =>                        word_pxi_req_in                                                                               ,
--  pull_pxi_req_busout                    =>                        pull_pxi_req_busout1                                                                           ,
--  avail_pxi_resp_in                      =>                        avail_pxi_resp_in                                                                            ,
--  en_pxi_resp_regout                     =>                        en_pxi_resp_regout1                                                                           ,
--  addr_pxi_resp_regout                   =>                        addr_pxi_resp_regout1                                                                         ,
--  word_pxi_resp_regout                   =>                        word_pxi_resp_regout1                                                                         ,
--  interrupt_pxi_regout                   =>                        interrupt_pxi_regout1                                                                         ,
--  axi_clock                              =>                        axi_clock1                                                                                     ,
--  axi_reset                              =>                        axi_reset1                                                                                     ,
--  arqos_axi_busout                       =>                        arqos_axi_busout1                                                                             ,
--  arid_axi_busout                        =>                        arid_axi_busout1                                                                              ,
--  araddr_axi_busout                      =>                        araddr_axi_busout1                                                                           ,
--  arlen_axi_busout                       =>                        arlen_axi_busout1                                                                             ,
--  arsize_axi_busout                      =>                        arsize_axi_busout1                                                                            ,
--  arburst_axi_busout                     =>                        arburst_axi_busout1                                                                           ,
--  arlock_axi_busout                      =>                        arlock_axi_busout1                                                                            ,
--  arvalid_axi_busout                     =>                        arvalid_axi_busout1                                                                           ,
--  arready_axi_in                         =>                        arready_axi_in                                                                               ,
--  rid_axi_in                             =>                        rid_axi_in                                                                                   ,
--  rdata_axi_in                           =>                        rdata_axi_in                                                                                 ,
--  rresp_axi_in                           =>                        rresp_axi_in                                                                                 ,
--  rlast_axi_in                           =>                        rlast_axi_in                                                                                 ,
--  rvalid_axi_in                          =>                        rvalid_axi_in                                                                                ,
--  rready_axi_busout                      =>                        rready_axi_busout1                                                                            ,
--  awqos_axi_busout                       =>                        awqos_axi_busout1                                                                             ,
--  awid_axi_regout                        =>                        awid_axi_regout1                                                                              ,
--  awaddr_axi_regout                      =>                        awaddr_axi_regout1                                                                            ,
--  awlen_axi_regout                       =>                        awlen_axi_regout1                                                                             ,
--  awsize_axi_regout                      =>                        awsize_axi_regout1                                                                            ,
--  awburst_axi_regout                     =>                        awburst_axi_regout1                                                                           ,
--  awlock_axi_regout                      =>                        awlock_axi_regout1                                                                            ,
--  awvalid_axi_regout                     =>                        awvalid_axi_regout1                                                                           ,
--  awready_axi_in                         =>                        awready_axi_in                                                                               ,
--  wid_axi_regout                         =>                        wid_axi_regout1                                                                               ,
--  wdata_axi_regout                       =>                        wdata_axi_regout1                                                                             ,
--  wstrb_axi_regout                       =>                        wstrb_axi_regout1                                                                             ,
--  wlast_axi_regout                       =>                        wlast_axi_regout1                                                                             ,
--  wvalid_axi_regout                      =>                        wvalid_axi_regout1                                                                            ,
--  wready_axi_in                          =>                        wready_axi_in                                                                                ,
--  bid_axi_in                             =>                        bid_axi_in                                                                                   ,
--  bresp_axi_in                           =>                        bresp_axi_in                                                                                 ,
--  bvalid_axi_in                          =>                        bvalid_axi_in                                                                                ,
--  bready_axi_regout                      =>                        bready_axi_regout1                                                                            
--  );
  
  
  GSNN_TOP0: gsnn_unit_top port map(                                
  clock                                  =>                        clock_top2                                                                                      ,
  reset                                  =>                        reset                                                                                      ,
  slp_in                                 =>                        slp_in                                                                                       ,
  sd_in                                  =>                        sd_in                                                                                        ,
  pxi_clock                              =>                        pxi_clock2                                                                                      ,
  pxi_reset                              =>                        pxi_reset2                                                                                      ,
  en_pxi_req_in                          =>                        en_pxi_req_in                                                                                 ,
  we_pxi_req_in                          =>                        we_pxi_req_in                                                                                 ,
  addr_pxi_req_in                        =>                        addr_pxi_req_in                                                                               ,
  word_pxi_req_in                        =>                        word_pxi_req_in                                                                               ,
  pull_pxi_req_busout                    =>                        pull_pxi_req_busout2                                                                           ,
  avail_pxi_resp_in                      =>                        avail_pxi_resp_in                                                                            ,
  en_pxi_resp_regout                     =>                        en_pxi_resp_regout2                                                                           ,
  addr_pxi_resp_regout                   =>                        addr_pxi_resp_regout2                                                                         ,
  word_pxi_resp_regout                   =>                        word_pxi_resp_regout2                                                                         ,
  interrupt_pxi_regout                   =>                        interrupt_pxi_regout2                                                                         ,
  axi_clock                              =>                        axi_clock2                                                                                     ,
  axi_reset                              =>                        axi_reset2                                                                                     ,
  arqos_axi_busout                       =>                        arqos_axi_busout2                                                                             ,
  arid_axi_busout                        =>                        arid_axi_busout2                                                                              ,
  araddr_axi_busout                      =>                        araddr_axi_busout2                                                                           ,
  arlen_axi_busout                       =>                        arlen_axi_busout2                                                                             ,
  arsize_axi_busout                      =>                        arsize_axi_busout2                                                                            ,
  arburst_axi_busout                     =>                        arburst_axi_busout2                                                                           ,
  arlock_axi_busout                      =>                        arlock_axi_busout2                                                                            ,
  arvalid_axi_busout                     =>                        arvalid_axi_busout2                                                                           ,
  arready_axi_in                         =>                        arready_axi_in                                                                               ,
  rid_axi_in                             =>                        rid_axi_in                                                                                   ,
  rdata_axi_in                           =>                        rdata_axi_in                                                                                 ,
  rresp_axi_in                           =>                        rresp_axi_in                                                                                 ,
  rlast_axi_in                           =>                        rlast_axi_in                                                                                 ,
  rvalid_axi_in                          =>                        rvalid_axi_in                                                                                ,
  rready_axi_busout                      =>                        rready_axi_busout2                                                                            ,
  awqos_axi_busout                       =>                        awqos_axi_busout2                                                                             ,
  awid_axi_regout                        =>                        awid_axi_regout2                                                                              ,
  awaddr_axi_regout                      =>                        awaddr_axi_regout2                                                                            ,
  awlen_axi_regout                       =>                        awlen_axi_regout2                                                                             ,
  awsize_axi_regout                      =>                        awsize_axi_regout2                                                                            ,
  awburst_axi_regout                     =>                        awburst_axi_regout2                                                                           ,
  awlock_axi_regout                      =>                        awlock_axi_regout2                                                                            ,
  awvalid_axi_regout                     =>                        awvalid_axi_regout2                                                                           ,
  awready_axi_in                         =>                        awready_axi_in                                                                               ,
  wid_axi_regout                         =>                        wid_axi_regout2                                                                               ,
  wdata_axi_regout                       =>                        wdata_axi_regout2                                                                             ,
  wstrb_axi_regout                       =>                        wstrb_axi_regout2                                                                             ,
  wlast_axi_regout                       =>                        wlast_axi_regout2                                                                             ,
  wvalid_axi_regout                      =>                        wvalid_axi_regout2                                                                            ,
  wready_axi_in                          =>                        wready_axi_in                                                                                ,
  bid_axi_in                             =>                        bid_axi_in                                                                                   ,
  bresp_axi_in                           =>                        bresp_axi_in                                                                                 ,
  bvalid_axi_in                          =>                        bvalid_axi_in                                                                                ,
  bready_axi_regout                      =>                        bready_axi_regout2                                                                            
  );

--end GENERATE;


end architecture;