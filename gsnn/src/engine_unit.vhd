-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

LIBRARY work;
USE work.gsnn_package.ALL;

ENTITY engine_unit IS
  GENERIC(
    engine_id               : IN engine_id_type
  );
  PORT(
    clock_engine, clock_mult                  : IN std_logic;
    reset                   : IN bit_type;

    block_in                : IN bit_type;

  -- global regs
    global_0_in             : IN bit32_type;
    global_1_in             : IN bit32_type;
    global_2_in             : IN bit32_type;
    global_3_in             : IN bit32_type;
    global_4_in             : IN bit32_type;
    global_5_in             : IN bit32_type;
    global_6_in             : IN bit32_type;
    global_7_in             : IN bit32_type;
    global_8_in             : IN bit32_type;
    global_9_in             : IN bit32_type;
    global_a_in             : IN bit32_type;
    global_b_in             : IN bit32_type;
    global_c_in             : IN bit32_type;
    global_d_in             : IN bit32_type;
    global_e_in             : IN bit32_type;
    global_f_in             : IN bit32_type;

  -- insn mem read
    en_insn_in              : IN bit_type;
    word_insn_in            : IN bit32_type;

  -- data mem read
    en_data_in              : IN bit_type;
    word_data_in            : IN bit32_type;

  -- set global register
    en_global_busout        : OUT bit_type;
    rt_global_busout        : OUT bit4_type;
    word_global_busout      : OUT bit32_type;

  -- new insn request
    en_mem_insn_busout      : OUT bit_type;
    addr_mem_insn_busout    : OUT insn_addr_type;

  -- new data request
    en_mem_data_busout      : OUT bit_type;
    addr_mem_data_busout    : OUT data_addr_type;
    we_mem_data_busout      : OUT bit_type;
    word_mem_data_busout    : OUT bit32_type;

  -- special command
    en_cmd_busout           : OUT bit_type;
    insn_cmd_busout         : OUT insn_type;
    ctl_cmd_busout          : OUT bit16_type;
    ctr_cmd_busout          : OUT bit32_type;
    base0_cmd_busout        : OUT bit32_type;
    base1_cmd_busout        : OUT bit32_type;
    base2_cmd_busout        : OUT bit32_type;
    spec_cmd_busout         : OUT bit32_type;

  -- err command
    err_align_busout        : OUT bit_type;
    err_page_busout         : OUT bit_type;

-- registers
    r0_busout               : OUT bit32_type;
    r1_busout               : OUT bit32_type;
    r2_busout               : OUT bit32_type;
    r3_busout               : OUT bit32_type;
    r4_busout               : OUT bit32_type;
    r5_busout               : OUT bit32_type;
    r6_busout               : OUT bit32_type;
    r7_busout               : OUT bit32_type;
    r8_busout               : OUT bit32_type;
    r9_busout               : OUT bit32_type;
    r10_busout              : OUT bit32_type;
    r11_busout              : OUT bit32_type;
    r12_busout              : OUT bit32_type;
    r13_busout              : OUT bit32_type;
    r14_busout              : OUT bit32_type;
    r15_busout              : OUT bit32_type;

    pc_busout               : OUT bit32_type
  );
END ENTITY engine_unit;

ARCHITECTURE behavior OF engine_unit IS
-- pragma translate_off
  CONSTANT disable_print : boolean := true;
  CONSTANT start_print   : time := 0 ns;
-- pragma translate_on


  SIGNAL  locals_q          : array_bit32_type(0 TO 15);
  SIGNAL  insn_q            : insn_type;
  SIGNAL  insn_d            : insn_type;
  SIGNAL  insn_en           : bit_type;
  SIGNAL  pc_q              : insn_addr_type;
  SIGNAL  pc_d              : insn_addr_type;
  SIGNAL  pc1_q             : insn_addr_type;
  SIGNAL  pc1_d             : insn_addr_type;
  SIGNAL  redo_q            : bit_type;
  SIGNAL  redo_d            : bit_type;

  SIGNAL  en_exec_q         : bit_type;
  SIGNAL  en_exec_d         : bit_type;
  SIGNAL  ra_val_q          : bit32_type;
  SIGNAL  rbi_val_q         : bit32_type;
  SIGNAL  upd_val_q         : data_addr_type;
  SIGNAL  rt_exec_q         : reg_type;
  SIGNAL  rt_exec_d         : reg_type;
  SIGNAL  op_q              : op_type;

  SIGNAL  en_data_q         : bit_type;
  SIGNAL  en_data_d         : bit_type;
  SIGNAL  rt_data_q         : reg_type;

  SIGNAL  regs_i            : array_bit32_type(0 TO 31 );
  SIGNAL  eq0s_i            : array_bit_type(0 TO 31 );
  SIGNAL  redo_i            : bit_type;
  SIGNAL  insn_i            : insn_type;
  SIGNAL  op_i              : op_type;
  SIGNAL  cc_i              : cc_type;
  SIGNAL  rt_i              : reg_type;
  SIGNAL  ra_i              : reg_type;
  SIGNAL  rb_i              : reg_type;
  SIGNAL  imm_i             : imm_type;
  SIGNAL  off_i             : off_type;
  SIGNAL  ctl_spec_i        : ctl_spec_type;
  SIGNAL  ctl_i             : bit16_type;

  SIGNAL  rt_val_i          : bit32_type;
  SIGNAL  ra_val_i          : bit32_type;
  SIGNAL  ra_eq0_val_i      : bit_type;
  SIGNAL  rb_val_i          : bit32_type;
  SIGNAL  rb_reg_val_i      : bit32_type;
  SIGNAL  rbi_val_i         : bit32_type;

  TYPE dec_type IS RECORD
    is_j        : bit_type;
    is_call      : bit_type;
    is_wait     : bit_type;
    is_special  : bit_type;
    is_mem      : bit_type;
    is_st       : bit_type;
    is_ld       : bit_type;
    is_upd      : bit_type; -- NO LDU/STU UNUSED
    is_arith    : bit_type;
    is_imm      : bit_type;
  END RECORD dec_type;
  SIGNAL  dec_i             : dec_type;

  SIGNAL  is_j_dec_i        : bit_type;
  SIGNAL  is_call_dec_i     : bit_type;
  SIGNAL  is_wait_dec_i     : bit_type;
  SIGNAL  is_special_dec_i  : bit_type;
  SIGNAL  is_mem_dec_i      : bit_type;
  SIGNAL  is_st_dec_i       : bit_type;
  SIGNAL  is_ld_dec_i       : bit_type;
  SIGNAL  is_upd_dec_i      : bit_type; -- NO LDU/STU UNUSED
  SIGNAL  is_arith_dec_i    : bit_type;
  SIGNAL  is_imm_dec_i      : bit_type;
  SIGNAL  block_i           : bit_type;

  SIGNAL  rb_data_addr_i    : data_addr_type;
  SIGNAL  data_addr_i       : data_addr_type;
  SIGNAL  soff_i            : data_addr_type;
  SIGNAL  prod_i            : bit48_type;
  SIGNAL  exec_val_i        : bit32_type;
  SIGNAL  exec_eq0_i        : bit_type;
  SIGNAL  taken_i           : bit_type;
  SIGNAL  eq0_data_i        : bit_type;

  SIGNAL  is_special_inc0_i : bit_type;
  SIGNAL  is_special_inc1_i : bit_type;
  SIGNAL  is_special_inc2_i : bit_type;

  SIGNAL  en_cmd_i          : bit_type;
  SIGNAL  err_align_i       : bit_type;
  SIGNAL  err_page_i        : bit_type;

-- jason signals below here

signal prod_no_carry : std_logic_vector(47 downto 0);
signal prod_LSB : std_logic_vector(47 downto 0);  
signal prod_i_dsp : std_logic_vector(47 downto 0);


signal exec_eq0_i_mul, exec_eq0_i_pattern : bit_type;
signal exec_eq0_i_mux : bit_type;
signal exec_val_i_mux : bit32_type;

signal pattern_lsb, pattern_msb : std_logic;

signal prod_i_dsp_reg : std_logic_vector(47 downto 0);

signal mult_enable : std_logic;

component mult_reg_fpga is
  GENERIC(
    WIDTHA : integer := 32;
    WIDTHB : integer := 16
  );
  PORT(
    clk : in std_logic;
    A   : in std_logic_vector(WIDTHA - 1 downto 0);
    B   : in std_logic_vector(WIDTHB - 1 downto 0);
    RES : out std_logic_vector(WIDTHA + WIDTHB - 1 downto 0);
    eq_zero : out std_logic
  );
END component mult_reg_fpga;

--component clk_wiz_0 is
--  Port ( 
--    clk_out1 : out STD_LOGIC;
--    clk_out2 : out STD_LOGIC;
--    reset : in STD_LOGIC;
--    locked : out STD_LOGIC;
--    clk_in1 : in STD_LOGIC
--  );
--end component; 

--signal clock1x, clock2x : std_logic;

--begin

--PLL: clk_wiz_0 port map(
--clk_out1 => clock1x,
--clk_out2 => clock2x,
--clk_in1 => clock_engine,
--reset => '0'
--);
component mult_trans_latch is 
port(
clock : in std_logic;
D : in std_logic_vector(47 downto 0);

Q : out std_logic_vector(47 downto 0)
);
end component;

signal latch_in, latch_out : std_logic_vector(47 downto 0);

begin

--a_mult_trans_latch : mult_trans_latch port map(clock_engine, latch_in, latch_out);

--a_mult_LSB : mult_reg_fpga port map(
--clk => clock_mult,
--A => ra_val_q,
-- B => rbi_val_q(15 downto 0),
--RES => latch_in
--);

--mult_enable <= clock_mult xor clock_engine;

--prod_i_dsp <= latch_out;

a_mult_LSB : mult_reg_fpga port map(
clk => clock_engine,
A => ra_val_i,
 B => rbi_val_i(15 downto 0),
RES => prod_i_dsp,
eq_zero => exec_eq0_i_pattern(0)
);

--prod_i_dsp <= prod_i_dsp_reg when rising_edge(clock_engine);


-- copy registers to array for easy access
  regs_local_gen: FOR i IN locals_q'range GENERATE
    regs_i(i)   <= locals_q(i);
  END GENERATE regs_local_gen;

  regs_i(16+0)  <= global_0_in;
  regs_i(16+1)  <= global_1_in;
  regs_i(16+2)  <= global_2_in;
  regs_i(16+3)  <= global_3_in;
  regs_i(16+4)  <= global_4_in;
  regs_i(16+5)  <= global_5_in;
  regs_i(16+6)  <= global_6_in;
  regs_i(16+7)  <= global_7_in;
  regs_i(16+8)  <= global_8_in;
  regs_i(16+9)  <= global_9_in;
  regs_i(16+10) <= global_a_in;
  regs_i(16+11) <= global_b_in;
  regs_i(16+12) <= global_c_in;
  regs_i(16+13) <= global_d_in;
  regs_i(16+14) <= global_e_in;
  regs_i(16+15) <= global_f_in;

  eq0s_gen: FOR i IN 0 TO 31 GENERATE
    eq0s_i(i) <= "1" WHEN regs_i(i) = (regs_i(i)'range => '0') ELSE "0";
  END GENERATE eq0s_gen;

  
-- decode insn
  insn_i        <= insn_q WHEN redo_q = "1" ELSE word_insn_in;
  op_i          <= insn_i( OP_INSN_FIELD);
  cc_i          <= insn_i( CC_INSN_FIELD );
  rt_i          <= insn_i( RT_INSN_FIELD);
  ra_i          <= insn_i( RA_INSN_FIELD);
  rb_i          <= insn_i( RB_INSN_FIELD);
  imm_i         <= insn_i( IMM_INSN_FIELD);
  off_i         <= insn_i( OFF_INSN_FIELD);
  ctl_spec_i    <= insn_i( CTL_SPEC_INSN_FIELD);

  WITH op_i SELECT
    dec_i <=
        dec_type'( "1", "0", "0", "0", "0", "0", "0", "0", "0", "0" )  WHEN J_OP,
        dec_type'( "1", "0", "0", "0", "0", "0", "0", "0", "0", "1" )  WHEN JA_OP,
        dec_type'( "0", "1", "0", "0", "0", "0", "0", "0", "1", "0" )  WHEN CALL_OP,
        dec_type'( "0", "1", "0", "0", "0", "0", "0", "0", "1", "1" )  WHEN CALLA_OP,
        dec_type'( "0", "0", "1", "0", "0", "0", "0", "0", "0", "0" )  WHEN WAIT_OP,
        dec_type'( "0", "0", "0", "1", "0", "0", "0", "0", "0", "0" )  WHEN INPUT_OP|FILTER_OP|POST_OP|OUTPUT_OP,
        dec_type'( "0", "0", "0", "1", "0", "0", "0", "0", "0", "1" )  WHEN INPUTI_OP|FILTERI_OP|POSTI_OP|OUTPUTI_OP,
        dec_type'( "0", "0", "0", "0", "1", "1", "0", "0", "0", "0" )  WHEN ST_OP,
        dec_type'( "0", "0", "0", "0", "1", "1", "0", "0", "0", "1" )  WHEN STA_OP,
        -- NO LDU/STU dec_type'( "0", "0", "0", "0", "1", "1", "0", "1", "0", "0" )  WHEN STU_OP,
        dec_type'( "0", "0", "0", "0", "1", "0", "1", "0", "0", "0" )  WHEN LD_OP,
        dec_type'( "0", "0", "0", "0", "1", "0", "1", "0", "0", "1" )  WHEN LDA_OP,
        -- NO LDU/STU dec_type'( "0", "0", "0", "0", "1", "0", "1", "1", "0", "0" )  WHEN LDU_OP,
        dec_type'( "0", "0", "0", "0", "1", "0", "1", "0", "1", "0" )  WHEN COPY_OP|COPYU_OP|ADD_OP|SUB_OP|SHR_OP|SHRA_OP|SHL_OP|MUL_OP|AND_OP|OR_OP|XOR_OP|CAND_OP,
        dec_type'( "0", "0", "0", "0", "1", "0", "1", "0", "1", "1" )  WHEN COPYI_OP|COPYUI_OP|ADDI_OP|SUBI_OP|SHRI_OP|SHRAI_OP|SHLI_OP|MULI_OP|ANDI_OP|ORI_OP|XORI_OP|CANDI_OP,
        dec_type'( "X", "X", "X", "X", "X", "X", "X", "X", "X", "X" )  WHEN OTHERS;

  (is_j_dec_i, is_call_dec_i, is_wait_dec_i, is_special_dec_i,
      is_mem_dec_i, is_st_dec_i, is_ld_dec_i, is_upd_dec_i,
      is_arith_dec_i, is_imm_dec_i ) <= dec_i;

  block_i <= "1" WHEN block_in = "1" ELSE
             "1" WHEN is_special_dec_i = "1" AND en_exec_q = "1" ELSE
             "1" WHEN is_special_dec_i = "1" AND en_data_q = "1" ELSE
             "0";

  -- bypass
  ra_val_i      <= exec_val_i   WHEN en_exec_q = "1" AND ra_i = rt_exec_q ELSE
                   word_data_in WHEN en_data_q = "1" AND ra_i = rt_data_q ELSE
                   regs_i(to_integer(unsigned(ra_i)));


--  exec_eq0_i_pattern  <= PATTERNDETECTx OR PATTERNDETECTx1;
  exec_eq0_i_mul <= "1" when prod_i_dsp = (prod_i_dsp'range => '0') else "0";

  exec_eq0_i    <= "1" WHEN exec_val_i = ( exec_val_i'range  => '0') ELSE "0";
  eq0_data_i    <= "1" WHEN word_data_in = (word_data_in'range => '0') ELSE "0";

  with op_q SELECT
    exec_eq0_i_mux <= 
       exec_eq0_i_pattern WHEN MUL_OP | MULI_OP,
       exec_eq0_i         WHEN OTHERS;

  ra_eq0_val_i  <= exec_eq0_i_mux   WHEN en_exec_q = "1" AND ra_i = rt_exec_q ELSE
                   eq0_data_i   WHEN en_data_q = "1" AND ra_i = rt_data_q ELSE
                   eq0s_i(to_integer(unsigned(ra_i)));

  rb_reg_val_i  <= regs_i(to_integer(unsigned(rb_i)));
  rb_val_i      <= exec_val_i   WHEN en_exec_q = "1" AND rb_i = rt_exec_q ELSE
                   word_data_in WHEN en_data_q = "1" AND rb_i = rt_data_q ELSE
                   rb_reg_val_i;
  rb_data_addr_i  <= std_logic_vector(unsigned(exec_val_i(data_addr_type'range)) + unsigned(soff_i))
                                WHEN en_exec_q = "1" AND rb_i = rt_exec_q ELSE
                    std_logic_vector(unsigned(word_data_in(data_addr_type'range)) + unsigned(soff_i))
                                WHEN en_data_q = "1" AND rb_i = rt_data_q ELSE
                    std_logic_vector(unsigned(rb_reg_val_i(data_addr_type'range)) + unsigned(soff_i));

  rbi_val_i     <= (31 DOWNTO 16 => '0') & imm_i WHEN is_imm_dec_i = "1" ELSE rb_val_i;

  pc1_d         <= std_logic_vector(unsigned(pc_q)+1);
  -- next pc
  pc_block: IF true GENERATE
    SIGNAL  ra_eq_i   : bit_type;
    SIGNAL  ra_ge_i   : bit_type;
    SIGNAL  ctr0_i    : bit_type;
    SIGNAL  do_branch_i : bit_type;
    SIGNAL  pc_mem_i  : insn_addr_type;
  BEGIN
    -- ra_eq_i     <= "1" WHEN ra_val_i = (ra_val_i'range => '0') ELSE "0";
    ra_eq_i     <= ra_eq0_val_i;
    ra_ge_i     <= "1" WHEN ra_val_i(ra_val_i'high) = '0' ELSE "0";
    WITH cc_i SELECT
      taken_i   <=  "1"                         WHEN ALWAYS_CC,
                    ra_eq_i                     WHEN EQ_CC,
                    NOT ra_eq_i                 WHEN NE_CC,
                    ra_ge_i                     WHEN GE_CC,
                    NOT ra_ge_i                 WHEN LT_CC,
                    ra_ge_i AND (NOT ra_eq_i)   WHEN GT_CC,
                    (NOT ra_ge_i) OR ra_eq_i    WHEN LE_CC,
                    "1"                         WHEN OTHERS;
    ctr0_i      <=  "1" WHEN ctl_spec_i = ONCE_CTL_SPEC ELSE
                    -- "1" WHEN locals_q(CTR_REG) = (bit32_type'range =>'0') ELSE
                    "1" WHEN eq0s_i(CTR_REG) = "1" ELSE
                    "0";

    WITH bit2_type'(is_wait_dec_i & is_special_dec_i) SELECT
      redo_i    <=  NOT taken_i WHEN "10",
                    NOT ctr0_i  WHEN "01",
                    "0"         WHEN OTHERS;
    redo_d      <=  "1" WHEN reset = "1" OR block_i = "1" ELSE redo_i;
    redo_q <=  redo_d  AFTER 10 ps WHEN rising_edge(clock_engine);

    insn_d      <=  RESTART_INSN WHEN reset = "1" ELSE word_insn_in;
    insn_en     <=  "1"          WHEN reset = "1" OR redo_q = "0" ELSE "0";
    insn_q <=  insn_d AFTER 10 ps WHEN rising_edge(clock_engine); -- AND insn_en = "1";

    do_branch_i <=  "1" WHEN ( taken_i = "1" AND is_j_dec_i = "1" ) OR is_call_dec_i = "1" ELSE "0";

    pc_d        <=  (OTHERS=>'0')                     WHEN reset = "1" ELSE
                    rbi_val_i(insn_addr_type'range)   WHEN do_branch_i = "1" ELSE
                    pc_q                              WHEN redo_d = "1" ELSE
                    pc1_d;
    pc_q <= pc_d AFTER 10 ps WHEN rising_edge(clock_engine);

    pc_mem_i    <=  rbi_val_i(insn_addr_type'range)   WHEN do_branch_i = "1" ELSE
                    pc1_d;

    en_mem_insn_busout
                <=  "0" WHEN reset = "1" ELSE
                    "0" WHEN redo_d = "1" ELSE
                    "1"; 
    addr_mem_insn_busout  <= pc_mem_i;
  END GENERATE pc_block;

  soff_i                <= (data_addr_type'high DOWNTO off_i'high+1 => off_i(off_i'high)) & off_i;
  data_addr_i           <= imm_i(data_addr_type'range) WHEN is_imm_dec_i = "1" ELSE rb_data_addr_i;
  en_mem_data_busout    <= is_mem_dec_i AND NOT block_i;
  addr_mem_data_busout  <= data_addr_i;
  we_mem_data_busout    <= is_st_dec_i;
  word_mem_data_busout  <= ra_val_i;

  en_exec_d             <= "0" WHEN reset = "1" OR block_i = "1" ELSE is_arith_dec_i
                           -- NO LDU/STU OR is_upd_dec_i
                           ;
  rt_exec_d             <=  rt_i -- NO LDU/STU WHEN is_arith_dec_i = "1" ELSE
                            -- NO LDU/STU rb_i WHEN is_upd_dec_i   = "1" ELSE
                            -- NO LDU/STU (OTHERS=>'X')
                            ;

  en_exec_q <= en_exec_d    AFTER 10 ps WHEN rising_edge(clock_engine);
  ra_val_q  <= ra_val_i     AFTER 10 ps WHEN rising_edge(clock_engine); -- AND is_arith_dec_i = "1";
  rbi_val_q <= rbi_val_i    AFTER 10 ps WHEN rising_edge(clock_engine); -- AND is_arith_dec_i = "1";
  -- NO LDU/STU upd_val_q <= data_addr_i  AFTER 10 ps WHEN rising_edge(clock_engine); -- AND is_upd_dec_i = "1";
  rt_exec_q <= rt_exec_d    AFTER 10 ps WHEN rising_edge(clock_engine);
  pc1_q     <= pc1_d        AFTER 10 ps WHEN rising_edge(clock_engine);
  op_q      <= op_i         AFTER 10 ps WHEN rising_edge(clock_engine);

  en_data_d <= "0" WHEN reset = "1" OR block_i = "1" ELSE is_ld_dec_i;
  en_data_q <= en_data_d AFTER 10 ps WHEN rising_edge(clock_engine);
  rt_data_q <= rt_i AFTER 10 ps WHEN rising_edge(clock_engine);

    
  with op_q SELECT
      exec_val_i <=
       prod_i_dsp(31 downto 0) 
                                                            WHEN MUL_OP | MULI_OP,
       exec_val_i_mux                                           WHEN OTHERS;

    
  WITH op_q SELECT
    exec_val_i_mux <=
        rbi_val_q                                         WHEN COPY_OP  | COPYI_OP,
        rbi_val_q(15 DOWNTO 0)&(15 DOWNTO 0=>'0')         WHEN COPYU_OP | COPYUI_OP,

        std_logic_vector(unsigned(ra_val_q) + unsigned(rbi_val_q))
                                                          WHEN ADD_OP   | ADDI_OP,
        std_logic_vector(unsigned(ra_val_q) - unsigned(rbi_val_q))
                                                          WHEN SUB_OP   | SUBI_OP,

        std_logic_vector(shift_right( unsigned(ra_val_q), to_integer(unsigned(rbi_val_q(4 DOWNTO 0)))))
                                                          WHEN SHR_OP   | SHRI_OP,
        std_logic_vector(shift_right( signed(ra_val_q), to_integer(unsigned(rbi_val_q(4 DOWNTO 0)))))
                                                          WHEN SHRA_OP  | SHRAI_OP,
        std_logic_vector(shift_left( unsigned(ra_val_q), to_integer(unsigned(rbi_val_q(4 DOWNTO 0)))))
                                                          WHEN SHL_OP   | SHLI_OP,
      

        ra_val_q AND rbi_val_q                            WHEN AND_OP   | ANDI_OP,
        ra_val_q OR rbi_val_q                             WHEN OR_OP    | ORI_OP,
        ra_val_q XOR rbi_val_q                            WHEN XOR_OP   | XORI_OP,
        (NOT ra_val_q) AND rbi_val_q                      WHEN CAND_OP  | CANDI_OP,
        -- NO LDU/STU (31 DOWNTO upd_val_q'high+1 => '0') & upd_val_q   WHEN LDU_OP   | STU_OP,
        (31 DOWNTO pc1_q'high+1 => '0') & pc1_q           WHEN CALL_OP  | CALLA_OP,
        (OTHERS => 'X')                                   WHEN OTHERS;

  ctl_i <=  imm_i WHEN is_imm_dec_i = "1" ELSE rb_reg_val_i(15 DOWNTO 0); -- do not to worry about bypass as in rbi_val_i, since bypass results in block
  input_gen: IF engine_id = INPUT_ENGINE GENERATE
    SIGNAL base1_gi   : bit32_type;
    SIGNAL sum_gi     : bit9_type;
  BEGIN
    is_special_inc0_i <= "1";
    is_special_inc1_i <= "1";
    is_special_inc2_i <= "0";

    base1_gi          <= locals_q(BASE1_REG);
    sum_gi            <= std_logic_vector( unsigned(bit9_type('0'&base1_gi(11 DOWNTO 4))) + unsigned(ctl_i(SIZE_INPUT_INSN_FIELD)) );
    err_align_i       <= "1" WHEN base1_gi(3 DOWNTO 0) /= x"0" ELSE "0";
    err_page_i        <= "1" WHEN sum_gi(8) = '1' ELSE "0"; -- overflow
  END GENERATE input_gen;
  filter_gen: IF engine_id = FILTER_ENGINE GENERATE
    SIGNAL sum_gi     : sum_filter_type;
    SIGNAL kind_gi    : kind_filter_type;
    SIGNAL active_gi  : bit_type;
  BEGIN
    sum_gi <= ctl_i(SUM_FILTER_INSN_FIELD);
    kind_gi <= ctl_i(KIND_FILTER_INSN_FIELD);
    active_gi <= "1" WHEN kind_gi = X1_KIND_FILTER ELSE
                 "1" WHEN kind_gi = X2_KIND_FILTER AND locals_q(CTR_REG)(0) = '0' ELSE
                 "1" WHEN kind_gi = X4_KIND_FILTER AND locals_q(CTR_REG)(1 DOWNTO 0) = "00" ELSE
                 "0";

    is_special_inc0_i <= active_gi;
    is_special_inc1_i <= "1";
    is_special_inc2_i <= "1" WHEN sum_gi = PARTIAL_SUM_FILTER and active_gi = "1" ELSE "0";
    err_align_i       <= "0";
    err_page_i        <= "0";
  END GENERATE filter_gen;
  post_gen: IF engine_id = POST_ENGINE GENERATE
    SIGNAL rows_gi  : rows_post_type;
    SIGNAL soft_gi  : bit_type;
  BEGIN
    rows_gi <= ctl_i(ROWS_POST_INSN_FIELD);
    soft_gi <= "1" WHEN ctl_i(KIND_POST_INSN_FIELD) = SOFT_KIND_POST ELSE "0";

    is_special_inc0_i <= "1" WHEN soft_gi = "1" AND ctl_i(CTL_POST_INSN_FIELD) = LAST_CTL_POST ELSE
                         "0" WHEN soft_gi = "1" ELSE
                         "1" WHEN rows_gi = R1_ROWS_POST ELSE
                         "1" WHEN rows_gi = R2_ROWS_POST AND locals_q(CTR_REG)(0) = '0' ELSE
                         "1" WHEN rows_gi = R4_ROWS_POST AND locals_q(CTR_REG)(1 DOWNTO 0) = "00" ELSE
                         "0";
    is_special_inc1_i <= "1";
    is_special_inc2_i <= "0";
    err_align_i       <= "0";
    err_page_i        <= "0";
  END GENERATE post_gen;
  output_gen: IF engine_id = OUTPUT_ENGINE GENERATE
    SIGNAL base0_gi     : bit32_type;
    SIGNAL err16_gi     : bit_type;
    SIGNAL err8_gi      : bit_type;
  BEGIN
    is_special_inc0_i <= "1";
    is_special_inc1_i <= "1";
    is_special_inc2_i <= "0";

    base0_gi          <=  locals_q(BASE0_REG);
    err16_gi          <=  "1" WHEN base0_gi(3 DOWNTO 0) /= x"0" ELSE "0";
    err8_gi           <=  "1" WHEN base0_gi(2 DOWNTO 0) /= "000" ELSE "0";
    WITH ctl_i(TYPE_INPUT_INSN_FIELD) SELECT
      err_align_i     <=  err16_gi    WHEN FP16_TYPE_INPUT,
                          err8_gi     WHEN U8_TYPE_INPUT|S8_TYPE_INPUT,
                          "X"         WHEN OTHERS;
    err_page_i        <=  "0";
  END GENERATE output_gen;

  wb_gen: FOR rn IN locals_q'range GENERATE
    SIGNAL  local_en  : bit_type;
    SIGNAL  local_d   : bit32_type;
  BEGIN
    ctr_gen: IF rn = CTR_REG GENERATE
      local_en          <=  "1"           WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            "1"           WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            NOT block_i   WHEN is_special_dec_i = "1" AND ctl_spec_i = LOOP_CTL_SPEC ELSE
                            "0";
      local_d           <=  exec_val_i    WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            word_data_in  WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            std_logic_vector( unsigned(locals_q(CTR_REG)) - 1)
                                          WHEN is_special_dec_i = "1" ELSE
                            (OTHERS => 'X');
    END GENERATE ctr_gen;
    base0_gen: IF rn = BASE0_REG GENERATE
      local_en          <=  "1"           WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            "1"           WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            NOT block_i   WHEN is_special_dec_i = "1" AND is_special_inc0_i = "1" ELSE
                             "0";
      local_d           <=  exec_val_i    WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            word_data_in  WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            std_logic_vector( unsigned(locals_q(BASE0_REG)) + unsigned(locals_q(INCR0_REG)))
                                          WHEN is_special_dec_i = "1" AND is_special_inc0_i = "1" ELSE
                            (OTHERS => 'X');
    END GENERATE base0_gen;
    base1_gen: IF rn = BASE1_REG GENERATE
      local_en          <=  "1"           WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            "1"           WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            NOT block_i   WHEN is_special_dec_i = "1" AND is_special_inc1_i = "1" ELSE
                             "0";
      local_d           <=  exec_val_i    WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            word_data_in  WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            std_logic_vector( unsigned(locals_q(BASE1_REG)) + unsigned(locals_q(INCR1_REG)))
                                          WHEN is_special_dec_i = "1" AND is_special_inc1_i = "1" ELSE
                            (OTHERS => 'X');
    END GENERATE base1_gen;
    base2_gen: IF rn = BASE2_REG GENERATE
      local_en          <=  "1"           WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            "1"           WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            NOT block_i   WHEN is_special_dec_i = "1" AND is_special_inc2_i = "1" ELSE
                             "0";
      local_d           <=  exec_val_i    WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            word_data_in  WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            std_logic_vector( unsigned(locals_q(BASE2_REG)) + unsigned(locals_q(INCR2_REG)))
                                          WHEN is_special_dec_i = "1" AND is_special_inc2_i = "1" ELSE
                            (OTHERS => 'X');
    END GENERATE base2_gen;

    other_gen: IF rn /= CTR_REG AND rn /= BASE0_REG AND rn /= BASE1_REG AND rn /= BASE2_REG GENERATE
      local_en          <=  "1"  WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            "1"  WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            "0"; 
      local_d           <=  exec_val_i   WHEN en_exec_q = "1" AND rn = unsigned(rt_exec_q) ELSE
                            word_data_in WHEN en_data_q = "1" AND rn = unsigned(rt_data_q) ELSE
                            (OTHERS => 'X');
    END GENERATE other_gen;

    locals_q(rn) <= local_d AFTER 10 ps WHEN rising_edge(clock_engine) AND local_en = "1";
  END GENERATE wb_gen;

  en_global_busout
                <=  "1"  WHEN en_exec_q = "1" AND rt_exec_q(4) = '1' ELSE
                    "1"  WHEN en_data_q = "1" AND rt_data_q(4) = '1' ELSE
                    "0";
  rt_global_busout
                <=  rt_exec_q(3 DOWNTO 0)  WHEN en_exec_q = "1" AND rt_exec_q(4) = '1' ELSE
                    rt_exec_q(3 DOWNTO 0)  WHEN en_data_q = "1" AND rt_data_q(4) = '1' ELSE
                    (OTHERS => 'X');
  word_global_busout    <=  exec_val_i   WHEN en_exec_q = "1" AND rt_exec_q(4) = '1' ELSE
                          word_data_in WHEN en_data_q = "1" AND rt_data_q(4) = '1' ELSE 
                          (OTHERS => 'X');

  en_cmd_i              <=  "1" WHEN is_special_dec_i = "1" AND block_i = "0" ELSE "0";
  en_cmd_busout         <=  "1" WHEN en_cmd_i = "1" AND err_align_i = "0" AND err_page_i = "0" ELSE "0";
  insn_cmd_busout       <=  insn_i;
  ctl_cmd_busout        <=  ctl_i;
  ctr_cmd_busout        <=  locals_q(CTR_REG);
  base0_cmd_busout      <=  locals_q(BASE0_REG);
  base1_cmd_busout      <=  locals_q(BASE1_REG);
  base2_cmd_busout      <=  locals_q(BASE2_REG);
  spec_cmd_busout       <=  locals_q(SPEC_REG);

  err_align_busout      <=  "1" WHEN en_cmd_i = "1" AND err_align_i = "1" ELSE "0";
  err_page_busout       <=  "1" WHEN en_cmd_i = "1" AND err_page_i = "1" ELSE "0";

  r0_busout             <= locals_q(0);
  r1_busout             <= locals_q(1);
  r2_busout             <= locals_q(2);
  r3_busout             <= locals_q(3);
  r4_busout             <= locals_q(4);
  r5_busout             <= locals_q(5);
  r6_busout             <= locals_q(6);
  r7_busout             <= locals_q(7);
  r8_busout             <= locals_q(8);
  r9_busout             <= locals_q(9);
  r10_busout            <= locals_q(10);
  r11_busout            <= locals_q(11);
  r12_busout            <= locals_q(12);
  r13_busout            <= locals_q(13);
  r14_busout            <= locals_q(14);
  r15_busout            <= locals_q(15);

  pc_busout             <= (pc_busout'high DOWNTO pc_q'high+1 => '0' ) & pc_q;

-- pragma translate_off
  dump: PROCESS
    VARIABLE buf      : line;
    VARIABLE tick     : integer;
    VARIABLE diff     : boolean;
    VARIABLE last_reg : array_bit32_type(0 TO 15);
    VARIABLE name     : string(1 TO 5) := (OTHERS=>' ');
    VARIABLE first    : boolean := true;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;

    IF first THEN 
      CASE engine_id IS
        WHEN "00" => name(1 TO 4) := "EIN>";
        WHEN "01" => name(1 TO 4) := "EFL>";
        WHEN "10" => name(1 TO 4) := "EPS>";
        WHEN "11" => name(1 TO 4) := "EOT>";
        WHEN OTHERS => name(1 TO 4) := "E??>";
      END CASE;
      first := true;
    END IF;
    WAIT UNTIL falling_edge( clock_engine );
    WAIT FOR 100 ps;

    IF now >= start_print THEN
      tick := ( now / 1 ns ) - 2;

      IF false THEN
      dump_cycle( buf, name);
      dump_bit( buf, reset, " reset " );
      dump_bit( buf, block_in, " block_in " );
      dump_bit( buf, err_align_i, " err_align_i " );
      dump_bit( buf, err_page_i, " err_page_i " );
      writeline( output, buf );
      END IF;

      IF reset /= "1" THEN

        dump_cycle( buf, name);
        dump_hex( buf, pc_q, " @" );
        dump_insn( buf, insn_i, " " );
        CASE op_i IS
          WHEN J_OP|JA_OP       => dump_hex(buf, taken_i, " : " ); dump_hex(buf, ra_val_i, " "); dump_hex(buf,  rbi_val_i, " -> " );
          WHEN WAIT_OP          => dump_hex(buf, taken_i, " : " ); dump_hex(buf, ra_val_i, " ");
          WHEN LDA_OP|LD_OP|COPY_OP|COPYU_OP|COPYI_OP|COPYUI_OP|CALL_OP|CALLA_OP
          -- no LDU |LDU_OP
                                => dump_hex(buf, rbi_val_i, " : " );
          WHEN INPUT_OP|OUTPUT_OP|INPUTI_OP|OUTPUTI_OP => dump_hex(buf, locals_q(CTR_REG), " ctr ");
                                    dump_hex(buf, locals_q(BASE0_REG), " base0 ");
                                    dump_hex(buf, locals_q(BASE1_REG), " base1 ");
                                    dump_hex(buf, locals_q(SPEC_REG), " spec ");
          WHEN POST_OP|POSTI_OP          => dump_hex(buf, locals_q(CTR_REG), " ctr ");
                                    dump_hex(buf, locals_q(BASE0_REG), " base0 ");
                                    dump_hex(buf, locals_q(BASE1_REG), " base1 ");
          WHEN FILTER_OP|FILTERI_OP        => dump_hex(buf, locals_q(CTR_REG), " ctr ");
                                    dump_hex(buf, locals_q(BASE0_REG), " base0 ");
                                    dump_hex(buf, locals_q(BASE1_REG), " base1 ");
                                    dump_hex(buf, locals_q(BASE2_REG), " base2 ");
                                    dump_hex(buf, locals_q(SPEC_REG), " spec ");
          WHEN OTHERS           => dump_hex(buf, ra_val_i, " : " ); dump_hex( buf, rbi_val_i, " " );
        END CASE;

        --dump_bit( buf, redo_q, " redo_q ");
        --dump_hex( buf, pc_d, " pc_d ");
        --dump_bit( buf, redo_d, " redo_d ");
        writeline( output, buf );


        diff := false;
        FOR i IN 0 TO 15 LOOP
          IF locals_q(i) /= last_reg(i) THEN
            diff := true;
          END IF;
        END LOOP;

        IF diff THEN
          dump_cycle( buf, name );
          FOR i IN 0 TO 15 LOOP
            IF locals_q(i) /= last_reg(i) THEN
              dump_integer( buf, i, " $r" );
              dump_hex( buf, locals_q(i), " := " );
              last_reg(i) := locals_q(i);
            END IF;
          END LOOP;
          writeline( output, buf );
        END IF;
      END IF;

    END IF;

  END PROCESS dump;
-- pragma translate_on
END ARCHITECTURE behavior;
