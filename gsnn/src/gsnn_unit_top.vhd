-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;
USE work.gsnn_interface_package.ALL;

ENTITY gsnn_unit_top IS
  PORT(
  clock                           : IN std_logic;
  reset                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- Memory control signals
  ---------------------------------------------------------------------------
  slp_in                          : IN bit_type;
  sd_in                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- PXI bus signals
  ---------------------------------------------------------------------------
  pxi_clock                      : OUT std_logic;
  pxi_reset                      : OUT bit_type;

  en_pxi_req_in                  : IN bit_type;
  we_pxi_req_in                  : IN bit_type;
  addr_pxi_req_in                : IN addr_pxi_type;
  word_pxi_req_in                : IN bit32_type;
  pull_pxi_req_busout            : OUT bit_type;

  avail_pxi_resp_in              : IN bit_type;
  en_pxi_resp_regout             : OUT bit_type;
  addr_pxi_resp_regout           : OUT addr_pxi_type;
  word_pxi_resp_regout           : OUT bit32_type;

  interrupt_pxi_regout           : OUT bit_type;


  ---------------------------------------------------------------------------
  -- AXI bus signals
  ---------------------------------------------------------------------------
  axi_clock                      : OUT std_logic;
  axi_reset                      : OUT bit_type;

  -- AR Channel
  arqos_axi_busout                : OUT bit4_type;
  arid_axi_busout                 : OUT arid_axi_type;  -- currently defined as 1 bit
  araddr_axi_busout               : OUT addr_axi_type;
  arlen_axi_busout                : OUT bit4_type;      -- number of bursts
  arsize_axi_busout               : OUT bit3_type;      -- size of each burst
  arburst_axi_busout              : OUT bit2_type;      -- type of burst,  will be INCR=01
  arlock_axi_busout               : OUT alock_axi_type; -- locking, will be NORMAL=00
--arcache_axi_busout              : OUT bit4_type;      -- not used by DDR
--arprot_axi_busout               : OUT bit3_type;      -- not used by DDR
  arvalid_axi_busout              : OUT std_logic;
  arready_axi_in                  : IN  std_logic;

  -- R Channel
  rid_axi_in                      : IN  arid_axi_type;
  rdata_axi_in                    : IN  data_axi_type;
  rresp_axi_in                    : IN  resp_axi_type;
  rlast_axi_in                    : IN  std_logic;
  rvalid_axi_in                   : IN  std_logic;
  rready_axi_busout               : OUT std_logic;

  -- AW Channel
  awqos_axi_busout                : OUT bit4_type;
  awid_axi_regout                 : OUT awid_axi_type;
  awaddr_axi_regout               : OUT addr_axi_type;
  awlen_axi_regout                : OUT bit4_type;
  awsize_axi_regout               : OUT bit3_type;
  awburst_axi_regout              : OUT awburst_axi_type;
  awlock_axi_regout               : OUT alock_axi_type;
--awcache_axi_regout              : OUT bit4_type;        -- not implemented by DDR
--awprot_axi_regout               : OUT bit3_type;        -- not implemented by DDR
  awvalid_axi_regout              : OUT std_logic;
  awready_axi_in                  : IN  std_logic;

  -- W Channel
  wid_axi_regout                  : OUT awid_axi_type;
  wdata_axi_regout                : OUT data_axi_type;
  wstrb_axi_regout                : OUT wstrb_axi_type;
  wlast_axi_regout                : OUT std_logic;
  wvalid_axi_regout               : OUT std_logic;
  wready_axi_in                   : IN  std_logic;

  -- B Channel
  bid_axi_in                      : IN  awid_axi_type;
  bresp_axi_in                    : IN  resp_axi_type;
  bvalid_axi_in                   : IN  std_logic;
  bready_axi_regout               : OUT std_logic
  );
END ENTITY gsnn_unit_top;

ARCHITECTURE behavior OF gsnn_unit_top IS
-- pragma translate_off
  CONSTANT disable_print : boolean := false;
  CONSTANT start_print   : time := 0 ns;
-- pragma translate_on

  COMPONENT gsnn_clock_gen IS
    PORT(
      clock                         : IN std_logic;
      reset                         : IN bit_type;

      axi_clock                     : OUT std_logic;
      axi_reset                     : OUT bit_type;
      axi_early_busout              : OUT bit_type;
      pxi_clock                     : OUT std_logic;
      pxi_reset                     : OUT bit_type;
      pxi_early_busout              : OUT bit_type
    );
  END COMPONENT gsnn_clock_gen;

  COMPONENT engine_unit_top IS
    GENERIC(
      engine_id                 : engine_id_type
    );
    PORT(
      clock                   : IN std_logic;
      reset                   : IN bit_type;
      reset_mem               : IN bit_type;

      slp_in                  : IN bit_type;
      sd_in                   : IN bit_type;

      block_in                : IN bit_type;
    -- global regs
      global_0_in             : IN bit32_type;
      global_1_in             : IN bit32_type;
      global_2_in             : IN bit32_type;
      global_3_in             : IN bit32_type;
      global_4_in             : IN bit32_type;
      global_5_in             : IN bit32_type;
      global_6_in             : IN bit32_type;
      global_7_in             : IN bit32_type;
      global_8_in             : IN bit32_type;
      global_9_in             : IN bit32_type;
      global_a_in             : IN bit32_type;
      global_b_in             : IN bit32_type;
      global_c_in             : IN bit32_type;
      global_d_in             : IN bit32_type;
      global_e_in             : IN bit32_type;
      global_f_in             : IN bit32_type;

    -- set global register
      en_global_busout        : OUT bit_type;
      rt_global_busout        : OUT bit4_type;
      word_global_busout      : OUT bit32_type;

    -- special command
      en_cmd_busout           : OUT bit_type;
      insn_cmd_busout         : OUT insn_type;
      ctl_cmd_busout          : OUT bit16_type;
      ctr_cmd_busout          : OUT bit32_type;
      base0_cmd_busout        : OUT bit32_type;
      base1_cmd_busout        : OUT bit32_type;
      base2_cmd_busout        : OUT bit32_type;
      spec_cmd_busout         : OUT bit32_type;

  -- error
      err_align_busout        : OUT bit_type;
      err_page_busout         : OUT bit_type;

      ---------------------------------------------------------------------------
      -- INSN PXI signals
      ---------------------------------------------------------------------------
      en_mem_insn_pxi_in        : IN bit_type;
      we_mem_insn_pxi_in        : IN bit_type;
      addr_mem_insn_pxi_in      : IN addr_pxi_type;
      word_mem_insn_pxi_in      : IN bit32_type;

      en_insn_pxi_busout        : OUT bit_type;
      word_insn_pxi_busout      : OUT bit32_type;
      collide_insn_pxi_busout   : OUT bit_type;

      ---------------------------------------------------------------------------
      -- DATA PXI signals
      ---------------------------------------------------------------------------
      en_mem_data_pxi_in        : IN bit_type;
      we_mem_data_pxi_in        : IN bit_type;
      addr_mem_data_pxi_in      : IN addr_pxi_type;
      word_mem_data_pxi_in      : IN bit32_type;

      en_data_pxi_busout        : OUT bit_type;
      word_data_pxi_busout      : OUT bit32_type;
      collide_data_pxi_busout   : OUT bit_type;

  -- registers
      r0_busout               : OUT bit32_type;
      r1_busout               : OUT bit32_type;
      r2_busout               : OUT bit32_type;
      r3_busout               : OUT bit32_type;
      r4_busout               : OUT bit32_type;
      r5_busout               : OUT bit32_type;
      r6_busout               : OUT bit32_type;
      r7_busout               : OUT bit32_type;
      r8_busout               : OUT bit32_type;
      r9_busout               : OUT bit32_type;
      r10_busout              : OUT bit32_type;
      r11_busout              : OUT bit32_type;
      r12_busout              : OUT bit32_type;
      r13_busout              : OUT bit32_type;
      r14_busout              : OUT bit32_type;
      r15_busout              : OUT bit32_type;

      pc_busout               : OUT bit32_type
    );
  END COMPONENT engine_unit_top;

  COMPONENT global_memory_unit IS
    PORT(
    clock                           : IN std_logic;
    reset                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- Memory control signals
    ---------------------------------------------------------------------------
    slp_in                          : IN bit_type;
    sd_in                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- Collision error
    ---------------------------------------------------------------------------
    err_collide_busout              : OUT bit7_type;

    ---------------------------------------------------------------------------
    -- INPUT engine signals
    ---------------------------------------------------------------------------
    en_mem_input_in                 : IN bit_type;
    slice_mem_input_in              : IN slice_mask_type;
    addr_mem_input_in               : IN slice_addr_type;
    word_mem_input_in               : IN bit128_type;

    ---------------------------------------------------------------------------
    -- FILTER engine signals
    ---------------------------------------------------------------------------
    en_mem_data_filter_in           : IN bit_type;
    width_mem_data_filter_in        : IN bit2_type;
    slice_mem_data_filter_in        : IN slice_mask_type;
    addr_mem_data_filter_in         : IN slice_addr_type;
    word0_data_filter_busout        : OUT bit144_type;
    word1_data_filter_busout        : OUT bit144_type;
    word2_data_filter_busout        : OUT bit144_type;
    word3_data_filter_busout        : OUT bit144_type;
    word4_data_filter_busout        : OUT bit144_type;
    word5_data_filter_busout        : OUT bit144_type;
    word6_data_filter_busout        : OUT bit144_type;
    word7_data_filter_busout        : OUT bit144_type;

    en_mem_psum_filter_in           : IN bit_type;
    width_mem_psum_filter_in        : IN bit_type;
    slice_mem_psum_filter_in        : IN slice_mask_type;
    addr_mem_psum_filter_in         : IN slice_addr_type;
    word0_psum_filter_busout        : OUT bit64_type;
    word1_psum_filter_busout        : OUT bit64_type;
    word2_psum_filter_busout        : OUT bit64_type;
    word3_psum_filter_busout        : OUT bit64_type;
    word4_psum_filter_busout        : OUT bit64_type;
    word5_psum_filter_busout        : OUT bit64_type;
    word6_psum_filter_busout        : OUT bit64_type;
    word7_psum_filter_busout        : OUT bit64_type;

    en_mem_res_filter_in            : IN bit_type;
    width_mem_res_filter_in         : IN bit_type;
    slice_mem_res_filter_in         : IN slice_mask_type;
    addr_mem_res_filter_in          : IN slice_addr_type;
    word0_mem_res_filter_in         : IN bit64_type;
    word1_mem_res_filter_in         : IN bit64_type;
    word2_mem_res_filter_in         : IN bit64_type;
    word3_mem_res_filter_in         : IN bit64_type;
    word4_mem_res_filter_in         : IN bit64_type;
    word5_mem_res_filter_in         : IN bit64_type;
    word6_mem_res_filter_in         : IN bit64_type;
    word7_mem_res_filter_in         : IN bit64_type;

    ---------------------------------------------------------------------------
    -- POST engine signals
    --------------------------------------------------------------------------
    en_mem_data_post_in             : IN bit_type;
    addr_mem_data_post_in           : IN global_addr_type;
    word_data_post_busout           : OUT bit128_type;

    en_mem_res_post_in              : IN bit_type;
    wr4_mem_res_post_in             : IN bit_type;    -- write 2 or 4 half-words
    addr_mem_res_post_in            : IN global_addr_type;
    word_mem_res_post_in            : IN bit64_type;

    ---------------------------------------------------------------------------
    -- OUTPUT engine signals
    ---------------------------------------------------------------------------
    en_mem_output_in                : IN bit_type;
    addr_mem_output_in              : IN global_addr_type;
    word_output_busout              : OUT bit128_type
    );
  END COMPONENT global_memory_unit;

  COMPONENT input_unit IS
    PORT(
    clock                           : IN std_logic;
    reset                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- engine signals
    ---------------------------------------------------------------------------
    en_cmd_in                       : IN bit_type;
    insn_cmd_in                     : IN insn_type;
    ctl_cmd_in                      : IN bit16_type;
    ctr_cmd_in                      : IN bit32_type;
    base0_cmd_in                    : IN bit32_type;
    base1_cmd_in                    : IN bit32_type;
    spec_cmd_in                     : IN bit32_type;

    block_busout                    : OUT bit_type;

    ---------------------------------------------------------------------------
    -- memory signals
    ---------------------------------------------------------------------------
    en_mem_busout                   : OUT bit_type;
    slice_mem_busout                : OUT slice_mask_type;
    addr_mem_busout                 : OUT slice_addr_type;
    word_mem_busout                 : OUT bit128_type;

    ---------------------------------------------------------------------------
    -- AXI bus signals
    ---------------------------------------------------------------------------
    axi_clock                       : IN std_logic;
    -- AR Channel
    arid_axi_busout                 : OUT arid_axi_type;  -- currently defined as 1 bit
    araddr_axi_busout               : OUT addr_axi_type;
    arlen_axi_busout                : OUT bit4_type;      -- number of bursts
    arsize_axi_busout               : OUT bit3_type;      -- size of each burst
    arburst_axi_busout              : OUT bit2_type;      -- type of burst,  will be INCR=01
    arlock_axi_busout               : OUT alock_axi_type; -- locking, will be NORMAL=00
    arvalid_axi_busout              : OUT std_logic;
    arready_axi_in                  : IN  std_logic;

    -- R Channel
    rid_axi_in                      : IN  arid_axi_type;
    rdata_axi_in                    : IN  data_axi_type;
    rresp_axi_in                    : IN  resp_axi_type;
    rlast_axi_in                    : IN  std_logic;
    rvalid_axi_in                   : IN  std_logic;
    rready_axi_busout               : OUT std_logic

    );
  END COMPONENT input_unit;

  COMPONENT filter_unit IS
    PORT(
    clock                           : IN std_logic;
    reset                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- engine signals
    ---------------------------------------------------------------------------
    en_cmd_in                       : IN bit_type;
    insn_cmd_in                     : IN insn_type;
    ctl_cmd_in                      : IN bit16_type;
    ctr_cmd_in                      : IN bit32_type;
    base0_cmd_in                    : IN bit32_type;
    base1_cmd_in                    : IN bit32_type;
    base2_cmd_in                    : IN bit32_type;
    spec_cmd_in                     : IN bit32_type;

    ---------------------------------------------------------------------------
    -- memory signals
    ---------------------------------------------------------------------------
    en_mem_data_busout              : OUT bit_type;
    width_mem_data_busout           : OUT bit2_type;
    slice_mem_data_busout           : OUT slice_mask_type;
    addr_mem_data_busout            : OUT slice_addr_type;
    word0_data_in                   : IN bit144_type;
    word1_data_in                   : IN bit144_type;
    word2_data_in                   : IN bit144_type;
    word3_data_in                   : IN bit144_type;
    word4_data_in                   : IN bit144_type;
    word5_data_in                   : IN bit144_type;
    word6_data_in                   : IN bit144_type;
    word7_data_in                   : IN bit144_type;

    en_mem_psum_busout              : OUT bit_type;
    width_mem_psum_busout           : OUT bit_type;
    slice_mem_psum_busout           : OUT slice_mask_type;
    addr_mem_psum_busout            : OUT slice_addr_type;
    word0_psum_in                   : IN bit64_type;
    word1_psum_in                   : IN bit64_type;
    word2_psum_in                   : IN bit64_type;
    word3_psum_in                   : IN bit64_type;
    word4_psum_in                   : IN bit64_type;
    word5_psum_in                   : IN bit64_type;
    word6_psum_in                   : IN bit64_type;
    word7_psum_in                   : IN bit64_type;

    en_mem_res_busout               : OUT bit_type;
    width_mem_res_busout            : OUT bit_type;
    slice_mem_res_busout            : OUT slice_mask_type;
    addr_mem_res_busout             : OUT slice_addr_type;
    word0_mem_res_busout            : OUT bit64_type;
    word1_mem_res_busout            : OUT bit64_type;
    word2_mem_res_busout            : OUT bit64_type;
    word3_mem_res_busout            : OUT bit64_type;
    word4_mem_res_busout            : OUT bit64_type;
    word5_mem_res_busout            : OUT bit64_type;
    word6_mem_res_busout            : OUT bit64_type;
    word7_mem_res_busout            : OUT bit64_type
    );
  END COMPONENT filter_unit;

  COMPONENT post_unit IS
    PORT(
    clock                           : IN std_logic;
    reset                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- engine signals
    ---------------------------------------------------------------------------
    en_cmd_in                       : IN bit_type;
    insn_cmd_in                     : IN insn_type;
    ctl_cmd_in                      : IN bit16_type;
    ctr_cmd_in                      : IN bit32_type;
    base0_cmd_in                    : IN bit32_type;
    base1_cmd_in                    : IN bit32_type;

    ---------------------------------------------------------------------------
    -- memory signals
    ---------------------------------------------------------------------------
    en_mem_data_busout              : OUT bit_type;
    addr_mem_data_busout            : OUT global_addr_type;
    word_data_in                    : IN bit128_type;

    en_mem_res_busout               : OUT bit_type;
    wr4_mem_res_busout              : OUT bit_type;
    addr_mem_res_busout             : OUT global_addr_type;
    word_mem_res_busout             : OUT bit64_type
    );
  END COMPONENT post_unit;

  COMPONENT output_unit IS
    PORT(
    clock                           : IN std_logic;
    reset                           : IN bit_type;

    ---------------------------------------------------------------------------
    -- engine signals
    ---------------------------------------------------------------------------
    en_cmd_in                       : IN bit_type;
    insn_cmd_in                     : IN insn_type;
    ctl_cmd_in                      : IN bit16_type;
    ctr_cmd_in                      : IN bit32_type;
    base0_cmd_in                    : IN bit32_type;
    base1_cmd_in                    : IN bit32_type;
    spec_cmd_in                     : IN bit32_type;

    block_busout                    : OUT bit_type;

    ---------------------------------------------------------------------------
    -- memory signals
    ---------------------------------------------------------------------------
    en_mem_busout                   : OUT bit_type;
    addr_mem_busout                 : OUT global_addr_type;
    word_in                         : IN bit128_type;

    ---------------------------------------------------------------------------
    -- AXI signals
    ---------------------------------------------------------------------------
    axi_clock                       : IN std_logic;
    -- AW Channel
    awid_axi_regout                 : OUT awid_axi_type;
    awaddr_axi_regout               : OUT addr_axi_type;
    awlen_axi_regout                : OUT bit4_type;
    awsize_axi_regout               : OUT bit3_type;
    awburst_axi_regout              : OUT awburst_axi_type;
    awlock_axi_regout               : OUT alock_axi_type;
    awvalid_axi_regout              : OUT std_logic;
    awready_axi_in                  : IN  std_logic;

    -- W Channel
    wid_axi_regout                  : OUT awid_axi_type;
    wdata_axi_regout                : OUT data_axi_type;
    wstrb_axi_regout                : OUT wstrb_axi_type;
    wlast_axi_regout                : OUT std_logic;
    wvalid_axi_regout               : OUT std_logic;
    wready_axi_in                   : IN  std_logic;

    -- B Channel
    bid_axi_in                      : IN  awid_axi_type;
    bresp_axi_in                    : IN  resp_axi_type;
    bvalid_axi_in                   : IN  std_logic;
    bready_axi_regout               : OUT std_logic
    );
  END COMPONENT output_unit;

  -- model is the following
  --  + engine will signal to cpu by writing $g15
  --  + writing to $g15 with high bits 31/30 does the following
  --      + 01: always generate interrupt, do not copy: interupt condition?
  --      + 11: always generate interrupt, copy next
  --      + 10: do not genarate interrupt, copy next
  --  + the next command is controlled by the next register
  --      + if bit 15 of the register is 1, then based on 
  --        bit 0..14 shadow 0..14 are copied to $g0..14; $g15 is always copied
  --      + bit 15 is cleared after copy
  --      + NOTE: if $g15:31 is 1 when $next:15 is written as 1, then the copy will happen
  --  + the control register is set up to do the following:
  --      + register an interrupt when $g15 says to generate an interrupt [i.e. $g15:30=1]
  --      + register an interrupt when $g15 says to copy next, but next is not available [$g15:31=1 $next:15=0]
  --      + register an interrupt when next is copied [$g15:31=1, $next:15=1]
  --      + there is an additional mask an be used to control which of the two PXIs get the
  --        interrupt
  --      + the fields are:
  --          @ 0-1:  pxi mask
  --          @ 4-7:  interrupt mask
  --          @ 8-11: saved interupt

  ----------------------------------------------------------------
  -- PXI VISIBLE REGISTERS
  ----------------------------------------------------------------
  SIGNAL    globals_q             : array_bit32_type(0 TO 15);
  SUBTYPE   NEXT_G15              IS natural RANGE 31 DOWNTO 31;
  SUBTYPE   INT_G15               IS natural RANGE 30 DOWNTO 30;
  SIGNAL    shadows_q             : array_bit32_type(0 TO 15);
  SUBTYPE   ctl_type              IS std_logic_vector(11 DOWNTO 0);
  SIGNAL    ctl_q                 : ctl_type;
  SUBTYPE   RESETB_CTL            IS natural RANGE 0 DOWNTO 0;
--  SUBTYPE   EN_PXI0_CTL           IS natural RANGE 1 DOWNTO 1;
--  SUBTYPE   EN_PXI1_CTL           IS natural RANGE 2 DOWNTO 2;
  SUBTYPE   EN_STALL_CTL          IS natural RANGE 2 DOWNTO 2;
  SUBTYPE   STALL_CTL             IS natural RANGE 3 DOWNTO 3;
  SUBTYPE   EN_PXI_CTL            IS natural RANGE 3 DOWNTO 0;
  SUBTYPE   EN_COPY_INT_CTL       IS natural RANGE 4 DOWNTO 4;
  SUBTYPE   EN_UNAVAIL_INT_CTL    IS natural RANGE 5 DOWNTO 5;
  SUBTYPE   EN_G15_INT_CTL        IS natural RANGE 6 DOWNTO 6;
  SUBTYPE   EN_ERR_INT_CTL        IS natural RANGE 7 DOWNTO 6;
  SUBTYPE   EN_INT_CTL            IS natural RANGE 7 DOWNTO 4;
  SUBTYPE   COPY_INT_CTL          IS natural RANGE 8 DOWNTO 8;
  SUBTYPE   UNAVAIL_INT_CTL       IS natural RANGE 9 DOWNTO 9;
  SUBTYPE   G15_INT_CTL           IS natural RANGE 10 DOWNTO 10;
  SUBTYPE   ERR_INT_CTL           IS natural RANGE 11 DOWNTO 11;
  SUBTYPE   INT_CTL               IS natural RANGE 11 DOWNTO 8;
  SIGNAL    next_q                : bit16_type;
  SIGNAL    qos_q                 : bit8_type;
  SUBTYPE   COPY_NEXT             IS natural RANGE 15 DOWNTO 15;

  SIGNAL    err_q                 : bit15_type;
  SUBTYPE   IN_ALIGN_ERR          IS natural RANGE 0 DOWNTO 0;
  SUBTYPE   IN_PAGE_ERR           IS natural RANGE 1 DOWNTO 1;
  SUBTYPE   OUT_ALIGN_ERR         IS natural RANGE 2 DOWNTO 2;
  SUBTYPE   BRESP_ERR             IS natural RANGE 5 DOWNTO 4;
  SUBTYPE   RRESP_ERR             IS natural RANGE 7 DOWNTO 6;

  SUBTYPE   COLL_ERR              IS natural RANGE 14 DOWNTO 8;
  SUBTYPE   IN_RES_COLL_ERR       IS natural RANGE 8 DOWNTO 8;
  SUBTYPE   FILT_DATA_COLL_ERR    IS natural RANGE 9 DOWNTO 9;
  SUBTYPE   FILT_PSUM_COLL_ERR    IS natural RANGE 10 DOWNTO 10;
  SUBTYPE   FILT_RES_COLL_ERR     IS natural RANGE 11 DOWNTO 11;
  SUBTYPE   POST_DATA_COLL_ERR    IS natural RANGE 12 DOWNTO 12;
  SUBTYPE   POST_RES_COLL_ERR     IS natural RANGE 13 DOWNTO 13;
  SUBTYPE   OUT_DATA_COLL_ERR     IS natural RANGE 14 DOWNTO 14;


  SIGNAL    in_align_err_i        : bit_type;
  SIGNAL    in_page_err_i         : bit_type;
  SIGNAL    out_align_err_i       : bit_type;
  SIGNAL    collide_err_i         : bit7_type;
  SIGNAL    bresp_err_i           : bit2_type;
  SIGNAL    rresp_err_i           : bit2_type;
  SIGNAL    all_err_i             : bit15_type;

  SIGNAL    stall_i               : bit_type;

  ----------------------------------------------------------------
  -- PXI REQUEST/RESPONSE CONTROLS
  ----------------------------------------------------------------
  SIGNAL    en_pxi_q              : bit_type;
  SIGNAL    we_pxi_q              : bit_type;
  SIGNAL    addr_pxi_q            : addr_pxi_type;
  SIGNAL    word_in_pxi_q         : bit32_type;

  SIGNAL    en_pxi_resp_q         : bit_type;
  SIGNAL    word_pxi_resp_q       : bit32_type;

  SUBTYPE   pxi_sm_type IS std_logic_vector(2 DOWNTO 0);
  CONSTANT  DONE_PXI_SM           : pxi_sm_type := "000";
  CONSTANT  READY_PXI_SM          : pxi_sm_type := "001";
  CONSTANT  SEND_PXI_SM           : pxi_sm_type := "010";
  CONSTANT  READ_PXI_SM           : pxi_sm_type := "100";
  CONSTANT  OUT_PXI_SM            : pxi_sm_type := "101";
  SIGNAL    sm_pxi_q              : pxi_sm_type;

  ----------------------------------------------------------------
  -- PXI REQUEST DATA PATH
  ----------------------------------------------------------------
  SIGNAL    en_mem_pxi_insn_i     : array_bit_type(ENGINE_RANGE);
  SIGNAL    en_mem_pxi_data_i     : array_bit_type(ENGINE_RANGE);
  SIGNAL    we_mem_pxi_i          : bit_type;
  SIGNAL    addr_mem_pxi_i        : addr_pxi_type;
  SIGNAL    word_in_mem_pxi_i     : bit32_type;

  SIGNAL    en_mem_pxi_q1         : bit_type;
  SIGNAL    we_mem_pxi_i1         : bit_type;
  SIGNAL    en_rd_pxi_i1          : std_logic_vector(2*NUM_ENGINES-1 DOWNTO 0);
  SIGNAL    collide_rw_pxi_i1     : std_logic_vector(2*NUM_ENGINES-1 DOWNTO 0);
  SIGNAL    word_rd_mem_pxi_i1    : array_bit32_type(0 TO 2*NUM_ENGINES-1);

  SIGNAL    en_reg_pxi_q1         : bit_type;
  SIGNAL    we_reg_pxi_i1         : bit_type;
  SIGNAL    addr_reg_pxi_i1       : addr_pxi_type;
  SIGNAL    word_in_reg_pxi_i1    : bit32_type;
  SIGNAL    word_rd_reg_pxi_i1    : bit32_type;

  SIGNAL    engine_regs_i         : array2_bit32_type(ENGINE_RANGE, 0 TO 15);
  SIGNAL    pcs_i                 : array_bit32_type(ENGINE_RANGE);

  ----------------------------------------------------------------
  -- ENGINE -> GLOBAL REG WRITE INTERFACE
  ----------------------------------------------------------------
  SIGNAL    block_i               : array_bit_type(ENGINE_RANGE);
  SIGNAL    en_global_i           : array_bit_type(ENGINE_RANGE);
  SIGNAL    rt_global_i           : array_bit4_type(ENGINE_RANGE);
  SIGNAL    word_global_i         : array_bit32_type(ENGINE_RANGE);
  SIGNAL    input_block_i         : bit_type;
  SIGNAL    output_block_i        : bit_type;

  ----------------------------------------------------------------
  -- ENGINE -> SPECIALIZED UNIT (INPUT/FILTER/POST/OUTPUT) INTERFACE
  ----------------------------------------------------------------
  SIGNAL    en_cmd_i              : array_bit_type(ENGINE_RANGE);
  SIGNAL    insn_cmd_i            : array_bit32_type(ENGINE_RANGE);
  SIGNAL    ctl_cmd_i             : array_bit16_type(ENGINE_RANGE);
  SIGNAL    ctr_cmd_i             : array_bit32_type(ENGINE_RANGE);
  SIGNAL    base0_cmd_i           : array_bit32_type(ENGINE_RANGE);
  SIGNAL    base1_cmd_i           : array_bit32_type(ENGINE_RANGE);
  SIGNAL    base2_cmd_i           : array_bit32_type(ENGINE_RANGE);
  SIGNAL    spec_cmd_i            : array_bit32_type(ENGINE_RANGE);
  SIGNAL    align_err_i           : array_bit_type(ENGINE_RANGE);
  SIGNAL    page_err_i            : array_bit_type(ENGINE_RANGE);

  ----------------------------------------------------------------
  -- SPECIALIZED UNIT <-> GLOBAL MEMORY INTERFACES
  ----------------------------------------------------------------
  SIGNAL    en_input_global_i           : bit_type;
  SIGNAL    slice_input_global_i        : slice_mask_type;
  SIGNal    addr_input_global_i         : slice_addr_type;
  SIGNAL    word_input_global_i         : bit128_type;

  SIGNAL    en_data_filter_global_i     : bit_type;
  SIGNAL    width_data_filter_global_i  : bit2_type;
  SIGNAL    slice_data_filter_global_i  : slice_mask_type;
  SIGNAL    addr_data_filter_global_i   : slice_addr_type;
  SIGNAL    word_data_global_filter_i   : array_bit144_type(SLICE_RANGE);

  SIGNAL    en_psum_filter_global_i     : bit_type;
  SIGNAL    width_psum_filter_global_i  : bit_type;
  SIGNAL    slice_psum_filter_global_i  : slice_mask_type;
  SIGNAL    addr_psum_filter_global_i   : slice_addr_type;
  SIGNAL    word_psum_global_filter_i   : array_bit64_type(SLICE_RANGE);

  SIGNAL    en_res_filter_global_i      : bit_type;
  SIGNAL    width_res_filter_global_i   : bit_type;
  SIGNAL    slice_res_filter_global_i   : slice_mask_type;
  SIGNAL    addr_res_filter_global_i    : slice_addr_type;
  SIGNAL    word_res_filter_global_i    : array_bit64_type(SLICE_RANGE);

  SIGNAL    en_data_post_global_i       : bit_type;
  SIGNAL    addr_data_post_global_i     : global_addr_type;
  SIGNAL    word_data_global_post_i     : bit128_type;
  SIGNAL    word_data_global_post_i2    : bit128_type;

  SIGNAL    en_res_post_global_i        : bit_type;
  SIGNAL    wr4_res_post_global_i       : bit_type;
  SIGNAL    addr_res_post_global_i      : global_addr_type;
  SIGNAL    word_res_post_global_i      : bit64_type;

  SIGNAL    en_output_global_i          : bit_type;
  SIGNAL    addr_output_global_i        : global_addr_type;
  SIGNAL    word_global_output_i        : bit128_type;
  SIGNAL    word_global_output_i2       : bit128_type;

  ----------------------------------------------------------------
  -- GENERATED RESET/CLOCK
  ----------------------------------------------------------------
  SIGNAL    resetx                      : bit_type;
  SIGNAL    axi_clock_i                 : std_logic;
  SIGNAL    pxi_clock_i                 : std_logic;
  SIGNAL    axi_reset_i                 : bit_type;
  SIGNAL    pxi_reset_i                 : bit_type;
  SIGNAL    axi_early_i                 : bit_type;
  SIGNAL    pxi_early_i                 : bit_type;

  ----------------------------------------------------------------
  -- AXI input signals blanked with reset
  ----------------------------------------------------------------
  SIGNAL    arready_axi_ri              : std_logic;
  SIGNAL    awready_axi_ri              : std_logic;
  SIGNAL    wready_axi_ri               : std_logic;
  SIGNAL    rvalid_axi_ri               : std_logic;
  SIGNAL    bvalid_axi_ri               : std_logic;

  SIGNAL    awid_axi_i                  : awid_axi_type;
  SIGNAL    awaddr_axi_i                : addr_axi_type;
  SIGNAL    awlen_axi_i                 : bit4_type;
  SIGNAL    awsize_axi_i                : bit3_type;
  SIGNAL    awburst_axi_i               : awburst_axi_type;
  SIGNAL    awlock_axi_i                : alock_axi_type;
  SIGNAL    awvalid_axi_i               : std_logic;
  SIGNAL    wid_axi_i                   : awid_axi_type;
  SIGNAL    wdata_axi_i                 : data_axi_type;
  SIGNAL    wstrb_axi_i                 : wstrb_axi_type;
  SIGNAL    wlast_axi_i                 : std_logic;
  SIGNAL    wvalid_axi_i                : std_logic;
  SIGNAL    bready_axi_i                : std_logic;

  SIGNAL   arid_axi_i                   : arid_axi_type;  -- currently defined as 1 bit
  SIGNAL   araddr_axi_i                 : addr_axi_type;
  SIGNAL   arlen_axi_i                  : bit4_type;      -- number of bursts
  SIGNAL   arsize_axi_i                 : bit3_type;      -- size of each burst
  SIGNAL   arburst_axi_i                : bit2_type;      -- type of burst,  will be INCR=01
  SIGNAL   arlock_axi_i                 : alock_axi_type; -- locking, will be NORMAL=00
  SIGNAL   arvalid_axi_i                : std_logic;

  SIGNAL  interrupt_pxi_d               : bit_type;
BEGIN

  resetx  <= "1" WHEN reset = "1" ELSE
             "1" WHEN ctl_q(RESETB_CTL) = "0" ELSE
             "0";

  a_clock_gen: gsnn_clock_gen PORT MAP(clock, reset, axi_clock_i, axi_reset_i, axi_early_i, pxi_clock_i, pxi_reset_i, pxi_early_i );
  axi_clock <= axi_clock_i;
  axi_reset <= axi_reset_i;
  pxi_clock <= pxi_clock_i;
  pxi_reset <= pxi_reset_i;

  pxi_block: IF true GENERATE
    SIGNAL    en_pxi_d              : bit_type;
    SIGNAL    sm_pxi_d              : pxi_sm_type;
    SIGNAL    do_req_bi             : bit_type;
    SIGNAL    do_send_bi            : bit_type;
    SIGNAL    is_global_bi          : bit_type;
    SIGNAL    is_data_bi            : bit_type;
    SIGNAL    engine_bi             : bit2_type;
    SIGNAL    en_reg_pxi_bi         : bit_type;
    SIGNAL    en_mem_pxi_bi         : bit_type;
    SIGNAL    collision_bi1         : bit_type;
    SIGNAL    en_rd_bi1             : bit_type;
    SIGNAL    word_rd_mem_bi1       : bit32_type;
    SIGNAL    word_resp_bi1         : bit32_type;
    SIGNAL    en_resp_bi            : bit_type;
  BEGIN

    -- we will latch the data from the pxi when there is nothing in the pxi registers and pxi_early is true
    do_req_bi       <=  "1"  WHEN en_pxi_q = "0" AND en_pxi_req_in = "1" AND pxi_reset_i = "0" AND pxi_early_i = "1" ELSE "0";
    en_pxi_d            <=  "0"             WHEN reset = "1" ELSE
                            "0"             WHEN pxi_reset_i = "1" ELSE
                            en_pxi_q        WHEN pxi_early_i = "0" ELSE
                            en_pxi_req_in   WHEN en_pxi_q = "0" ELSE
                            "0"             WHEN en_pxi_q = "1" AND sm_pxi_d = DONE_PXI_SM ELSE
                            "0"             WHEN en_pxi_q = "1" AND sm_pxi_q = DONE_PXI_SM ELSE
                            en_pxi_q;
    en_pxi_q            <=  en_pxi_d        AFTER 10 ps WHEN rising_edge(clock);
    we_pxi_q            <=  we_pxi_req_in   AFTER 10 ps WHEN rising_edge(clock) AND do_req_bi = "1";
    addr_pxi_q          <=  addr_pxi_req_in AFTER 10 ps WHEN rising_edge(clock) AND do_req_bi = "1";
    word_in_pxi_q       <=  word_pxi_req_in AFTER 10 ps WHEN rising_edge(clock) AND do_req_bi = "1";
    pull_pxi_req_busout <=  NOT en_pxi_q;

    sm_pxi_d            <=  DONE_PXI_SM     WHEN reset = "1" ELSE
                            READY_PXI_SM    WHEN do_req_bi = "1" ELSE
                            SEND_PXI_SM     WHEN sm_pxi_q = READY_PXI_SM ELSE
                            SEND_PXI_SM     WHEN sm_pxi_q = SEND_PXI_SM AND collision_bi1 = "1" ELSE
                            DONE_PXI_SM     WHEN sm_pxi_q = SEND_PXI_SM AND collision_bi1 = "0" AND we_pxi_q = "1" ELSE
                            READ_PXI_SM     WHEN sm_pxi_q = SEND_PXI_SM AND collision_bi1 = "0" AND we_pxi_q = "0" ELSE
                            OUT_PXI_SM      WHEN sm_pxi_q = READ_PXI_SM AND pxi_early_i = "1" ELSE
                            DONE_PXI_SM     WHEN sm_pxi_q = OUT_PXI_SM AND avail_pxi_resp_in = "1" AND pxi_early_i = "1" ELSE
                            sm_pxi_q;
    sm_pxi_q            <= sm_pxi_d AFTER 10 ps WHEN rising_edge(clock);

    do_send_bi           <=  "1" WHEN sm_pxi_q = READY_PXI_SM ELSE
                            "1" WHEN sm_pxi_q = SEND_PXI_SM AND collision_bi1 = "1" ELSE
                            "0";

    is_global_bi        <=  addr_pxi_q(GLOBAL_SEL_ADDR_PXI);
    en_reg_pxi_bi       <=  "0" WHEN reset = "1" ELSE
                            "1" WHEN do_send_bi = "1" AND is_global_bi = "1" ELSE
                            "0";
    en_reg_pxi_q1       <=  en_reg_pxi_bi   AFTER 10 ps WHEN rising_edge(clock);

    we_reg_pxi_i1       <=  we_pxi_q; -- ideally we should latch them, but they will be held constant
    addr_reg_pxi_i1     <=  addr_pxi_q;
    word_in_reg_pxi_i1  <=  word_in_pxi_q;

    en_mem_pxi_bi       <=  "0" WHEN reset = "1" ELSE
                            "1" WHEN do_send_bi = "1" AND is_global_bi = "0" ELSE
                            "0";
    we_mem_pxi_i        <=  we_pxi_q;
    addr_mem_pxi_i      <=  addr_pxi_q;
    word_in_mem_pxi_i   <=  word_in_pxi_q;

    en_mem_pxi_q1       <=  en_mem_pxi_bi AFTER 10 ps WHEN rising_edge(clock);
    we_mem_pxi_i1       <=  we_pxi_q; -- should be latched, but this value is guaranteed to be constant

    engine_bi           <=  addr_pxi_q(ENGINE_SEL_ADDR_PXI);
    is_data_bi          <=  addr_pxi_q(DATA_SEL_ADDR_PXI);
    en_mem_gen: FOR e IN ENGINE_RANGE GENERATE
      en_mem_pxi_insn_i(e)  <=  "1" WHEN en_mem_pxi_bi = "1" AND is_data_bi = "0" AND to_integer(unsigned(engine_bi)) = e ELSE
                                "0";
      en_mem_pxi_data_i(e)  <=  "1" WHEN en_mem_pxi_bi = "1" AND is_data_bi = "1" AND to_integer(unsigned(engine_bi)) = e ELSE
                                "0";
    END GENERATE en_mem_gen;

    collision_bi1       <=  "1"  WHEN collide_rw_pxi_i1 /= (collide_rw_pxi_i1'range => '0') ELSE "0";
    en_rd_bi1           <=  "1" WHEN en_mem_pxi_q1 = "1" AND we_mem_pxi_i1 = "0" AND collision_bi1 = "0" ELSE
                            "1" WHEN en_reg_pxi_q1 = "1" AND we_reg_pxi_i1 = "0" ELSE
                            "0";

    WITH en_rd_pxi_i1 SELECT
      word_rd_mem_bi1   <=  word_rd_mem_pxi_i1(0)   WHEN x"01",
                            word_rd_mem_pxi_i1(1)   WHEN x"02",
                            word_rd_mem_pxi_i1(2)   WHEN x"04",
                            word_rd_mem_pxi_i1(3)   WHEN x"08",
                            word_rd_mem_pxi_i1(4)   WHEN x"10",
                            word_rd_mem_pxi_i1(5)   WHEN x"20",
                            word_rd_mem_pxi_i1(6)   WHEN x"40",
                            word_rd_mem_pxi_i1(7)   WHEN x"80",
                            (OTHERS=>'X')       WHEN OTHERS;

    word_resp_bi1       <= word_rd_mem_bi1 WHEN en_mem_pxi_q1 = "1" ELSE word_rd_reg_pxi_i1;
    word_pxi_resp_q     <= word_resp_bi1 AFTER 10 ps WHEN rising_edge(clock) AND en_rd_bi1 = "1";

    en_resp_bi          <=  "0" WHEN pxi_reset_i = "1" ELSE
                            "1"  WHEN sm_pxi_q = READ_PXI_SM ELSE
                            "0"  WHEN sm_pxi_q = OUT_PXI_SM AND avail_pxi_resp_in = "1" ELSE
                            en_pxi_resp_q;
    en_pxi_resp_q       <= en_resp_bi AFTER 10 ps WHEN rising_edge(clock) AND pxi_early_i = "1";

    en_pxi_resp_regout <= en_pxi_resp_q;
    addr_pxi_resp_regout <= addr_pxi_q;
    word_pxi_resp_regout <= word_pxi_resp_q;

  END GENERATE pxi_block;

  reg_block: IF true GENERATE
    SIGNAL  set_pxi_bi1       : bit2_type;
    SIGNAL  dbg_pxi_bi1       : bit2_type;
    SIGNAL  reg_pxi_bi1       : bit4_type;
    SIGNAL  en_global_pxi_bi1 : bit_type;
    SIGNAL  en_shadow_pxi_bi1 : bit_type;
    SIGNAL  en_copy_bi        : bit_type;

    SIGNAL  en_int_bi         : bit_type;
  BEGIN

    en_copy_bi          <= "1" WHEN next_q(COPY_NEXT) = "1" AND globals_q(15)(NEXT_G15) = "1"  AND resetx = "0" ELSE "0";

    set_pxi_bi1         <= addr_reg_pxi_i1( SET_SEL_ADDR_PXI );
    reg_pxi_bi1         <= addr_reg_pxi_i1( REG_SEL_ADDR_PXI );
    dbg_pxi_bi1         <= addr_reg_pxi_i1( DBG_SEL_ADDR_PXI );

    -- GLOBAL REGISTERS
    en_global_pxi_bi1   <= "1" WHEN set_pxi_bi1 = "00" AND en_reg_pxi_q1 = "1" AND we_reg_pxi_i1 = "1" ELSE "0";
    global_gen: FOR reg IN globals_q'range GENERATE
      CONSTANT  GLOBAL_REG    : bit4_type := std_logic_vector(to_unsigned(reg, 4));
      SIGNAL  en_pxi_gbi1     : bit_type;
      SIGNAL  en_copy_gbi     : bit_type;
      SIGNAL  en_engine_gbi   : bit4_type;
      SIGNAL  en_gbi          : bit_type;
      SIGNAL  word_gbi        : bit32_type;
    BEGIN
      en_pxi_gbi1       <=  "1" WHEN en_global_pxi_bi1 = "1" AND GLOBAL_REG = reg_pxi_bi1 ELSE "0";
      en_copy_gbi       <=  "1" WHEN en_copy_bi = "1" AND next_q(reg) = '1' ELSE "0";
      engine_gen: FOR e IN ENGINE_RANGE GENERATE
        en_engine_gbi(e) <= '1' WHEN en_global_i(e) = "1" AND GLOBAL_REG = rt_global_i(e) ELSE '0';
      END GENERATE engine_gen;

      en_gbi            <=  "1" WHEN reset = "1" OR en_pxi_gbi1 = "1" OR en_engine_gbi /= "0000" OR en_copy_gbi = "1" ELSE "0";
      word_gbi          <=  (OTHERS => '0')     WHEN reset = "1" ELSE
                            shadows_q(reg)      WHEN en_copy_bi = "1" ELSE
                            word_global_i(0)    WHEN en_engine_gbi(0) = '1' ELSE
                            word_global_i(1)    WHEN en_engine_gbi(1) = '1' ELSE
                            word_global_i(2)    WHEN en_engine_gbi(2) = '1' ELSE
                            word_global_i(3)    WHEN en_engine_gbi(3) = '1' ELSE
                            word_in_reg_pxi_i1  WHEN en_pxi_gbi1 = "1" ELSE
                            (OTHERS=>'X');
      globals_q(reg)    <=  word_gbi AFTER 10 ps WHEN rising_edge(clock) AND en_gbi = "1";
    END GENERATE global_gen;

    -- SHADOW REGISTERS
    en_shadow_pxi_bi1   <= "1" WHEN set_pxi_bi1 = "01" AND en_reg_pxi_q1 = "1" AND we_reg_pxi_i1 = "1" ELSE "0";
    shadow_gen: FOR reg IN shadows_q'range GENERATE
      CONSTANT  SHADOW_REG    : bit4_type := std_logic_vector(to_unsigned(reg, 4));
      SIGNAL    en_pxi_gbi1   : bit_type;
    BEGIN
      en_pxi_gbi1       <= "1" WHEN en_shadow_pxi_bi1 = "1" AND SHADOW_REG = reg_pxi_bi1 ELSE "0";
      shadows_q(reg)    <= word_in_reg_pxi_i1 AFTER 10 ps WHEN rising_edge(clock) AND en_pxi_gbi1 = "1";
    END GENERATE shadow_gen;

    rresp_err_i   <= rresp_axi_in WHEN rvalid_axi_in = '1' AND axi_early_i = "1" ELSE "00";
    bresp_err_i   <= bresp_axi_in WHEN bvalid_axi_in = '1' AND axi_early_i = "1" ELSE "00";
    all_err_i     <=  collide_err_i & rresp_err_i & bresp_err_i & '0' & align_err_i(OUTPUT_ENGINE_N) & page_err_i(INPUT_ENGINE_N) & align_err_i(INPUT_ENGINE_N);

    -- CONTROL
    ctl_block: IF true GENERATE
      SIGNAL  en_ctl_pxi_bbi1   : bit_type;
      SIGNAL  ctl_pxi_bbi1      : ctl_type;
      SIGNAL  ctl_in_bbi        : ctl_type;
      SIGNAL  int_ctl_bbi       : bit_type;
      SIGNAL  err_ctl_bbi       : bit_type;
      SIGNAL  unavail_ctl_bbi   : bit_type;
      SIGNAL  next_ctl_bbi      : bit_type;
      SIGNAL  ctl_bbi           : ctl_type;
      SIGNAL  ctl_d             : ctl_type;
      SIGNAL  err_bbi           : bit_type;
    BEGIN
      en_ctl_pxi_bbi1     <=  "1" WHEN set_pxi_bi1 = "11" AND en_reg_pxi_q1 = "1" AND we_reg_pxi_i1 = "1"
                                    AND ( reg_pxi_bi1 = x"0" OR reg_pxi_bi1 = x"1" OR reg_pxi_bi1 = x"2"  ) ELSE
                              "0";
      WITH reg_pxi_bi1 SELECT
        ctl_pxi_bbi1      <=  word_in_reg_pxi_i1(ctl_q'range)                   WHEN x"0",
                              word_in_reg_pxi_i1(ctl_q'range) OR ctl_q          WHEN x"1", 
                              (NOT word_in_reg_pxi_i1(ctl_q'range)) AND ctl_q   WHEN x"2", 
                              (OTHERS => 'X')                                   WHEN OTHERS;

      ctl_in_bbi          <= ctl_pxi_bbi1 WHEN en_ctl_pxi_bbi1 = "1" ELSE ctl_q;

      int_ctl_bbi         <=  globals_q(15)(INT_G15);
      unavail_ctl_bbi     <=  globals_q(15)(NEXT_G15) AND NOT next_q(COPY_NEXT);
      next_ctl_bbi        <=  globals_q(15)(NEXT_G15) AND next_q(COPY_NEXT);
      err_bbi             <=  "0" WHEN all_err_i = (all_err_i'range => '0') ELSE "1";

      ctl_bbi(INT_CTL)    <=  ctl_in_bbi(INT_CTL) OR (err_bbi&int_ctl_bbi&unavail_ctl_bbi&next_ctl_bbi);
      ctl_bbi(EN_INT_CTL) <=  ctl_in_bbi(EN_INT_CTL);
      ctl_bbi(EN_PXI_CTL) <=  ( ctl_in_bbi(STALL_CTL) OR err_bbi) & ctl_in_bbi(EN_STALL_CTL) & "0" & ctl_in_bbi(RESETB_CTL);

      ctl_d               <=  (OTHERS=>'0') WHEN reset = "1" ELSE ctl_bbi;
      ctl_q <= ctl_d AFTER 10 ps WHEN rising_edge(clock);
    END GENERATE ctl_block;

    -- NEXT
    next_block: IF true GENERATE
      SIGNAL  en_next_pxi_bbi1  : bit_type;
      SIGNAL  next_bbi          : bit16_type;
      SIGNAL  next_d            : bit16_type;
    BEGIN
      en_next_pxi_bbi1 <= "1" WHEN set_pxi_bi1 = "11" AND en_reg_pxi_q1 = "1" AND we_reg_pxi_i1 = "1" AND reg_pxi_bi1 = x"4" ELSE "0";
      next_bbi(14 DOWNTO 0) <= 
                              word_in_reg_pxi_i1(14 DOWNTO 0) WHEN en_next_pxi_bbi1 = "1" ELSE
                              next_q(14 DOWNTO 0);
      next_bbi(15)        <=  word_in_reg_pxi_i1(15)         WHEN en_next_pxi_bbi1 = "1" ELSE
                              '0'                             WHEN en_copy_bi = "1" ELSE
                              next_q(15);

      next_d              <=  (OTHERS=> '0') WHEN reset = "1" ELSE next_bbi;
      next_q <= next_d AFTER 10 ps WHEN rising_edge(clock);
    END GENERATE next_block;

    -- QOS
    qos_block: IF true GENERATE
      SIGNAL  en_qos_pxi_bbi1  : bit_type;
      SIGNAL  qos_d            : bit8_type;
    BEGIN
      en_qos_pxi_bbi1 <= "1" WHEN set_pxi_bi1 = "11" AND en_reg_pxi_q1 = "1" AND we_reg_pxi_i1 = "1" AND reg_pxi_bi1 = x"8" ELSE "0";
      qos_d              <=  (OTHERS=> '0') WHEN reset = "1" ELSE
                             word_in_reg_pxi_i1(qos_d'range) WHEN en_qos_pxi_bbi1  = "1" ELSE
                             qos_q;
      qos_q <= qos_d AFTER 10 ps WHEN rising_edge(clock);

      arqos_axi_busout <= qos_q(3 DOWNTO 0);
      awqos_axi_busout <= qos_q(7 DOWNTO 4);
    END GENERATE qos_block;

    -- ERR
    err_block: IF true GENERATE
      SIGNAL  en_err_pxi_bbi1  : bit_type;
      SIGNAL  err_d            : bit15_type;
    BEGIN
      en_err_pxi_bbi1 <= "1" WHEN set_pxi_bi1 = "11" AND en_reg_pxi_q1 = "1" AND we_reg_pxi_i1 = "1" AND reg_pxi_bi1 = x"c" ELSE "0";
      err_d              <= (OTHERS => '0') WHEN reset = "1" ELSE
                            word_in_reg_pxi_i1(err_d'range) OR all_err_i WHEN en_err_pxi_bbi1 = "1" ELSE
                            err_q OR all_err_i;
      err_q              <= err_d AFTER 10 ps WHEN rising_edge(clock);
    END GENERATE err_block;

    -- INTERRUPT
    interrupt_block: IF true GENERATE
      SIGNAL  en_int_bi           : bit_type;
    BEGIN
      en_int_bi <=  "0"   WHEN reset = "1" ELSE
                    "1"   WHEN (ctl_q(EN_INT_CTL) AND ctl_q(INT_CTL)) /= "0000" ELSE
                    "0";
      interrupt_pxi_d <= en_int_bi;
      interrupt_pxi_regout <= interrupt_pxi_d AFTER 10 ps WHEN rising_edge(clock);

    END GENERATE interrupt_block;

    -- READ REGISTER
    word_rd_reg_pxi_i1  <=  globals_q(to_integer(unsigned(reg_pxi_bi1)))  WHEN set_pxi_bi1 = "00" AND dbg_pxi_bi1 = "00" ELSE
                            shadows_q(to_integer(unsigned(reg_pxi_bi1)))  WHEN set_pxi_bi1 = "01" AND dbg_pxi_bi1 = "00" ELSE
                            (31 DOWNTO ctl_q'high+1 => '0') & ctl_q       WHEN set_pxi_bi1 = "11" AND reg_pxi_bi1 = x"0" AND dbg_pxi_bi1 = "00" ELSE
                            (31 DOWNTO next_q'high+1 => '0') & next_q     WHEN set_pxi_bi1 = "11" AND reg_pxi_bi1 = x"4" AND dbg_pxi_bi1 = "00" ELSE
                            (31 DOWNTO qos_q'high+1 => '0') & qos_q       WHEN set_pxi_bi1 = "11" AND reg_pxi_bi1 = x"8" AND dbg_pxi_bi1 = "00" ELSE
                            (31 DOWNTO err_q'high+1 => '0') & err_q       WHEN set_pxi_bi1 = "11" AND reg_pxi_bi1 = x"c" AND dbg_pxi_bi1 = "00" ELSE

                            engine_regs_i(to_integer(unsigned(set_pxi_bi1)), to_integer(unsigned(reg_pxi_bi1)))
                                                                          WHEN dbg_pxi_bi1 = "10" ELSE
                            pcs_i(to_integer(unsigned(set_pxi_bi1)) )     WHEN dbg_pxi_bi1 = "11" ELSE
                            (OTHERS => 'X');

  END GENERATE reg_block;

  stall_i                   <= ctl_q(STALL_CTL) AND ctl_q(EN_STALL_CTL);
  block_i(INPUT_ENGINE_N)   <= stall_i OR input_block_i;
  block_i(FILTER_ENGINE_N)  <= stall_i;
  block_i(POST_ENGINE_N)    <= stall_i;
  block_i(OUTPUT_ENGINE_N)  <= stall_i OR output_block_i;
  engine_top_gen: FOR engine IN ENGINE_RANGE GENERATE
    CONSTANT  engine_id             : engine_id_type := std_logic_vector( to_unsigned( engine, 2 ) );
  BEGIN
    a_engine_top: engine_unit_top GENERIC MAP( engine_id => engine_id ) PORT MAP(
                      clock                     =>  clock,
                      reset                     =>  resetx,
                      reset_mem                 =>  reset,

                      slp_in                    =>  slp_in,
                      sd_in                     =>  sd_in,

                      block_in                  =>  block_i( engine ) ,

                      global_0_in               =>  globals_q( 0 ),
                      global_1_in               =>  globals_q( 1 ),
                      global_2_in               =>  globals_q( 2 ),
                      global_3_in               =>  globals_q( 3 ),
                      global_4_in               =>  globals_q( 4 ),
                      global_5_in               =>  globals_q( 5 ),
                      global_6_in               =>  globals_q( 6 ),
                      global_7_in               =>  globals_q( 7 ),
                      global_8_in               =>  globals_q( 8 ),
                      global_9_in               =>  globals_q( 9 ),
                      global_a_in               =>  globals_q( 10 ),
                      global_b_in               =>  globals_q( 11 ),
                      global_c_in               =>  globals_q( 12 ),
                      global_d_in               =>  globals_q( 13 ),
                      global_e_in               =>  globals_q( 14 ),
                      global_f_in               =>  globals_q( 15 ),

                      en_global_busout          =>  en_global_i( engine ),
                      rt_global_busout          =>  rt_global_i( engine ),
                      word_global_busout        =>  word_global_i( engine ),

                      en_cmd_busout             =>  en_cmd_i( engine ),
                      insn_cmd_busout           =>  insn_cmd_i( engine ),
                      ctl_cmd_busout            =>  ctl_cmd_i( engine ),
                      ctr_cmd_busout            =>  ctr_cmd_i( engine ),
                      base0_cmd_busout          =>  base0_cmd_i( engine ),
                      base1_cmd_busout          =>  base1_cmd_i( engine ),
                      base2_cmd_busout          =>  base2_cmd_i( engine ),
                      spec_cmd_busout           =>  spec_cmd_i( engine ),

                      err_align_busout          =>  align_err_i( engine ),
                      err_page_busout           =>  page_err_i( engine ),
                      
                      r0_busout                 =>  engine_regs_i(engine, 0),
                      r1_busout                 =>  engine_regs_i(engine, 1),
                      r2_busout                 =>  engine_regs_i(engine, 2),
                      r3_busout                 =>  engine_regs_i(engine, 3),
                      r4_busout                 =>  engine_regs_i(engine, 4),
                      r5_busout                 =>  engine_regs_i(engine, 5),
                      r6_busout                 =>  engine_regs_i(engine, 6),
                      r7_busout                 =>  engine_regs_i(engine, 7),
                      r8_busout                 =>  engine_regs_i(engine, 8),
                      r9_busout                 =>  engine_regs_i(engine, 9),
                      r10_busout                =>  engine_regs_i(engine, 10),
                      r11_busout                =>  engine_regs_i(engine, 11),
                      r12_busout                =>  engine_regs_i(engine, 12),
                      r13_busout                =>  engine_regs_i(engine, 13),
                      r14_busout                =>  engine_regs_i(engine, 14),
                      r15_busout                =>  engine_regs_i(engine, 15),
                      pc_busout                 =>  pcs_i(engine),

                      en_mem_insn_pxi_in        => en_mem_pxi_insn_i( engine ),
                      we_mem_insn_pxi_in        => we_mem_pxi_i,
                      addr_mem_insn_pxi_in      => addr_mem_pxi_i,
                      word_mem_insn_pxi_in      => word_in_mem_pxi_i,

                      en_insn_pxi_busout(0)     => en_rd_pxi_i1( 2*engine+0 ),
                      word_insn_pxi_busout      => word_rd_mem_pxi_i1( 2*engine+0 ),
                      collide_insn_pxi_busout(0)   => collide_rw_pxi_i1( 2*engine+0 ),

                      en_mem_data_pxi_in        => en_mem_pxi_data_i( engine ),
                      we_mem_data_pxi_in        => we_mem_pxi_i,
                      addr_mem_data_pxi_in      => addr_mem_pxi_i,
                      word_mem_data_pxi_in      => word_in_mem_pxi_i,

                      en_data_pxi_busout(0)     => en_rd_pxi_i1( 2*engine+1 ),
                      word_data_pxi_busout      => word_rd_mem_pxi_i1( 2*engine+1 ),
                      collide_data_pxi_busout(0)   => collide_rw_pxi_i1( 2*engine+1 )
                      );

  END GENERATE engine_top_gen;

  arready_axi_ri  <= arready_axi_in AND NOT axi_reset_i(0);
  rvalid_axi_ri  <= rvalid_axi_in AND NOT axi_reset_i(0);
  a_input_unit: input_unit PORT MAP(
                      clock                 =>  clock,
                      reset                 =>  resetx,
                      
                      en_cmd_in             =>  en_cmd_i( INPUT_ENGINE_N ),
                      insn_cmd_in           =>  insn_cmd_i( INPUT_ENGINE_N ),
                      ctl_cmd_in            =>  ctl_cmd_i( INPUT_ENGINE_N ),
                      ctr_cmd_in            =>  ctr_cmd_i( INPUT_ENGINE_N ),
                      base0_cmd_in          =>  base0_cmd_i( INPUT_ENGINE_N ),
                      base1_cmd_in          =>  base1_cmd_i( INPUT_ENGINE_N ),
                      spec_cmd_in           =>  spec_cmd_i( INPUT_ENGINE_N ),

                      block_busout          =>  input_block_i,

                      en_mem_busout         =>  en_input_global_i,
                      slice_mem_busout      =>  slice_input_global_i,
                      addr_mem_busout       =>  addr_input_global_i,
                      word_mem_busout       =>  word_input_global_i,


                      axi_clock             =>  axi_clock_i,

                      arid_axi_busout       =>  arid_axi_i,
                      araddr_axi_busout     =>  araddr_axi_i,
                      arlen_axi_busout      =>  arlen_axi_i,
                      arsize_axi_busout     =>  arsize_axi_i,
                      arburst_axi_busout    =>  arburst_axi_i,
                      arlock_axi_busout     =>  arlock_axi_i,
                      arvalid_axi_busout    =>  arvalid_axi_i,
                      arready_axi_in        =>  arready_axi_ri,

                      rid_axi_in            =>  rid_axi_in,
                      rdata_axi_in          =>  rdata_axi_in,
                      rresp_axi_in          =>  rresp_axi_in,
                      rlast_axi_in          =>  rlast_axi_in,
                      rvalid_axi_in         =>  rvalid_axi_ri,
                      rready_axi_busout     =>  rready_axi_busout
                      );

  arid_axi_busout       <=  arid_axi_i;
  araddr_axi_busout     <=  araddr_axi_i;
  arlen_axi_busout      <=  arlen_axi_i;
  arsize_axi_busout     <=  arsize_axi_i;
  arburst_axi_busout    <=  arburst_axi_i;
  arlock_axi_busout     <=  arlock_axi_i;
  arvalid_axi_busout    <=  arvalid_axi_i;

  a_filter: filter_unit PORT MAP(
                      clock                 =>  clock,
                      reset                 =>  resetx,
                      
                      en_cmd_in             =>  en_cmd_i( FILTER_ENGINE_N ),
                      insn_cmd_in           =>  insn_cmd_i( FILTER_ENGINE_N ),
                      ctl_cmd_in            =>  ctl_cmd_i( FILTER_ENGINE_N ),
                      ctr_cmd_in            =>  ctr_cmd_i( FILTER_ENGINE_N ),
                      base0_cmd_in          =>  base0_cmd_i( FILTER_ENGINE_N ),
                      base1_cmd_in          =>  base1_cmd_i( FILTER_ENGINE_N ),
                      base2_cmd_in          =>  base2_cmd_i( FILTER_ENGINE_N ),
                      spec_cmd_in           =>  spec_cmd_i( FILTER_ENGINE_N ),

                      en_mem_data_busout    =>  en_data_filter_global_i,
                      width_mem_data_busout =>  width_data_filter_global_i,
                      slice_mem_data_busout =>  slice_data_filter_global_i,
                      addr_mem_data_busout  =>  addr_data_filter_global_i,
                      word0_data_in         =>  word_data_global_filter_i(0),
                      word1_data_in         =>  word_data_global_filter_i(1),
                      word2_data_in         =>  word_data_global_filter_i(2),
                      word3_data_in         =>  word_data_global_filter_i(3),
                      word4_data_in         =>  word_data_global_filter_i(4),
                      word5_data_in         =>  word_data_global_filter_i(5),
                      word6_data_in         =>  word_data_global_filter_i(6),
                      word7_data_in         =>  word_data_global_filter_i(7),

                      en_mem_psum_busout    =>  en_psum_filter_global_i,
                      width_mem_psum_busout =>  width_psum_filter_global_i,
                      slice_mem_psum_busout =>  slice_psum_filter_global_i,
                      addr_mem_psum_busout  =>  addr_psum_filter_global_i,

                      word0_psum_in         =>  word_psum_global_filter_i(0),
                      word1_psum_in         =>  word_psum_global_filter_i(1),
                      word2_psum_in         =>  word_psum_global_filter_i(2),
                      word3_psum_in         =>  word_psum_global_filter_i(3),
                      word4_psum_in         =>  word_psum_global_filter_i(4),
                      word5_psum_in         =>  word_psum_global_filter_i(5),
                      word6_psum_in         =>  word_psum_global_filter_i(6),
                      word7_psum_in         =>  word_psum_global_filter_i(7),

                      en_mem_res_busout     =>  en_res_filter_global_i,
                      width_mem_res_busout  =>  width_res_filter_global_i,
                      slice_mem_res_busout  =>  slice_res_filter_global_i,
                      addr_mem_res_busout   =>  addr_res_filter_global_i,
                      word0_mem_res_busout  =>  word_res_filter_global_i(0),
                      word1_mem_res_busout  =>  word_res_filter_global_i(1),
                      word2_mem_res_busout  =>  word_res_filter_global_i(2),
                      word3_mem_res_busout  =>  word_res_filter_global_i(3),
                      word4_mem_res_busout  =>  word_res_filter_global_i(4),
                      word5_mem_res_busout  =>  word_res_filter_global_i(5),
                      word6_mem_res_busout  =>  word_res_filter_global_i(6),
                      word7_mem_res_busout  =>  word_res_filter_global_i(7)
                      );

  word_data_global_post_i2 <=  word_data_global_post_i AFTER 10 ps WHEN rising_edge(clock);
  a_post: post_unit PORT MAP(
                      clock                 =>  clock,
                      reset                 =>  resetx,
                      
                      en_cmd_in             =>  en_cmd_i( POST_ENGINE_N ),
                      insn_cmd_in           =>  insn_cmd_i( POST_ENGINE_N ),
                      ctl_cmd_in            =>  ctl_cmd_i( POST_ENGINE_N ),
                      ctr_cmd_in            =>  ctr_cmd_i( POST_ENGINE_N ),
                      base0_cmd_in          =>  base0_cmd_i( POST_ENGINE_N ),
                      base1_cmd_in          =>  base1_cmd_i( POST_ENGINE_N ),

                      en_mem_data_busout    =>  en_data_post_global_i,
                      addr_mem_data_busout  =>  addr_data_post_global_i,
                      word_data_in          =>  word_data_global_post_i2,

                      en_mem_res_busout     =>  en_res_post_global_i,
                      wr4_mem_res_busout    =>  wr4_res_post_global_i,
                      addr_mem_res_busout   =>  addr_res_post_global_i,
                      word_mem_res_busout   =>  word_res_post_global_i
                      );

  awready_axi_ri  <= awready_axi_in AND NOT axi_reset_i(0);
  wready_axi_ri   <= wready_axi_in AND NOT axi_reset_i(0);
  bvalid_axi_ri   <= bvalid_axi_in AND NOT axi_reset_i(0);

  word_global_output_i2 <= word_global_output_i AFTER 10 ps WHEN rising_edge(clock); 
  a_output_unit: output_unit PORT MAP(
                      clock                 =>  clock,
                      reset                 =>  resetx,

                      en_cmd_in             =>  en_cmd_i( OUTPUT_ENGINE_N ),
                      insn_cmd_in           =>  insn_cmd_i( OUTPUT_ENGINE_N ),
                      ctl_cmd_in            =>  ctl_cmd_i( OUTPUT_ENGINE_N ),
                      ctr_cmd_in            =>  ctr_cmd_i( OUTPUT_ENGINE_N ),
                      base0_cmd_in          =>  base0_cmd_i( OUTPUT_ENGINE_N ),
                      base1_cmd_in          =>  base1_cmd_i( OUTPUT_ENGINE_N ),
                      spec_cmd_in           =>  spec_cmd_i( OUTPUT_ENGINE_N ),

                      block_busout          =>  output_block_i,

                      en_mem_busout         =>  en_output_global_i,
                      addr_mem_busout       =>  addr_output_global_i,
                      word_in               =>  word_global_output_i2,


                      axi_clock             => axi_clock_i,

                      awid_axi_regout       => awid_axi_i,
                      awaddr_axi_regout     => awaddr_axi_i,
                      awlen_axi_regout      => awlen_axi_i,
                      awsize_axi_regout     => awsize_axi_i,
                      awburst_axi_regout    => awburst_axi_i,
                      awlock_axi_regout     => awlock_axi_i,
                      awvalid_axi_regout    => awvalid_axi_i,
                      awready_axi_in        => awready_axi_ri,

                      wid_axi_regout        => wid_axi_i,
                      wdata_axi_regout      => wdata_axi_i,
                      wstrb_axi_regout      => wstrb_axi_i,
                      wlast_axi_regout      => wlast_axi_i,
                      wvalid_axi_regout     => wvalid_axi_i,
                      wready_axi_in         => wready_axi_in,

                      bid_axi_in            => bid_axi_in,
                      bresp_axi_in          => bresp_axi_in,
                      bvalid_axi_in         => bvalid_axi_ri,
                      bready_axi_regout     => bready_axi_i
                      );

  awid_axi_regout       <= awid_axi_i;
  awaddr_axi_regout     <= awaddr_axi_i;
  awlen_axi_regout      <= awlen_axi_i;
  awsize_axi_regout     <= awsize_axi_i;
  awburst_axi_regout    <= awburst_axi_i;
  awlock_axi_regout     <= awlock_axi_i;
  awvalid_axi_regout    <= awvalid_axi_i;
  wid_axi_regout        <= wid_axi_i;
  wdata_axi_regout      <= wdata_axi_i;
  wstrb_axi_regout      <= wstrb_axi_i;
  wlast_axi_regout      <= wlast_axi_i;
  wvalid_axi_regout     <= wvalid_axi_i;
  bready_axi_regout     <= bready_axi_i;

  a_global_memory: global_memory_unit PORT MAP(
                      clock                       => clock,
                      reset                       => resetx,

                      slp_in                      => slp_in,
                      sd_in                       => sd_in,

                      err_collide_busout          => collide_err_i,

                      en_mem_input_in             => en_input_global_i,
                      slice_mem_input_in          => slice_input_global_i,
                      addr_mem_input_in           => addr_input_global_i,
                      word_mem_input_in           => word_input_global_i,

                      en_mem_data_filter_in       => en_data_filter_global_i,
                      width_mem_data_filter_in    => width_data_filter_global_i,
                      slice_mem_data_filter_in    => slice_data_filter_global_i,
                      addr_mem_data_filter_in     => addr_data_filter_global_i,
                      word0_data_filter_busout    => word_data_global_filter_i(0),
                      word1_data_filter_busout    => word_data_global_filter_i(1),
                      word2_data_filter_busout    => word_data_global_filter_i(2),
                      word3_data_filter_busout    => word_data_global_filter_i(3),
                      word4_data_filter_busout    => word_data_global_filter_i(4),
                      word5_data_filter_busout    => word_data_global_filter_i(5),
                      word6_data_filter_busout    => word_data_global_filter_i(6),
                      word7_data_filter_busout    => word_data_global_filter_i(7),

                      en_mem_psum_filter_in       => en_psum_filter_global_i,
                      width_mem_psum_filter_in    => width_psum_filter_global_i,
                      slice_mem_psum_filter_in    => slice_psum_filter_global_i,
                      addr_mem_psum_filter_in     => addr_psum_filter_global_i,
                      word0_psum_filter_busout    => word_psum_global_filter_i(0),
                      word1_psum_filter_busout    => word_psum_global_filter_i(1),
                      word2_psum_filter_busout    => word_psum_global_filter_i(2),
                      word3_psum_filter_busout    => word_psum_global_filter_i(3),
                      word4_psum_filter_busout    => word_psum_global_filter_i(4),
                      word5_psum_filter_busout    => word_psum_global_filter_i(5),
                      word6_psum_filter_busout    => word_psum_global_filter_i(6),
                      word7_psum_filter_busout    => word_psum_global_filter_i(7),

                      en_mem_res_filter_in        => en_res_filter_global_i,
                      width_mem_res_filter_in     => width_res_filter_global_i,
                      slice_mem_res_filter_in     => slice_res_filter_global_i,
                      addr_mem_res_filter_in      => addr_res_filter_global_i,
                      word0_mem_res_filter_in     => word_res_filter_global_i(0),
                      word1_mem_res_filter_in     => word_res_filter_global_i(1),
                      word2_mem_res_filter_in     => word_res_filter_global_i(2),
                      word3_mem_res_filter_in     => word_res_filter_global_i(3),
                      word4_mem_res_filter_in     => word_res_filter_global_i(4),
                      word5_mem_res_filter_in     => word_res_filter_global_i(5),
                      word6_mem_res_filter_in     => word_res_filter_global_i(6),
                      word7_mem_res_filter_in     => word_res_filter_global_i(7),

                      en_mem_data_post_in         => en_data_post_global_i,
                      addr_mem_data_post_in       => addr_data_post_global_i,
                      word_data_post_busout       => word_data_global_post_i,

                      en_mem_res_post_in          => en_res_post_global_i,
                      wr4_mem_res_post_in         => wr4_res_post_global_i,
                      addr_mem_res_post_in        => addr_res_post_global_i,
                      word_mem_res_post_in        => word_res_post_global_i,

                      en_mem_output_in            => en_output_global_i,
                      addr_mem_output_in          => addr_output_global_i,
                      word_output_busout          => word_global_output_i
                    );

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
    VARIABLE  tick      : integer;
    VARIABLE  last_reg  : array_bit32_type(0 TO 15) := (OTHERS => (31 DOWNTO 0 => '0'));
    VARIABLE  diff      : boolean;
    VARIABLE  last_ctl  : ctl_type := (OTHERS => '0');
    VARIABLE  last_next : bit16_type := (OTHERS => '0');
    VARIABLE  last_err  : bit15_type := (OTHERS => '0');
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 101 ps;

    IF now >= start_print and reset /= "1" THEN
      tick := ( now / 1 ns ) - 2;

      IF stall_i = "1" OR block_i /= (block_i'range => "0") THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, stall_i,  " stall_i ");
        dump_string( buf, " block_i "); 
        FOR i IN block_i'range LOOP
          dump_bit( buf, block_i(i) );
        END LOOP;
        writeline(output, buf );
      END IF;

      IF interrupt_pxi_d = "1" THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, interrupt_pxi_d,  " interrupt_pxi ");
        writeline(output, buf );
      END IF;

      IF pxi_early_i = "1" AND en_pxi_req_in = "1" THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, pxi_early_i,  " pxi_early_i ");
        dump_bit( buf, pxi_clock_i,  " pxi_clock_i ");
        dump_bit( buf, en_pxi_req_in, " en_pxi_req_in ");
        dump_bit( buf, we_pxi_req_in, " we_pxi_req_in ");
        dump_hex( buf, addr_pxi_req_in, " addr_pxi_req_in ");
        dump_hex( buf, word_pxi_req_in, " word_pxi_req_in ");
        writeline(output, buf );
      END IF;

      IF axi_early_i = "1" AND awvalid_axi_i = '1' THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, axi_early_i,  " axi_early_i ");
        dump_bit( buf, awvalid_axi_i,  " awvalid_axi_i ");
        dump_hex( buf, awaddr_axi_i, " awaddr_axi_i ");
        dump_bit( buf, awburst_axi_i, " awburst_axi_i ");
        dump_bit( buf, awsize_axi_i, " awsize_axi_i ");
        dump_bit( buf, awlen_axi_i, " awlen_axi_i ");
        writeline(output, buf );
      END IF;

      IF axi_early_i = "1" AND wvalid_axi_i = '1' THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, axi_early_i,  " axi_early_i ");
        dump_bit( buf, wvalid_axi_i,  " wvalid_axi_i ");
        dump_hex( buf, wdata_axi_i,  " wdata_axi_i ");
        dump_bit( buf, wlast_axi_i, " wlast_axi_i ");
        writeline(output, buf );
      END IF;

      IF axi_early_i = "1" AND arvalid_axi_i = '1' THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, axi_early_i,  " axi_early_i ");
        dump_bit( buf, arvalid_axi_i,  " arvalid_axi_i ");
        dump_hex( buf, araddr_axi_i, " araddr_axi_i ");
        dump_bit( buf, arlen_axi_i, " arlen_axi_i ");
        dump_bit( buf, arsize_axi_i, " arsize_axi_i ");
        dump_bit( buf, arburst_axi_i, " arburst_axi_i ");
        dump_bit( buf, arready_axi_in, " arready_axi_in ");
        writeline(output, buf );
      END IF;

      IF axi_early_i = "1" AND rvalid_axi_in = '1' THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, axi_early_i,  " axi_early_i ");
        dump_bit( buf, rvalid_axi_in,  " rvalid_axi_in ");
        dump_hex( buf, rdata_axi_in,  " rdata_axi_in ");
        dump_bit( buf, rlast_axi_in, " rlast_axi_in ");
        dump_bit( buf, rresp_axi_in, " rresp_axi_in ");
        writeline(output, buf );
      END IF;

      IF axi_early_i = "1" AND bvalid_axi_in = '1' THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, axi_early_i,  " axi_early_i ");
        dump_bit( buf, bvalid_axi_in,  " bvalid_axi_in ");
        dump_bit( buf, bresp_axi_in, " bresp_axi_in ");
        writeline(output, buf );
      END IF;

      IF en_pxi_q = "1" THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, en_pxi_q, " en_pxi_q ");
        dump_bit( buf, we_pxi_q, " we_pxi_q ");
        dump_hex( buf, addr_pxi_q, " addr_pxi_q ");
        dump_bit( buf, addr_pxi_q(GLOBAL_SEL_ADDR_PXI), " global_pxi_q ");
        writeline(output, buf );
      END IF;

      IF en_pxi_resp_q = "1" AND pxi_early_i = "1" THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, avail_pxi_resp_in, " avail_pxi_resp ");
        dump_bit( buf, en_pxi_resp_q, " en_pxi_resp ");
        dump_hex( buf, addr_pxi_q, " addr_pxi_resp ");
        dump_hex( buf, word_pxi_resp_q, " word_pxi_resp ");
        writeline(output, buf );
      END IF;

      IF all_err_i /= (all_err_i'range => '0') THEN
        dump_cycle( buf, "GSN>");
        dump_bit( buf, all_err_i, " ERROR ");
        dump_bit( buf, collide_err_i, " collide " );
        IF collide_err_i(0) /= '0' THEN
          dump_string( buf, " 1:input_res" );
        END IF;
        IF collide_err_i(1) /= '0' THEN
          dump_string( buf, " 0:filter_data" );
        END IF;
        IF collide_err_i(2) /= '0' THEN
          dump_string( buf, " 2:filter_psum" );
        END IF;
        IF collide_err_i(3) /= '0' THEN
          dump_string( buf, " 3:filter_res" );
        END IF;
        IF collide_err_i(4) /= '0' THEN
          dump_string( buf, " 4:post_data" );
        END IF;
        IF collide_err_i(5) /= '0' THEN
          dump_string( buf, " 5:post_res" );
        END IF;
        IF collide_err_i(6) /= '0' THEN
          dump_string( buf, " 6:output_data" );
        END IF;
        dump_bit( buf, rresp_err_i, " rresp " );
        dump_bit( buf, bresp_err_i, " bresp " );
        dump_bit( buf, align_err_i(OUTPUT_ENGINE_N), " o_align " );
        dump_bit( buf, page_err_i(INPUT_ENGINE_N), " i_page " );
        dump_bit( buf, align_err_i(INPUT_ENGINE_N), " i_align " );
        writeline(output, buf );
      END IF;

      FOR e IN ENGINE_RANGE LOOP

        IF en_rd_pxi_i1( 2*e + 0 ) = '1' THEN
          dump_cycle( buf, "GSN>");
          dump_integer( buf, e, "insn engine#");
          dump_bit( buf, en_rd_pxi_i1(2*e+0), " en ");
          dump_bit( buf, en_rd_pxi_i1, " ");
          dump_bit( buf, collide_rw_pxi_i1(2*e+0), " collide ");
          dump_hex( buf, word_rd_mem_pxi_i1(2*e+0), "  ");
          dump_bit( buf, en_mem_pxi_q1, " en_mem_pxi_q1 ");
          writeline(output, buf );
        END IF;

        IF en_rd_pxi_i1( 2*e + 1 ) = '1' THEN
          dump_cycle( buf, "GSN>");
          dump_integer( buf, e, "data engine#");
          dump_bit( buf, en_rd_pxi_i1(2*e+1), " en ");
          dump_bit( buf, en_rd_pxi_i1, " ");
          dump_bit( buf, collide_rw_pxi_i1(2*e+1), " collide ");
          dump_hex( buf, word_rd_mem_pxi_i1(2*e+1), "  ");
          dump_bit( buf, en_mem_pxi_q1, " en_mem_pxi_q1 ");
          writeline(output, buf );
        END IF;

      END LOOP;

      diff := false;
      FOR i IN 0 TO 15 LOOP
        IF globals_q(i) /= last_reg(i) THEN
          diff := true;
        END IF;
      END LOOP;
      IF ctl_q /= last_ctl THEN
        diff := true;
      END IF;
      IF next_q /= last_next THEN
        diff := true;
      END IF;

      IF diff THEN
        dump_cycle( buf, "GLB> ");
        FOR i IN 0 TO 15 LOOP
          IF globals_q(i) /= last_reg(i) THEN
            dump_integer( buf, i, " $g" );
            dump_hex( buf, globals_q(i), " := " );
            last_reg(i) := globals_q(i);
          END IF;
        END LOOP;
        IF ctl_q /= last_ctl THEN
          dump_hex( buf, ctl_q, " $ctl := " );
          last_ctl := ctl_q;
        END IF;
        IF next_q /= last_next THEN
          dump_hex( buf, next_q, " $next := " );
          last_next := next_q;
        END IF;
        IF err_q /= last_err THEN
          dump_hex( buf, err_q, " $err := " );
          last_err := err_q;
        END IF;
        writeline( output, buf );
      END IF;
    END IF;

  END PROCESS dump;

--  pxi_clock_dump: PROCESS( pxi_reset_i, pxi_clock_i, pxi_early_i )
--    VARIABLE  buf       : line;
--  BEGIN
--    dump_cycle(buf, "PXI>" );
--    dump_bit(buf, pxi_clock_i, " clock " );
--    dump_bit(buf, pxi_early_i, " early " );
--    dump_bit(buf, pxi_reset_i, " reset " );
--    writeline(output, buf );
--  END PROCESS pxi_clock_dump;
--
--  axi_clock_dump: PROCESS( axi_reset_i, axi_clock_i, axi_early_i )
--    VARIABLE  buf       : line;
--  BEGIN
--    dump_cycle(buf, "AXI>" );
--    dump_bit(buf, axi_clock_i, " clock " );
--    dump_bit(buf, axi_early_i, " early " );
--    dump_bit(buf, axi_reset_i, " reset " );
--    writeline(output, buf );
--  END PROCESS axi_clock_dump;

-- pragma translate_on

END ARCHITECTURE behavior;
