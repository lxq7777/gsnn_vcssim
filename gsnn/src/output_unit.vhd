
---------------------------------------------------------------------------
-- input unit writes from GSNN memory to external memory via AXI bus 
---------------------------------------------------------------------------

-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;
USE work.gsnn_interface_package.ALL;

ENTITY output_unit IS
  PORT(
  clock                           : IN std_logic;
  reset                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- engine signals
  ---------------------------------------------------------------------------
  en_cmd_in                       : IN bit_type;
  insn_cmd_in                     : IN insn_type;
  ctl_cmd_in                      : IN bit16_type;
  ctr_cmd_in                      : IN bit32_type;
  base0_cmd_in                    : IN bit32_type;
  base1_cmd_in                    : IN bit32_type;
  spec_cmd_in                     : IN bit32_type;

  block_busout                    : OUT bit_type;

  ---------------------------------------------------------------------------
  -- memory signals
  ---------------------------------------------------------------------------
  en_mem_busout                   : OUT bit_type;
  addr_mem_busout                 : OUT global_addr_type;
  word_in                         : IN bit128_type;

  ---------------------------------------------------------------------------
  -- AXI signals
  ---------------------------------------------------------------------------
  axi_clock                       : IN std_logic;
  -- AW Channel
  awid_axi_regout                 : OUT awid_axi_type;
  awaddr_axi_regout               : OUT addr_axi_type;
  awlen_axi_regout                : OUT bit4_type;
  awsize_axi_regout               : OUT bit3_type;
  awburst_axi_regout              : OUT awburst_axi_type;
  awlock_axi_regout               : OUT alock_axi_type;
  awvalid_axi_regout              : OUT std_logic;
  awready_axi_in                  : IN  std_logic;

  -- W Channel
  wid_axi_regout                  : OUT awid_axi_type;
  wdata_axi_regout                : OUT data_axi_type;
  wstrb_axi_regout                : OUT wstrb_axi_type;
  wlast_axi_regout                : OUT std_logic;
  wvalid_axi_regout               : OUT std_logic;
  wready_axi_in                   : IN  std_logic;

  -- B Channel
  bid_axi_in                      : IN  awid_axi_type;
  bresp_axi_in                    : IN  resp_axi_type;
  bvalid_axi_in                   : IN  std_logic;
  bready_axi_regout               : OUT std_logic
  );
END ENTITY output_unit;

ARCHITECTURE behavior OF output_unit IS
-- pragma translate_off
  CONSTANT disable_print : boolean := false;
  CONSTANT start_print   : time := 0 ns;
-- pragma translate_on

  CONSTANT SIZE_OUTPUT : integer := 6;
  SUBTYPE mask_entry_type IS std_logic_vector( SIZE_OUTPUT-1 DOWNTO 0);
  SUBTYPE num_entry_type IS std_logic_vector(2 DOWNTO 0);
  CONSTANT MAX_OUTPUT : num_entry_type := std_logic_vector( to_unsigned( SIZE_OUTPUT-1, num_entry_type'length) );
  CONSTANT CHECK_OUTPUT : num_entry_type := std_logic_vector( to_unsigned( SIZE_OUTPUT-3, num_entry_type'length) );

  TYPE entry_output_type IS RECORD
    valida          : bit_type;
    validw          : bit_type;
    last            : bit_type;
    byte            : bit_type;
    a_to            : addr_axi_type;
    word            : bit128_type;
  END RECORD entry_output_type;
  TYPE array_entry_output_type IS ARRAY( natural RANGE<>) OF entry_output_type;

  SIGNAL  entries_q     : array_entry_output_type( 0 TO SIZE_OUTPUT-1);
  SIGNAL  ctr0_i0       : bit_type;

  SIGNAL  en_cmd_in0     : bit_type;
  SIGNAL  ctr0_q0       : bit_type;
  SIGNAL  en_cmd_q0     : bit_type;
  SIGNAL  type_q0       : type_output_type;
  SIGNAL  base0_cmd_q0  : addr_axi_type;

  SIGNAL  en_cmd_i0     : bit_type;
  SIGNAL  ctr0_q1       : bit_type;
  SIGNAL  en_cmd_q1     : bit_type;
  SIGNAL  type_q1       : type_output_type;
  SIGNAL  base0_cmd_q1  : addr_axi_type;

  SIGNAL  en_cmd_i1     : bit_type;
  SIGNAL  ctr0_q2       : bit_type;
  SIGNAL  en_cmd_q2     : bit_type;
  SIGNAL  type_q2       : type_output_type;
  SIGNAL  base0_cmd_q2  : addr_axi_type;

  SIGNAL  en_cmd_i2     : bit_type;
  SIGNAL  ctr0_q3       : bit_type;
  SIGNAL  en_cmd_q3     : bit_type;
  SIGNAL  type_q3       : type_output_type;
  SIGNAL  base0_cmd_q3  : addr_axi_type;

  SIGNAL  en_cmd_i3     : bit_type;
  SIGNAL  ctr0_q4       : bit_type;
  SIGNAL  en_cmd_q4     : bit_type;
  SIGNAL  type_q4       : type_output_type;
  SIGNAL  base0_cmd_q4  : addr_axi_type;

  SIGNAL  wordb_i4      : bit64_type;
  SIGNAL  word_i4       : bit128_type;
  SIGNAL  is_byte_i4    : bit_type;

  SIGNAL  hda_d         : num_entry_type;
  SIGNAL  hda_q         : num_entry_type;
  SIGNAL  hdw_d         : num_entry_type;
  SIGNAL  hdw_q         : num_entry_type;
  SIGNAL  tl_d          : num_entry_type;
  SIGNAL  tl_q          : num_entry_type;
  SIGNAL  tl2_d         : num_entry_type;
  SIGNAL  tl2_q         : num_entry_type;

  SIGNAL  push_i4       : bit_type;
  SIGNAL  popa_i        : bit_type;
  SIGNAL  popw_i        : bit_type;
  SIGNAL  hda_entry_i   : entry_output_type;
  SIGNAL  hdw_entry_i   : entry_output_type;

  SIGNAL awaddr_axi_q   : addr_axi_type;
  SIGNAL awsize_axi_i   : bit3_type;
  SIGNAL awsize_axi_q   : bit3_type;
  SIGNAL awvalid_axi_i  : bit_type;
  SIGNAL awvalid_axi_q  : bit_type;

  SIGNAL wdata_axi_q    : data_axi_type;
  SIGNAL wstrb_axi_i    : wstrb_axi_type;
  SIGNAL wstrb_axi_q    : wstrb_axi_type;
  SIGNAL wvalid_axi_i   : bit_type;
  SIGNAL wvalid_axi_q   : bit_type;
BEGIN
  block_busout <= "1" WHEN entries_q(to_integer(unsigned(tl2_q))).valida = "1" ELSE
                  "1" WHEN entries_q(to_integer(unsigned(tl2_q))).validw = "1" ELSE
                  "0";

  ctr0_i0               <=  "1" WHEN ctr_cmd_in = (ctr_cmd_in'range => '0') ELSE "0";
  en_cmd_in0             <= "0" WHEN reset = "1" ELSE en_cmd_in;

  en_mem_busout         <= en_cmd_in0                             AFTER 10 ps WHEN rising_edge(clock);
  addr_mem_busout       <= base1_cmd_in(global_addr_type'range)   AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in0 = "1";

  en_cmd_q0             <= en_cmd_in0                              AFTER 10 ps WHEN rising_edge(clock);
  type_q0               <= ctl_cmd_in(TYPE_OUTPUT_INSN_FIELD)     AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in0 = "1";
  base0_cmd_q0          <= spec_cmd_in(7 DOWNTO 0) & base0_cmd_in AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in0 = "1";
  ctr0_q0               <= ctr0_i0                                AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in0 = "1";

  en_cmd_i0             <= "0" WHEN reset = "1" ELSE en_cmd_q0;
  en_cmd_q1             <= en_cmd_i0    AFTER 10 ps WHEN rising_edge(clock);
  type_q1               <= type_q0      AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1";
  base0_cmd_q1          <= base0_cmd_q0 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1";
  ctr0_q1               <= ctr0_q0      AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1";

  en_cmd_i1             <= "0" WHEN reset = "1" ELSE en_cmd_q1;
  en_cmd_q2             <= en_cmd_i1    AFTER 10 ps WHEN rising_edge(clock);
  type_q2               <= type_q1      AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1";
  base0_cmd_q2          <= base0_cmd_q1 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1";
  ctr0_q2               <= ctr0_q1      AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1";

  en_cmd_i2             <= "0" WHEN reset = "1" ELSE en_cmd_q2;
  en_cmd_q3             <= en_cmd_i2   AFTER 10 ps WHEN rising_edge(clock);
  type_q3               <= type_q2     AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i2 = "1";
  base0_cmd_q3          <= base0_cmd_q2 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i2 = "1";
  ctr0_q3               <= ctr0_q2      AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i2 = "1";

  en_cmd_i3             <= "0" WHEN reset = "1" ELSE en_cmd_q3;
  en_cmd_q4             <= en_cmd_i3   AFTER 10 ps WHEN rising_edge(clock);
  type_q4               <= type_q3     AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i3 = "1";
  base0_cmd_q4          <= base0_cmd_q3 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i3 = "1";
  ctr0_q4               <= ctr0_q3      AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i3 = "1";

  byte_gen: FOR i IN 0 TO 7 GENERATE
    SIGNAL  fp16_gi4    : fp16_type;
    SIGNAL  sign_gi4    : bit_type;
    SIGNAL  exp_gi4     : bit5_type;
    SIGNAL  mant_gi4    : bit10_type;
    SIGNAL  is_s_gi4    : bit_type;
    SIGNAL  byte_gi4    : bit8_type;
    SIGNAL  sbyte_gi4   : bit8_type;
  BEGIN
    fp16_gi4    <= word_in(16*i+15 DOWNTO 16*i);
    sign_gi4    <= fp16_gi4(SIGN_FP16);
    exp_gi4     <= fp16_gi4(EXP_FP16);
    mant_gi4    <= fp16_gi4(MANT_FP16);
    WITH type_q4 SELECT
      is_s_gi4 <= 
                "1" WHEN S8_TYPE_OUTPUT,
                "0" WHEN U8_TYPE_OUTPUT,
                "X" WHEN OTHERS;

    byte_gi4   <= "00000001"                     WHEN exp_gi4 = "01111" ELSE
                 "00000000"                      WHEN exp_gi4(4) = '0'  ELSE
                 "0000001" & mant_gi4(9)         WHEN exp_gi4 = "10000" ELSE
                 "000001" & mant_gi4(9 DOWNTO 8) WHEN exp_gi4 = "10001" ELSE
                 "00001" & mant_gi4(9 DOWNTO 7)  WHEN exp_gi4 = "10010" ELSE
                 "0001" & mant_gi4(9 DOWNTO 6)   WHEN exp_gi4 = "10011" ELSE
                 "001" & mant_gi4(9 DOWNTO 5)    WHEN exp_gi4 = "10100" ELSE
                 "01" & mant_gi4(9 DOWNTO 4)     WHEN exp_gi4 = "10101" ELSE
                 "01111111"                      WHEN is_s_gi4 = "1" and sign_gi4 = "0" ELSE
                 "10000000"                      WHEN is_s_gi4 = "1" and sign_gi4 = "1" ELSE
                 "1" & mant_gi4(9 DOWNTO 3)      WHEN exp_gi4 = "10110" AND is_s_gi4 = "0" ELSE
                 "11111111";
    sbyte_gi4                   <= byte_gi4             WHEN is_s_gi4 = "0" AND sign_gi4 = "0"  ELSE
                                  (7 DOWNTO 0 => '0')   WHEN is_s_gi4 = "0" AND sign_gi4 = "1" ELSE
                                  byte_gi4              WHEN is_s_gi4 = "1" AND sign_gi4 = "0"  ELSE
                                  std_logic_vector(0-unsigned(byte_gi4));
    wordb_i4(8*i+7 DOWNTO 8*i)  <= sbyte_gi4;
  END GENERATE byte_gen;

  WITH type_q4 SELECT
    is_byte_i4 <= "1" WHEN S8_TYPE_OUTPUT | U8_TYPE_OUTPUT,
               "0" WHEN FP16_TYPE_OUTPUT,
               "X" WHEN OTHERS;
  word_i4 <= wordb_i4 & wordb_i4  WHEN is_byte_i4 = "1" ELSE word_in;

  popa_i              <=  "1" WHEN ( awready_axi_in = '1' OR awvalid_axi_q = "0")
                                AND entries_q(to_integer(unsigned(hda_q))).valida = "1" AND axi_clock = '0' ELSE "0";
  popw_i              <=  "1" WHEN ( wready_axi_in = '1' OR wvalid_axi_q = "0" )
                                AND entries_q(to_integer(unsigned(hdw_q))).validw = "1" AND axi_clock = '0' ELSE "0";
  push_i4             <=  "1" WHEN en_cmd_q4 = "1" ELSE "0";

  entry_gen: FOR i IN entries_q'range GENERATE
    CONSTANT  entry         : num_entry_type  := std_logic_vector(to_unsigned(i, num_entry_type'length));
    SIGNAL    hda_gi        : bit_type;
    SIGNAL    hdw_gi        : bit_type;
    SIGNAL    tl_gi         : bit_type;
    SIGNAL    push_en_gi    : bit_type;
    SIGNAL    popa_en_gi    : bit_type;
    SIGNAL    valida_gi     : bit_type;
    SIGNAL    popw_en_gi    : bit_type;
    SIGNAL    validw_gi     : bit_type;
  BEGIN
    hda_gi                <=  "1" WHEN hda_q = entry ELSE "0";
    hdw_gi                <=  "1" WHEN hdw_q = entry ELSE "0";
    tl_gi                 <=  "1" WHEN tl_q = entry ELSE "0";

    push_en_gi            <=  tl_gi AND push_i4;
    popa_en_gi            <=  hda_gi AND popa_i;
    popw_en_gi            <=  hdw_gi AND popw_i;

    valida_gi             <=  "0" WHEN reset = "1" ELSE
                              "1" WHEN push_en_gi = "1" ELSE
                              "0" WHEN popa_i = "1" AND hda_gi = "1" ELSE
                              entries_q(i).valida;
    entries_q(i).valida   <=  valida_gi AFTER 10 ps WHEN rising_edge(clock);

    validw_gi             <=  "0" WHEN reset = "1" ELSE
                              "1" WHEN push_en_gi = "1" ELSE
                              "0" WHEN popw_i = "1" AND hdw_gi = "1" ELSE
                              entries_q(i).validw;
    entries_q(i).validw   <=  validw_gi AFTER 10 ps WHEN rising_edge(clock);

    entries_q(i).last     <=  ctr0_q4       AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";
    entries_q(i).byte     <=  is_byte_i4    AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";
    entries_q(i).a_to     <=  base0_cmd_q4  AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";
    entries_q(i).word     <=  word_i4       AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";

  END GENERATE entry_gen;

  hda_d     <= (OTHERS=>'0')                        WHEN reset = "1" ELSE
               (OTHERS=>'0')                     WHEN popa_i = "1" AND hda_q = MAX_OUTPUT ELSE
               std_logic_vector(unsigned(hda_q)+1)  WHEN popa_i = "1" ELSE
               hda_q;
  hda_q      <= hda_d AFTER 10 ps WHEN rising_edge(clock);

  hdw_d     <= (OTHERS=>'0')                        WHEN reset = "1" ELSE
               (OTHERS=>'0')                     WHEN popw_i = "1" AND hdw_q = MAX_OUTPUT ELSE
               std_logic_vector(unsigned(hdw_q)+1)  WHEN popw_i = "1" ELSE
               hdw_q;
  hdw_q      <= hdw_d AFTER 10 ps WHEN rising_edge(clock);

  tl_d      <= (OTHERS=>'0')                        WHEN reset = "1" ELSE
               (OTHERS=>'0')                     WHEN push_i4 = "1" AND tl_q = MAX_OUTPUT ELSE
               std_logic_vector(unsigned(tl_q)+1)   WHEN push_i4 = "1" ELSE
               tl_q;
  tl_q      <= tl_d AFTER 10 ps WHEN rising_edge(clock);

  tl2_d     <= CHECK_OUTPUT                         WHEN reset = "1" ELSE
               (OTHERS=>'0')                     WHEN push_i4 = "1" AND tl2_q = MAX_OUTPUT ELSE
               std_logic_vector(unsigned(tl2_q)+1)  WHEN push_i4 = "1" ELSE
               tl2_q;
  tl2_q     <= tl2_d AFTER 10 ps WHEN rising_edge(clock);

  awid_axi_regout       <= (OTHERS => '0');
  awlen_axi_regout      <= (OTHERS => '0');
  awburst_axi_regout    <= INCR_AWBURST_AXI;
  awlock_axi_regout     <= NORMAL_ALOCK_AXI;

  hda_entry_i <= entries_q(to_integer(unsigned(hda_q)));
  awaddr_axi_regout     <= awaddr_axi_q;
  awsize_axi_regout     <= awsize_axi_q;
  awvalid_axi_regout    <= awvalid_axi_q(0);

  awaddr_axi_q          <= hda_entry_i.a_to AFTER 10 ps WHEN rising_edge(clock) AND popa_i = "1";
  awsize_axi_i          <= "100" WHEN hda_entry_i.byte = "0" ELSE "011";
  awsize_axi_q          <= awsize_axi_i AFTER 10 ps WHEN rising_edge(clock) AND popa_i = "1";
  awvalid_axi_i         <= "0"            WHEN reset = "1" ELSE
                           awvalid_axi_q  WHEN axi_clock = '1' ELSE
                           "1"            WHEN popa_i = "1" ELSE
                           "0"            WHEN awready_axi_in = '1' ELSE
                           awvalid_axi_q;
  awvalid_axi_q         <= awvalid_axi_i AFTER 10 ps WHEN rising_edge(clock);

  wid_axi_regout        <= (OTHERS => '0');
  wlast_axi_regout      <= '1';

  hdw_entry_i <= entries_q(to_integer(unsigned(hdw_q)));
  wdata_axi_regout      <= wdata_axi_q;
  wstrb_axi_regout      <= wstrb_axi_q;
  wvalid_axi_regout     <= wvalid_axi_q(0);

  wdata_axi_q           <= hdw_entry_i.word AFTER 10 ps WHEN rising_edge(clock) AND popw_i = "1";
  wstrb_axi_i           <= x"ffff" WHEN hdw_entry_i.byte = "0" ELSE
                           x"00ff" WHEN hdw_entry_i.a_to(3) = '0' ELSE
                           x"ff00";
  wstrb_axi_q           <= wstrb_axi_i AFTER 10 ps WHEN rising_edge(clock) AND popw_i = "1";
  wvalid_axi_i          <= "0"            WHEN reset = "1" ELSE
                           wvalid_axi_q   WHEN axi_clock = '1' ELSE
                           "1"            WHEN popw_i = "1" ELSE
                           "0"            WHEN wready_axi_in = '1' ELSE
                           wvalid_axi_q;
  wvalid_axi_q          <= wvalid_axi_i AFTER 10 ps WHEN rising_edge(clock);

  bready_axi_regout     <= '1';

-- -- pragma translate_off
--   dump: PROCESS
--     VARIABLE  buf       : line;
--     VARIABLE  tick      : integer;
--   BEGIN
--     IF disable_print THEN
--       WAIT;
--     END IF;
--     WAIT UNTIL falling_edge( clock );
--     WAIT FOR 101 ps;

--     IF now >= start_print THEN
--       tick := ( now / 1 ns ) - 2;

--       IF true THEN
--       dump_cycle( buf, "OUT>");
--       dump_hex( buf, hda_q,       " hda_q " );
--       dump_hex( buf, hdw_q,       " hdw_q " );
--       dump_hex( buf, tl_q,        " tl_q " );
--       dump_hex( buf, tl2_q,       " tl2_q " );
--       dump_bit( buf, popa_i,      " popa_i " );
--       dump_bit( buf, popw_i,      " popw_i " );
--       dump_bit( buf, awready_axi_in,  " awready_axi_in ");
--       dump_bit( buf, wready_axi_in,   " wready_axi_in ");
--       writeline(output, buf );
--       END IF;

--       IF en_cmd_in = "1" THEN
--         dump_cycle( buf, "OUT>");
--         dump_insn( buf, insn_cmd_in, " " );
--         dump_hex( buf, ctl_cmd_in, " ctl " );
--         dump_hex( buf, ctr_cmd_in, " $ctr " );
--         dump_hex( buf, base0_cmd_in, " $base0 " );
--         dump_hex( buf, base1_cmd_in, " $base1 " );
--         dump_hex( buf, spec_cmd_in, " $spec1 " );
--         writeline(output, buf );
--       END IF;

--       IF awvalid_axi_q = "1" AND axi_clock = '0' AND awready_axi_in = '1' THEN
--         dump_cycle( buf, "OUT>");
--         dump_bit( buf, awvalid_axi_q, " awvalid_axi_q ");
--         dump_hex( buf, awsize_axi_q, " awsize_axi_q ");
--         dump_hex( buf, awaddr_axi_q, " awaddr_axi_q ");
--         writeline(output, buf );
--       END IF;

--       IF wvalid_axi_q = "1" AND axi_clock = '0' AND wready_axi_in = '1' THEN
--         dump_cycle( buf, "OUT>");
--         dump_bit( buf, wvalid_axi_q, " wvalid_axi_q ");
--         dump_hex( buf, wstrb_axi_q, " wstrb_axi_q ");
--         dump_hex( buf, wdata_axi_q, " wdata_axi_q ");
--         writeline(output, buf );
--       END IF;

--       FOR i IN entries_q'range LOOP
--         IF entries_q(i).valida = "1" OR entries_q(i).validw = "1" THEN
--           dump_cycle( buf, "OUT>");
--           dump_integer( buf, i, " Q#" );
--           dump_bit( buf, entries_q(i).valida,     " valida ");
--           dump_bit( buf, entries_q(i).validw,     " validw ");
--           dump_bit( buf, entries_q(i).last,       " last ");
--           dump_bit( buf, entries_q(i).byte,       " byte ");
--           dump_bit( buf, entries_q(i).last,       " last ");
--           dump_hex( buf, entries_q(i).a_to,       " a_to ");
--           dump_hex( buf, entries_q(i).word,       " word ");
--           writeline(output, buf );
--         END IF;
--       END LOOP;

--     END IF;

--   END PROCESS dump;
-- -- pragma translate_on

END ARCHITECTURE behavior;
