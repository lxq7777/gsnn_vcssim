LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

ENTITY mult_reg_fpga is
  GENERIC(
    WIDTHA : integer := 32;
    WIDTHB : integer := 16
  );
  PORT(
    clk : in std_logic;
    A   : in std_logic_vector(WIDTHA - 1 downto 0);
    B   : in std_logic_vector(WIDTHB - 1 downto 0);
    RES : out std_logic_vector(WIDTHA + WIDTHB - 1 downto 0);
    eq_zero : out std_logic
  );
END ENTITY mult_reg_fpga;

ARCHITECTURE behavior OF mult_reg_fpga IS
  SIGNAL A_d : std_logic_vector(WIDTHA -1 downto 0);
  SIGNAL B_d : std_logic_vector(WIDTHB -1 downto 0);

  signal ACOUTx : std_logic_vector(29 downto 0); -- 30-bit output: A port cascade
  signal BCOUTx : std_logic_vector(17 downto 0); -- 18-bit output: B cascade
  signal CARRYCASCOUTx : std_logic; -- 1-bit output: Cascade carry
  signal MULTSIGNOUTx : std_logic; -- 1-bit output: Multiplier sign cascade
  signal OVERFLOWx : std_logic; -- 1-bit output: Overflow in add/acc
  signal PATTERNBDETECTx : std_logic; -- 1-bit output: Pattern bar detect
  signal PATTERNDETECTx : std_logic; -- 1-bit output: Pattern detect
  signal UNDERFLOWx : std_logic; -- 1-bit output: Underflow in add/acc
  signal CARRYOUTx : std_logic_vector(3 downto 0); -- 4-bit output: Carry
  signal XOROUTx : std_logic_vector(7 downto 0); -- 8-bit output: XOR data
  signal ACINx : std_logic_vector(29 downto 0); -- 30-bit input: A cascade data
  signal BCINx : std_logic_vector(17 downto 0); -- 18-bit input: B cascade
  signal CARRYCASCINx : std_logic; -- 1-bit input: Cascade carry
  signal MULTSIGNINx : std_logic; -- 1-bit input: Multiplier sign cascade
  signal PCINx : std_logic_vector(47 downto 0); -- 48-bit input: P cascade
  signal ALUMODEx : std_logic_vector(3 downto 0); -- 4-bit input: ALU control
  signal CARRYINSELx : std_logic_vector(2 downto 0); -- 3-bit input: Carry select
  signal INMODEx : std_logic_vector(4 downto 0); -- 5-bit input: INMODE control
  signal OPMODEx : std_logic_vector(8 downto 0); -- 9-bit input: Operation mode
  signal Cx : std_logic_vector(47 downto 0); -- 48-bit input: C data
  signal CARRYINx : std_logic; -- 1-bit input: Carry-in
  signal Dx : std_logic_vector(26 downto 0); -- 27-bit input: D data
  signal RSTAx : std_logic; -- 1-bit input: Reset for AREG
  signal RSTALLCARRYINx : std_logic; -- 1-bit input: Reset for CARRYINREG
  signal RSTALUMODEx : std_logic; -- 1-bit input: Reset for ALUMODEREG
  signal RSTBx : std_logic; -- 1-bit input: Reset for BREG
  signal RSTCx : std_logic; -- 1-bit input: Reset for CREG
  signal RSTCTRLx : std_logic; -- 1-bit input: Reset for OPMODEREG and CARRYINSELREG
  signal RSTDx : std_logic; -- 1-bit input: Reset for DREG and ADREG
  signal RSTINMODEx : std_logic; -- 1-bit input: Reset for INMODEREG
  signal RSTMx : std_logic; -- 1-bit input: Reset for MREG
  signal RSTPx : std_logic; -- 1-bit input: Reset for PREG
  signal PCOUTx : std_logic_vector(47 downto 0);
  
  signal ACOUTx1 :std_logic_vector(29 downto 0); -- 30-bit output: A port cascade
  signal BCOUTx1 : std_logic_vector(17 downto 0); -- 18-bit output: B cascade
  signal CARRYCASCOUTx1 : std_logic; -- 1-bit output: Cascade carry
  signal MULTSIGNOUTx1 : std_logic; -- 1-bit output: Multiplier sign cascade
  signal OVERFLOWx1 : std_logic; -- 1-bit output: Overflow in add/acc
  signal PATTERNBDETECTx1 : std_logic; -- 1-bit output: Pattern bar detect
  signal PATTERNDETECTx1 : std_logic; -- 1-bit output: Pattern detect
  signal UNDERFLOWx1 : std_logic; -- 1-bit output: Underflow in add/acc
  signal CARRYOUTx1 : std_logic_vector(3 downto 0); -- 4-bit output: Carry
  signal XOROUTx1 : std_logic_vector(7 downto 0); -- 8-bit output: XOR data
  signal ACINx1 : std_logic_vector(29 downto 0); -- 30-bit input: A cascade data
  signal BCINx1 : std_logic_vector(17 downto 0); -- 18-bit input: B cascade
  signal CARRYCASCINx1 : std_logic; -- 1-bit input: Cascade carry
  signal MULTSIGNINx1 : std_logic; -- 1-bit input: Multiplier sign cascade
  signal PCINx1 : std_logic_vector(47 downto 0); -- 48-bit input: P cascade
  signal ALUMODEx1 : std_logic_vector(3 downto 0); -- 4-bit input: ALU control
  signal CARRYINSELx1 : std_logic_vector(2 downto 0); -- 3-bit input: Carry select
  signal INMODEx1 : std_logic_vector(4 downto 0); -- 5-bit input: INMODE control
  signal OPMODEx1 : std_logic_vector(8 downto 0); -- 9-bit input: Operation mode
  signal Cx1 : std_logic_vector(47 downto 0); -- 48-bit input: C data
  signal CARRYINx1 : std_logic; -- 1-bit input: Carry-in
  signal Dx1 : std_logic_vector(26 downto 0); -- 27-bit input: D data
  signal RSTAx1 : std_logic; -- 1-bit input: Reset for AREG
  signal RSTALLCARRYINx1 : std_logic; -- 1-bit input: Reset for CARRYINREG
  signal RSTALUMODEx1 : std_logic; -- 1-bit input: Reset for ALUMODEREG
  signal RSTBx1 : std_logic; -- 1-bit input: Reset for BREG
  signal RSTCx1 : std_logic; -- 1-bit input: Reset for CREG
  signal RSTCTRLx1 : std_logic; -- 1-bit input: Reset for OPMODEREG and CARRYINSELREG
  signal RSTDx1 : std_logic; -- 1-bit input: Reset for DREG and ADREG
  signal RSTINMODEx1 : std_logic; -- 1-bit input: Reset for INMODEREG
  signal RSTMx1 : std_logic; -- 1-bit input: Reset for MREG
  signal RSTPx1 : std_logic; -- 1-bit input: Reset for PREG
  signal PCOUTx1 : std_logic_vector(47 downto 0);

  signal prod_no_carry : std_logic_vector(47 downto 0);
  signal prod_LSB : std_logic_vector(47 downto 0);  
BEGIN
  --PROCESS( clk )
  --BEGIN
  --  IF rising_edge(clk) THEN
  --    A_d <= A;
  --  END IF;
  --END PROCESS;

  --PROCESS( clk )
  --BEGIN
  --  IF rising_edge(clk) THEN
  --    B_d <= B;
  --  END IF;
  --END PROCESS;
  --RES <= A_d * B_d;

  RES(31 downto 16) <= std_logic_vector(unsigned(prod_LSB(31 downto 16)) + unsigned(prod_no_carry(15 downto 0)));
  RES(15 downto 0)  <= prod_LSB(15 downto 0);

  a_mult_LSB : DSP48E2
     generic map (
        -- Feature Control Attributes: Data Path Selection
        AMULTSEL => "A",                   -- Selects A input to multiplier (A, AD)
        A_INPUT => "DIRECT",               -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
        BMULTSEL => "B",                   -- Selects B input to multiplier (AD, B)
        B_INPUT => "DIRECT",               -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
        PREADDINSEL => "A",                -- Selects input to pre-adder (A, B)
        RND => X"000000000000",            -- Rounding Constant
        USE_MULT => "MULTIPLY",            -- Select multiplier usage (DYNAMIC, MULTIPLY, NONE)
        USE_SIMD => "ONE48",               -- SIMD selection (FOUR12, ONE48, TWO24)
        USE_WIDEXOR => "FALSE",            -- Use the Wide XOR function (FALSE, TRUE)
        XORSIMD => "XOR24_48_96",          -- Mode of operation for the Wide XOR (XOR12, XOR24_48_96)
        -- Pattern Detector Attributes: Pattern Detection Configuration
        AUTORESET_PATDET => "NO_RESET",    -- NO_RESET, RESET_MATCH, RESET_NOT_MATCH
        AUTORESET_PRIORITY => "RESET",     -- Priority of AUTORESET vs. CEP (CEP, RESET).
        MASK => X"ffff00000000",           -- 48-bit mask value for pattern detect (1=ignore)
        PATTERN => X"000000000000",        -- 48-bit pattern match for pattern detect
        SEL_MASK => "MASK",                -- C, MASK, ROUNDING_MODE1, ROUNDING_MODE2
        SEL_PATTERN => "PATTERN",          -- Select pattern value (C, PATTERN)
        USE_PATTERN_DETECT => "PATDET", -- Enable pattern detect (NO_PATDET, PATDET)
        -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
        IS_ALUMODE_INVERTED => "0000",     -- Optional inversion for ALUMODE
        IS_CARRYIN_INVERTED => '0',        -- Optional inversion for CARRYIN
        IS_CLK_INVERTED => '0',            -- Optional inversion for CLK
        IS_INMODE_INVERTED => "00000",     -- Optional inversion for INMODE
        IS_OPMODE_INVERTED => "000000000", -- Optional inversion for OPMODE
        IS_RSTALLCARRYIN_INVERTED => '0',  -- Optional inversion for RSTALLCARRYIN
        IS_RSTALUMODE_INVERTED => '0',     -- Optional inversion for RSTALUMODE
        IS_RSTA_INVERTED => '0',           -- Optional inversion for RSTA
        IS_RSTB_INVERTED => '0',           -- Optional inversion for RSTB
        IS_RSTCTRL_INVERTED => '0',        -- Optional inversion for RSTCTRL
        IS_RSTC_INVERTED => '0',           -- Optional inversion for RSTC
        IS_RSTD_INVERTED => '0',           -- Optional inversion for RSTD
        IS_RSTINMODE_INVERTED => '0',      -- Optional inversion for RSTINMODE
        IS_RSTM_INVERTED => '0',           -- Optional inversion for RSTM
        IS_RSTP_INVERTED => '0',           -- Optional inversion for RSTP
        -- Register Control Attributes: Pipeline Register Configuration
        ACASCREG => 1,                     -- Number of pipeline stages between A/ACIN and ACOUT (0-2)
        ADREG => 0,                        -- Pipeline stages for pre-adder (0-1)
        ALUMODEREG => 0,                   -- Pipeline stages for ALUMODE (0-1)
        AREG => 1,                         -- Pipeline stages for A (0-2)
        BCASCREG => 1,                     -- Number of pipeline stages between B/BCIN and BCOUT (0-2)
        BREG => 1,                         -- Pipeline stages for B (0-2)
        CARRYINREG => 0,                   -- Pipeline stages for CARRYIN (0-1)
        CARRYINSELREG => 0,                -- Pipeline stages for CARRYINSEL (0-1)
        CREG => 0,                         -- Pipeline stages for C (0-1)
        DREG => 0,                         -- Pipeline stages for D (0-1)
        INMODEREG => 0,                    -- Pipeline stages for INMODE (0-1)
        MREG => 0,                         -- Multiplier pipeline stages (0-1)
        OPMODEREG => 0,                    -- Pipeline stages for OPMODE (0-1)
        PREG => 0                          -- Number of pipeline stages for P (0-1)
     )
     port map (
        -- Cascade outputs: Cascade Ports
        ACOUT => ACOUTx,                   -- 30-bit output: A port cascade
        BCOUT => BCOUTx,                   -- 18-bit output: B cascade
        CARRYCASCOUT => CARRYCASCOUTx,     -- 1-bit output: Cascade carry
        MULTSIGNOUT => MULTSIGNOUTx,       -- 1-bit output: Multiplier sign cascade
        PCOUT => PCOUTx,                   -- 48-bit output: Cascade output
        -- Control outputs: Control Inputs/Status Bits
        OVERFLOW => OVERFLOWx,             -- 1-bit output: Overflow in add/acc
        PATTERNBDETECT => PATTERNBDETECTx, -- 1-bit output: Pattern bar detect
        PATTERNDETECT => PATTERNDETECTx,   -- 1-bit output: Pattern detect
        UNDERFLOW => UNDERFLOWx,           -- 1-bit output: Underflow in add/acc
        -- Data outputs: Data Ports
        CARRYOUT => CARRYOUTx,             -- 4-bit output: Carry
        P => prod_LSB,                     -- 48-bit output: Primary data
        XOROUT => XOROUTx,                 -- 8-bit output: XOR data
        -- Cascade inputs: Cascade Ports
        ACIN => ACINx,                     -- 30-bit input: A cascade data
        BCIN => BCINx,                     -- 18-bit input: B cascade
        CARRYCASCIN => '0',                -- 1-bit input: Cascade carry
        MULTSIGNIN => '0',                 -- 1-bit input: Multiplier sign cascade
        PCIN => PCINx,                     -- 48-bit input: P cascade
        -- Control inputs: Control Inputs/Status Bits
        ALUMODE => "0000",                 -- 4-bit input: ALU control
        CARRYINSEL => "000",               -- 3-bit input: Carry select
        CLK => clk,                      -- 1-bit input: Clock
        INMODE => "10001",                 -- 5-bit input: INMODE control
        OPMODE => "000000101",             -- 9-bit input: Operation mode
        -- Data inputs: Data Ports
        A(15 downto 0) => A(15 downto 0),  -- 30-bit input: A data
        A(29 downto 16) => "00000000000000",
        B(15 downto 0) => B(15 downto 0),  -- 18-bit input: B data
        B(17 downto 16) => "00",
        C => Cx,                           -- 48-bit input: C data
        CARRYIN => '0',                    -- 1-bit input: Carry-in
        D => Dx,                           -- 27-bit input: D data
        -- Reset/Clock Enable inputs: Reset/Clock Enable Inputs
        CEA1 => '1',                     -- 1-bit input: Clock enable for 1st stage AREG
        CEA2 => '1',                     -- 1-bit input: Clock enable for 2nd stage AREG
        CEAD => '1',                     -- 1-bit input: Clock enable for ADREG
        CEALUMODE => '1',           -- 1-bit input: Clock enable for ALUMODE
        CEB1 => '1',                     -- 1-bit input: Clock enable for 1st stage BREG
        CEB2 => '1',                     -- 1-bit input: Clock enable for 2nd stage BREG
        CEC => '1',                       -- 1-bit input: Clock enable for CREG
        CECARRYIN => '1',           -- 1-bit input: Clock enable for CARRYINREG
        CECTRL => '1',                 -- 1-bit input: Clock enable for OPMODEREG and CARRYINSELREG
        CED => '1',                       -- 1-bit input: Clock enable for DREG
        CEINMODE => '1',             -- 1-bit input: Clock enable for INMODEREG
        CEM => '1',                       -- 1-bit input: Clock enable for MREG
        CEP => '1',                     -- 1-bit input: Clock enable for PREG
        RSTA => RSTAx,                     -- 1-bit input: Reset for AREG
        RSTALLCARRYIN => RSTALLCARRYINx,   -- 1-bit input: Reset for CARRYINREG
        RSTALUMODE => RSTALUMODEx,         -- 1-bit input: Reset for ALUMODEREG
        RSTB => RSTBx,                     -- 1-bit input: Reset for BREG
        RSTC => RSTCx,                     -- 1-bit input: Reset for CREG
        RSTCTRL => RSTCTRLx,               -- 1-bit input: Reset for OPMODEREG and CARRYINSELREG
        RSTD => RSTDx,                     -- 1-bit input: Reset for DREG and ADREG
        RSTINMODE => RSTINMODEx,           -- 1-bit input: Reset for INMODEREG
        RSTM => RSTMx,                     -- 1-bit input: Reset for MREG
        RSTP => RSTPx                      -- 1-bit input: Reset for PREG
     );
     
     a_mult_MSB_NOCARRY : DSP48E2
     generic map (
        -- Feature Control Attributes: Data Path Selection
        AMULTSEL => "A",                   -- Selects A input to multiplier (A, AD)
        A_INPUT => "DIRECT",               -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
        BMULTSEL => "B",                   -- Selects B input to multiplier (AD, B)
        B_INPUT => "DIRECT",               -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
        PREADDINSEL => "A",                -- Selects input to pre-adder (A, B)
        RND => X"000000000000",            -- Rounding Constant
        USE_MULT => "MULTIPLY",            -- Select multiplier usage (DYNAMIC, MULTIPLY, NONE)
        USE_SIMD => "ONE48",               -- SIMD selection (FOUR12, ONE48, TWO24)
        USE_WIDEXOR => "FALSE",            -- Use the Wide XOR function (FALSE, TRUE)
        XORSIMD => "XOR24_48_96",          -- Mode of operation for the Wide XOR (XOR12, XOR24_48_96)
        -- Pattern Detector Attributes: Pattern Detection Configuration
        AUTORESET_PATDET => "NO_RESET",    -- NO_RESET, RESET_MATCH, RESET_NOT_MATCH
        AUTORESET_PRIORITY => "RESET",     -- Priority of AUTORESET vs. CEP (CEP, RESET).
        MASK => X"ffffffff0000",           -- 48-bit mask value for pattern detect (1=ignore)
        PATTERN => X"000000000000",        -- 48-bit pattern match for pattern detect
        SEL_MASK => "MASK",                -- C, MASK, ROUNDING_MODE1, ROUNDING_MODE2
        SEL_PATTERN => "PATTERN",          -- Select pattern value (C, PATTERN)
        USE_PATTERN_DETECT => "PATDET", -- Enable pattern detect (NO_PATDET, PATDET)
        -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
        IS_ALUMODE_INVERTED => "0000",     -- Optional inversion for ALUMODE
        IS_CARRYIN_INVERTED => '0',        -- Optional inversion for CARRYIN
        IS_CLK_INVERTED => '0',            -- Optional inversion for CLK
        IS_INMODE_INVERTED => "00000",     -- Optional inversion for INMODE
        IS_OPMODE_INVERTED => "000000000", -- Optional inversion for OPMODE
        IS_RSTALLCARRYIN_INVERTED => '0',  -- Optional inversion for RSTALLCARRYIN
        IS_RSTALUMODE_INVERTED => '0',     -- Optional inversion for RSTALUMODE
        IS_RSTA_INVERTED => '0',           -- Optional inversion for RSTA
        IS_RSTB_INVERTED => '0',           -- Optional inversion for RSTB
        IS_RSTCTRL_INVERTED => '0',        -- Optional inversion for RSTCTRL
        IS_RSTC_INVERTED => '0',           -- Optional inversion for RSTC
        IS_RSTD_INVERTED => '0',           -- Optional inversion for RSTD
        IS_RSTINMODE_INVERTED => '0',      -- Optional inversion for RSTINMODE
        IS_RSTM_INVERTED => '0',           -- Optional inversion for RSTM
        IS_RSTP_INVERTED => '0',           -- Optional inversion for RSTP
        -- Register Control Attributes: Pipeline Register Configuration
       ACASCREG => 1,                     -- Number of pipeline stages between A/ACIN and ACOUT (0-2)
        ADREG => 0,                        -- Pipeline stages for pre-adder (0-1)
        ALUMODEREG => 0,                   -- Pipeline stages for ALUMODE (0-1)
        AREG => 1,                         -- Pipeline stages for A (0-2)
        BCASCREG => 1,                     -- Number of pipeline stages between B/BCIN and BCOUT (0-2)
        BREG => 1,                         -- Pipeline stages for B (0-2)
        CARRYINREG => 0,                   -- Pipeline stages for CARRYIN (0-1)
        CARRYINSELREG => 0,                -- Pipeline stages for CARRYINSEL (0-1)
        CREG => 0,                         -- Pipeline stages for C (0-1)
        DREG => 0,                         -- Pipeline stages for D (0-1)
        INMODEREG => 0,                    -- Pipeline stages for INMODE (0-1)
        MREG => 0,                         -- Multiplier pipeline stages (0-1)
        OPMODEREG => 0,                    -- Pipeline stages for OPMODE (0-1)
        PREG => 0                         -- Number of pipeline stages for P (0-1)
     )
     port map (
        -- Cascade outputs: Cascade Ports
        ACOUT => ACOUTx1,                   -- 30-bit output: A port cascade
        BCOUT => BCOUTx1,                   -- 18-bit output: B cascade
        CARRYCASCOUT => CARRYCASCOUTx1,     -- 1-bit output: Cascade carry
        MULTSIGNOUT => MULTSIGNOUTx1,       -- 1-bit output: Multiplier sign cascade
        PCOUT => PCOUTx1,                   -- 48-bit output: Cascade output
        -- Control outputs: Control Inputs/Status Bits
        OVERFLOW => OVERFLOWx1,             -- 1-bit output: Overflow in add/acc
        PATTERNBDETECT => PATTERNBDETECTx1, -- 1-bit output: Pattern bar detect
        PATTERNDETECT => PATTERNDETECTx1,   -- 1-bit output: Pattern detect
        UNDERFLOW => UNDERFLOWx1,           -- 1-bit output: Underflow in add/acc
  --       Data outputs: Data Ports
        CARRYOUT => CARRYOUTx1,             -- 4-bit output: Carry
        P => prod_no_carry,
                              -- 48-bit output: Primary data
        XOROUT => XOROUTx1,                 -- 8-bit output: XOR data
        -- Cascade inputs: Cascade Ports
        ACIN => ACINx1,                     -- 30-bit input: A cascade data
        BCIN => BCINx1,                     -- 18-bit input: B cascade
        CARRYCASCIN => CARRYCASCINx1,       -- 1-bit input: Cascade carry
        MULTSIGNIN => MULTSIGNINx1,         -- 1-bit input: Multiplier sign cascade
        PCIN => PCINx1,                     -- 48-bit input: P cascade
        -- Control inputs: Control Inputs/Status Bits
       ALUMODE => "0000",               -- 4-bit input: ALU control
        CARRYINSEL => "000",         -- 3-bit input: Carry select
        CLK => clk,                       -- 1-bit input: Clock
        INMODE => "10001",                 -- 5-bit input: INMODE control
        OPMODE => "000000101",                 -- 9-bit input: Operation mode
        -- Data inputs: Data Ports
        A(15 downto 0) => A(31 downto 16),                           -- 30-bit input: A data
        A(29 downto 16) => "00000000000000",
        B(15 downto 0) => B(15 downto 0),                           -- 18-bit input: B data
        B(17 downto 16) => "00",
        C => Cx1,                           -- 48-bit input: C data
        CARRYIN => '0',               -- 1-bit input: Carry-in
        D => Dx1,                           -- 27-bit input: D data
  --       Reset/Clock Enable inputs: Reset/Clock Enable Inputs
        CEA1 => '1',                     -- 1-bit input: Clock enable for 1st stage AREG
        CEA2 => '1',                     -- 1-bit input: Clock enable for 2nd stage AREG
        CEAD => '1',                     -- 1-bit input: Clock enable for ADREG
        CEALUMODE => '1',           -- 1-bit input: Clock enable for ALUMODE
        CEB1 => '1',                     -- 1-bit input: Clock enable for 1st stage BREG
        CEB2 => '1',                     -- 1-bit input: Clock enable for 2nd stage BREG
        CEC => '1',                       -- 1-bit input: Clock enable for CREG
        CECARRYIN => '1',           -- 1-bit input: Clock enable for CARRYINREG
        CECTRL => '1',                 -- 1-bit input: Clock enable for OPMODEREG and CARRYINSELREG
        CED => '1',                       -- 1-bit input: Clock enable for DREG
        CEINMODE => '1',             -- 1-bit input: Clock enable for INMODEREG
        CEM => '1',                       -- 1-bit input: Clock enable for MREG
        CEP => '1',                     -- 1-bit input: Clock enable for PREG
        RSTA => RSTAx1,                     -- 1-bit input: Reset for AREG
        RSTALLCARRYIN => RSTALLCARRYINx1,   -- 1-bit input: Reset for CARRYINREG
        RSTALUMODE => RSTALUMODEx1,         -- 1-bit input: Reset for ALUMODEREG
        RSTB => RSTBx1,                     -- 1-bit input: Reset for BREG
        RSTC => RSTCx1,                     -- 1-bit input: Reset for CREG
        RSTCTRL => RSTCTRLx1,               -- 1-bit input: Reset for OPMODEREG and CARRYINSELREG
        RSTD => RSTDx1,                     -- 1-bit input: Reset for DREG and ADREG
        RSTINMODE => RSTINMODEx1,           -- 1-bit input: Reset for INMODEREG
        RSTM => RSTMx1,                     -- 1-bit input: Reset for MREG
        RSTP => RSTPx1                      -- 1-bit input: Reset for PREG
     );
   
  eq_zero <= PATTERNDETECTx1 AND PATTERNDETECTx;
   
   
   
END ARCHITECTURE behavior;
