
-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_interface_package.ALL;
USE work.gsnn_package.ALL;

ENTITY insn_memory_unit IS
  GENERIC(
  id  : engine_id_type
  );
  PORT(
  clock                           : IN std_logic;
  reset                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- Memory control signals
  ---------------------------------------------------------------------------
  slp_in                          : IN bit_type;
  sd_in                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- PXI signals
  ---------------------------------------------------------------------------
  en_mem_pxi_in                 : IN bit_type;
  we_mem_pxi_in                 : IN bit_type;
  addr_mem_pxi_in               : IN addr_pxi_type;
  word_mem_pxi_in               : IN bit32_type;

  en_pxi_busout                 : OUT bit_type;
  word_pxi_busout               : OUT bit32_type;
  collide_pxi_busout            : OUT bit_type;

  ---------------------------------------------------------------------------
  -- engine signals
  ---------------------------------------------------------------------------
  en_mem_engine_in              : IN bit_type;
  addr_mem_engine_in            : IN insn_addr_type;

  en_engine_busout              : OUT bit_type;
  word_engine_busout            : OUT bit32_type
  );
END ENTITY insn_memory_unit;

ARCHITECTURE behavior OF insn_memory_unit IS
-- pragma translate_off
  CONSTANT disable_print : boolean := true;
  CONSTANT start_print   : time := 0 ns;
-- pragma translate_on

  COMPONENT wrapper_engine_mem_fpga IS
    GENERIC (
      id : bit2_type
    );
    PORT (
      clk                : IN std_logic;

      ena                : IN bit_type;
      wea                : IN bit_type;
      addra              : IN bit12_type;
      worda_in           : IN bit32_type;
      worda_out          : OUT bit32_type;

      enb                : IN bit_type;
      web                : IN bit_type;
      addrb              : IN bit12_type;
      wordb_in           : IN bit32_type;
      wordb_out          : OUT bit32_type
      );
  END COMPONENT wrapper_engine_mem_fpga;

  SIGNAL en_pxi_d       : bit_type;
  SIGNAL en_pxi_q       : bit_type;

  SIGNAL en_engine_d    : bit_type;
  SIGNAL en_engine_q    : bit_type;
BEGIN
  a_mem : wrapper_engine_mem_fpga GENERIC MAP( id => id )
              PORT MAP(
                clk         =>  clock,

                ena         =>  en_mem_engine_in,
                wea         =>  "0",
                addra       =>  addr_mem_engine_in,
                worda_in    =>  (31 DOWNTO 0 => 'X'),
                worda_out   =>  word_engine_busout,

                enb         =>  en_mem_pxi_in,
                web         =>  we_mem_pxi_in,
                addrb       =>  addr_mem_pxi_in(insn_addr_type'high+2 DOWNTO 2),
                wordb_in    =>  word_mem_pxi_in,
                wordb_out   =>  word_pxi_busout
              );

  collide_pxi_busout            <= "0";

  en_pxi_d                      <= "0" WHEN reset = "1" ELSE en_mem_pxi_in;
  en_engine_d                   <= "0" WHEN reset = "1" ELSE en_mem_engine_in;
  en_pxi_q                      <= en_pxi_d     AFTER 10 ps WHEN rising_edge(clock);
  en_engine_q                   <= en_engine_d  AFTER 10 ps WHEN rising_edge(clock);
  en_pxi_busout                 <= en_pxi_q;
  en_engine_busout              <= en_engine_q;
END ARCHITECTURE behavior;
