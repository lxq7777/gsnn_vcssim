
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY cmp_fp16 IS
  PORT(
    a     : IN fp16_type;
    b     : IN fp16_type;
    gt    : OUT bit_type;
    eq    : OUT bit_type
  );
END ENTITY cmp_fp16;

ARCHITECTURE behavior OF cmp_fp16 IS
  SIGNAL  sign_a  : bit_type;
  SIGNAL  sign_b  : bit_type;
  SIGNAL  a_gtu_b : bit_type;
  SIGNAL  a_gt_b  : bit_type;
  SIGNAL  a_equ_b : bit_type;
BEGIN
  sign_a  <= a(SIGN_FP16);
  sign_b  <= b(SIGN_FP16);
  a_gtu_b <= "1" WHEN unsigned(a(14 DOWNTO 0)) > unsigned(b(14 DOWNTO 0)) ELSE "0";
  a_equ_b <= "1" WHEN a(14 DOWNTO 0) = b(14 DOWNTO 0) ELSE "0";

  gt      <= "1"  WHEN sign_a = "0" AND (sign_b = "1"  OR a_gtu_b = "1") ELSE
             "1"  WHEN sign_a = "1" AND sign_b = "1" AND a_gtu_b = "0" AND a_equ_b = "0" ELSE
             "0";
  eq      <= "1" WHEN a_equ_b = "1" AND sign_a = sign_b ELSE "0";
END ARCHITECTURE behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY max2_fp16 IS
  PORT(
    pick  : IN bit_type;
    a : IN fp16_type;
    b : IN fp16_type;
    z : OUT fp16_type
  );
END ENTITY max2_fp16;

ARCHITECTURE behavior OF max2_fp16 IS
  SIGNAL  sign_a  : bit_type;
  SIGNAL  sign_b  : bit_type;
  SIGNAL  a_gtu_b : bit_type;
  SIGNAL  a_gt_b  : bit_type;
BEGIN
  sign_a <= a(SIGN_FP16);
  sign_b <= b(SIGN_FP16);
  a_gtu_b <= "1" WHEN unsigned(a(14 DOWNTO 0)) > unsigned(b(14 DOWNTO 0)) ELSE "0";
  a_gt_b  <= "1"  WHEN sign_a = "0" AND (sign_b = "1"  OR a_gtu_b = "1") ELSE
             "1"  WHEN sign_a = "1" AND sign_b = "1" AND a_gtu_b = "0" ELSE
             "0";
  z <= a WHEN pick = "1" OR a_gt_b = "1" ELSE b;
END ARCHITECTURE behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY sigmoid_fp16 IS
  PORT(
    x       : IN fp16_type;
    z       : OUT fp16_type
  );
END ENTITY sigmoid_fp16;

ARCHITECTURE behavior OF sigmoid_fp16 IS
  TYPE sig_tbl_type IS RECORD
    init    : bit16_type;
    sh1     : bit2_type;
  END RECORD sig_tbl_type;

  SIGNAL  sign_x          : bit_type;
  SIGNAL  exp_x           : bit5_type;
  SIGNAL  mant_x          : bit10_type;
  SIGNAL  range_x         : bit7_type;

  SIGNAL  y_54_sh_val_i   : bit8_type;
  SIGNAL  y_tbl_i         : sig_tbl_type;
  SIGNAL  y_t1_i          : bit6_type;
  SIGNAL  y_t_i           : bit10_type;
  SIGNAL  y_i             : bit16_type;
  SIGNAL  neg_i           : bit10_type;
  SIGNAL  y_neg_i         : bit13_type;

  SIGNAL  z1_i            : fp16_type;
  SIGNAL  z_70_i          : fp16_type;
  SIGNAL  z_i             : fp16_type;

BEGIN
  sign_x      <=  x(SIGN_FP16);
  exp_x       <=  x(EXP_FP16);
  mant_x      <=  x(MANT_FP16);

  range_x     <=  x(14 DOWNTO 8);

  WITH exp_x SELECT
    y_54_sh_val_i <=
                  "00000001"                        WHEN "00110",
                  "0000001"&mant_x(9)               WHEN "00111",
                  "000001"&mant_x(9 DOWNTO 8)       WHEN "01000",
                  "00001"&mant_x(9 DOWNTO 7)        WHEN "01001",
                  "0001"&mant_x(9 DOWNTO 6)         WHEN "01010",
                  "001"&mant_x(9 DOWNTO 5)          WHEN "01011",
                  "01"&mant_x(9 DOWNTO 4)           WHEN "01100",
                  "1"&mant_x(9 DOWNTO 3)            WHEN "01101",
                  (OTHERS => 'X')                   WHEN OTHERS;

  WITH range_x SELECT
    y_tbl_i   <=  sig_tbl_type'( x"38c5", "10" )  WHEN "0110110", -- 54
                  sig_tbl_type'( x"38e4", "10" )  WHEN "0110111", -- 55
                  sig_tbl_type'( x"3908", "01" )  WHEN "0111000", -- 56
                  sig_tbl_type'( x"3943", "01" )  WHEN "0111001", -- 57
                  sig_tbl_type'( x"397a", "01" )  WHEN "0111010", -- 58
                  sig_tbl_type'( x"39af", "01" )  WHEN "0111011", -- 59
                  sig_tbl_type'( x"39e9", "00" )  WHEN "0111100", -- 60
                  sig_tbl_type'( x"3a42", "00" )  WHEN "0111101", -- 61
                  sig_tbl_type'( x"3a8e", "00" )  WHEN "0111110", -- 62
                  sig_tbl_type'( x"3adf", "01" )  WHEN "0111111", -- 63
                  sig_tbl_type'( x"3b1b", "00" )  WHEN "1000000", -- 64
                  sig_tbl_type'( x"3b74", "01" )  WHEN "1000001", -- 65
                  sig_tbl_type'( x"3ba3", "01" )  WHEN "1000010", -- 66
                  sig_tbl_type'( x"3bc8", "10" )  WHEN "1000011", -- 67
                  sig_tbl_type'( x"3be1", "10" )  WHEN "1000100", -- 68
                  sig_tbl_type'( x"3bef", "10" )  WHEN "1000101", -- 69
                  sig_tbl_type'( (OTHERS=>'X'), "XX") WHEN OTHERS;

  WITH y_tbl_i.sh1 SELECT
    y_t1_i    <=  mant_x(7 DOWNTO 2)        WHEN "00",
                  "0"&mant_x(7 DOWNTO 3)    WHEN "01",
                  "00"&mant_x(7 DOWNTO 4)   WHEN "10",
                  (OTHERS =>'X')            WHEN OTHERS;


  y_t_i       <=  std_logic_vector( unsigned(y_tbl_i.init(9 DOWNTO 0)) + unsigned(y_t1_i));
  y_i         <=  x"38" & y_54_sh_val_i WHEN unsigned(range_x)<54 ELSE "001110"&y_t_i;

  neg_i       <=  std_logic_vector(0-unsigned(y_i(9 DOWNTO 0)));
  y_neg_i     <=  x"d"&neg_i(8 DOWNTO 0)            WHEN y_i(9) = '0' OR y_i(9 DOWNTO 0) = "1000000000" ELSE
                  x"c"&neg_i(7 DOWNTO 0)&"0"        WHEN y_i(8) = '0' OR y_i(8 DOWNTO 0) = "100000000" ELSE
                  x"b"&neg_i(6 DOWNTO 0)&"00"       WHEN y_i(7) = '0' OR y_i(7 DOWNTO 0) = "10000000" ELSE
                  x"a"&neg_i(5 DOWNTO 0)&"000"      WHEN y_i(6) = '0' OR y_i(6 DOWNTO 0) = "1000000" ELSE
                  x"9"&neg_i(4 DOWNTO 0)&"0000"     WHEN y_i(5) = '0' OR y_i(5 DOWNTO 0) = "100000" ELSE
                  x"8"&neg_i(3 DOWNTO 0)&"00000"    WHEN y_i(4) = '0' OR y_i(4 DOWNTO 0) = "10000" ELSE
                  x"7"&neg_i(2 DOWNTO 0)&"000000"   WHEN y_i(3) = '0' OR y_i(3 DOWNTO 0) = "1000" ELSE
                  x"6"&neg_i(1 DOWNTO 0)&"0000000"  WHEN y_i(2) = '0' OR y_i(2 DOWNTO 0) = "100" ELSE
                  x"5"&neg_i(0)&"00000000";

  z1_i        <=  "00"&y_neg_i&"0" WHEN sign_x = "1" ELSE y_i;
  z_70_i      <=  x"0000" WHEN sign_x = "1" ELSE x"3c00";
  z_i         <=  z_70_i  WHEN unsigned(range_x) >= 70 ELSE
                  x"3800" WHEN unsigned(range_x) < 24 ELSE
                  z1_i;

  z           <= z_i;
END ARCHITECTURE behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY tanh_fp16 IS
  PORT(
    x       : IN fp16_type;
    z       : OUT fp16_type
  );
END ENTITY tanh_fp16;

ARCHITECTURE behavior OF tanh_fp16 IS
  TYPE tanh_tbl_type IS RECORD
    init    : bit16_type;
    sh1     : bit2_type;
  END RECORD tanh_tbl_type;

  SIGNAL  sign_x          : bit_type;
  SIGNAL  exp_x           : bit5_type;
  SIGNAL  mant_x          : bit10_type;
  SIGNAL  range_x         : bit7_type;

  SIGNAL  y_tbl_i         : tanh_tbl_type;
  SIGNAL  y_t1_i          : bit8_type;
  SIGNAL  y_t_i           : bit12_type;
  SIGNAL  y_i             : bit16_type;

  SIGNAL  z_i             : fp16_type;

BEGIN
  sign_x      <=  x(SIGN_FP16);
  exp_x       <=  x(EXP_FP16);
  mant_x      <=  x(MANT_FP16);

  range_x     <=  x(14 DOWNTO 8);

  WITH range_x SELECT
    y_tbl_i   <= 
                  tanh_tbl_type'( x"30f2", "00" )  WHEN "0110001", -- 49
                  tanh_tbl_type'( x"31e9", "00" )  WHEN "0110010", -- 50
                  tanh_tbl_type'( x"32dd", "00" )  WHEN "0110011", -- 51
                  tanh_tbl_type'( x"33e0", "00" )  WHEN "0110100", -- 52
                  tanh_tbl_type'( x"34c9", "00" )  WHEN "0110101", -- 53
                  tanh_tbl_type'( x"35a8", "00" )  WHEN "0110110", -- 54
                  tanh_tbl_type'( x"367d", "00" )  WHEN "0110111", -- 55
                  tanh_tbl_type'( x"3780", "00" )  WHEN "0111000", -- 56
                  tanh_tbl_type'( x"3882", "01" )  WHEN "0111001", -- 57
                  tanh_tbl_type'( x"391b", "01" )  WHEN "0111010", -- 58
                  tanh_tbl_type'( x"399d", "01" )  WHEN "0111011", -- 59
                  tanh_tbl_type'( x"3a30", "01" )  WHEN "0111100", -- 60
                  tanh_tbl_type'( x"3ac5", "01" )  WHEN "0111101", -- 61
                  tanh_tbl_type'( x"3b43", "10" )  WHEN "0111110", -- 62
                  tanh_tbl_type'( x"3b7f", "10" )  WHEN "0111111", -- 63
                  tanh_tbl_type'( x"3bae", "10" )  WHEN "1000000", -- 64
                  tanh_tbl_type'( x"3be6", "11" )  WHEN "1000001", -- 65
                  tanh_tbl_type'( (OTHERS=>'X'), "XX") WHEN OTHERS;

  WITH y_tbl_i.sh1 SELECT
    y_t1_i    <=  mant_x(7 DOWNTO 0)        WHEN "00",
                  "0"&mant_x(7 DOWNTO 1)    WHEN "01",
                  "00"&mant_x(7 DOWNTO 2)   WHEN "10",
                  "0000"&mant_x(7 DOWNTO 4) WHEN "11",
                  (OTHERS =>'X')            WHEN OTHERS;
  y_t_i       <=  std_logic_vector(unsigned(y_tbl_i.init(11 DOWNTO 0)) + unsigned(y_t1_i));

  y_i         <=  x       WHEN unsigned(range_x) < 49 ELSE
                  x"3c00" WHEN unsigned(range_x) > 65 ELSE
                  x"3"&y_t_i;

  z_i         <=  sign_x & y_i(14 DOWNTO 0);

  z           <= z_i;
END ARCHITECTURE behavior;

-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY post_unit IS
  PORT(
  clock                           : IN std_logic;
  reset                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- engine signals
  ---------------------------------------------------------------------------
  en_cmd_in                       : IN bit_type;
  insn_cmd_in                     : IN insn_type;
  ctl_cmd_in                      : IN bit16_type;
  ctr_cmd_in                      : IN bit32_type;
  base0_cmd_in                    : IN bit32_type;
  base1_cmd_in                    : IN bit32_type;

  ---------------------------------------------------------------------------
  -- memory signals
  ---------------------------------------------------------------------------
  en_mem_data_busout              : OUT bit_type;
  addr_mem_data_busout            : OUT global_addr_type;
  word_data_in                    : IN bit128_type;

  en_mem_res_busout               : OUT bit_type;
  wr4_mem_res_busout              : OUT bit_type;
  addr_mem_res_busout             : OUT global_addr_type;
  word_mem_res_busout             : OUT bit64_type
  );
END ENTITY post_unit;

ARCHITECTURE behavior OF post_unit IS
-- pragma translate_off
  CONSTANT disable_print : boolean := false;
  CONSTANT start_print   : time := 0 ns;
-- pragma translate_on

  COMPONENT cmp_fp16 IS
    PORT(
      a     : IN fp16_type;
      b     : IN fp16_type;
      gt    : OUT bit_type;
      eq    : OUT bit_type
    );
  END COMPONENT cmp_fp16;

  COMPONENT max2_fp16 IS
    PORT(
      pick  : IN bit_type;
      a : IN fp16_type;
      b : IN fp16_type;
      z : OUT fp16_type
    );
  END COMPONENT max2_fp16;

  COMPONENT sigmoid_fp16 IS
    PORT(
      x       : IN fp16_type;
      z       : OUT fp16_type
    );
  END COMPONENT sigmoid_fp16;

  COMPONENT tanh_fp16 IS
    PORT(
      x       : IN fp16_type;
      z       : OUT fp16_type
    );
  END COMPONENT tanh_fp16;

  SIGNAL  en_cmd_in0     : bit_type;
  SIGNAL  en_cmd_q0     : bit_type;
  SIGNAL  insn_cmd_q0   : insn_type;
  SIGNAL  ctl_cmd_q0    : bit16_type;
  SIGNAL  ctr_cmd_q0    : bit32_type;
  SIGNAL  base0_cmd_q0  : global_addr_type;

  SIGNAL  en_cmd_i0     : bit_type;
  SIGNAL  en_cmd_q1     : bit_type;
  SIGNAL  insn_cmd_q1   : insn_type;
  SIGNAL  ctl_cmd_q1    : bit16_type;
  SIGNAL  ctr_cmd_q1    : bit32_type;
  SIGNAL  base0_cmd_q1  : global_addr_type;

  SIGNAL  en_cmd_i1     : bit_type;
  SIGNAL  en_cmd_q2     : bit_type;
  SIGNAL  insn_cmd_q2   : insn_type;
  SIGNAL  ctl_cmd_q2    : bit16_type;
  SIGNAL  ctr_cmd_q2    : bit32_type;
  SIGNAL  base0_cmd_q2  : global_addr_type;

  SIGNAL  en_cmd_i2     : bit_type;
  SIGNAL  en_cmd_q3     : bit_type;
  SIGNAL  insn_cmd_q3   : insn_type;
  SIGNAL  ctl_cmd_q3    : bit16_type;
  SIGNAL  ctr_cmd_q3    : bit32_type;
  SIGNAL  base0_cmd_q3  : global_addr_type;

  SIGNAL  en_cmd_i3     : bit_type;
  SIGNAL  en_cmd_q4     : bit_type;
  SIGNAL  insn_cmd_q4   : insn_type;
  SIGNAL  ctl_cmd_q4    : bit16_type;
  SIGNAL  ctr_cmd_q4    : bit32_type;
  SIGNAL  base0_cmd_q4  : global_addr_type;
  SIGNAL  word_data_q4  : array_fp16_type(0 TO 7);

  SIGNAL  max_i4        : array_fp16_type(0 TO 3);
  SIGNAL  first_i4      : bit_type;
  SIGNAL  last_i4       : bit_type;

  SIGNAL  soft_count_q  : bit14_type;
  SIGNAL  soft_max_q    : fp16_type;
  SIGNAL  soft_pos_q    : bit16_type;
  SIGNAL  soft_ceil_q   : fp16_type;
  SIGNAL  soft_after_q  : bit16_type;


  SIGNAL  en_cmd_i4     : bit_type;
  SIGNAL  en_cmd_q5     : bit_type;
  SIGNAL  base0_cmd_q5  : global_addr_type;
  SIGNAL  insn_cmd_q5   : insn_type;
  SIGNAL  ctl_cmd_q5    : bit16_type;
  SIGNAL  first_q5      : bit_type;
  SIGNAL  last_q5       : bit_type;
  SIGNAL  max_q5        : array_fp16_type(0 TO 3); 
  SIGNAL  reg_q         : array_fp16_type(0 TO 3); 
  SIGNAL  out_i5        : array_fp16_type(0 TO 3);
  SIGNAL  reg_i5        : array_fp16_type(0 TO 3);
  SIGNAL  en_reg_i5     : bit_type;
  SIGNAL  en_soft_i5    : bit_type;

  SIGNAL  en_res_i5     : bit_type;
  SIGNAL  wr4_res_i5    : bit_type;
  SIGNAL  word_res_i5   : bit64_type;
BEGIN
  en_cmd_in0             <= "0" WHEN reset = "1" ELSE en_cmd_in;
  en_mem_data_busout    <= en_cmd_in0                           AFTER 10 ps WHEN rising_edge(clock);
  addr_mem_data_busout  <= base1_cmd_in(global_addr_type'range) AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in0 = "1";

  en_cmd_q0             <= en_cmd_in0                           AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q0           <= insn_cmd_in                          AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in0 = "1";
  ctl_cmd_q0            <= ctl_cmd_in                           AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in0 = "1";
  ctr_cmd_q0            <= ctr_cmd_in                           AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in0 = "1";
  base0_cmd_q0          <= base0_cmd_in(global_addr_type'range) AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in0 = "1";

  en_cmd_i0             <= "0" WHEN reset = "1" ELSE en_cmd_q0;
  en_cmd_q1             <= en_cmd_i0 AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q1           <= insn_cmd_q0 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1";
  ctl_cmd_q1            <= ctl_cmd_q0 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1";
  ctr_cmd_q1            <= ctr_cmd_q0 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1";
  base0_cmd_q1          <= base0_cmd_q0 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1";

  en_cmd_i1             <= "0" WHEN reset = "1" ELSE en_cmd_q1;
  en_cmd_q2             <= en_cmd_i1 AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q2           <= insn_cmd_q1 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1";
  ctl_cmd_q2            <= ctl_cmd_q1 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1";
  ctr_cmd_q2            <= ctr_cmd_q1 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1";
  base0_cmd_q2          <= base0_cmd_q1 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1";

  en_cmd_i2             <= "0" WHEN reset = "1" ELSE en_cmd_q2;
  en_cmd_q3             <= en_cmd_i2 AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q3           <= insn_cmd_q2 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i2 = "1";
  ctl_cmd_q3            <= ctl_cmd_q2 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i2 = "1";
  ctr_cmd_q3            <= ctr_cmd_q2 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i2 = "1";
  base0_cmd_q3          <= base0_cmd_q2 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i2 = "1";

  en_cmd_i3             <= "0" WHEN reset = "1" ELSE en_cmd_q3;
  en_cmd_q4             <= en_cmd_i3 AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q4           <= insn_cmd_q3 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i3 = "1";
  ctl_cmd_q4            <= ctl_cmd_q3 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i3 = "1";
  ctr_cmd_q4            <= ctr_cmd_q3 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i3 = "1";
  base0_cmd_q4          <= base0_cmd_q3 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i3 = "1";

  word_gen: FOR i IN 0 TO 7 GENERATE
    SIGNAL  w_i4    : fp16_type;
  BEGIN
    w_i4 <= word_data_in(16*i+15 DOWNTO 16*i);
    word_data_q4(i) <= w_i4;
  END GENERATE word_gen;

  pool_block: IF true GENERATE 
    SIGNAL  pick_bi3       : bit_type;
    SIGNAL  max2_bi3       : array_fp16_type(0 TO 3);
    SIGNAL  max3_bi3       : array_fp16_type(0 TO 1);
    SIGNAL  max4_bi3       : array_fp16_type(0 TO 1);
    SIGNAL  rows_bi3       : cols_post_type;
  BEGIN
    pick_bi3 <= "1" WHEN ctl_cmd_q4(KIND_POST_INSN_FIELD) = PICK_KIND_POST ELSE "0";
    mx2_gen: FOR i IN 0 TO 3 GENERATE
      a_mx2_max:  max2_fp16 PORT MAP( pick_bi3, word_data_q4(2*i+0), word_data_q4(2*i+1), max2_bi3(i));
    END GENERATE mx2_gen;
    a_mx3_max0: max2_fp16 PORT MAP( pick_bi3, max2_bi3(0), word_data_q4(2), max3_bi3(0));
    a_mx3_max1: max2_fp16 PORT MAP( pick_bi3, word_data_q4(3), max2_bi3(3), max3_bi3(1));
    a_mx4_max0: max2_fp16 PORT MAP( pick_bi3, max2_bi3(0), max2_bi3(1), max4_bi3(0));
    a_mx4_max1: max2_fp16 PORT MAP( pick_bi3, max2_bi3(2), max2_bi3(3), max4_bi3(1));

    WITH ctl_cmd_q4(COLS_POST_INSN_FIELD) SELECT
      max_i4(0) <=
                  word_data_q4(0)     WHEN C1_COLS_POST,
                  max2_bi3(0)         WHEN C2_COLS_POST,
                  max3_bi3(0)         WHEN C3_COLS_POST,
                  max4_bi3(0)         WHEN C4_COLS_POST,
                  (OTHERS=>'X')       WHEN OTHERS;

    WITH ctl_cmd_q4(COLS_POST_INSN_FIELD) SELECT
      max_i4(1) <=
                  word_data_q4(1)     WHEN C1_COLS_POST,
                  max2_bi3(1)         WHEN C2_COLS_POST,
                  max3_bi3(1)         WHEN C3_COLS_POST,
                  max4_bi3(1)         WHEN C4_COLS_POST,
                  (OTHERS=>'X')       WHEN OTHERS;

    WITH ctl_cmd_q4(COLS_POST_INSN_FIELD) SELECT
      max_i4(2) <=
                  word_data_q4(2)     WHEN C1_COLS_POST,
                  max2_bi3(2)          WHEN C2_COLS_POST,
                  (OTHERS=>'X')       WHEN OTHERS;

    WITH ctl_cmd_q4(COLS_POST_INSN_FIELD) SELECT
      max_i4(3) <=
                  word_data_q4(3)     WHEN C1_COLS_POST,
                  max2_bi3(3)          WHEN C2_COLS_POST,
                  (OTHERS=>'X')       WHEN OTHERS;

    rows_bi3   <=  ctl_cmd_q4(ROWS_POST_INSN_FIELD);
    first_i4  <=  "1" WHEN rows_bi3 = R1_ROWS_POST ELSE
                  "1" WHEN rows_bi3 = R2_ROWS_POST AND ctr_cmd_q4(0) = '1' ELSE
                  -- FIXME: R3_ROWS
                  "1" WHEN rows_bi3 = R4_ROWS_POST AND ctr_cmd_q4(1 DOWNTO 0) = "11" ELSE
                  "0";
    last_i4  <=  "1" WHEN rows_bi3 = R1_ROWS_POST ELSE
                  "1" WHEN rows_bi3 = R2_ROWS_POST AND ctr_cmd_q4(0) = '0' ELSE
                  -- FIXME: R3_ROWS
                  "1" WHEN rows_bi3 = R4_ROWS_POST AND ctr_cmd_q4(1 DOWNTO 0) = "00" ELSE
                  "0";
  END GENERATE pool_block;

  soft_block: IF true GENERATE
    SIGNAL  en_soft_bi3       : bit_type;
    SIGNAL  ctl_bi3           : ctl_post_type;
    SIGNAL  is_first_bi3      : bit_type;
    SIGNAL  is_init_bi3       : bit_type;
    SIGNAL  en_ceil_after_bi3 : bit_type;
    SIGNAL  ceil_bi3          : fp16_type;
    SIGNAL  after_bi3         : fp16_type;
    SIGNAL  count_bi3         : bit14_type;
    SIGNAL  count_d_bi3       : bit14_type;

    SIGNAL  count_gt_bi3      : bit_type;
    SIGNAL  count_eq_bi3      : bit_type;
    SIGNAL  valid_bi3         : bit4_type;
    SIGNAL  gt10_bi3          : bit_type;
    SIGNAL  gt32_bi3          : bit_type;
    SIGNAL  pos10_bi3         : bit_type;
    SIGNAL  max10_bi3         : fp16_type;
    SIGNAL  pos32_bi3         : bit_type;
    SIGNAL  max32_bi3         : fp16_type;
    SIGNAL  pos32_10_bi3      : bit_type;
    SIGNAL  valid32_10_bi3    : bit2_type;
    SIGNAL  gt32_10_bi3       : bit_type;
    SIGNAL  gt32_mx1_bi3      : bit_type;
    SIGNAL  gt10_mx1_bi3      : bit_type;
    SIGNAL  gt32_mx_bi3       : bit_type;
    SIGNAL  gt10_mx_bi3       : bit_type;
    SIGNAL  en_max_pos_bi3    : bit_type;
    SIGNAL  soft_max_d_bi3    : fp16_type;
    SIGNAL  soft_pos_d_bi3    : bit16_type;

  BEGIN
    en_soft_bi3   <=  "1" WHEN en_cmd_i4 = "1" AND ctl_cmd_q4(KIND_POST_INSN_FIELD) = SOFT_KIND_POST ELSE "0";

    ctl_bi3       <=  ctl_cmd_q4(CTL_POST_INSN_FIELD);
    is_init_bi3   <=  "1" WHEN ctl_bi3 = INIT_CTL_POST ELSE "0";
    is_first_bi3  <=  "1" WHEN ctl_bi3 = FIRST_CTL_POST ELSE "0";

    WITH ctl_bi3 SELECT
      ceil_bi3    <=  x"7fff"       WHEN INIT_CTL_POST,
                      soft_max_q    WHEN FIRST_CTL_POST,
                      soft_ceil_q   WHEN OTHERS;

    WITH ctl_bi3 SELECT
      after_bi3   <=  (OTHERS=>'0') WHEN INIT_CTL_POST,
                      soft_pos_q    WHEN FIRST_CTL_POST,
                      soft_after_q  WHEN OTHERS;

    en_ceil_after_bi3   <= en_soft_bi3 AND (is_first_bi3 OR is_init_bi3);
    soft_ceil_q   <= ceil_bi3 AFTER 10 ps WHEN rising_edge(clock) AND en_ceil_after_bi3 = "1";
    soft_after_q  <= after_bi3 AFTER 10 ps WHEN rising_edge(clock) AND en_ceil_after_bi3 = "1";

    WITH ctl_bi3 SELECT
      count_bi3   <=  (OTHERS=>'0') WHEN INIT_CTL_POST|FIRST_CTL_POST,
                      soft_count_q  WHEN OTHERS;
    count_d_bi3   <= std_logic_vector(unsigned(count_bi3) + 1);
    soft_count_q  <= count_d_bi3 AFTER 10 ps WHEN rising_edge(clock) AND en_soft_bi3 = "1";

    count_gt_bi3  <= "X"  WHEN ctl_bi3 = INIT_CTL_POST ELSE
                     "0"  WHEN ctl_bi3 = FIRST_CTL_POST ELSE
                     "1"  WHEN unsigned(soft_count_q) > unsigned(soft_after_q(15 DOWNTO 2)) ELSE
                     "0";
    count_eq_bi3  <= "X"  WHEN ctl_bi3 = INIT_CTL_POST ELSE
                     "1"  WHEN soft_pos_q(15 DOWNTO 2) = (15 DOWNTO 2 => '0')  AND ctl_bi3 = FIRST_CTL_POST ELSE
                     "0"  WHEN ctl_bi3 = FIRST_CTL_POST ELSE
                     "1"  WHEN soft_count_q = soft_after_q(15 DOWNTO 2) ELSE
                     "0";

    ----------- FIRST LAYER ------
    a_cmp_10: cmp_fp16 PORT MAP(word_data_q4(1), word_data_q4(0), gt10_bi3, OPEN);
    a_cmp_32: cmp_fp16 PORT MAP(word_data_q4(3), word_data_q4(2), gt32_bi3, OPEN);

    a_cmp_gen: FOR i IN 0 TO 3 GENERATE
      SIGNAL  gt_gbi3 : bit_type;
      SIGNAL  eq_gbi3 : bit_type;
    BEGIN
      a_cmp_ceil: cmp_fp16 PORT MAP(ceil_bi3, word_data_q4(i), gt_gbi3, eq_gbi3);

      valid_bi3(i)  <=  '1' WHEN ctl_bi3 = INIT_CTL_POST ELSE
                        '1' WHEN gt_gbi3 = "1" ELSE
                        '1' WHEN eq_gbi3 = "1" AND count_gt_bi3 = "1" ELSE
                        '1' WHEN eq_gbi3 = "1" AND count_eq_bi3 = "1" AND i > unsigned(after_bi3(1 DOWNTO 0)) ELSE
                        '0';
    END GENERATE a_cmp_gen;

    pos10_bi3 <= "0" WHEN valid_bi3(1 DOWNTO 0) = "01" ELSE
                 "1" WHEN valid_bi3(1 DOWNTO 0) = "10" ELSE
                 gt10_bi3 WHEN valid_bi3(1 DOWNTO 0) = "11" ELSE
                 "X";
    WITH pos10_bi3 SELECT
      max10_bi3 <= word_data_q4(0) WHEN "0",
                   word_data_q4(1) WHEN "1",
                   (OTHERS => 'X') WHEN OTHERS;
    valid32_10_bi3(0) <= '0' WHEN valid_bi3(1 DOWNTO 0) = "00" ELSE '1';

    pos32_bi3 <= "0" WHEN valid_bi3(3 DOWNTO 2) = "01" ELSE
                 "1" WHEN valid_bi3(3 DOWNTO 2) = "10" ELSE
                 gt32_bi3 WHEN valid_bi3(3 DOWNTO 2) = "11" ELSE
                 "X";
    WITH pos32_bi3 SELECT
      max32_bi3 <= word_data_q4(2) WHEN "0",
                   word_data_q4(3) WHEN "1",
                   (OTHERS => 'X') WHEN OTHERS;
    valid32_10_bi3(1) <= '0' WHEN valid_bi3(3 DOWNTO 2) = "00" ELSE '1';

    ----------- SECOND LAYER ------
    a_cmp32_10: cmp_fp16 PORT MAP(max32_bi3, max10_bi3, gt32_10_bi3, OPEN);
    a_cmp10_mx: cmp_fp16 PORT MAP(max10_bi3, soft_max_q, gt10_mx1_bi3, OPEN);
    a_cmp32_mx: cmp_fp16 PORT MAP(max32_bi3, soft_max_q, gt32_mx1_bi3, OPEN);

    gt10_mx_bi3   <= "1" WHEN is_first_bi3 = "1" OR is_init_bi3 = "1" ELSE gt10_mx1_bi3;
    gt32_mx_bi3   <= "1" WHEN is_first_bi3 = "1" OR is_init_bi3 = "1" ELSE gt32_mx1_bi3;

    pos32_10_bi3  <=  "0"         WHEN valid32_10_bi3 = "01" ELSE
                      "1"         WHEN valid32_10_bi3 = "10" ELSE
                      gt32_10_bi3 WHEN valid32_10_bi3 = "11" ELSE
                      "X";
    en_max_pos_bi3 <= 
                      "0"   WHEN en_soft_bi3 = "0" ELSE
                      "1"   WHEN (gt10_mx_bi3 = "1" AND valid32_10_bi3(0) = '1')  ELSE
                      "1"   WHEN (gt32_mx_bi3 = "1" AND valid32_10_bi3(1) = '1')  ELSE
                      "1"   WHEN is_first_bi3  = "1" ELSE
                      "0";
    soft_max_d_bi3  <=  max10_bi3 WHEN gt10_mx_bi3 = "1" AND pos32_10_bi3 = "0" AND valid32_10_bi3(0) = '1' ELSE
                    max32_bi3 WHEN gt32_mx_bi3 = "1" AND pos32_10_bi3 = "1" AND valid32_10_bi3(1) = '1' ELSE
                    x"FFFF"   WHEN is_first_bi3 = "1" ELSE
                    (OTHERS=>'X');
    soft_pos_d_bi3  <=  count_bi3 & "0" & pos10_bi3 WHEN gt10_mx_bi3 = "1" AND pos32_10_bi3 = "0" AND valid32_10_bi3(0) = '1' ELSE
                    count_bi3 & "1" & pos32_bi3 WHEN gt32_mx_bi3 = "1" AND pos32_10_bi3 = "1" AND valid32_10_bi3(1) = '1' ELSE
                    (OTHERS=>'0') WHEN is_first_bi3 = "1" ELSE
                    soft_pos_q;
    soft_max_q  <=  soft_max_d_bi3  AFTER 10 ps WHEN rising_edge(clock) AND en_max_pos_bi3 = "1";
    soft_pos_q  <=  soft_pos_d_bi3  AFTER 10 ps WHEN rising_edge(clock) AND en_max_pos_bi3 = "1";

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 101 ps;

    IF now >= start_print THEN

      IF en_soft_bi3 = "1" THEN
        dump_cycle( buf, "PST>" );
        dump_insn( buf, insn_cmd_q5, " ");
        dump_hex( buf, ctl_cmd_q5, " ctl ");
        dump_hex( buf, soft_max_q, " soft_max_q ");
        dump_hex( buf, soft_pos_q, " soft_pos_q ");
        dump_hex( buf, soft_ceil_q, " soft_ceil_q ");
        dump_hex( buf, soft_after_q, " soft_after_q ");
        dump_hex( buf, soft_count_q, " soft_count_q ");
        writeline(output, buf );
      END IF;
    END IF;

  END PROCESS dump;
-- pragma translate_on
  END GENERATE soft_block;

  en_cmd_i4     <= "0" WHEN reset = "1" ELSE en_cmd_q4;
  en_cmd_q5     <= en_cmd_i4 AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q5   <= insn_cmd_q4 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i4 = "1";
  ctl_cmd_q5    <= ctl_cmd_q4 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i4 = "1";
  base0_cmd_q5  <= base0_cmd_q4 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i4 = "1";
  max_q5        <= max_i4 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i4 = "1";
  first_q5      <= first_i4 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i4 = "1";
  last_q5       <= last_i4 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i4 = "1";
  en_soft_i5    <= "1" WHEN ctl_cmd_q5(KIND_POST_INSN_FIELD) = SOFT_KIND_POST ELSE "0";


  en_reg_i5     <= "1" WHEN en_cmd_q5 = "1" AND en_soft_i5 = "0" AND last_q5 = "0" ELSE "0";
  acc_max_gen: FOR i IN 0 TO 3 GENERATE
    SIGNAL  mxr_i5  : fp16_type;
    SIGNAL  sig_i5  : fp16_type;
    SIGNAL  tanh_i5 : fp16_type;
  BEGIN
    a_max:  max2_fp16 PORT MAP( "0", reg_q(i), max_q5(i), mxr_i5 );
    reg_i5(i) <= max_q5(i) WHEN first_q5 = "1" ELSE mxr_i5;
    reg_q(i)  <= reg_i5(i) AFTER 10 ps WHEN rising_edge(clock) AND en_reg_i5 = "1";

    a_sig:  sigmoid_fp16 PORT MAP( reg_i5(i), sig_i5);
    a_tanh:  tanh_fp16 PORT MAP( reg_i5(i), tanh_i5);
    out_i5(i) <= (OTHERS => '0')  WHEN ctl_cmd_q5(ACT_POST_INSN_FIELD) = RECT_ACT_POST AND reg_i5(i)(SIGN_FP16) = "1" ELSE
                 sig_i5           WHEN ctl_cmd_q5(ACT_POST_INSN_FIELD) = SIG_ACT_POST ELSE
                 tanh_i5          WHEN ctl_cmd_q5(ACT_POST_INSN_FIELD) = TANH_ACT_POST ELSE
                 reg_i5(i);
  END GENERATE acc_max_gen;

  en_res_i5           <= "0" WHEN en_cmd_q5 = "0" ELSE
                         "1" WHEN en_soft_i5 = "0" AND last_q5 = "1"  ELSE
                         "1" WHEN en_soft_i5 = "1" AND ctl_cmd_q5(CTL_POST_INSN_FIELD) = LAST_CTL_POST  ELSE
                         "0";
  wr4_res_i5          <= "0" WHEN en_soft_i5 = "1" ELSE
                         "1" WHEN (ctl_cmd_q5(COLS_POST_INSN_FIELD) = C1_COLS_POST OR ctl_cmd_q5(COLS_POST_INSN_FIELD) = C2_COLS_POST) ELSE
                         "0";
  word_res_i5         <= out_i5(3) & out_i5(2) & out_i5(1) & out_i5(0) WHEN en_soft_i5 = "0" ELSE
                         (63 DOWNTO 32 => 'X') & soft_pos_q & soft_max_q;
  en_mem_res_busout   <= en_res_i5;
  wr4_mem_res_busout  <= wr4_res_i5;
  addr_mem_res_busout <= base0_cmd_q5;
  word_mem_res_busout <= word_res_i5;

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 101 ps;

    IF now >= start_print THEN

      IF false THEN
        writeline(output, buf );
        dump_cycle( buf, "PST>");
        dump_bit( buf, en_cmd_i0, " en_cmd_i0 ");
        dump_bit( buf, en_cmd_q4, " en_cmd_q4 ");

        dump_bit( buf, en_cmd_i4, " en_cmd_i4 ");
        dump_bit( buf, first_i4, " first_i4 ");
        dump_bit( buf, last_i4, " last_i4 ");

        dump_bit( buf, en_cmd_q5, " en_cmd_q5 ");
        dump_bit( buf, first_q5, " first_q5 ");
        dump_bit( buf, last_q5, " last_q5 ");
        dump_bit( buf, en_reg_i5, " en_reg_i5 ");
        writeline(output, buf );
      END IF;
      IF en_cmd_in = "1" THEN
        dump_cycle( buf, "PST>");
        dump_insn( buf, insn_cmd_in, " " );
        dump_hex( buf, ctl_cmd_in, " ctl " );
        dump_hex( buf, ctr_cmd_in, " $ctr " );
        dump_hex( buf, base0_cmd_in, " $base0 " );
        dump_hex( buf, base1_cmd_in, " $base1 " );
        writeline(output, buf );
      END IF;

      IF en_cmd_i4 = "1" THEN
        dump_cycle( buf, "PST>");
        dump_string( buf, " max_i4 ");
        FOR i IN max_i4'range LOOP
          dump_integer( buf, i, " #" );
          dump_hex( buf, max_i4(i), " ");
        END LOOP;
        writeline(output, buf );
      END IF;

      IF en_cmd_q5 = "1" AND last_q5 = "1" THEN
        dump_cycle( buf, "PST>");
        dump_hex( buf, base0_cmd_q5, " base0_cmd_q5 " );
        dump_string( buf, " out_i5 ");
        FOR i IN out_i5'range LOOP
          dump_integer( buf, i, " #" );
          dump_hex( buf, out_i5(i), " ");
        END LOOP;
        writeline(output, buf );
      END IF;

      IF en_reg_i5 = "1" THEN
        dump_cycle( buf, "PST>");
        dump_string( buf, " reg_i5 ");
        FOR i IN reg_i5'range LOOP
          dump_integer( buf, i, " #" );
          dump_hex( buf, reg_i5(i), " ");
        END LOOP;
        writeline(output, buf );
      END IF;

      IF en_res_i5 = "1" THEN
        dump_cycle( buf, "PST>" );
        dump_bit( buf, en_res_i5, " en_res_i5 ");
        dump_bit( buf, wr4_res_i5, " wr4_res_i5 ");
        dump_hex( buf, base0_cmd_q5, " base0_cmd_q5 ");
        dump_hex( buf, word_res_i5, " word_res_i5 ");
        dump_bit( buf, en_soft_i5, " en_soft_i5 ");
        writeline(output, buf );
      END IF;

      IF en_cmd_i0 = "1" THEN
        dump_cycle( buf, "PST>" );
        dump_bit( buf, en_cmd_i0, " en_mem_data_busout ");
        dump_hex( buf, base1_cmd_in, " addr_mem_data_busout ");
        writeline(output, buf );
      END IF;

      IF en_res_i5 = "1" THEN
        dump_cycle( buf, "PST>" );
        dump_bit( buf, en_res_i5, " en_mem_res_busout ");
        dump_bit( buf, wr4_res_i5, " wr4_mem_res_busout ");
        dump_hex( buf, base0_cmd_q5, " addr_mem_res_busout ");
        writeline(output, buf );
      END IF;

    END IF;

  END PROCESS dump;
-- pragma translate_on

END ARCHITECTURE behavior;
