-- pragma translate_off
USE std.textio.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
-- pragma translate_off
USE work.dump_package.ALL;
-- pragma translate_on

PACKAGE gsnn_package IS

  ----------------------------------------------------------------
  -- common types
  ----------------------------------------------------------------
  SUBTYPE bit_type                IS std_logic_vector(0 DOWNTO 0);
  TYPE array_bit_type             IS ARRAY(natural RANGE<>) OF bit_type;
  SUBTYPE bit2_type               IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE bit3_type               IS std_logic_vector(2 DOWNTO 0);
  SUBTYPE bit4_type               IS std_logic_vector(3 DOWNTO 0);
  TYPE array_bit4_type            IS ARRAY(natural RANGE<>) OF bit4_type;
  SUBTYPE bit5_type               IS std_logic_vector(4 DOWNTO 0);
  SUBTYPE bit6_type               IS std_logic_vector(5 DOWNTO 0);
  SUBTYPE bit7_type               IS std_logic_vector(6 DOWNTO 0);
  TYPE array_bit7_type            IS ARRAY(natural RANGE<>) OF bit7_type;
  SUBTYPE bit8_type               IS std_logic_vector(7 DOWNTO 0);
  SUBTYPE bit9_type               IS std_logic_vector(8 DOWNTO 0);
  SUBTYPE bit10_type              IS std_logic_vector(9 DOWNTO 0);
  SUBTYPE bit11_type              IS std_logic_vector(10 DOWNTO 0);
  TYPE array_bit11_type           IS ARRAY(natural RANGE<>) OF bit11_type;
  TYPE array2_bit11_type           IS ARRAY(natural RANGE<>, natural RANGE<>) OF bit11_type;
  SUBTYPE bit12_type              IS std_logic_vector(11 DOWNTO 0);
  SUBTYPE bit13_type              IS std_logic_vector(12 DOWNTO 0);
  SUBTYPE bit14_type              IS std_logic_vector(13 DOWNTO 0);
  SUBTYPE bit15_type              IS std_logic_vector(14 DOWNTO 0);
  SUBTYPE bit16_type              IS std_logic_vector(15 DOWNTO 0);
  TYPE array_bit16_type           IS ARRAY(natural RANGE<>) OF bit16_type;
  SUBTYPE bit20_type              IS std_logic_vector(19 DOWNTO 0);
  SUBTYPE bit22_type              IS std_logic_vector(21 DOWNTO 0);
  SUBTYPE bit31_type              IS std_logic_vector(30 DOWNTO 0);
  SUBTYPE bit32_type              IS std_logic_vector(31 DOWNTO 0);
  TYPE array_bit32_type           IS ARRAY(natural RANGE<>) OF bit32_type;
  TYPE array2_bit32_type           IS ARRAY(natural RANGE<>, natural RANGE<>) OF bit32_type;
  SUBTYPE bit42_type              IS std_logic_vector(41 DOWNTO 0);
  SUBTYPE bit43_type              IS std_logic_vector(42 DOWNTO 0);
  SUBTYPE bit44_type              IS std_logic_vector(43 DOWNTO 0);
  SUBTYPE bit48_type              IS std_logic_vector(47 DOWNTO 0);
  SUBTYPE bit53_type              IS std_logic_vector(52 DOWNTO 0);
  SUBTYPE bit59_type              IS std_logic_vector(58 DOWNTO 0);
  TYPE array_bit59_type           IS ARRAY(natural RANGE<>) OF bit59_type;
  SUBTYPE bit64_type              IS std_logic_vector(63 DOWNTO 0);
  TYPE array_bit64_type           IS ARRAY(natural RANGE<>) OF bit64_type;
  TYPE array2_bit64_type           IS ARRAY(natural RANGE<>, natural RANGE<>) OF bit64_type;
  SUBTYPE bit66_type              IS std_logic_vector(65 DOWNTO 0);
  TYPE array_bit66_type           IS ARRAY(natural RANGE<>) OF bit66_type;
  SUBTYPE bit96_type              IS std_logic_vector(95 DOWNTO 0);
  TYPE array_bit96_type           IS ARRAY(natural RANGE<>) OF bit96_type;
  SUBTYPE bit128_type             IS std_logic_vector(127 DOWNTO 0);
  TYPE array_bit128_type          IS ARRAY(natural RANGE<>) OF bit128_type;
  SUBTYPE bit144_type             IS std_logic_vector(143 DOWNTO 0);
  TYPE array_bit144_type          IS ARRAY(natural RANGE<>) OF bit144_type;
  SUBTYPE bit256_type             IS std_logic_vector(255 DOWNTO 0);

  SUBTYPE fp16_type               IS std_logic_vector(15 DOWNTO 0);
  SUBTYPE SIGN_FP16               IS natural RANGE 15 DOWNTO 15;
  SUBTYPE EXP_FP16                IS natural RANGE 14 DOWNTO 10;
  SUBTYPE MANT_FP16               IS natural RANGE 9 DOWNTO 0;
  TYPE array_fp16_type            IS ARRAY(natural RANGE<>) OF fp16_type;
  TYPE array2_fp16_type           IS ARRAY(natural RANGE<>, natural RANGE<>) OF fp16_type;
  TYPE array3_fp16_type           IS ARRAY(natural RANGE<>, natural RANGE<>, natural RANGE<>) OF fp16_type;

  ----------------------------------------------------------------
  -- global defines
  ----------------------------------------------------------------
  CONSTANT LG2_NUM_SLICES         : integer := 3;
  CONSTANT NUM_SLICES             : integer := 8;
  SUBTYPE SLICE_RANGE             IS natural RANGE 0 TO NUM_SLICES-1;
  SUBTYPE slice_type              IS std_logic_vector(NUM_SLICES-1 DOWNTO 0);
  TYPE array_slice_type           IS ARRAY( natural RANGE<>) OF slice_type;

  CONSTANT BANKS_MEM_SLICE        : integer := 16; -- this is even/odd, so actually twice as many
  SUBTYPE BANKS_MEM_RANGE         IS natural RANGE 0 TO BANKS_MEM_SLICE-1;
  SUBTYPE bank_type               IS std_logic_vector(BANKS_MEM_SLICE-1 DOWNTO 0);
  TYPE array_bank_type            IS ARRAY( natural RANGE<>) OF bank_type;

  CONSTANT LG2_BYTES_SLICE        : integer := 18;
  CONSTANT SIZE_FILTER            : integer := 3; -- 3x3
  CONSTANT NUM_FILTERS            : integer := 4; -- 4 per slice
  CONSTANT NUM_FILTER_INPUTS      : integer := SIZE_FILTER+NUM_FILTERS-1;
  SUBTYPE FILTER_DIM              IS natural RANGE 0 TO SIZE_FILTER-1;
  SUBTYPE FILTER_RANGE            IS natural RANGE 0 TO NUM_FILTERS-1;
  SUBTYPE FILTER_INPUT_RANGE      IS natural RANGE 0 TO NUM_FILTER_INPUTS-1;
  SUBTYPE FILTER_DIM2             IS natural RANGE 0 TO (SIZE_FILTER*SIZE_FILTER)-1;

  ----------------------------------------------------------------
  --  engine_unit defines
  ----------------------------------------------------------------
  CONSTANT NUM_ENGINES            : integer := 4;
  SUBTYPE ENGINE_RANGE            IS natural RANGE 0 TO NUM_ENGINES-1;
  SUBTYPE data_addr_type          IS std_logic_vector(11 DOWNTO 0);
  TYPE array_data_addr_type       IS ARRAY(natural RANGE<>) OF data_addr_type;
  SUBTYPE insn_addr_type          IS std_logic_vector(11 DOWNTO 0);
  TYPE array_insn_addr_type       IS ARRAY(natural RANGE<>) OF insn_addr_type;
  SUBTYPE insn_type               IS std_logic_vector(31 DOWNTO 0);
  CONSTANT RESTART_INSN           : insn_type := x"C4000000"; -- j always to 0

  SUBTYPE reg_type                IS std_logic_vector(4 DOWNTO 0);
  SUBTYPE RT_INSN_FIELD           IS natural RANGE 25 DOWNTO 21;
  SUBTYPE RA_INSN_FIELD           IS natural RANGE 20 DOWNTO 16;
  SUBTYPE RB_INSN_FIELD           IS natural RANGE 15 DOWNTO 11;

  CONSTANT CTR_REG                : integer   := 8;
  CONSTANT BASE0_REG              : integer   := 9;
  CONSTANT BASE1_REG              : integer   := 10;
  CONSTANT BASE2_REG              : integer   := 11;
  CONSTANT INCR0_REG              : integer   := 12;
  CONSTANT INCR1_REG              : integer   := 13;
  CONSTANT INCR2_REG              : integer   := 14;
  CONSTANT SPEC_REG               : integer   := 15;

  SUBTYPE imm_type                IS std_logic_vector(15 DOWNTO 0);
  SUBTYPE IMM_INSN_FIELD          IS natural RANGE 15 DOWNTO 0;

  SUBTYPE off_type                IS std_logic_vector(10 DOWNTO 0);
  SUBTYPE OFF_INSN_FIELD          IS natural RANGE 10 DOWNTO 0;

  SUBTYPE engine_id_type          IS std_logic_vector(1 DOWNTO 0);
  CONSTANT INPUT_ENGINE           : engine_id_type := "00";
  CONSTANT FILTER_ENGINE          : engine_id_type := "01";
  CONSTANT POST_ENGINE            : engine_id_type := "10";
  CONSTANT OUTPUT_ENGINE          : engine_id_type := "11";
  CONSTANT INPUT_ENGINE_N         : integer := 0;
  CONSTANT FILTER_ENGINE_N        : integer := 1;
  CONSTANT POST_ENGINE_N          : integer := 2;
  CONSTANT OUTPUT_ENGINE_N        : integer := 3;

  SUBTYPE op_type                 IS std_logic_vector(5 DOWNTO 0);
  SUBTYPE OP_INSN_FIELD           IS natural RANGE 31 DOWNTO 26;
  CONSTANT COPY_OP                : op_type   := "000000";
  CONSTANT COPYI_OP               : op_type   := "000001";
  CONSTANT COPYU_OP               : op_type   := "000010";
  CONSTANT COPYUI_OP              : op_type   := "000011";
  CONSTANT ADD_OP                 : op_type   := "000100";
  CONSTANT ADDI_OP                : op_type   := "000101";
  CONSTANT SUB_OP                 : op_type   := "000110";
  CONSTANT SUBI_OP                : op_type   := "000111";

  CONSTANT SHR_OP                 : op_type   := "001000";
  CONSTANT SHRI_OP                : op_type   := "001001";
  CONSTANT SHRA_OP                : op_type   := "001010";
  CONSTANT SHRAI_OP               : op_type   := "001011";
  CONSTANT SHL_OP                 : op_type   := "001100";
  CONSTANT SHLI_OP                : op_type   := "001101";
  CONSTANT MUL_OP                 : op_type   := "001110";
  CONSTANT MULI_OP                : op_type   := "001111";

  CONSTANT OR_OP                  : op_type   := "010000";
  CONSTANT ORI_OP                 : op_type   := "010001";
  CONSTANT AND_OP                 : op_type   := "010010";
  CONSTANT ANDI_OP                : op_type   := "010011";
  CONSTANT XOR_OP                 : op_type   := "010100";
  CONSTANT XORI_OP                : op_type   := "010101";
  CONSTANT CAND_OP                : op_type   := "010110";
  CONSTANT CANDI_OP               : op_type   := "010111";

  CONSTANT LD_OP                  : op_type   := "100000";
  CONSTANT LDA_OP                 : op_type   := "100001";
  -- NO LDU/STU CONSTANT LDU_OP                 : op_type   := "100010";

  CONSTANT ST_OP                  : op_type   := "100100";
  CONSTANT STA_OP                 : op_type   := "100101";
  -- NO LDU/STU CONSTANT STU_OP                 : op_type   := "100110";

  CONSTANT J_OP                   : op_type   := "110000";
  CONSTANT JA_OP                  : op_type   := "110001";
  CONSTANT WAIT_OP                : op_type   := "110011";
  CONSTANT CALL_OP                : op_type   := "110100";
  CONSTANT CALLA_OP               : op_type   := "110101";

  CONSTANT INPUT_OP               : op_type   := "111000";
  CONSTANT INPUTI_OP              : op_type   := "111001";
  CONSTANT FILTER_OP              : op_type   := "111010";
  CONSTANT FILTERI_OP             : op_type   := "111011";
  CONSTANT POST_OP                : op_type   := "111100";
  CONSTANT POSTI_OP               : op_type   := "111101";
  CONSTANT OUTPUT_OP              : op_type   := "111110";
  CONSTANT OUTPUTI_OP             : op_type   := "111111";

  SUBTYPE cc_type                 IS std_logic_vector(2 DOWNTO 0);
  SUBTYPE CC_INSN_FIELD           IS natural RANGE 23 DOWNTO 21;
  CONSTANT ALWAYS_CC              : cc_type   := "000";
  CONSTANT EQ_CC                  : cc_type   := "001";
  CONSTANT GE_CC                  : cc_type   := "010";
  CONSTANT GT_CC                  : cc_type   := "011";
  CONSTANT UNUSED_4_CC            : cc_type   := "100";
  CONSTANT NE_CC                  : cc_type   := "101";
  CONSTANT LT_CC                  : cc_type   := "110";
  CONSTANT LE_CC                  : cc_type   := "111";

  SUBTYPE ctl_spec_type           IS std_logic_vector(25 DOWNTO 25);
  SUBTYPE CTL_SPEC_INSN_FIELD     IS natural RANGE 25 DOWNTO 25;
  CONSTANT LOOP_CTL_SPEC          : ctl_spec_type := "0";
  CONSTANT ONCE_CTL_SPEC          : ctl_spec_type := "1";

  SUBTYPE type_input_type         IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE TYPE_INPUT_INSN_FIELD   IS natural RANGE 10 DOWNTO 9;
  CONSTANT FP16_TYPE_INPUT        : type_input_type := "00";
  CONSTANT U8_TYPE_INPUT          : type_input_type := "10";
  CONSTANT S8_TYPE_INPUT          : type_input_type := "11";

  SUBTYPE mode_input_type         IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE MODE_INPUT_INSN_FIELD   IS natural RANGE 8 DOWNTO 7;
  CONSTANT NORMAL_MODE_INPUT      : mode_input_type := "00";
  CONSTANT BROAD_MODE_INPUT       : mode_input_type := "10";
  CONSTANT DEINT_MODE_INPUT       : mode_input_type := "11";

  SUBTYPE size_input_type         IS std_logic_vector(3 DOWNTO 0);
  SUBTYPE SIZE_INPUT_INSN_FIELD   IS natural RANGE 3 DOWNTO 0;

  SUBTYPE zero_filter_type        IS std_logic_vector(8 DOWNTO 0);
  SUBTYPE ZERO_FILTER_INSN_FIELD  IS natural RANGE 15 DOWNTO 7;

  SUBTYPE sum_filter_type         IS std_logic_vector(0 DOWNTO 0);
  SUBTYPE SUM_FILTER_INSN_FIELD   IS natural RANGE 6 DOWNTO 6;
  CONSTANT DIRECT_SUM_FILTER      : sum_filter_type := "0";
  CONSTANT PARTIAL_SUM_FILTER     : sum_filter_type := "1";

  SUBTYPE mode_filter_type        IS std_logic_vector(0 DOWNTO 0);
  SUBTYPE MODE_FILTER_INSN_FIELD  IS natural RANGE 5 DOWNTO 5;
  CONSTANT GLOBAL_MODE_FILTER     : mode_filter_type := "0";
  CONSTANT SLICE_MODE_FILTER      : mode_filter_type := "1";

  SUBTYPE kind_filter_type        IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE KIND_FILTER_INSN_FIELD  IS natural RANGE 4 DOWNTO 3;
  CONSTANT INACTIVE_KIND_FILTER   : kind_filter_type := "00";
  CONSTANT X1_KIND_FILTER         : kind_filter_type := "01";
  CONSTANT X2_KIND_FILTER         : kind_filter_type := "10";
  CONSTANT X4_KIND_FILTER         : kind_filter_type := "11";

  SUBTYPE data_filter_type        IS std_logic_vector(2 DOWNTO 0);
  SUBTYPE DATA_FILTER_INSN_FIELD  IS natural RANGE 2 DOWNTO 0;
  CONSTANT WT3_DATA_FILTER        : data_filter_type := "000";
  CONSTANT WT9_DATA_FILTER        : data_filter_type := "010";
  CONSTANT IN6_DATA_FILTER        : data_filter_type := "100";
  CONSTANT IN9x2_DATA_FILTER      : data_filter_type := "110";
  CONSTANT IN9_DATA_FILTER        : data_filter_type := "111";

  SUBTYPE ctl_post_type          IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE CTL_POST_INSN_FIELD    IS natural RANGE 9 DOWNTO 8;
  CONSTANT NEXT_CTL_POST        : ctl_post_type := "00";
  CONSTANT LAST_CTL_POST        : ctl_post_type := "01";
  CONSTANT FIRST_CTL_POST       : ctl_post_type := "10";
  CONSTANT INIT_CTL_POST        : ctl_post_type := "11";

  SUBTYPE rows_post_type          IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE ROWS_POST_INSN_FIELD    IS natural RANGE 7 DOWNTO 6;
  CONSTANT R1_ROWS_POST           : rows_post_type := "00";
  CONSTANT R2_ROWS_POST           : rows_post_type := "01";
  CONSTANT R3_ROWS_POST           : rows_post_type := "10";
  CONSTANT R4_ROWS_POST           : rows_post_type := "11";

  SUBTYPE cols_post_type          IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE COLS_POST_INSN_FIELD    IS natural RANGE 5 DOWNTO 4;
  CONSTANT C1_COLS_POST           : cols_post_type := "00";
  CONSTANT C2_COLS_POST           : cols_post_type := "01";
  CONSTANT C3_COLS_POST           : cols_post_type := "10";
  CONSTANT C4_COLS_POST           : cols_post_type := "11";

  SUBTYPE kind_post_type          IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE KIND_POST_INSN_FIELD    IS natural RANGE 3 DOWNTO 2;
  CONSTANT POOL_KIND_POST         : kind_post_type := "00";
  CONSTANT PICK_KIND_POST         : kind_post_type := "01";
  CONSTANT SOFT_KIND_POST         : kind_post_type := "10";

  SUBTYPE act_post_type           IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE ACT_POST_INSN_FIELD     IS natural RANGE 1 DOWNTO 0;
  CONSTANT NONE_ACT_POST          : act_post_type := "00";
  CONSTANT RECT_ACT_POST          : act_post_type := "01";
  CONSTANT SIG_ACT_POST           : act_post_type := "10";
  CONSTANT TANH_ACT_POST          : act_post_type := "11";

  SUBTYPE type_output_type        IS std_logic_vector(1 DOWNTO 0);
  SUBTYPE TYPE_OUTPUT_INSN_FIELD  IS natural RANGE 10 DOWNTO 9;
  CONSTANT FP16_TYPE_OUTPUT       : type_output_type := "00";
  CONSTANT U8_TYPE_OUTPUT         : type_output_type := "10";
  CONSTANT S8_TYPE_OUTPUT         : type_output_type := "11";

  ----------------------------------------------------------------
  -- mem_unit defines
  ----------------------------------------------------------------
  SUBTYPE slice_mask_type         IS std_logic_vector(NUM_SLICES-1 DOWNTO 0);
  SUBTYPE slice_addr_type         IS std_logic_vector(LG2_BYTES_SLICE-1 DOWNTO 0);
  SUBTYPE global_addr_type        IS std_logic_vector(LG2_BYTES_SLICE+LG2_NUM_SLICES-1 DOWNTO 0);

  SUBTYPE   BYTE_ADDR_MEM   IS natural RANGE 3 DOWNTO 0;
  SUBTYPE   HALF_ADDR_MEM   IS natural RANGE 3 DOWNTO 1;
  SUBTYPE   EO_ADDR_MEM     IS natural RANGE 4 DOWNTO 4;
  SUBTYPE   EOHALF_ADDR_MEM IS natural RANGE 4 DOWNTO 1;
  SUBTYPE   row_addr_type   IS std_logic_vector(8 DOWNTO 0);
  SUBTYPE   ROW_ADDR_MEM    IS natural RANGE 13 DOWNTO 5;
  SUBTYPE   bank_addr_type  IS std_logic_vector(3 DOWNTO 0);
  SUBTYPE   BANK_ADDR_MEM   IS natural RANGE 17 DOWNTO 14;
  SUBTYPE   SLICE_SEL_ADDR_MEM  IS natural RANGE 20 DOWNTO 18;
  SUBTYPE   slice_sel_addr_type IS std_logic_vector(LG2_NUM_SLICES-1 DOWNTO 0);

  CONSTANT  INPUT_RES_COLL        : integer := 0;
  CONSTANT  FILTER_DATA_COLL      : integer := 1;
  CONSTANT  FILTER_PSUM_COLL      : integer := 2;
  CONSTANT  FILTER_RES_COLL       : integer := 3;
  CONSTANT  POST_DATA_COLL        : integer := 4;
  CONSTANT  POST_RES_COLL         : integer := 5;
  CONSTANT  OUTPUT_DATA_COLL      : integer := 6;
  SUBTYPE   COLL_RANGE            IS natural RANGE 0 TO 6;
  SUBTYPE   coll_type             IS std_logic_vector(6 DOWNTO 0);

  PURE FUNCTION  slice_sel_to_mask( sel : IN slice_sel_addr_type ) RETURN slice_mask_type;
-- pragma translate_off
  PROCEDURE dump_insn(          L : INOUT line; V : IN insn_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_op(            L : INOUT line; V : IN op_type;   B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_cc(            L : INOUT line; V : IN cc_type;   B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_reg(           L : INOUT line; V : IN reg_type;  B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_local(         L : INOUT line; V : IN bit4_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_global(        L : INOUT line; V : IN bit4_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_fp16(          L : INOUT line; V : IN fp16_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_ctl_spec(      L : INOUT line; V : IN ctl_spec_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_kind_filter(   L : INOUT line; V : IN kind_filter_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_mode_filter(   L : INOUT line; V : IN mode_filter_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_data_filter(   L : INOUT line; V : IN data_filter_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_sum_filter(    L : INOUT line; V : IN sum_filter_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_kind_post(     L : INOUT line; V : IN kind_post_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_rows_post(     L : INOUT line; V : IN rows_post_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_cols_post(     L : INOUT line; V : IN cols_post_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_ctl_post(      L : INOUT line; V : IN ctl_post_type; B : IN string := ""; A : IN string := "" );
  PROCEDURE dump_act_post(      L : INOUT line; V : IN act_post_type; B : IN string := ""; A : IN string := "" );
-- pragma translate_on

END gsnn_package;

PACKAGE BODY gsnn_package IS
  PURE FUNCTION  slice_sel_to_mask( sel : IN slice_sel_addr_type ) RETURN slice_mask_type IS
  BEGIN
    CASE sel IS
      WHEN "000" => RETURN "00000001";
      WHEN "001" => RETURN "00000010";
      WHEN "010" => RETURN "00000100";
      WHEN "011" => RETURN "00001000";
      WHEN "100" => RETURN "00010000";
      WHEN "101" => RETURN "00100000";
      WHEN "110" => RETURN "01000000";
      WHEN "111" => RETURN "10000000";
      WHEN OTHERS => RETURN "XXXXXXXX";
    END CASE;
  END FUNCTION slice_sel_to_mask;

-- pragma translate_off

  PROCEDURE dump_insn(
    L : INOUT line;
    V : IN    insn_type;
    B : IN    string := "";
    A : IN    string := ""
    )
  IS
    VARIABLE  op : op_type;
    VARIABLE  cc : cc_type;
    VARIABLE  rt : reg_type;
    VARIABLE  ra : reg_type;
    VARIABLE  rb : reg_type;
    VARIABLE  imm : imm_type;
    VARIABLE  off : off_type;
  BEGIN
    write(L, B);
    op := V(OP_INSN_FIELD);
    rt := V(RT_INSN_FIELD);
    ra := V(RA_INSN_FIELD);
    rb := V(RB_INSN_FIELD);
    cc := V(CC_INSN_FIELD);
    imm := V(IMM_INSN_FIELD);
    off := V(OFF_INSN_FIELD);

    CASE op IS
      WHEN COPY_OP|COPYU_OP     => dump_op(L, op); dump_reg(L, rt, " " ); dump_reg(L, rb, ",");
      WHEN COPYI_OP|COPYUI_OP   => dump_op(L, op); dump_reg(L, rt, " " ); dump_hex(L, imm, ",");
      WHEN ADD_OP|SUB_OP|AND_OP|OR_OP|XOR_OP|CAND_OP|SHR_OP|SHRA_OP|SHL_OP|MUL_OP
                                => dump_op(L, op); dump_reg(L, rt, " " ); dump_reg(L, ra, ","); dump_reg(L, rb, ",");
      WHEN ADDI_OP|SUBI_OP|ANDI_OP|ORI_OP|XORI_OP|CANDI_OP|SHRI_OP|SHRAI_OP|SHLI_OP|MULI_OP
                                => dump_op(L, op); dump_reg(L, rt, " " ); dump_reg(L, ra, ","); dump_hex(L, imm, ",");
      WHEN LD_OP -- NO LDU/STU |LDU_OP         
                                => dump_op(L, op); dump_reg(L, rt, " " ); dump_reg(L, rb, ","); dump_integer(L, to_integer(signed(off)), ",");
      WHEN LDA_OP               => dump_op(L, op); dump_reg(L, rt, " " ); dump_hex(L, imm, ",");
      WHEN ST_OP -- NO LDU/STU |STU_OP
                                => dump_op(L, op); dump_reg(L, ra, " " ); dump_reg(L, rb, ","); dump_integer(L, to_integer(signed(off)), ",");
      WHEN STA_OP               => dump_op(L, op); dump_reg(L, ra, " " ); dump_hex(L, imm, ",");
      WHEN J_OP                 => dump_op(L, op); dump_cc(L, cc, " "); dump_reg(L, ra, "," ); dump_reg(L, rb, ",");
      WHEN JA_OP                => dump_op(L, op); dump_cc(L, cc, " "); dump_reg(L, ra, "," ); dump_hex(L, imm, ",");
      WHEN WAIT_OP              => dump_op(L, op); dump_cc(L, cc, " "); dump_reg(L, ra, "," );
      WHEN CALL_OP              => dump_op(L, op); dump_reg(L, rt, " "); dump_reg(L, rb, ",");
      WHEN CALLA_OP             => dump_op(L, op); dump_reg(L, rt, " "); dump_hex(L, imm, ",");
      WHEN INPUT_OP|FILTER_OP|POST_OP|OUTPUT_OP
                                => dump_op(L, op);
                                    dump_ctl_spec(L, ctl_spec_type'(V(CTL_SPEC_INSN_FIELD)), ",");
                                    dump_reg(L, rb, ",");
      WHEN INPUTI_OP            => write(L, string'("?inputi?"));
      WHEN FILTERI_OP           => dump_op(L, op );
                                    dump_ctl_spec(L, ctl_spec_type'(V(CTL_SPEC_INSN_FIELD)), ",");
                                    dump_data_filter(L, data_filter_type'(V(DATA_FILTER_INSN_FIELD)), ",");
                                    dump_hex(L, bit9_type'(V(ZERO_FILTER_INSN_FIELD)), ",0x");
                                    dump_kind_filter(L, kind_filter_type'(V(KIND_FILTER_INSN_FIELD)), ",");
                                    dump_mode_filter(L, mode_filter_type'(V(MODE_FILTER_INSN_FIELD)), ",");
                                    dump_sum_filter(L, sum_filter_type'(V(SUM_FILTER_INSN_FIELD)), ",");
      WHEN POSTI_OP             => dump_op(L, op );
                                    dump_ctl_spec(L, ctl_spec_type'(V(CTL_SPEC_INSN_FIELD)), ",");
                                    dump_kind_post(L, kind_post_type'(V(KIND_POST_INSN_FIELD)), ",");
                                    dump_rows_post(L, rows_post_type'(V(ROWS_POST_INSN_FIELD)), ",");
                                    dump_cols_post(L, cols_post_type'(V(COLS_POST_INSN_FIELD)), ",");
                                    dump_ctl_post(L, ctl_post_type'(V(CTL_POST_INSN_FIELD)), ",");
                                    dump_act_post(L, act_post_type'(V(ACT_POST_INSN_FIELD)), ",");
      WHEN OUTPUTI_OP           => write(L, string'("?outputi?"));
      WHEN OTHERS               => dump_hex(L, V, "?insn:", "?");
    END CASE;

    write(L, A);
  END PROCEDURE dump_insn;


  PROCEDURE dump_op(
    L : INOUT line;
    V : IN    op_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN COPY_OP          => write(L, string'("copy"));
      WHEN COPYI_OP         => write(L, string'("copyi"));
      WHEN COPYU_OP         => write(L, string'("copyu"));
      WHEN COPYUI_OP        => write(L, string'("copyui"));
      WHEN ADD_OP           => write(L, string'("add"));
      WHEN ADDI_OP          => write(L, string'("addi"));
      WHEN SUB_OP           => write(L, string'("sub"));
      WHEN SUBI_OP          => write(L, string'("subi"));
      WHEN SHR_OP           => write(L, string'("shr"));
      WHEN SHRI_OP          => write(L, string'("shri"));
      WHEN SHRA_OP          => write(L, string'("shra"));
      WHEN SHRAI_OP         => write(L, string'("shrai"));
      WHEN SHL_OP           => write(L, string'("shl"));
      WHEN SHLI_OP          => write(L, string'("shli"));
      WHEN MUL_OP           => write(L, string'("mul"));
      WHEN MULI_OP          => write(L, string'("muli"));
      WHEN OR_OP            => write(L, string'("or"));
      WHEN ORI_OP           => write(L, string'("ori"));
      WHEN AND_OP           => write(L, string'("and"));
      WHEN ANDI_OP          => write(L, string'("andi"));
      WHEN XOR_OP           => write(L, string'("xor"));
      WHEN XORI_OP          => write(L, string'("xori"));
      WHEN CAND_OP          => write(L, string'("cand"));
      WHEN CANDI_OP         => write(L, string'("candi"));
      WHEN LD_OP            => write(L, string'("ld"));
      WHEN LDA_OP           => write(L, string'("lda"));
      -- NO LDU/STU WHEN LDU_OP           => write(L, string'("ldu"));
      WHEN ST_OP            => write(L, string'("st"));
      WHEN STA_OP           => write(L, string'("sta"));
      -- NO LDU/STU WHEN STU_OP           => write(L, string'("stu"));
      WHEN J_OP             => write(L, string'("j"));
      WHEN JA_OP            => write(L, string'("ja"));
      WHEN WAIT_OP          => write(L, string'("wait"));
      WHEN CALL_OP          => write(L, string'("call"));
      WHEN CALLA_OP         => write(L, string'("calla"));
      WHEN INPUT_OP         => write(L, string'("input"));
      WHEN INPUTI_OP        => write(L, string'("inputi"));
      WHEN FILTER_OP        => write(L, string'("filter"));
      WHEN FILTERI_OP       => write(L, string'("filteri"));
      WHEN POST_OP          => write(L, string'("post"));
      WHEN POSTI_OP         => write(L, string'("posti"));
      WHEN OUTPUT_OP        => write(L, string'("output"));
      WHEN OUTPUTI_OP       => write(L, string'("outputi"));
      WHEN OTHERS           => dump_hex(L, V, "?op:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_op;

  PROCEDURE dump_cc(
    L : INOUT line;
    V : IN    cc_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN ALWAYS_CC  => write(L, string'("always"));
      WHEN EQ_CC  => write(L, string'("eq"));
      WHEN GE_CC  => write(L, string'("ge"));
      WHEN GT_CC  => write(L, string'("gt"));
      WHEN NE_CC  => write(L, string'("ne"));
      WHEN LT_CC  => write(L, string'("lt"));
      WHEN LE_CC  => write(L, string'("le"));
      WHEN OTHERS => dump_bit(L, V, "?cc:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_cc;

  PROCEDURE dump_reg(
    L : INOUT line;
    V : IN    reg_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
    VARIABLE  reg: integer;
  BEGIN
    write(L, B);

    reg := to_integer(unsigned(V));

    IF reg < 16 THEN
      dump_integer(L, reg, "$r");
    ELSE
      dump_integer(L, reg-16, "$g");
    END IF;

    write(L, A);
  END PROCEDURE dump_reg;

  PROCEDURE dump_local(
    L : INOUT line;
    V : IN    bit4_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
    VARIABLE  reg: integer;
  BEGIN
    write(L, B);

    reg := to_integer(unsigned(V));
    dump_integer(L, reg, "$r");

    write(L, A);
  END PROCEDURE dump_local;

  PROCEDURE dump_global(
    L : INOUT line;
    V : IN    bit4_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
    VARIABLE  reg: integer;
  BEGIN
    write(L, B);

    reg := to_integer(unsigned(V));
    dump_integer(L, reg, "$g");

    write(L, A);
  END PROCEDURE dump_global;

  PROCEDURE dump_fp16(
    L : INOUT line;
    V : IN    fp16_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
    VARIABLE  reg: integer;
    VARIABLE  mant : bit10_type;
    VARIABLE  sign : bit_type;
    VARIABLE  exp  : bit5_type;
  BEGIN
    write(L, B);

    sign  := V(SIGN_FP16);
    exp   := V(EXP_FP16);
    mant  := V(MANT_FP16);

    IF sign = "1" THEN
      dump_string( L, "-" );
    END IF;

    IF exp = "00000" THEN
      dump_string( L, "0.0" );
    ELSIF exp = "11111" THEN
      dump_string( L, "Inf" );
    ELSE
      dump_hex( L, bit12_type'(mant&"00"), "1.");
      dump_integer( L, to_integer(unsigned(exp))-15, "p");
    END IF;

    write(L, A);
  END PROCEDURE dump_fp16;

  PROCEDURE dump_ctl_spec(
    L : INOUT line;
    V : IN    ctl_spec_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN ONCE_CTL_SPEC  => write(L, string'("once"));
      WHEN LOOP_CTL_SPEC  => write(L, string'("loop"));
      WHEN OTHERS => dump_bit(L, V, "?ctl_spec:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_ctl_spec;

  PROCEDURE dump_sum_filter(
    L : INOUT line;
    V : IN    sum_filter_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN PARTIAL_SUM_FILTER  => write(L, string'("partial"));
      WHEN DIRECT_SUM_FILTER  => write(L, string'("direct"));
      WHEN OTHERS => dump_bit(L, V, "?sum_filter:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_sum_filter;

  PROCEDURE dump_kind_filter(
    L : INOUT line;
    V : IN    kind_filter_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN INACTIVE_KIND_FILTER  => write(L, string'("inactive"));
      WHEN X1_KIND_FILTER  => write(L, string'("x1"));
      WHEN X2_KIND_FILTER  => write(L, string'("x2"));
      WHEN X4_KIND_FILTER  => write(L, string'("x4"));
      WHEN OTHERS => dump_bit(L, V, "?kind_filter:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_kind_filter;

  PROCEDURE dump_mode_filter(
    L : INOUT line;
    V : IN    mode_filter_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN GLOBAL_MODE_FILTER  => write(L, string'("global"));
      WHEN SLICE_MODE_FILTER  => write(L, string'("slice"));
      WHEN OTHERS => dump_bit(L, V, "?mode_filter:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_mode_filter;

  PROCEDURE dump_data_filter(
    L : INOUT line;
    V : IN    data_filter_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN WT3_DATA_FILTER  => write(L, string'("wt3"));
      WHEN WT9_DATA_FILTER  => write(L, string'("wt9"));
      WHEN IN6_DATA_FILTER  => write(L, string'("in6"));
      WHEN IN9x2_DATA_FILTER  => write(L, string'("in9x2"));
      WHEN IN9_DATA_FILTER  => write(L, string'("in9"));
      WHEN OTHERS => dump_bit(L, V, "?data_filter:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_data_filter;

  PROCEDURE dump_kind_post(
    L : INOUT line;
    V : IN    kind_post_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN POOL_KIND_POST  => write(L, string'("pool"));
      WHEN PICK_KIND_POST  => write(L, string'("pick"));
      WHEN SOFT_KIND_POST  => write(L, string'("soft"));
      WHEN OTHERS => dump_bit(L, V, "?kind_post:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_kind_post;

  PROCEDURE dump_rows_post(
    L : INOUT line;
    V : IN    rows_post_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN R1_ROWS_POST  => write(L, string'("r1"));
      WHEN R2_ROWS_POST  => write(L, string'("r2"));
      WHEN R3_ROWS_POST  => write(L, string'("r3"));
      WHEN R4_ROWS_POST  => write(L, string'("r4"));
      WHEN OTHERS => dump_bit(L, V, "?rows_post:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_rows_post;

  PROCEDURE dump_cols_post(
    L : INOUT line;
    V : IN    cols_post_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN C1_COLS_POST  => write(L, string'("c1"));
      WHEN C2_COLS_POST  => write(L, string'("c2"));
      WHEN C3_COLS_POST  => write(L, string'("c3"));
      WHEN C4_COLS_POST  => write(L, string'("c4"));
      WHEN OTHERS => dump_bit(L, V, "?cols_post:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_cols_post;

  PROCEDURE dump_ctl_post(
    L : INOUT line;
    V : IN    ctl_post_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN NEXT_CTL_POST  => write(L, string'("next"));
      WHEN FIRST_CTL_POST  => write(L, string'("first"));
      WHEN LAST_CTL_POST  => write(L, string'("last"));
      WHEN INIT_CTL_POST  => write(L, string'("init"));
      WHEN OTHERS => dump_bit(L, V, "?ctl_post:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_ctl_post;

  PROCEDURE dump_act_post(
    L : INOUT line;
    V : IN    act_post_type;
    B : IN    string := "";
    A : IN    string := ""
    ) IS
  BEGIN
    write(L, B);

    CASE V IS
      WHEN NONE_ACT_POST  => write(L, string'("none"));
      WHEN RECT_ACT_POST  => write(L, string'("rect"));
      WHEN SIG_ACT_POST   => write(L, string'("sig"));
      WHEN TANH_ACT_POST  => write(L, string'("tanh"));
      WHEN OTHERS => dump_bit(L, V, "?act_post:", "?");
    END CASE;
    write(L, A);
  END PROCEDURE dump_act_post;

-- pragma translate_on

END gsnn_package;

