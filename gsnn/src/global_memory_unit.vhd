LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY decode_input_global_memory_unit IS
  PORT(
  addr_write_in                   : IN slice_addr_type;
  word_write_in                   : IN bit128_type;

  en_e_busout                     : OUT bit_type;
  bank_e_busout                   : OUT bank_addr_type;
  row_e_busout                    : OUT row_addr_type;
  mask_e_busout                   : OUT bit8_type;
  word_e_busout                   : OUT bit128_type;

  en_o_busout                     : OUT bit_type;
  bank_o_busout                   : OUT bank_addr_type;
  row_o_busout                    : OUT row_addr_type;
  mask_o_busout                   : OUT bit8_type;
  word_o_busout                   : OUT bit128_type
  );
END ENTITY decode_input_global_memory_unit;

ARCHITECTURE behavior OF decode_input_global_memory_unit IS

  SIGNAL    half_addr   : bit3_type;
  SIGNAL    eo_addr     : bit_type;
  SIGNAL    row_addr    : row_addr_type;
  SIGNAL    bank_addr   : bank_addr_type;
  SIGNAL    row_addr1   : row_addr_type;
  SIGNAL    bank_addr1  : bank_addr_type;
  SIGNAL    eo_cross    : bit_type;
  SIGNAL    mask2       : bit16_type;
  SIGNAL    mask_u      : bit8_type;
  SIGNAL    mask_l      : bit8_type;
  SIGNAL    word2       : bit256_type;
  SIGNAL    word_u      : bit128_type;
  SIGNAL    word_l      : bit128_type;
BEGIN
  half_addr     <=  addr_write_in( HALF_ADDR_MEM );
  eo_addr       <=  addr_write_in( EO_ADDR_MEM );
  row_addr      <=  addr_write_in( ROW_ADDR_MEM );
  bank_addr     <=  addr_write_in( BANK_ADDR_MEM );
  eo_cross      <=  "1" WHEN unsigned(half_addr) > 0 ELSE "0";

  row_addr1     <=  std_logic_vector( unsigned(row_addr) + 1 );
  bank_addr1    <=  std_logic_vector( unsigned(bank_addr) + 1 );
  WITH half_addr SELECT
    mask2 <=
        x"00ff"       WHEN "000",
        x"01fe"       WHEN "001",
        x"03fc"       WHEN "010",
        x"07f8"       WHEN "011",
        x"0ff0"       WHEN "100",
        x"1fe0"       WHEN "101",
        x"3fc0"       WHEN "110",
        x"7f80"       WHEN "111",
        (OTHERS=>'X') WHEN OTHERS;
  mask_u <= mask2(15 DOWNTO 8);
  mask_l <= mask2(7 DOWNTO 0);

  WITH half_addr SELECT
    word2 <=
        (16*8-1 DOWNTO 0 => 'X') & word_write_in                            WHEN  "000",
        (16*7-1 DOWNTO 0 => 'X') & word_write_in & (16*1-1 DOWNTO 0 =>'X')  WHEN  "001",
        (16*6-1 DOWNTO 0 => 'X') & word_write_in & (16*2-1 DOWNTO 0 =>'X')  WHEN  "010",
        (16*5-1 DOWNTO 0 => 'X') & word_write_in & (16*3-1 DOWNTO 0 =>'X')  WHEN  "011",
        (16*4-1 DOWNTO 0 => 'X') & word_write_in & (16*4-1 DOWNTO 0 =>'X')  WHEN  "100",
        (16*3-1 DOWNTO 0 => 'X') & word_write_in & (16*5-1 DOWNTO 0 =>'X')  WHEN  "101",
        (16*2-1 DOWNTO 0 => 'X') & word_write_in & (16*6-1 DOWNTO 0 =>'X')  WHEN  "110",
        (16*1-1 DOWNTO 0 => 'X') & word_write_in & (16*7-1 DOWNTO 0 =>'X')  WHEN  "111",
        (OTHERS=>'X')                                                       WHEN OTHERS;
  word_u <= word2(255 DOWNTO 128);
  word_l <= word2(127 DOWNTO 0);

  en_e_busout   <=  "1" WHEN eo_addr = "0" OR eo_cross = "1" ELSE "0";
  row_e_busout  <=  row_addr        WHEN eo_addr = "0" ELSE
                    row_addr1       WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
  bank_e_busout <=  bank_addr       WHEN eo_addr = "0"
                                      OR ( eo_cross = "1" AND row_addr /= (row_addr'range => '1') ) ELSE
                    bank_addr1      WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
  mask_e_busout <=  mask_l          WHEN eo_addr = "0" ELSE
                    mask_u          WHEN eo_cross = "1" ELSE
                    (OTHERS => 'X');
  word_e_busout <=  word_l          WHEN eo_addr = "0" ELSE
                    word_u          WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');

  en_o_busout   <=  "1" WHEN eo_addr = "1" OR eo_cross = "1" ELSE "0";
  row_o_busout  <=  row_addr;
  bank_o_busout <=  bank_addr;
  mask_o_busout <=  mask_l          WHEN eo_addr = "1" ELSE
                    mask_u          WHEN eo_cross = "1" ELSE
                    (OTHERS => 'X');
  word_o_busout <=  word_l          WHEN eo_addr = "1" ELSE
                    word_u          WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');

END ARCHITECTURE behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY decode_res_filter_global_memory_unit IS
  PORT(
  clock                           : IN std_logic;
  addr_write_in                   : IN slice_addr_type;
  word_write_in                   : IN bit64_type;
  width_write_in                  : IN bit_type; -- write 8B or 2B

  en_e_busout                     : OUT bit_type;
  bank_e_busout                   : OUT bank_addr_type;
  row_e_busout                    : OUT row_addr_type;
  mask_e_busout                   : OUT bit8_type;
  word_e_busout                   : OUT bit128_type;

  en_o_busout                     : OUT bit_type;
  bank_o_busout                   : OUT bank_addr_type;
  row_o_busout                    : OUT row_addr_type;
  mask_o_busout                   : OUT bit8_type;
  word_o_busout                   : OUT bit128_type
  );
END ENTITY decode_res_filter_global_memory_unit;

ARCHITECTURE behavior OF decode_res_filter_global_memory_unit IS
  SIGNAL    mask_wr     : bit4_type;
  SIGNAL    half_addr   : bit3_type;
  SIGNAL    eo_addr     : bit_type;
  SIGNAL    row_addr    : row_addr_type;
  SIGNAL    bank_addr   : bank_addr_type;
  SIGNAL    row_addr1   : row_addr_type;
  SIGNAL    bank_addr1  : bank_addr_type;
  SIGNAL    eo_cross    : bit_type;
  SIGNAL    mask2       : bit16_type;
  SIGNAL    mask_u      : bit8_type;
  SIGNAL    mask_l      : bit8_type;
  SIGNAL    word2       : bit256_type;
  SIGNAL    word_u      : bit128_type;
  SIGNAL    word_l      : bit128_type;
  SIGNAL    en_e_i      : bit_type;
  SIGNAL    en_o_i      : bit_type;
BEGIN
  half_addr     <=  addr_write_in( HALF_ADDR_MEM );
  eo_addr       <=  addr_write_in( EO_ADDR_MEM );
  row_addr      <=  addr_write_in( ROW_ADDR_MEM );
  bank_addr     <=  addr_write_in( BANK_ADDR_MEM );
  eo_cross      <=  "1" WHEN width_write_in = "1" AND unsigned(half_addr) > 4 ELSE
                    "0";

  row_addr1     <=  std_logic_vector( unsigned(row_addr) + 1 );
  bank_addr1    <=  std_logic_vector( unsigned(bank_addr) + 1 );
  mask_wr       <= x"f" WHEN width_write_in = "1" ELSE x"1";
  WITH half_addr SELECT
    mask2 <=
        (11 DOWNTO 0 =>'0') & mask_wr                       WHEN "000",
        (10 DOWNTO 0 =>'0') & mask_wr & (0 DOWNTO 0 =>'0')  WHEN "001",
        ( 9 DOWNTO 0 =>'0') & mask_wr & (1 DOWNTO 0 =>'0')  WHEN "010",
        ( 8 DOWNTO 0 =>'0') & mask_wr & (2 DOWNTO 0 =>'0')  WHEN "011",
        ( 7 DOWNTO 0 =>'0') & mask_wr & (3 DOWNTO 0 =>'0')  WHEN "100",
        ( 6 DOWNTO 0 =>'0') & mask_wr & (4 DOWNTO 0 =>'0')  WHEN "101",
        ( 5 DOWNTO 0 =>'0') & mask_wr & (5 DOWNTO 0 =>'0')  WHEN "110",
        ( 4 DOWNTO 0 =>'0') & mask_wr & (6 DOWNTO 0 =>'0')  WHEN "111",
        (OTHERS=>'X') WHEN OTHERS;
  mask_u <= mask2(15 DOWNTO 8);
  mask_l <= mask2(7 DOWNTO 0);

  WITH half_addr SELECT
    word2 <=
        (16*12-1 DOWNTO 0 => 'X') & word_write_in                           WHEN  "000",
        (16*11-1 DOWNTO 0 => 'X') & word_write_in & (16*1-1 DOWNTO 0 =>'X') WHEN  "001",
        (16*10-1 DOWNTO 0 => 'X') & word_write_in & (16*2-1 DOWNTO 0 =>'X') WHEN  "010",
        (16* 9-1 DOWNTO 0 => 'X') & word_write_in & (16*3-1 DOWNTO 0 =>'X') WHEN  "011",
        (16* 8-1 DOWNTO 0 => 'X') & word_write_in & (16*4-1 DOWNTO 0 =>'X') WHEN  "100",
        (16* 7-1 DOWNTO 0 => 'X') & word_write_in & (16*5-1 DOWNTO 0 =>'X') WHEN  "101",
        (16* 6-1 DOWNTO 0 => 'X') & word_write_in & (16*6-1 DOWNTO 0 =>'X') WHEN  "110",
        (16* 5-1 DOWNTO 0 => 'X') & word_write_in & (16*7-1 DOWNTO 0 =>'X') WHEN  "111",
        (OTHERS=>'X')                                                       WHEN OTHERS;
  word_u <= word2(255 DOWNTO 128);
  word_l <= word2(127 DOWNTO 0);

  en_e_i        <= "1" WHEN eo_addr = "0" OR eo_cross = "1" ELSE "0";
  en_e_busout   <=  en_e_i;
  row_e_busout  <=  row_addr        WHEN eo_addr = "0" ELSE
                    row_addr1       WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
  bank_e_busout <=  bank_addr       WHEN eo_addr = "0"
                                      OR ( eo_cross = "1" AND row_addr /= (row_addr'range => '1') ) ELSE
                    bank_addr1      WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
  mask_e_busout <=  mask_l          WHEN eo_addr = "0" ELSE
                    mask_u          WHEN eo_cross = "1" ELSE
                    (OTHERS => 'X');
  word_e_busout <=  word_l          WHEN eo_addr = "0" ELSE
                    word_u          WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');

  en_o_i        <= "1" WHEN eo_addr = "1" OR eo_cross = "1" ELSE "0";
  en_o_busout   <=  en_o_i;
  row_o_busout  <=  row_addr;
  bank_o_busout <=  bank_addr;
  mask_o_busout <=  mask_l          WHEN eo_addr = "1" ELSE
                    mask_u          WHEN eo_cross = "1" ELSE
                    (OTHERS => 'X');
  word_o_busout <=  word_l          WHEN eo_addr = "1" ELSE
                    word_u          WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
END ARCHITECTURE behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

ENTITY decode_res_post_global_memory_unit IS
  PORT(
  clock                           : IN std_logic;
  addr_write_in                   : IN slice_addr_type;
  word_write_in                   : IN bit64_type;
  width_write_in                  : IN bit_type; -- write 8B or 2B

  en_e_busout                     : OUT bit_type;
  bank_e_busout                   : OUT bank_addr_type;
  row_e_busout                    : OUT row_addr_type;
  mask_e_busout                   : OUT bit8_type;
  word_e_busout                   : OUT bit128_type;

  en_o_busout                     : OUT bit_type;
  bank_o_busout                   : OUT bank_addr_type;
  row_o_busout                    : OUT row_addr_type;
  mask_o_busout                   : OUT bit8_type;
  word_o_busout                   : OUT bit128_type
  );
END ENTITY decode_res_post_global_memory_unit;

ARCHITECTURE behavior OF decode_res_post_global_memory_unit IS
  SIGNAL    mask_wr     : bit4_type;
  SIGNAL    half_addr   : bit3_type;
  SIGNAL    eo_addr     : bit_type;
  SIGNAL    row_addr    : row_addr_type;
  SIGNAL    bank_addr   : bank_addr_type;
  SIGNAL    row_addr1   : row_addr_type;
  SIGNAL    bank_addr1  : bank_addr_type;
  SIGNAL    eo_cross    : bit_type;
  SIGNAL    mask2       : bit16_type;
  SIGNAL    mask_u      : bit8_type;
  SIGNAL    mask_l      : bit8_type;
  SIGNAL    word2       : bit256_type;
  SIGNAL    word_u      : bit128_type;
  SIGNAL    word_l      : bit128_type;
  SIGNAL    en_e_i      : bit_type;
  SIGNAL    en_o_i      : bit_type;
BEGIN
  half_addr     <=  addr_write_in( HALF_ADDR_MEM );
  eo_addr       <=  addr_write_in( EO_ADDR_MEM );
  row_addr      <=  addr_write_in( ROW_ADDR_MEM );
  bank_addr     <=  addr_write_in( BANK_ADDR_MEM );
  eo_cross      <=  "1" WHEN width_write_in = "1" AND unsigned(half_addr) > 4 ELSE
                    "1" WHEN width_write_in = "0" AND unsigned(half_addr) > 6 ELSE
                    "0";

  row_addr1     <=  std_logic_vector( unsigned(row_addr) + 1 );
  bank_addr1    <=  std_logic_vector( unsigned(bank_addr) + 1 );
  mask_wr       <= x"f" WHEN width_write_in = "1" ELSE x"3";
  WITH half_addr SELECT
    mask2 <=
        (11 DOWNTO 0 =>'0') & mask_wr                       WHEN "000",
        (10 DOWNTO 0 =>'0') & mask_wr & (0 DOWNTO 0 =>'0')  WHEN "001",
        ( 9 DOWNTO 0 =>'0') & mask_wr & (1 DOWNTO 0 =>'0')  WHEN "010",
        ( 8 DOWNTO 0 =>'0') & mask_wr & (2 DOWNTO 0 =>'0')  WHEN "011",
        ( 7 DOWNTO 0 =>'0') & mask_wr & (3 DOWNTO 0 =>'0')  WHEN "100",
        ( 6 DOWNTO 0 =>'0') & mask_wr & (4 DOWNTO 0 =>'0')  WHEN "101",
        ( 5 DOWNTO 0 =>'0') & mask_wr & (5 DOWNTO 0 =>'0')  WHEN "110",
        ( 4 DOWNTO 0 =>'0') & mask_wr & (6 DOWNTO 0 =>'0')  WHEN "111",
        (OTHERS=>'X') WHEN OTHERS;
  mask_u <= mask2(15 DOWNTO 8);
  mask_l <= mask2(7 DOWNTO 0);

  WITH half_addr SELECT
    word2 <=
        (16*12-1 DOWNTO 0 => 'X') & word_write_in                           WHEN  "000",
        (16*11-1 DOWNTO 0 => 'X') & word_write_in & (16*1-1 DOWNTO 0 =>'X') WHEN  "001",
        (16*10-1 DOWNTO 0 => 'X') & word_write_in & (16*2-1 DOWNTO 0 =>'X') WHEN  "010",
        (16* 9-1 DOWNTO 0 => 'X') & word_write_in & (16*3-1 DOWNTO 0 =>'X') WHEN  "011",
        (16* 8-1 DOWNTO 0 => 'X') & word_write_in & (16*4-1 DOWNTO 0 =>'X') WHEN  "100",
        (16* 7-1 DOWNTO 0 => 'X') & word_write_in & (16*5-1 DOWNTO 0 =>'X') WHEN  "101",
        (16* 6-1 DOWNTO 0 => 'X') & word_write_in & (16*6-1 DOWNTO 0 =>'X') WHEN  "110",
        (16* 5-1 DOWNTO 0 => 'X') & word_write_in & (16*7-1 DOWNTO 0 =>'X') WHEN  "111",
        (OTHERS=>'X')                                                       WHEN OTHERS;
  word_u <= word2(255 DOWNTO 128);
  word_l <= word2(127 DOWNTO 0);

  en_e_i        <= "1" WHEN eo_addr = "0" OR eo_cross = "1" ELSE "0";
  en_e_busout   <=  en_e_i;
  row_e_busout  <=  row_addr        WHEN eo_addr = "0" ELSE
                    row_addr1       WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
  bank_e_busout <=  bank_addr       WHEN eo_addr = "0"
                                      OR ( eo_cross = "1" AND row_addr /= (row_addr'range => '1') ) ELSE
                    bank_addr1      WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
  mask_e_busout <=  mask_l          WHEN eo_addr = "0" ELSE
                    mask_u          WHEN eo_cross = "1" ELSE
                    (OTHERS => 'X');
  word_e_busout <=  word_l          WHEN eo_addr = "0" ELSE
                    word_u          WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');

  en_o_i        <= "1" WHEN eo_addr = "1" OR eo_cross = "1" ELSE "0";
  en_o_busout   <=  en_o_i;
  row_o_busout  <=  row_addr;
  bank_o_busout <=  bank_addr;
  mask_o_busout <=  mask_l          WHEN eo_addr = "1" ELSE
                    mask_u          WHEN eo_cross = "1" ELSE
                    (OTHERS => 'X');
  word_o_busout <=  word_l          WHEN eo_addr = "1" ELSE
                    word_u          WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
END ARCHITECTURE behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY decode_data_filter_global_memory_unit IS
  PORT(
  addr_read_in                    : IN slice_addr_type;
  width_read_in                   : IN bit2_type;

  en_e_busout                     : OUT bit_type;
  bank_e_busout                   : OUT bank_addr_type;
  row_e_busout                    : OUT row_addr_type;

  en_o_busout                     : OUT bit_type;
  bank_o_busout                   : OUT bank_addr_type;
  row_o_busout                    : OUT row_addr_type
  );
END ENTITY decode_data_filter_global_memory_unit;

ARCHITECTURE behavior OF decode_data_filter_global_memory_unit IS
  SIGNAL    half_addr   : bit3_type;
  SIGNAL    eo_addr     : bit_type;
  SIGNAL    row_addr    : row_addr_type;
  SIGNAL    bank_addr   : bank_addr_type;
  SIGNAL    row_addr1   : row_addr_type;
  SIGNAL    bank_addr1  : bank_addr_type;
  SIGNAL    eo_cross    : bit_type;
BEGIN
  half_addr     <=  addr_read_in( HALF_ADDR_MEM );
  eo_addr       <=  addr_read_in( EO_ADDR_MEM );
  row_addr      <=  addr_read_in( ROW_ADDR_MEM );
  bank_addr     <=  addr_read_in( BANK_ADDR_MEM );
  eo_cross      <=  "1" WHEN width_read_in = "00" ELSE -- 18B
                    "1" WHEN width_read_in = "10" AND unsigned(half_addr) > 5 ELSE -- 6B
                    "1" WHEN width_read_in = "11" AND unsigned(half_addr) > 2 ELSE -- 12B
                    "0";

  row_addr1     <=  std_logic_vector( unsigned(row_addr) + 1 );
  bank_addr1    <=  std_logic_vector( unsigned(bank_addr) + 1 );

  en_e_busout   <=  "1" WHEN eo_addr = "0" OR eo_cross = "1" ELSE "0";
  row_e_busout  <=  row_addr        WHEN eo_addr = "0" ELSE
                    row_addr1       WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
  bank_e_busout <=  bank_addr       WHEN eo_addr = "0"
                                      OR ( eo_cross = "1" AND row_addr /= (row_addr'range => '1') ) ELSE
                    bank_addr1      WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');

  en_o_busout   <=  "1" WHEN eo_addr = "1" OR eo_cross = "1" ELSE "0";
  row_o_busout  <=  row_addr;
  bank_o_busout <=  bank_addr;
END ARCHITECTURE behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY decode_psum_filter_global_memory_unit IS
  PORT(
  addr_read_in                    : IN slice_addr_type;
  width_read_in                   : IN bit_type;

  en_e_busout                     : OUT bit_type;
  bank_e_busout                   : OUT bank_addr_type;
  row_e_busout                    : OUT row_addr_type;

  en_o_busout                     : OUT bit_type;
  bank_o_busout                   : OUT bank_addr_type;
  row_o_busout                    : OUT row_addr_type
  );
END ENTITY decode_psum_filter_global_memory_unit;

ARCHITECTURE behavior OF decode_psum_filter_global_memory_unit IS
  SIGNAL    half_addr   : bit3_type;
  SIGNAL    eo_addr     : bit_type;
  SIGNAL    row_addr    : row_addr_type;
  SIGNAL    bank_addr   : bank_addr_type;
  SIGNAL    row_addr1   : row_addr_type;
  SIGNAL    bank_addr1  : bank_addr_type;
  SIGNAL    eo_cross    : bit_type;
BEGIN
  half_addr     <=  addr_read_in( HALF_ADDR_MEM );
  eo_addr       <=  addr_read_in( EO_ADDR_MEM );
  row_addr      <=  addr_read_in( ROW_ADDR_MEM );
  bank_addr     <=  addr_read_in( BANK_ADDR_MEM );
  eo_cross      <=  "1" WHEN width_read_in = "0" AND unsigned(half_addr) > 4 ELSE -- 8B
                    "0"; -- 2B/4B no cross

  row_addr1     <=  std_logic_vector( unsigned(row_addr) + 1 );
  bank_addr1    <=  std_logic_vector( unsigned(bank_addr) + 1 );

  en_e_busout   <=  "1" WHEN eo_addr = "0" OR eo_cross = "1" ELSE "0";
  row_e_busout  <=  row_addr        WHEN eo_addr = "0" ELSE
                    row_addr1       WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
  bank_e_busout <=  bank_addr       WHEN eo_addr = "0"
                                      OR ( eo_cross = "1" AND row_addr /= (row_addr'range => '1') ) ELSE
                    bank_addr1      WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');

  en_o_busout   <=  "1" WHEN eo_addr = "1" OR eo_cross = "1" ELSE "0";
  row_o_busout  <=  row_addr;
  bank_o_busout <=  bank_addr;
END ARCHITECTURE behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY decode_read_global_memory_unit IS
  GENERIC(
    read_halfs : integer
  );
  PORT(
  addr_read_in                    : IN slice_addr_type;

  en_e_busout                     : OUT bit_type;
  bank_e_busout                   : OUT bank_addr_type;
  row_e_busout                    : OUT row_addr_type;

  en_o_busout                     : OUT bit_type;
  bank_o_busout                   : OUT bank_addr_type;
  row_o_busout                    : OUT row_addr_type
  );
END ENTITY decode_read_global_memory_unit;

ARCHITECTURE behavior OF decode_read_global_memory_unit IS
  SIGNAL    half_addr   : bit3_type;
  SIGNAL    eo_addr     : bit_type;
  SIGNAL    row_addr    : row_addr_type;
  SIGNAL    bank_addr   : bank_addr_type;
  SIGNAL    row_addr1   : row_addr_type;
  SIGNAL    bank_addr1  : bank_addr_type;
  SIGNAL    eo_cross    : bit_type;
BEGIN
  half_addr     <=  addr_read_in( HALF_ADDR_MEM );
  eo_addr       <=  addr_read_in( EO_ADDR_MEM );
  row_addr      <=  addr_read_in( ROW_ADDR_MEM );
  bank_addr     <=  addr_read_in( BANK_ADDR_MEM );
  eo_cross      <=  "1" WHEN unsigned(half_addr) > (8-read_halfs) ELSE "0";

  row_addr1     <=  std_logic_vector( unsigned(row_addr) + 1 );
  bank_addr1    <=  std_logic_vector( unsigned(bank_addr) + 1 );

  en_e_busout   <=  "1" WHEN eo_addr = "0" OR eo_cross = "1" ELSE "0";
  row_e_busout  <=  row_addr        WHEN eo_addr = "0" ELSE
                    row_addr1       WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');
  bank_e_busout <=  bank_addr       WHEN eo_addr = "0"
                                      OR ( eo_cross = "1" AND row_addr /= (row_addr'range => '1') ) ELSE
                    bank_addr1      WHEN eo_cross = "1" ELSE
                    (OTHERS=>'X');

  en_o_busout   <=  "1" WHEN eo_addr = "1" OR eo_cross = "1" ELSE "0";
  row_o_busout  <=  row_addr;
  bank_o_busout <=  bank_addr;
END ARCHITECTURE behavior;

-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY global_memory_unit IS
  PORT(
  clock                           : IN std_logic;
  reset                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- Memory control signals
  ---------------------------------------------------------------------------
  slp_in                          : IN bit_type;
  sd_in                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- INPUT engine signals
  ---------------------------------------------------------------------------
  en_mem_input_in                 : IN bit_type;
  slice_mem_input_in              : IN slice_mask_type;
  addr_mem_input_in               : IN slice_addr_type;
  word_mem_input_in               : IN bit128_type;

  err_collide_busout              : OUT bit7_type;

  ---------------------------------------------------------------------------
  -- FILTER engine signals
  ---------------------------------------------------------------------------
  en_mem_data_filter_in           : IN bit_type;
  width_mem_data_filter_in        : IN bit2_type;
  slice_mem_data_filter_in        : IN slice_mask_type;
  addr_mem_data_filter_in         : IN slice_addr_type;
  word0_data_filter_busout        : OUT bit144_type;
  word1_data_filter_busout        : OUT bit144_type;
  word2_data_filter_busout        : OUT bit144_type;
  word3_data_filter_busout        : OUT bit144_type;
  word4_data_filter_busout        : OUT bit144_type;
  word5_data_filter_busout        : OUT bit144_type;
  word6_data_filter_busout        : OUT bit144_type;
  word7_data_filter_busout        : OUT bit144_type;

  en_mem_psum_filter_in           : IN bit_type;
  width_mem_psum_filter_in        : IN bit_type;
  slice_mem_psum_filter_in        : IN slice_mask_type;
  addr_mem_psum_filter_in         : IN slice_addr_type;
  word0_psum_filter_busout        : OUT bit64_type;
  word1_psum_filter_busout        : OUT bit64_type;
  word2_psum_filter_busout        : OUT bit64_type;
  word3_psum_filter_busout        : OUT bit64_type;
  word4_psum_filter_busout        : OUT bit64_type;
  word5_psum_filter_busout        : OUT bit64_type;
  word6_psum_filter_busout        : OUT bit64_type;
  word7_psum_filter_busout        : OUT bit64_type;

  en_mem_res_filter_in            : IN bit_type;
  width_mem_res_filter_in         : IN bit_type;
  slice_mem_res_filter_in         : IN slice_mask_type;
  addr_mem_res_filter_in          : IN slice_addr_type;
  word0_mem_res_filter_in         : IN bit64_type;
  word1_mem_res_filter_in         : IN bit64_type;
  word2_mem_res_filter_in         : IN bit64_type;
  word3_mem_res_filter_in         : IN bit64_type;
  word4_mem_res_filter_in         : IN bit64_type;
  word5_mem_res_filter_in         : IN bit64_type;
  word6_mem_res_filter_in         : IN bit64_type;
  word7_mem_res_filter_in         : IN bit64_type;

  ---------------------------------------------------------------------------
  -- POST engine signals
  --------------------------------------------------------------------------
  en_mem_data_post_in             : IN bit_type;
  addr_mem_data_post_in           : IN global_addr_type;
  word_data_post_busout           : OUT bit128_type;

  en_mem_res_post_in              : IN bit_type;
  wr4_mem_res_post_in             : IN bit_type;    -- write 2 or 4 half-words
  addr_mem_res_post_in            : IN global_addr_type;
  word_mem_res_post_in            : IN bit64_type;

  ---------------------------------------------------------------------------
  -- OUTPUT engine signals
  ---------------------------------------------------------------------------
  en_mem_output_in                : IN bit_type;
  addr_mem_output_in              : IN global_addr_type;
  word_output_busout              : OUT bit128_type
  );
END ENTITY global_memory_unit;

ARCHITECTURE behavior OF global_memory_unit IS
-- pragma translate_off
  CONSTANT disable_print : boolean := false;
  CONSTANT start_print   : time := 0 ns;
-- pragma translate_on

  COMPONENT wrapper_bank_mem IS
-- pragma translate_off
    GENERIC(
      name              : string := ""
      );
-- pragma translate_on
    PORT (
      clk               : IN std_logic;
      en                : IN bit_type;
      we                : IN bit_type;
      addr              : IN row_addr_type;
      mask_in           : IN bit8_type; -- half-word masks
      word_in           : IN bit128_type;
      slp_in              : IN bit_type;
      sd_in               : IN bit_type;
      word_out          : OUT bit128_type
      );
  END COMPONENT wrapper_bank_mem;

  COMPONENT decode_input_global_memory_unit IS
    PORT(
    addr_write_in                   : IN slice_addr_type;
    word_write_in                   : IN bit128_type;

    en_e_busout                     : OUT bit_type;
    bank_e_busout                   : OUT bank_addr_type;
    row_e_busout                    : OUT row_addr_type;
    mask_e_busout                   : OUT bit8_type;
    word_e_busout                   : OUT bit128_type;

    en_o_busout                     : OUT bit_type;
    bank_o_busout                   : OUT bank_addr_type;
    row_o_busout                    : OUT row_addr_type;
    mask_o_busout                   : OUT bit8_type;
    word_o_busout                   : OUT bit128_type
    );
  END COMPONENT decode_input_global_memory_unit;

  COMPONENT decode_data_filter_global_memory_unit IS
    PORT(
    addr_read_in                    : IN slice_addr_type;
    width_read_in                   : IN bit2_type;

    en_e_busout                     : OUT bit_type;
    bank_e_busout                   : OUT bank_addr_type;
    row_e_busout                    : OUT row_addr_type;

    en_o_busout                     : OUT bit_type;
    bank_o_busout                   : OUT bank_addr_type;
    row_o_busout                    : OUT row_addr_type
    );
  END COMPONENT decode_data_filter_global_memory_unit;

  COMPONENT decode_psum_filter_global_memory_unit IS
    PORT(
    addr_read_in                    : IN slice_addr_type;
    width_read_in                   : IN bit_type;

    en_e_busout                     : OUT bit_type;
    bank_e_busout                   : OUT bank_addr_type;
    row_e_busout                    : OUT row_addr_type;

    en_o_busout                     : OUT bit_type;
    bank_o_busout                   : OUT bank_addr_type;
    row_o_busout                    : OUT row_addr_type
    );
  END COMPONENT decode_psum_filter_global_memory_unit;


  COMPONENT decode_res_filter_global_memory_unit IS
    PORT(
    clock                           : IN std_logic;
    addr_write_in                   : IN slice_addr_type;
    word_write_in                   : IN bit64_type;
    width_write_in                  : IN bit_type;

    en_e_busout                     : OUT bit_type;
    bank_e_busout                   : OUT bank_addr_type;
    row_e_busout                    : OUT row_addr_type;
    mask_e_busout                   : OUT bit8_type;
    word_e_busout                   : OUT bit128_type;

    en_o_busout                     : OUT bit_type;
    bank_o_busout                   : OUT bank_addr_type;
    row_o_busout                    : OUT row_addr_type;
    mask_o_busout                   : OUT bit8_type;
    word_o_busout                   : OUT bit128_type
    );
  END COMPONENT decode_res_filter_global_memory_unit;

  COMPONENT decode_res_post_global_memory_unit IS
    PORT(
    clock                           : IN std_logic;
    addr_write_in                   : IN slice_addr_type;
    word_write_in                   : IN bit64_type;
    width_write_in                  : IN bit_type;

    en_e_busout                     : OUT bit_type;
    bank_e_busout                   : OUT bank_addr_type;
    row_e_busout                    : OUT row_addr_type;
    mask_e_busout                   : OUT bit8_type;
    word_e_busout                   : OUT bit128_type;

    en_o_busout                     : OUT bit_type;
    bank_o_busout                   : OUT bank_addr_type;
    row_o_busout                    : OUT row_addr_type;
    mask_o_busout                   : OUT bit8_type;
    word_o_busout                   : OUT bit128_type
    );
  END COMPONENT decode_res_post_global_memory_unit;

  COMPONENT decode_read_global_memory_unit IS
    GENERIC(
      read_halfs : integer
    );
    PORT(
    addr_read_in                    : IN slice_addr_type;

    en_e_busout                     : OUT bit_type;
    bank_e_busout                   : OUT bank_addr_type;
    row_e_busout                    : OUT row_addr_type;

    en_o_busout                     : OUT bit_type;
    bank_o_busout                   : OUT bank_addr_type;
    row_o_busout                    : OUT row_addr_type
    );
  END COMPONENT decode_read_global_memory_unit;

  TYPE sel_type IS RECORD
    en      : bit_type;
    we      : bit_type;
    addr    : row_addr_type;
    mask    : bit8_type;
    word    : bit128_type;
  END RECORD;

  SIGNAL  words_res_filter        : array_bit64_type(SLICE_RANGE);
  SIGNAL  words_res_post          : array_bit64_type(SLICE_RANGE);
  SIGNAL  words_data_filter       : array_bit144_type(SLICE_RANGE);
  SIGNAL  words_psum_filter       : array_bit64_type(SLICE_RANGE);

  SIGNAL  words_data_post         : array_bit128_type(SLICE_RANGE);
  SIGNAL  words_output            : array_bit128_type(SLICE_RANGE);

  SIGNAL  collide_slice           : array_slice_type( COLL_RANGE );
  SIGNAL  err_collide_i           : coll_type;

BEGIN

  words_res_filter(0) <= word0_mem_res_filter_in;
  words_res_filter(1) <= word1_mem_res_filter_in;
  words_res_filter(2) <= word2_mem_res_filter_in;
  words_res_filter(3) <= word3_mem_res_filter_in;
  words_res_filter(4) <= word4_mem_res_filter_in;
  words_res_filter(5) <= word5_mem_res_filter_in;
  words_res_filter(6) <= word6_mem_res_filter_in;
  words_res_filter(7) <= word7_mem_res_filter_in;

  slice_gen: FOR slice IN SLICE_RANGE GENERATE
    SIGNAL en_mem_input_i           : bit_type;
    SIGNAL slice_mem_input_i        : slice_mask_type;
    SIGNAL addr_mem_input_i         : slice_addr_type;
    SIGNAL word_mem_input_i         : bit128_type;

    SIGNAL en_e_input               : bit_type;
    SIGNAL bank_e_input             : bank_addr_type;
    SIGNAL row_e_input              : row_addr_type;
    SIGNAL mask_e_input             : bit8_type;
    SIGNAL word_e_input             : bit128_type;
    SIGNAL en_o_input               : bit_type;
    SIGNAL bank_o_input             : bank_addr_type;
    SIGNAL row_o_input              : row_addr_type;
    SIGNAL mask_o_input             : bit8_type;
    SIGNAL word_o_input             : bit128_type;

    SIGNAL en_e_data_filter         : bit_type;
    SIGNAL bank_e_data_filter       : bank_addr_type;
    SIGNAL row_e_data_filter        : row_addr_type;
    SIGNAL en_o_data_filter         : bit_type;
    SIGNAL bank_o_data_filter       : bank_addr_type;
    SIGNAL row_o_data_filter        : row_addr_type;

    SIGNAL en_e_psum_filter         : bit_type;
    SIGNAL bank_e_psum_filter       : bank_addr_type;
    SIGNAL row_e_psum_filter        : row_addr_type;
    SIGNAL en_o_psum_filter         : bit_type;
    SIGNAL bank_o_psum_filter       : bank_addr_type;
    SIGNAL row_o_psum_filter        : row_addr_type;

    SIGNAL en_e_res_filter          : bit_type;
    SIGNAL bank_e_res_filter        : bank_addr_type;
    SIGNAL row_e_res_filter         : row_addr_type;
    SIGNAL mask_e_res_filter        : bit8_type;
    SIGNAL word_e_res_filter        : bit128_type;
    SIGNAL en_o_res_filter          : bit_type;
    SIGNAL bank_o_res_filter        : bank_addr_type;
    SIGNAL row_o_res_filter         : row_addr_type;
    SIGNAL mask_o_res_filter        : bit8_type;
    SIGNAL word_o_res_filter        : bit128_type;

    SIGNAL en_e_data_post           : bit_type;
    SIGNAL bank_e_data_post         : bank_addr_type;
    SIGNAL row_e_data_post          : row_addr_type;
    SIGNAL en_o_data_post           : bit_type;
    SIGNAL bank_o_data_post         : bank_addr_type;
    SIGNAL row_o_data_post          : row_addr_type;

    SIGNAL en_e_res_post            : bit_type;
    SIGNAL bank_e_res_post          : bank_addr_type;
    SIGNAL row_e_res_post           : row_addr_type;
    SIGNAL mask_e_res_post          : bit8_type;
    SIGNAL word_e_res_post          : bit128_type;
    SIGNAL en_o_res_post            : bit_type;
    SIGNAL bank_o_res_post          : bank_addr_type;
    SIGNAL row_o_res_post           : row_addr_type;
    SIGNAL mask_o_res_post          : bit8_type;
    SIGNAL word_o_res_post          : bit128_type;

    SIGNAL en_mem_res_post_i        : bit_type;
    SIGNAL wr4_mem_res_post_i       : bit_type;    -- write 2 or 4 half-words
    SIGNAL addr_mem_res_post_i      : global_addr_type;
    SIGNAL word_mem_res_post_i      : bit64_type;

    SIGNAL en_mem_res_filter_i      : bit_type;
    SIGNAL width_mem_res_filter_i   : bit_type;
    SIGNAL slice_mem_res_filter_i   : slice_mask_type;
    SIGNAL addr_mem_res_filter_i    : slice_addr_type;
    SIGNAL word_res_filter_i        : bit64_type;

    SIGNAL en_e_output              : bit_type;
    SIGNAL bank_e_output            : bank_addr_type;
    SIGNAL row_e_output             : row_addr_type;
    SIGNAL en_o_output              : bit_type;
    SIGNAL bank_o_output            : bank_addr_type;
    SIGNAL row_o_output             : row_addr_type;

    SIGNAL words_e_out              : array_bit128_type(BANKS_MEM_RANGE);
    SIGNAL words_o_out              : array_bit128_type(BANKS_MEM_RANGE);

    SIGNAL eo_addr_data_filter_q    : bit_type;
    SIGNAL half_addr_data_filter_q  : bit3_type;
    SIGNAL bank_e_data_filter_q     : bit4_type;
    SIGNAL bank_o_data_filter_q     : bit4_type;
    SIGNAL word_e_data_filter       : bit128_type;
    SIGNAL word_o_data_filter       : bit128_type;
    SIGNAL word2_data_filter        : bit256_type;

    SIGNAL eo_addr_psum_filter_q    : bit_type;
    SIGNAL half_addr_psum_filter_q  : bit3_type;
    SIGNAL bank_e_psum_filter_q     : bit4_type;
    SIGNAL bank_o_psum_filter_q     : bit4_type;
    SIGNAL word_e_psum_filter       : bit128_type;
    SIGNAL word_o_psum_filter       : bit128_type;
    SIGNAL word2_psum_filter        : bit256_type;

    SIGNAL eo_addr_data_post_q      : bit_type;
    SIGNAL half_addr_data_post_q    : bit3_type;
    SIGNAL bank_e_data_post_q       : bit4_type;
    SIGNAL bank_o_data_post_q       : bit4_type;
    SIGNAL word_e_data_post         : bit128_type;
    SIGNAL word_o_data_post         : bit128_type;
    SIGNAL word2_data_post          : bit256_type;
    SIGNAL en_mem_data_post_i       : bit_type;
    SIGNAL addr_mem_data_post_i     : global_addr_type;

    SIGNAL eo_addr_output_q         : bit_type;
    SIGNAL half_addr_output_q       : bit3_type;
    SIGNAL bank_e_output_q          : bit4_type;
    SIGNAL bank_o_output_q          : bit4_type;
    SIGNAL word_e_output            : bit128_type;
    SIGNAL word_o_output            : bit128_type;
    SIGNAL word2_output             : bit256_type;
    SIGNAL en_mem_output_i          : bit_type;
    SIGNAL addr_mem_output_i        : global_addr_type;

    SIGNAL collide_e                : array_bank_type(COLL_RANGE);
    SIGNAL collide_o                : array_bank_type(COLL_RANGE);

  BEGIN

    en_mem_input_i        <= en_mem_input_in        AFTER 10 ps WHEN rising_edge(clock);
    slice_mem_input_i     <= slice_mem_input_in     AFTER 10 ps WHEN rising_edge(clock) ; -- DISABLE gating AND en_mem_input_in = "1";
    addr_mem_input_i      <= addr_mem_input_in      AFTER 10 ps WHEN rising_edge(clock) ; -- DISABLE gating AND en_mem_input_in = "1";
    word_mem_input_i      <= word_mem_input_in      AFTER 10 ps WHEN rising_edge(clock) ; -- DISABLE gating AND en_mem_input_in = "1";

    en_mem_res_post_i     <= en_mem_res_post_in     AFTER 10 ps WHEN rising_edge(clock);
    wr4_mem_res_post_i    <= wr4_mem_res_post_in    AFTER 10 ps WHEN rising_edge(clock) ; -- DISABLE gating AND en_mem_res_post_in = "1";
    addr_mem_res_post_i   <= addr_mem_res_post_in   AFTER 10 ps WHEN rising_edge(clock) ; -- DISABLE gating AND en_mem_res_post_in = "1";
    word_mem_res_post_i   <= word_mem_res_post_in   AFTER 10 ps WHEN rising_edge(clock) ; -- DISABLE gating AND en_mem_res_post_in = "1";

    en_mem_output_i       <= en_mem_output_in       AFTER 10 ps WHEN rising_edge(clock);
    addr_mem_output_i     <= addr_mem_output_in     AFTER 10 ps WHEN rising_edge(clock) ; -- DISABLE gating AND en_mem_output_in = "1";

    en_mem_data_post_i    <= en_mem_data_post_in    AFTER 10 ps WHEN rising_edge(clock);
    addr_mem_data_post_i  <= addr_mem_data_post_in  AFTER 10 ps WHEN rising_edge(clock) ; -- DISABLE gating AND en_mem_data_post_in = "1";

    en_mem_res_filter_i     <= en_mem_res_filter_in     AFTER 10 ps WHEN rising_edge(clock);
    width_mem_res_filter_i  <= width_mem_res_filter_in  AFTER 10 ps WHEN rising_edge(clock) ; 
    slice_mem_res_filter_i  <= slice_mem_res_filter_in  AFTER 10 ps WHEN rising_edge(clock) ; 
    addr_mem_res_filter_i   <= addr_mem_res_filter_in   AFTER 10 ps WHEN rising_edge(clock) ; 
    word_res_filter_i       <= words_res_filter(slice)  AFTER 10 ps WHEN rising_edge(clock);

    a_decode_input: decode_input_global_memory_unit PORT MAP(
        addr_write_in   => addr_mem_input_i,
        word_write_in   => word_mem_input_i,

        en_e_busout     => en_e_input,
        bank_e_busout   => bank_e_input,
        row_e_busout    => row_e_input,
        mask_e_busout   => mask_e_input,
        word_e_busout   => word_e_input,

        en_o_busout     => en_o_input,
        bank_o_busout   => bank_o_input,
        row_o_busout    => row_o_input,
        mask_o_busout   => mask_o_input,
        word_o_busout   => word_o_input
        );

    a_decode_data_filter: decode_data_filter_global_memory_unit PORT MAP(
        addr_read_in    => addr_mem_data_filter_in,
        width_read_in   => width_mem_data_filter_in,

        en_e_busout     => en_e_data_filter,
        bank_e_busout   => bank_e_data_filter,
        row_e_busout    => row_e_data_filter,

        en_o_busout     => en_o_data_filter,
        bank_o_busout   => bank_o_data_filter,
        row_o_busout    => row_o_data_filter
        );

    a_decode_psum_filter: decode_psum_filter_global_memory_unit PORT MAP(
        addr_read_in    => addr_mem_psum_filter_in,
        width_read_in   => width_mem_psum_filter_in,

        en_e_busout     => en_e_psum_filter,
        bank_e_busout   => bank_e_psum_filter,
        row_e_busout    => row_e_psum_filter,

        en_o_busout     => en_o_psum_filter,
        bank_o_busout   => bank_o_psum_filter,
        row_o_busout    => row_o_psum_filter
        );

    a_decode_res_filter: decode_res_filter_global_memory_unit
    PORT MAP(
        clock           => clock,
        addr_write_in   => addr_mem_res_filter_i,
        word_write_in   => word_res_filter_i, -- words_res_filter(slice),
        width_write_in  => width_mem_res_filter_i,

        en_e_busout     => en_e_res_filter,
        bank_e_busout   => bank_e_res_filter,
        row_e_busout    => row_e_res_filter,
        mask_e_busout   => mask_e_res_filter,
        word_e_busout   => word_e_res_filter,

        en_o_busout     => en_o_res_filter,
        bank_o_busout   => bank_o_res_filter,
        row_o_busout    => row_o_res_filter,
        mask_o_busout   => mask_o_res_filter,
        word_o_busout   => word_o_res_filter
        );

    a_decode_data_post: decode_read_global_memory_unit GENERIC MAP( read_halfs => 8 ) PORT MAP(
        addr_read_in    => addr_mem_data_post_i(slice_addr_type'range),

        en_e_busout     => en_e_data_post,
        bank_e_busout   => bank_e_data_post,
        row_e_busout    => row_e_data_post,

        en_o_busout     => en_o_data_post,
        bank_o_busout   => bank_o_data_post,
        row_o_busout    => row_o_data_post
        );

    a_decode_res_post: decode_res_post_global_memory_unit
    PORT MAP(
        clock           => clock,
        addr_write_in   => addr_mem_res_post_i(slice_addr_type'range),
        word_write_in   => word_mem_res_post_i,
        width_write_in  => wr4_mem_res_post_i,

        en_e_busout     => en_e_res_post,
        bank_e_busout   => bank_e_res_post,
        row_e_busout    => row_e_res_post,
        mask_e_busout   => mask_e_res_post,
        word_e_busout   => word_e_res_post,

        en_o_busout     => en_o_res_post,
        bank_o_busout   => bank_o_res_post,
        row_o_busout    => row_o_res_post,
        mask_o_busout   => mask_o_res_post,
        word_o_busout   => word_o_res_post
        );

    a_decode_output: decode_read_global_memory_unit GENERIC MAP( read_halfs => 8 ) PORT MAP(
        addr_read_in    => addr_mem_output_i(slice_addr_type'range),

        en_e_busout     => en_e_output,
        bank_e_busout   => bank_e_output,
        row_e_busout    => row_e_output,

        en_o_busout     => en_o_output,
        bank_o_busout   => bank_o_output,
        row_o_busout    => row_o_output
        );


    even_gen: FOR bank in BANKS_MEM_RANGE GENERATE
      SIGNAL input_hit        : bit_type;
      SIGNAL data_filter_hit  : bit_type;
      SIGNAL psum_filter_hit  : bit_type;
      SIGNAL res_filter_hit   : bit_type;
      SIGNAL data_post_hit    : bit_type;
      SIGNAL res_post_hit     : bit_type;
      SIGNAL output_hit       : bit_type;
      SIGNAL sel_hit          : bit7_type;
      SIGNAL sel              : sel_type;
      SIGNAL collide_i        : coll_type;
    BEGIN

      -- input addresses writes 16B per transfer
      input_hit   <= 
                      "1" WHEN en_mem_input_i = "1"
                            AND slice_mem_input_i(slice) = '1'
                            AND en_e_input = "1"
                            AND unsigned(bank_e_input) = bank ELSE
                      "0";
      data_filter_hit <=
                      "1" WHEN en_mem_data_filter_in = "1"
                            AND slice_mem_data_filter_in(slice) = '1'
                            AND en_e_data_filter = "1"
                            AND unsigned(bank_e_data_filter) = bank ELSE
                      "0";
      psum_filter_hit <=
                      "1" WHEN en_mem_psum_filter_in = "1"
                            AND slice_mem_psum_filter_in(slice) = '1'
                            AND en_e_psum_filter = "1"
                            AND unsigned(bank_e_psum_filter) = bank ELSE
                      "0";
      res_filter_hit  <= 
                      "1" WHEN en_mem_res_filter_i = "1"
                            AND slice_mem_res_filter_i(slice) = '1'
                            AND en_e_res_filter = "1"
                            AND unsigned(bank_e_res_filter) = bank ELSE
                      "0";
      data_post_hit <=
                      "1" WHEN en_mem_data_post_i = "1"
                            AND unsigned(addr_mem_data_post_i(SLICE_SEL_ADDR_MEM)) = slice
                            AND en_e_data_post = "1"
                            AND unsigned(bank_e_data_post) = bank ELSE
                      "0";
      res_post_hit  <= 
                      "1" WHEN en_mem_res_post_i = "1"
                            AND unsigned(addr_mem_res_post_i(SLICE_SEL_ADDR_MEM)) = slice
                            AND en_e_res_post = "1"
                            AND unsigned(bank_e_res_post) = bank ELSE
                      "0";
      output_hit <=
                      "1" WHEN en_mem_output_i = "1"
                            AND unsigned(addr_mem_output_i(SLICE_SEL_ADDR_MEM)) = slice
                            AND en_e_output = "1"
                            AND unsigned(bank_e_output) = bank ELSE
                      "0";

      sel_hit <= input_hit & data_filter_hit & psum_filter_hit & res_filter_hit & data_post_hit & res_post_hit & output_hit;
      WITH sel_hit SELECT
        sel <=
                      ( "1", "1", row_e_input,        mask_e_input,       word_e_input      ) WHEN "1000000",
                      ( "1", "0", row_e_data_filter,  (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN "0100000",
                      ( "1", "0", row_e_psum_filter,  (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN "0010000",
                      ( "1", "1", row_e_res_filter,   mask_e_res_filter,  word_e_res_filter ) WHEN "0001000",
                      ( "1", "0", row_e_data_post,    (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN "0000100",
                      ( "1", "1", row_e_res_post,     mask_e_res_post,    word_e_res_post   ) WHEN "0000010",
                      ( "1", "0", row_e_output,       (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN "0000001",
                      ( "0", "X", (OTHERS=>'X'),      (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN OTHERS;

      collide_i( INPUT_RES_COLL )   <= '1' WHEN input_hit = "1"       AND sel_hit /= "1000000" ELSE '0';
      collide_i( FILTER_DATA_COLL ) <= '1' WHEN data_filter_hit = "1" AND sel_hit /= "0100000" ELSE '0';
      collide_i( FILTER_PSUM_COLL ) <= '1' WHEN psum_filter_hit = "1" AND sel_hit /= "0010000" ELSE '0';
      collide_i( FILTER_RES_COLL )  <= '1' WHEN res_filter_hit = "1"  AND sel_hit /= "0001000" ELSE '0';
      collide_i( POST_DATA_COLL )   <= '1' WHEN data_post_hit = "1"   AND sel_hit /= "0000100" ELSE '0';
      collide_i( POST_RES_COLL )    <= '1' WHEN res_post_hit = "1"    AND sel_hit /= "0000010" ELSE '0';
      collide_i( OUTPUT_DATA_COLL ) <= '1' WHEN output_hit = "1"      AND sel_hit /= "0000001" ELSE '0';
      collide_e_gen: FOR i IN COLL_RANGE GENERATE
        SIGNAL  collide_d : std_logic;
      BEGIN
        collide_d <= '0' WHEN reset = "1" ELSE collide_i(i);
        collide_e(i)(bank) <= collide_d AFTER 10 ps WHEN rising_edge(clock);
      END GENERATE collide_e_gen;

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 102 ps;

    IF now >= start_print THEN
      IF sel_hit /= (sel_hit'range => '0') THEN
        dump_cycle( buf, "GLB>" );
        dump_integer( buf, slice,  " slice#" );
        dump_integer( buf, bank, " bank_e#" );
        dump_bit( buf, sel_hit, " sel_hit " );
        dump_bit( buf, collide_i, " collide_i " );
        writeline( output, buf );
      END IF;
    END IF;
  END PROCESS dump;
-- pragma translate_on

      a_even_mem: wrapper_bank_mem 
      -- pragma translate_off
      GENERIC MAP( name => integer'image(slice)& string'(".") & integer'image(bank) & string'(".e")) 
      -- pragma translate_on
      PORT MAP(
                      clk         => clock,
                      en          => sel.en,
                      we          => sel.we,
                      addr        => sel.addr,
                      mask_in     => sel.mask,
                      word_in     => sel.word,
                      slp_in      => slp_in,
                      sd_in       => sd_in,
                      word_out    => words_e_out( bank )
                      );

      -- pragma translate_off
      dump_even: PROCESS
        VARIABLE  buf       : line;
      BEGIN
        IF disable_print THEN
          WAIT;
        END IF;
        WAIT UNTIL falling_edge( clock );
        WAIT FOR 102 ps;

        IF now >= start_print THEN

          IF sel.en = "1" THEN
            dump_cycle( buf, "GLB>");
            dump_integer( buf, slice, " #" );
            dump_integer( buf, bank, " .", ".e" );
            dump_bit( buf, sel.we, " we " );
            dump_bit( buf, sel.addr, " addr " );
            dump_hex( buf, sel.mask, " mask " );
            dump_hex( buf, sel.word, " word_in " );
            dump_bit( buf, sel_hit, " : hit ");
            dump_bit( buf, input_hit, " input ");
            dump_bit( buf, data_filter_hit, " data_filter ");
            dump_bit( buf, psum_filter_hit, " psum_filter ");
            dump_bit( buf, res_filter_hit, " res_filter ");
            dump_bit( buf, data_post_hit, " data_post ");
            dump_bit( buf, res_post_hit, " res_post ");
            dump_bit( buf, output_hit, " output ");
            writeline(output, buf );
          END IF;
        END IF;
      END PROCESS dump_even;
      -- pragma translate_on
    END GENERATE even_gen;

    odd_gen: FOR bank in BANKS_MEM_RANGE GENERATE
      SIGNAL input_hit        : bit_type;
      SIGNAL data_filter_hit  : bit_type;
      SIGNAL psum_filter_hit  : bit_type;
      SIGNAL res_filter_hit   : bit_type;
      SIGNAL data_post_hit    : bit_type;
      SIGNAL res_post_hit     : bit_type;
      SIGNAL output_hit       : bit_type;
      SIGNAL sel_hit          : bit7_type;
      SIGNAL sel              : sel_type;
      SIGNAL collide_i        : bit7_type;
    BEGIN

      -- input addresses writes 16B per transfer
      input_hit   <= 
                      "1" WHEN en_mem_input_i = "1"
                            AND slice_mem_input_i(slice) = '1'
                            AND en_o_input = "1"
                            AND unsigned(bank_o_input) = bank ELSE
                      "0";
      data_filter_hit <=
                      "1" WHEN en_mem_data_filter_in = "1"
                            AND slice_mem_data_filter_in(slice) = '1'
                            AND en_o_data_filter = "1"
                            AND unsigned(bank_o_data_filter) = bank ELSE
                      "0";
      psum_filter_hit <=
                      "1" WHEN en_mem_psum_filter_in = "1"
                            AND slice_mem_psum_filter_in(slice) = '1'
                            AND en_o_psum_filter = "1"
                            AND unsigned(bank_o_psum_filter) = bank ELSE
                      "0";
      res_filter_hit  <= 
                      "1" WHEN en_mem_res_filter_i = "1"
                            AND slice_mem_res_filter_i(slice) = '1'
                            AND en_o_res_filter = "1"
                            AND unsigned(bank_o_res_filter) = bank ELSE
                      "0";
      data_post_hit <=
                      "1" WHEN en_mem_data_post_i = "1"
                            AND unsigned(addr_mem_data_post_i(SLICE_SEL_ADDR_MEM)) = slice
                            AND en_o_data_post = "1"
                            AND unsigned(bank_o_data_post) = bank ELSE
                      "0";
      res_post_hit  <= 
                      "1" WHEN en_mem_res_post_i = "1"
                            AND unsigned(addr_mem_res_post_i(SLICE_SEL_ADDR_MEM)) = slice
                            AND en_o_res_post = "1"
                            AND unsigned(bank_o_res_post) = bank ELSE
                      "0";
      output_hit <=
                      "1" WHEN en_mem_output_i = "1"
                            AND unsigned(addr_mem_output_i(SLICE_SEL_ADDR_MEM)) = slice
                            AND en_o_output = "1"
                            AND unsigned(bank_o_output) = bank ELSE
                      "0";

      sel_hit <= input_hit & data_filter_hit & psum_filter_hit & res_filter_hit & data_post_hit & res_post_hit & output_hit;
      WITH sel_hit SELECT
        sel <=
                      ( "1", "1", row_o_input,        mask_o_input,       word_o_input      ) WHEN "1000000",
                      ( "1", "0", row_o_data_filter,  (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN "0100000",
                      ( "1", "0", row_o_psum_filter,  (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN "0010000",
                      ( "1", "1", row_o_res_filter,   mask_o_res_filter,  word_o_res_filter ) WHEN "0001000",
                      ( "1", "0", row_o_data_post,    (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN "0000100",
                      ( "1", "1", row_o_res_post,     mask_o_res_post,    word_o_res_post   ) WHEN "0000010",
                      ( "1", "0", row_o_output,       (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN "0000001",
                      ( "0", "X", (OTHERS=>'X'),      (OTHERS=>'X'),      (OTHERS=>'X')     ) WHEN OTHERS;

      collide_i( INPUT_RES_COLL )   <= '1' WHEN input_hit = "1"       AND sel_hit /= "1000000" ELSE '0';
      collide_i( FILTER_DATA_COLL ) <= '1' WHEN data_filter_hit = "1" AND sel_hit /= "0100000" ELSE '0';
      collide_i( FILTER_PSUM_COLL ) <= '1' WHEN psum_filter_hit = "1" AND sel_hit /= "0010000" ELSE '0';
      collide_i( FILTER_RES_COLL )  <= '1' WHEN res_filter_hit = "1"  AND sel_hit /= "0001000" ELSE '0';
      collide_i( POST_DATA_COLL )   <= '1' WHEN data_post_hit = "1"   AND sel_hit /= "0000100" ELSE '0';
      collide_i( POST_RES_COLL )    <= '1' WHEN res_post_hit = "1"    AND sel_hit /= "0000010" ELSE '0';
      collide_i( OUTPUT_DATA_COLL ) <= '1' WHEN output_hit = "1"      AND sel_hit /= "0000001" ELSE '0';
      collide_o_gen: FOR i IN COLL_RANGE GENERATE
        SIGNAL  collide_d : std_logic;
      BEGIN
        collide_d <= '0' WHEN reset = "1" ELSE collide_i(i);
        collide_o(i)(bank) <= collide_d AFTER 10 ps WHEN rising_edge(clock);
      END GENERATE collide_o_gen;

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 102 ps;

    IF now >= start_print THEN
      IF sel_hit /= (sel_hit'range => '0') THEN
        dump_cycle( buf, "GLB>" );
        dump_integer( buf, slice,  " slice#" );
        dump_integer( buf, bank, " bank_o#" );
        dump_bit( buf, sel_hit, " sel_hit " );
        dump_bit( buf, collide_i, " collide_i " );
        writeline( output, buf );
      END IF;
    END IF;
  END PROCESS dump;
-- pragma translate_on

      a_odd_mem: wrapper_bank_mem
      -- pragma translate_off
      GENERIC MAP( name => integer'image(slice)& string'(".") & integer'image(bank) & string'(".o"))
      -- pragma translate_on
      PORT MAP(
                      clk         => clock,
                      en          => sel.en,
                      we          => sel.we,
                      addr        => sel.addr,
                      mask_in     => sel.mask,
                      word_in     => sel.word,
                      slp_in      => slp_in,
                      sd_in       => sd_in,
                      word_out    => words_o_out( bank )
                      );

      -- pragma translate_off
      dump_odd: PROCESS
        VARIABLE  buf       : line;
      BEGIN
        IF true OR disable_print THEN
          WAIT;
        END IF;
        WAIT UNTIL falling_edge( clock );
        WAIT FOR 102 ps;

        IF now >= start_print THEN

          IF sel.en = "1" THEN
            dump_cycle( buf, "GLB>");
            dump_integer( buf, slice, " #" );
            dump_integer( buf, bank, " .", ".o" );
            dump_bit( buf, sel.we, " we " );
            dump_bit( buf, sel.addr, " addr " );
            dump_hex( buf, sel.mask, " mask " );
            dump_hex( buf, sel.word, " word_in " );
            dump_bit( buf, sel_hit, " : hit ");
            dump_bit( buf, input_hit, " input ");
            dump_bit( buf, data_filter_hit, " data_filter ");
            dump_bit( buf, psum_filter_hit, " psum_filter ");
            dump_bit( buf, res_filter_hit, " res_filter ");
            dump_bit( buf, data_post_hit, " data_post ");
            dump_bit( buf, res_post_hit, " res_post ");
            dump_bit( buf, output_hit, " output ");
            writeline(output, buf );
          END IF;
        END IF;
      END PROCESS dump_odd;
      -- pragma translate_on

    END GENERATE odd_gen;

    collide_slice_gen: FOR i IN COLL_RANGE GENERATE
      collide_slice(i)(slice) <= '0' WHEN collide_e(i) = (bank_type'range => '0') AND collide_o(i) = (bank_type'range => '0') ELSE '1';
    END GENERATE collide_slice_gen;

    eo_addr_data_filter_q   <= addr_mem_data_filter_in(EO_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_filter_in = "1";
    half_addr_data_filter_q <= addr_mem_data_filter_in(HALF_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_filter_in = "1";
    bank_e_data_filter_q    <= bank_e_data_filter AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_filter_in = "1";
    bank_o_data_filter_q    <= bank_o_data_filter AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_filter_in = "1";

    word_e_data_filter      <= words_e_out( to_integer(unsigned(bank_e_data_filter_q)) );
    word_o_data_filter      <= words_o_out( to_integer(unsigned(bank_o_data_filter_q)) );
    word2_data_filter       <= word_o_data_filter & word_e_data_filter WHEN eo_addr_data_filter_q = "0" ELSE word_e_data_filter & word_o_data_filter;

    WITH half_addr_data_filter_q SELECT
      words_data_filter(slice) <=
                      word2_data_filter(143+16*0 DOWNTO 16*0)  WHEN "000",
                      word2_data_filter(143+16*1 DOWNTO 16*1)  WHEN "001",
                      word2_data_filter(143+16*2 DOWNTO 16*2)  WHEN "010",
                      word2_data_filter(143+16*3 DOWNTO 16*3)  WHEN "011",
                      word2_data_filter(143+16*4 DOWNTO 16*4)  WHEN "100",
                      word2_data_filter(143+16*5 DOWNTO 16*5)  WHEN "101",
                      word2_data_filter(143+16*6 DOWNTO 16*6)  WHEN "110",
                      word2_data_filter(143+16*7 DOWNTO 16*7)  WHEN "111",
                      (OTHERS=>'X')                           WHEN OTHERS;

    eo_addr_psum_filter_q   <= addr_mem_psum_filter_in(EO_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_psum_filter_in = "1";
    half_addr_psum_filter_q <= addr_mem_psum_filter_in(HALF_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_psum_filter_in = "1";
    bank_e_psum_filter_q    <= bank_e_psum_filter AFTER 10 ps WHEN rising_edge(clock) AND en_mem_psum_filter_in = "1";
    bank_o_psum_filter_q    <= bank_o_psum_filter AFTER 10 ps WHEN rising_edge(clock) AND en_mem_psum_filter_in = "1";

    word_e_psum_filter      <= words_e_out( to_integer(unsigned(bank_e_psum_filter_q)) );
    word_o_psum_filter      <= words_o_out( to_integer(unsigned(bank_o_psum_filter_q)) );
    word2_psum_filter       <= word_o_psum_filter & word_e_psum_filter WHEN eo_addr_psum_filter_q = "0" ELSE word_e_psum_filter & word_o_psum_filter;

    WITH half_addr_psum_filter_q SELECT
      words_psum_filter(slice) <=
                      word2_psum_filter(63+16*0 DOWNTO 16*0)  WHEN "000",
                      word2_psum_filter(63+16*1 DOWNTO 16*1)  WHEN "001",
                      word2_psum_filter(63+16*2 DOWNTO 16*2)  WHEN "010",
                      word2_psum_filter(63+16*3 DOWNTO 16*3)  WHEN "011",
                      word2_psum_filter(63+16*4 DOWNTO 16*4)  WHEN "100",
                      word2_psum_filter(63+16*5 DOWNTO 16*5)  WHEN "101",
                      word2_psum_filter(63+16*6 DOWNTO 16*6)  WHEN "110",
                      word2_psum_filter(63+16*7 DOWNTO 16*7)  WHEN "111",
                      (OTHERS=>'X')                           WHEN OTHERS;

    eo_addr_data_post_q   <= addr_mem_data_post_i(EO_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_post_i = "1";
    half_addr_data_post_q <= addr_mem_data_post_i(HALF_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_post_i = "1";
    bank_e_data_post_q    <= bank_e_data_post AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_post_i = "1";
    bank_o_data_post_q    <= bank_o_data_post AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_post_i = "1";

    word_e_data_post      <= words_e_out( to_integer(unsigned(bank_e_data_post_q)) );
    word_o_data_post      <= words_o_out( to_integer(unsigned(bank_o_data_post_q)) );
    word2_data_post       <= word_o_data_post & word_e_data_post WHEN eo_addr_data_post_q = "0" ELSE word_e_data_post & word_o_data_post;

    WITH half_addr_data_post_q SELECT
      words_data_post(slice) <=
                      word2_data_post(127+16*0 DOWNTO 16*0)   WHEN "000",
                      word2_data_post(127+16*1 DOWNTO 16*1)   WHEN "001",
                      word2_data_post(127+16*2 DOWNTO 16*2)   WHEN "010",
                      word2_data_post(127+16*3 DOWNTO 16*3)   WHEN "011",
                      word2_data_post(127+16*4 DOWNTO 16*4)   WHEN "100",
                      word2_data_post(127+16*5 DOWNTO 16*5)   WHEN "101",
                      word2_data_post(127+16*6 DOWNTO 16*6)   WHEN "110",
                      word2_data_post(127+16*7 DOWNTO 16*7)   WHEN "111",
                      (OTHERS=>'X')                           WHEN OTHERS;

    slice_output_block: IF true GENERATE
      SIGNAL en_mem_output_i          : bit_type;
      SIGNAL addr_mem_output_i        : global_addr_type;
    BEGIN
      en_mem_output_i     <= en_mem_output_in     AFTER 10 ps WHEN rising_edge(clock);
      addr_mem_output_i   <= addr_mem_output_in   AFTER 10 ps WHEN rising_edge(clock) AND en_mem_output_in = "1";

      eo_addr_output_q   <= addr_mem_output_i(EO_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_output_i = "1";
      half_addr_output_q <= addr_mem_output_i(HALF_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_output_i = "1";
      bank_e_output_q    <= bank_e_output AFTER 10 ps WHEN rising_edge(clock) AND en_mem_output_i = "1";
      bank_o_output_q    <= bank_o_output AFTER 10 ps WHEN rising_edge(clock) AND en_mem_output_i = "1";
    END GENERATE slice_output_block;

    word_e_output      <= words_e_out( to_integer(unsigned(bank_e_output_q)) );
    word_o_output      <= words_o_out( to_integer(unsigned(bank_o_output_q)) );
    word2_output       <= word_o_output & word_e_output WHEN eo_addr_output_q = "0" ELSE word_e_output & word_o_output;

    WITH half_addr_output_q SELECT
      words_output(slice) <=
                      word2_output(127+16*0 DOWNTO 16*0)   WHEN "000",
                      word2_output(127+16*1 DOWNTO 16*1)   WHEN "001",
                      word2_output(127+16*2 DOWNTO 16*2)   WHEN "010",
                      word2_output(127+16*3 DOWNTO 16*3)   WHEN "011",
                      word2_output(127+16*4 DOWNTO 16*4)   WHEN "100",
                      word2_output(127+16*5 DOWNTO 16*5)   WHEN "101",
                      word2_output(127+16*6 DOWNTO 16*6)   WHEN "110",
                      word2_output(127+16*7 DOWNTO 16*7)   WHEN "111",
                      (OTHERS=>'X')                           WHEN OTHERS;

    -- pragma translate_off
    dump_slice: PROCESS
      VARIABLE  buf       : line;
    BEGIN
      IF disable_print THEN
        WAIT;
      END IF;
      WAIT UNTIL falling_edge( clock );
      WAIT FOR 102 ps;

      IF now >= start_print THEN
        IF en_mem_input_i = "1"  THEN
          dump_cycle( buf, "GLB>");
          dump_integer( buf, slice, " #");
          dump_bit( buf, en_mem_input_i, " mem_input " );
          dump_bit( buf, slice_mem_input_i, " slice " );
          dump_hex( buf, addr_mem_input_i, " addr " );
          dump_hex( buf, word_mem_input_i, " word " );
          dump_bit( buf, en_e_input, " en_e " );
          dump_hex( buf, bank_e_input, " bank_e " );
          dump_hex( buf, row_e_input, " row_e " );
          dump_hex( buf, mask_e_input, " mask_e " );
          dump_hex( buf, word_e_input, " word_e " );
          dump_bit( buf, en_o_input, " en_o " );
          dump_hex( buf, bank_o_input, " bank_o " );
          dump_hex( buf, row_o_input, " row_o " );
          dump_hex( buf, mask_o_input, " mask_o " );
          dump_hex( buf, word_o_input, " word_o " );
          writeline(output, buf );
        END IF;

      END IF;

    END PROCESS dump_slice;
    -- pragma translate_on

  END GENERATE slice_gen;

  err_collide_gen: FOR i IN COLL_RANGE GENERATE
    err_collide_i(i) <= '0' WHEN collide_slice(i) = (slice_type'range => '0') ELSE '1';
  END GENERATE err_collide_gen;
  err_collide_busout <= err_collide_i;

  word0_data_filter_busout <= words_data_filter(0);
  word1_data_filter_busout <= words_data_filter(1);
  word2_data_filter_busout <= words_data_filter(2);
  word3_data_filter_busout <= words_data_filter(3);
  word4_data_filter_busout <= words_data_filter(4);
  word5_data_filter_busout <= words_data_filter(5);
  word6_data_filter_busout <= words_data_filter(6);
  word7_data_filter_busout <= words_data_filter(7);

  word0_psum_filter_busout <= words_psum_filter(0);
  word1_psum_filter_busout <= words_psum_filter(1);
  word2_psum_filter_busout <= words_psum_filter(2);
  word3_psum_filter_busout <= words_psum_filter(3);
  word4_psum_filter_busout <= words_psum_filter(4);
  word5_psum_filter_busout <= words_psum_filter(5);
  word6_psum_filter_busout <= words_psum_filter(6);
  word7_psum_filter_busout <= words_psum_filter(7);

  mem_post_block: IF true GENERATE
    SIGNAL  words_data_post2        : array_bit128_type(SLICE_RANGE);
    SIGNAL  slice_addr_data_post0   : slice_sel_addr_type;
    SIGNAL  slice_addr_data_post1   : slice_sel_addr_type;
    SIGNAL  slice_addr_data_post2   : slice_sel_addr_type;
    SIGNAL  en_mem_data_post0       : bit_type;
    SIGNAL  en_mem_data_post1       : bit_type;
  BEGIN
    en_mem_data_post0       <= en_mem_data_post_in    AFTER 10 ps WHEN rising_edge(clock);
    slice_addr_data_post0   <= addr_mem_data_post_in(SLICE_SEL_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_post_in = "1";

    en_mem_data_post1       <= en_mem_data_post0      AFTER 10 ps WHEN rising_edge(clock);
    slice_addr_data_post1   <= slice_addr_data_post0  AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_post0 = "1";

    slice_addr_data_post2   <= slice_addr_data_post1  AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_post1 = "1";

    words_data_post2_gen: FOR w IN words_data_post'range GENERATE
      words_data_post2(w) <= words_data_post(w) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_data_post1 = "1";
    END GENERATE words_data_post2_gen;

    word_data_post_busout   <= words_data_post2(to_integer(unsigned(slice_addr_data_post2))); -- TIMING FIX#2
  END GENERATE mem_post_block;

  -- word_data_post_busout   <= words_data_post(to_integer(unsigned(slice_addr_data_post_q))) AFTER 10 ps WHEN rising_edge(clock); -- TIMING FIX

  mem_output_block: IF true GENERATE
    SIGNAL  en_mem_output0          : bit_type;
    SIGNAL  en_mem_output1          : bit_type;
    SIGNAL  slice_addr_output0      : slice_sel_addr_type;
    SIGNAL  slice_addr_output1      : slice_sel_addr_type;
    SIGNAL  slice_addr_output2      : slice_sel_addr_type;
    SIGNAL  words_output2           : array_bit128_type(SLICE_RANGE);
  BEGIN
    en_mem_output0      <= en_mem_output_in     AFTER 10 ps WHEN rising_edge(clock);
    slice_addr_output0  <= addr_mem_output_in(SLICE_SEL_ADDR_MEM) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_output_in = "1";

    en_mem_output1      <= en_mem_output0       AFTER 10 ps WHEN rising_edge(clock);
    slice_addr_output1  <= slice_addr_output0   AFTER 10 ps WHEN rising_edge(clock) AND en_mem_output0 = "1";

    slice_addr_output2  <= slice_addr_output1   AFTER 10 ps WHEN rising_edge(clock) AND en_mem_output1 = "1";

    words_output2_gen: FOR w IN words_output'range GENERATE
      words_output2(w) <= words_output(w) AFTER 10 ps WHEN rising_edge(clock) AND en_mem_output1 = "1";
    END GENERATE words_output2_gen;

    word_output_busout   <= words_output2(to_integer(unsigned(slice_addr_output2))); -- TIMING FIX#2
  END GENERATE mem_output_block;

  -- word_output_busout   <= words_output(to_integer(unsigned(slice_addr_output))) AFTER 10 ps WHEN rising_edge(clock); -- TIMING FIX

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
    VARIABLE  tick      : integer;

    VARIABLE en_mem_data_filter_buf     : bit_type;
    VARIABLE width_mem_data_filter_buf  : bit2_type;
    VARIABLE slice_mem_data_filter_buf  : slice_mask_type;
    VARIABLE addr_mem_data_filter_buf   : slice_addr_type;

    VARIABLE en_mem_data_filter_buf2    : bit_type;
    VARIABLE width_mem_data_filter_buf2 : bit2_type;
    VARIABLE slice_mem_data_filter_buf2 : slice_mask_type;
    VARIABLE addr_mem_data_filter_buf2  : slice_addr_type;

    VARIABLE en_mem_psum_filter_buf     : bit_type;
    VARIABLE width_mem_psum_filter_buf  : bit_type;
    VARIABLE slice_mem_psum_filter_buf  : slice_mask_type;
    VARIABLE addr_mem_psum_filter_buf   : slice_addr_type;

    VARIABLE en_mem_psum_filter_buf2    : bit_type;
    VARIABLE width_mem_psum_filter_buf2 : bit_type;
    VARIABLE slice_mem_psum_filter_buf2 : slice_mask_type;
    VARIABLE addr_mem_psum_filter_buf2  : slice_addr_type;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 102 ps;

    IF now >= start_print THEN
      tick := ( now / 1 ns ) - 2;

      IF en_mem_input_in = "1" THEN
        dump_cycle( buf, "GLB>");
        dump_bit( buf, en_mem_input_in, " mem_input " );
        dump_bit( buf, slice_mem_input_in, " slice " );
        dump_hex( buf, addr_mem_input_in, " addr " );
        dump_hex( buf, word_mem_input_in, " word " );
        writeline(output, buf );
      END IF;

       en_mem_data_filter_buf2    := en_mem_data_filter_in;
      IF en_mem_data_filter_in = "1" THEN
         width_mem_data_filter_buf2 := width_mem_data_filter_in;
         slice_mem_data_filter_buf2 := slice_mem_data_filter_in;
         addr_mem_data_filter_buf2  := addr_mem_data_filter_in;
      END IF;

     en_mem_data_filter_buf     := en_mem_data_filter_buf2;
      IF en_mem_data_filter_buf2 = "1" THEN
         width_mem_data_filter_buf  := width_mem_data_filter_buf2;
         slice_mem_data_filter_buf  := slice_mem_data_filter_buf2;
         addr_mem_data_filter_buf   := addr_mem_data_filter_buf2;
      END IF;

      IF en_mem_data_filter_buf = "1" THEN
        dump_cycle( buf, "GLB>");
        dump_bit( buf, en_mem_data_filter_buf, " mem_data_filter " );
        dump_hex( buf, width_mem_data_filter_buf, " width " );
        dump_hex( buf, slice_mem_data_filter_buf, " slice " );
        dump_hex( buf, addr_mem_data_filter_buf, " addr ",  " words [" );
        FOR i IN 0 TO 7 LOOP
          dump_hex( buf, words_data_filter(i), " " );
        END LOOP;
        dump_string( buf, " ]");
        writeline(output, buf );
      END IF;

      en_mem_psum_filter_buf2    := en_mem_psum_filter_in;
      IF en_mem_psum_filter_in = "1" THEN
        width_mem_psum_filter_buf2 := width_mem_psum_filter_in;
        slice_mem_psum_filter_buf2 := slice_mem_psum_filter_in;
        addr_mem_psum_filter_buf2  := addr_mem_psum_filter_in;
      END IF;

      en_mem_psum_filter_buf     := en_mem_psum_filter_buf2;
      IF en_mem_psum_filter_buf2 = "1" THEN
        width_mem_psum_filter_buf  := width_mem_psum_filter_buf2;
        slice_mem_psum_filter_buf  := slice_mem_psum_filter_buf2;
        addr_mem_psum_filter_buf   := addr_mem_psum_filter_buf2;
      END IF;

      IF en_mem_psum_filter_buf = "1" THEN
        dump_cycle( buf, "GLB>");
        dump_bit( buf, en_mem_psum_filter_buf, " mem_psum_filter " );
        dump_bit( buf, width_mem_psum_filter_buf, " width " );
        dump_hex( buf, slice_mem_psum_filter_buf, " slice " );
        dump_hex( buf, addr_mem_psum_filter_buf, " addr ", " words [" );
        FOR i IN 0 TO 7 LOOP
          dump_hex( buf, words_psum_filter(i), " " );
        END LOOP;
        dump_string( buf, " ]");
        writeline(output, buf );
      END IF;

      IF en_mem_res_filter_in = "1" THEN
        dump_cycle( buf, "GLB>");
        dump_bit( buf, en_mem_res_filter_in, " mem_res_filter " );
        dump_bit( buf, slice_mem_res_filter_in, " slice " );
        dump_hex( buf, addr_mem_res_filter_in, " addr " );
        dump_hex( buf, word0_mem_res_filter_in, " words [ " );
        dump_hex( buf, word1_mem_res_filter_in, " " );
        dump_hex( buf, word2_mem_res_filter_in, " " );
        dump_hex( buf, word3_mem_res_filter_in, " " );
        dump_hex( buf, word4_mem_res_filter_in, " " );
        dump_hex( buf, word5_mem_res_filter_in, " " );
        dump_hex( buf, word6_mem_res_filter_in, " " );
        dump_hex( buf, word7_mem_res_filter_in, " ", " ]" );
        writeline(output, buf );
      END IF;

      IF en_mem_data_post_in = "1" THEN
        dump_cycle( buf, "GLB>");
        dump_bit( buf, en_mem_data_post_in, " mem_data_post " );
        dump_hex( buf, addr_mem_data_post_in, " addr " );
        writeline(output, buf );
      END IF;

      IF en_mem_res_post_in = "1" THEN
        dump_cycle( buf, "GLB>");
        dump_bit( buf, en_mem_res_post_in, " mem_res_post " );
        dump_bit( buf, wr4_mem_res_post_in, " wr4 " );
        dump_hex( buf, addr_mem_res_post_in, " addr " );
        dump_hex( buf, word_mem_res_post_in, " word " );
        writeline(output, buf );
      END IF;

      IF err_collide_i = "1" THEN
        dump_cycle( buf, "GLB>");
        dump_bit( buf, err_collide_i, " err_collide_i " );
        FOR i IN COLL_RANGE LOOP
          dump_integer( buf, i, " #" );
          dump_hex( buf, collide_slice( i ), " " );
        END LOOP;
        writeline(output, buf );
      END IF;

    END IF;

  END PROCESS dump;
-- pragma translate_on

END ARCHITECTURE behavior;
