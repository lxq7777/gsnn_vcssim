
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE work.gsnn_package.ALL;

ENTITY wrapper_bank_mem IS
-- pragma translate_off
  GENERIC(
    name                : string := ""
  );
-- pragma translate_on
  PORT (
    clk                 : IN std_logic;
    en                  : IN bit_type;
    we                  : IN bit_type;
    addr                : IN row_addr_type;
    mask_in             : IN bit8_type;
    word_in             : IN bit128_type;
    slp_in              : IN bit_type;
    sd_in               : IN bit_type;
    word_out            : OUT bit128_type
    );
END ENTITY wrapper_bank_mem;

ARCHITECTURE behavior OF wrapper_bank_mem IS
  COMPONENT memory_fpga_wm IS
    PORT(
      clka          : IN std_logic;
      ena           : IN std_logic;
      wea           : IN std_logic_vector( 3 DOWNTO 0);
      addra         : IN std_logic_vector( 9 DOWNTO 0 );
      dina          : IN std_logic_vector( 31 DOWNTO 0 );
      douta         : OUT std_logic_vector( 31 DOWNTO 0 );

      clkb          : IN std_logic;
      enb           : IN std_logic;
      web           : IN std_logic_vector( 3 DOWNTO 0);
      addrb         : IN std_logic_vector( 9 DOWNTO 0 );
      dinb          : IN std_logic_vector( 31 DOWNTO 0 );
      doutb         : OUT std_logic_vector( 31 DOWNTO 0 )
    );
  END COMPONENT memory_fpga_wm;

  --COMPONENT memory_fpga_wm IS
  --  GENERIC(
  --    lg2rows       : integer;
  --    bits          : integer;
  --    trace_write   : boolean := false;
  --    trace_read    : boolean := false;
  --    name          : string := "";
  --    load          : string := "";
  --    init          : std_logic := 'X'
  --  );
  --  PORT(
  --    clock         : IN std_logic;
  --    CEA_in        : IN std_logic;
  --    BWEA_in       : IN std_logic_vector( (bits/8)-1 DOWNTO 0);
  --    AA_in         : IN std_logic_vector( lg2rows-1 DOWNTO 0 );
  --    DA_in         : IN std_logic_vector( bits-1 DOWNTO 0 );
  --    GWEA_in       : IN std_logic;

  --    CEB_in        : IN std_logic;
  --    BWEB_in       : IN std_logic_vector( (bits/8)-1 DOWNTO 0);
  --    AB_in         : IN std_logic_vector( lg2rows-1 DOWNTO 0 );
  --    DB_in         : IN std_logic_vector( bits-1 DOWNTO 0 );
  --    GWEB_in       : IN std_logic;

  --    QA_out        : OUT std_logic_vector( bits-1 DOWNTO 0 );
  --    QB_out        : OUT std_logic_vector( bits-1 DOWNTO 0 )
  --  );
  --END COMPONENT memory_fpga_wm;

  SIGNAL  cea0  : bit_type;
  SIGNAL  aa0   : bit10_type;
  SIGNAL  bwea0 : bit4_type;
  SIGNAL  gwea0 : bit_type;

  SIGNAL  ceb0  : bit_type;
  SIGNAL  ab0   : bit10_type;
  SIGNAL  bweb0 : bit4_type;
  SIGNAL  gweb0 : bit_type;

  SIGNAL  cea1  : bit_type;
  SIGNAL  aa1   : bit10_type;
  SIGNAL  bwea1 : bit4_type;
  SIGNAL  gwea1 : bit_type;

  SIGNAL  ceb1  : bit_type;
  SIGNAL  ab1   : bit10_type;
  SIGNAL  bweb1 : bit4_type;
  SIGNAL  gweb1 : bit_type;

BEGIN

  cea0    <=  "0" WHEN en = "0" ELSE
              "1" WHEN we = "0" ELSE
              "0" WHEN mask_in(1 DOWNTO 0) = "00" ELSE
              "1";
  bwea0   <=  mask_in(1)&mask_in(1)&mask_in(0)&mask_in(0);
  aa0     <=  addr&'0';
  gwea0   <=  "1" WHEN we = "1" AND mask_in(1 DOWNTO 0) /= "00" ELSE "0";

  ceb0    <=  "0" WHEN en = "0" ELSE
              "1" WHEN we = "0" ELSE
              "0" WHEN mask_in(3 DOWNTO 2) = "00" ELSE
              "1";
  bweb0   <=  mask_in(3)&mask_in(3)&mask_in(2)&mask_in(2);
  ab0     <=  addr&'1';
  gweb0   <=  "1" WHEN we = "1" AND mask_in(3 DOWNTO 2) /= "00" ELSE "0";

  cea1    <=  "1" WHEN en = "0" ELSE
              "1" WHEN we = "0" ELSE
              "0" WHEN mask_in(5 DOWNTO 4) = "00" ELSE
              "1";
  bwea1   <=  mask_in(5)&mask_in(5)&mask_in(4)&mask_in(4);
  aa1     <=  addr&'0';
  gwea1   <=  "1" WHEN we = "1" AND mask_in(5 DOWNTO 4) /= "00" ELSE "0";

  ceb1    <=  "0" WHEN en = "0" ELSE
              "1" WHEN we = "0" ELSE
              "0" WHEN mask_in(7 DOWNTO 6) = "00" ELSE
              "1";
  bweb1   <=  mask_in(7)&mask_in(7)&mask_in(6)&mask_in(6);
  ab1     <=  addr&'1';
  gweb1   <=  "1" WHEN we = "1" AND mask_in(7 DOWNTO 6) /= "00" ELSE "0";

  --a_mem0: memory_fpga_wm  GENERIC MAP( lg2rows => 10, bits => 32 ) -- BLOCK RAM
  --                      PORT MAP(
  --                        clock     =>  clk,

  --                        CEA_in    =>  cea0(0),
  --                        BWEA_in   =>  bwea0,
  --                        AA_in     =>  aa0,
  --                        DA_in     =>  word_in(31 DOWNTO 0),
  --                        GWEA_in   =>  gwea0(0),

  --                        CEB_in    =>  ceb0(0),
  --                        BWEB_in   =>  bweb0,
  --                        AB_in     =>  ab0,
  --                        DB_in     =>  word_in(63 DOWNTO 32),
  --                        GWEB_in   =>  gweb0(0),

  --                        QA_out    =>  word_out(31 DOWNTO 0),
  --                        QB_out    =>  word_out(63 DOWNTO 32)
  --                      );

  --a_mem1: memory_fpga_wm  GENERIC MAP( lg2rows => 10, bits => 32 ) -- BLOCK RAM
  --                      PORT MAP(
  --                        clock     =>  clk,

  --                        CEA_in    =>  cea1(0),
  --                        BWEA_in   =>  bwea1,
  --                        AA_in     =>  aa1,
  --                        DA_in     =>  word_in(95 DOWNTO 64),
  --                        GWEA_in   =>  gwea1(0),

  --                        CEB_in    =>  ceb1(0),
  --                        BWEB_in   =>  bweb1,
  --                        AB_in     =>  ab1,
  --                        DB_in     =>  word_in(127 DOWNTO 96),
  --                        GWEB_in   =>  gweb1(0),

  --                        QA_out    =>  word_out(95 DOWNTO 64),
  --                        QB_out    =>  word_out(127 DOWNTO 96)
  --                      );

  a_mem0: memory_fpga_wm 
    PORT MAP(
      clka    =>  clk,
      ena     =>  cea0(0),
      wea     =>  bwea0,
      addra   =>  aa0,
      dina    =>  word_in(31 DOWNTO 0),
      douta   =>  word_out(31 DOWNTO 0),
           
      clkb    =>  clk,
      enb     =>  ceb0(0),
      web     =>  bweb0,
      addrb   =>  ab0,
      dinb    =>  word_in(63 DOWNTO 32),
      doutb   =>  word_out(63 DOWNTO 32)
    );

  a_mem1: memory_fpga_wm 
    PORT MAP(
      clka    =>  clk,
      ena     =>  cea1(0),
      wea     =>  bwea1,
      addra   =>  aa1,
      dina    =>  word_in(95 DOWNTO 64),
      douta   =>  word_out(95 DOWNTO 64),
           
      clkb    =>  clk,
      enb     =>  ceb1(0),
      web     =>  bweb1,
      addrb   =>  ab1,
      dinb    =>  word_in(127 DOWNTO 96),
      doutb   =>  word_out(127 DOWNTO 96)
    );
END ARCHITECTURE behavior;
