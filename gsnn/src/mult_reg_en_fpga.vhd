LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

ENTITY mult_reg_en_fpga is
  GENERIC(
    WIDTHA : integer := 11;
    WIDTHB : integer := 11
  );
  PORT(
    reset : in std_logic;
    clk : in std_logic;
    enA : in std_logic;
    A   : in std_logic_vector(WIDTHA - 1 downto 0);
    enB : in std_logic;
    B   : in std_logic_vector(WIDTHB - 1 downto 0);
    RES : out std_logic_vector(WIDTHA + WIDTHB - 1 downto 0)
  );
END ENTITY mult_reg_en_fpga;

ARCHITECTURE behavior OF mult_reg_en_fpga IS
  SIGNAL A_d : std_logic_vector(WIDTHA -1 downto 0);
  SIGNAL B_d : std_logic_vector(WIDTHB -1 downto 0);
  SIGNAL RES_d : std_logic_vector(WIDTHA + WIDTHB - 1 downto 0);

  SIGNAL ACOUT                :  std_logic_vector(29 downto 0);
  SIGNAL BCOUT                :  std_logic_vector(17 downto 0);
  SIGNAL CARRYCASCOUT         :  std_ulogic;
  SIGNAL CARRYOUT             :  std_logic_vector(3 downto 0);
  SIGNAL MULTSIGNOUT          :  std_ulogic;
  SIGNAL OVERFLOW             :  std_ulogic;
  SIGNAL P                    :  std_logic_vector(47 downto 0);
  SIGNAL PATTERNBDETECT       :  std_ulogic;
  SIGNAL PATTERNDETECT        :  std_ulogic;
  SIGNAL PCOUT                :  std_logic_vector(47 downto 0);
  SIGNAL UNDERFLOW            :  std_ulogic;
  SIGNAL XOROUT               :  std_logic_vector(7 downto 0);

  SIGNAL A_in                 :  std_logic_vector(29 downto 0);
  SIGNAL ACIN                 :  std_logic_vector(29 downto 0);
  SIGNAL ALUMODE              :  std_logic_vector(3 downto 0);
  SIGNAL B_in                 :  std_logic_vector(17 downto 0);
  SIGNAL BCIN                 :  std_logic_vector(17 downto 0);
  SIGNAL C                    :  std_logic_vector(47 downto 0);
  SIGNAL CARRYCASCIN          :  std_ulogic;
  SIGNAL CARRYIN              :  std_ulogic;
  SIGNAL CARRYINSEL           :  std_logic_vector(2 downto 0);
  SIGNAL CEA1                 :  std_ulogic;
  SIGNAL CEA2                 :  std_ulogic;
  SIGNAL CEAD                 :  std_ulogic;
  SIGNAL CEALUMODE            :  std_ulogic;
  SIGNAL CEB1                 :  std_ulogic;
  SIGNAL CEB2                 :  std_ulogic;
  SIGNAL CEC                  :  std_ulogic;
  SIGNAL CECARRYIN            :  std_ulogic;
  SIGNAL CECTRL               :  std_ulogic;
  SIGNAL CED                  :  std_ulogic;
  SIGNAL CEINMODE             :  std_ulogic;
  SIGNAL CEM                  :  std_ulogic;
  SIGNAL CEP                  :  std_ulogic;
  SIGNAL D                    :  std_logic_vector(26 downto 0);
  SIGNAL INMODE               :  std_logic_vector(4 downto 0);
  SIGNAL MULTSIGNIN           :  std_ulogic;
  SIGNAL OPMODE               :  std_logic_vector(8 downto 0);
  SIGNAL PCIN                 :  std_logic_vector(47 downto 0);
  SIGNAL RSTA                 :  std_ulogic;
  SIGNAL RSTALLCARRYIN        :  std_ulogic;
  SIGNAL RSTALUMODE           :  std_ulogic;
  SIGNAL RSTB                 :  std_ulogic;
  SIGNAL RSTC                 :  std_ulogic;
  SIGNAL RSTCTRL              :  std_ulogic;
  SIGNAL RSTD                 :  std_ulogic;
  SIGNAL RSTINMODE            :  std_ulogic;
  SIGNAL RSTM                 :  std_ulogic;
  SIGNAL RSTP                 :  std_ulogic;
BEGIN
  --PROCESS( clk )
  --BEGIN
  --  IF rising_edge(clk) THEN
  --    IF enA = '1' THEN
  --      A_d <= A;
  --    END IF;
  --  END IF;
  --END PROCESS;

  --PROCESS( clk )
  --BEGIN
  --  IF rising_edge(clk) THEN
  --    IF enB = '1' THEN
  --      B_d <= B;
  --    END IF;
  --  END IF;
  --END PROCESS;
  --RES_d <= A_d * B_d;

  --PROCESS( clk )
  --BEGIN
  --  IF rising_edge(clk) THEN
  --      RES <= RES_d;
  --  END IF;
  --END PROCESS;

  RES                <= P(21 downto 0);

  A_in(29 downto 11) <= (OTHERS => '0');       
  A_in(10 downto 0)  <= A;
  ACIN               <= (OTHERS => '0');       
  ALUMODE            <= (OTHERS => '0');       
  B_in(17 downto 11) <= (OTHERS => '0');       
  B_in(10 downto 0)  <= B;
  BCIN               <= (OTHERS => '0');       
  C                  <= (OTHERS => '0');       
  CARRYCASCIN        <= '0';       
  CARRYIN            <= '0';       
  CARRYINSEL         <= (OTHERS => '0');       
  CEA1               <= enA;
  CEA2               <= '0';       
  CEAD               <= '0';       
  CEALUMODE          <= '0';       
  CEB1               <= enB;
  CEB2               <= '0';       
  CEC                <= '0';       
  CECARRYIN          <= '0';       
  CECTRL             <= '0';       
  CED                <= '0';       
  CEINMODE           <= '0';       
  CEM                <= '1';       
  CEP                <= '0';       
  D                  <= (OTHERS => '0');       
  INMODE             <= "10001";
  MULTSIGNIN         <= '0';       
  OPMODE             <= "000000101";
  PCIN               <= (OTHERS => '0');       
  RSTA               <= reset; 
  RSTALLCARRYIN      <= reset; 
  RSTALUMODE         <= reset; 
  RSTB               <= reset; 
  RSTC               <= reset; 
  RSTCTRL            <= reset; 
  RSTD               <= reset; 
  RSTINMODE          <= reset; 
  RSTM               <= reset; 
  RSTP               <= reset; 

  DSP48E2_inst : DSP48E2
    generic map (
      ACASCREG => 1,
      ADREG => 0,
      ALUMODEREG => 0,
      AMULTSEL => "A",
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      AUTORESET_PRIORITY => "RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BMULTSEL => "B",
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 0,
      INMODEREG => 0,
      IS_ALUMODE_INVERTED => "0000",
      IS_CARRYIN_INVERTED => '0',
      IS_CLK_INVERTED => '0',
      IS_INMODE_INVERTED => "00000",
      IS_OPMODE_INVERTED => "000000000",
      IS_RSTALLCARRYIN_INVERTED => '0',
      IS_RSTALUMODE_INVERTED => '0',
      IS_RSTA_INVERTED => '0',
      IS_RSTB_INVERTED => '0',
      IS_RSTCTRL_INVERTED => '0',
      IS_RSTC_INVERTED => '0',
      IS_RSTD_INVERTED => '0',
      IS_RSTINMODE_INVERTED => '0',
      IS_RSTM_INVERTED => '0',
      IS_RSTP_INVERTED => '0',
      MASK => X"3FFFFFFFFFFF",
      MREG => 1,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREADDINSEL => "A",
      PREG => 0,
      RND => X"000000000000",
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48",
      USE_WIDEXOR => "FALSE",
      XORSIMD => "XOR24_48_96"
    )
    port map(
      ACOUT => ACOUT,
      BCOUT => BCOUT,
      CARRYCASCOUT => CARRYCASCOUT,
      CARRYOUT => CARRYOUT,
      MULTSIGNOUT => MULTSIGNOUT,
      OVERFLOW => OVERFLOW,
      P => P,
      PATTERNBDETECT => PATTERNBDETECT,
      PATTERNDETECT => PATTERNDETECT,
      PCOUT => PCOUT,
      UNDERFLOW => UNDERFLOW,
      XOROUT => XOROUT,
      A => A_in,
      ACIN => ACIN,
      ALUMODE => ALUMODE,
      B => B_in,
      BCIN => BCIN,
      C => C,
      CARRYCASCIN => CARRYCASCIN,
      CARRYIN => CARRYIN,
      CARRYINSEL => CARRYINSEL,
      CEA1 => CEA1,
      CEA2 => CEA2,
      CEAD => CEAD,
      CEALUMODE => CEALUMODE,
      CEB1 => CEB1,
      CEB2 => CEB2,
      CEC => CEC,
      CECARRYIN => CECARRYIN,
      CECTRL => CECTRL,
      CED => CED,
      CEINMODE => CEINMODE,
      CEM => CEM,
      CEP => CEP,
      CLK => CLK,
      D => D,
      INMODE => INMODE,
      MULTSIGNIN => MULTSIGNIN,
      OPMODE => OPMODE,
      PCIN => PCIN,
      RSTA => RSTA,
      RSTALLCARRYIN => RSTALLCARRYIN,
      RSTALUMODE => RSTALUMODE,
      RSTB => RSTB,
      RSTC => RSTC,
      RSTCTRL => RSTCTRL,
      RSTD => RSTD,
      RSTINMODE => RSTINMODE,
      RSTM => RSTM,
      RSTP => RSTP
    );

END ARCHITECTURE behavior;
