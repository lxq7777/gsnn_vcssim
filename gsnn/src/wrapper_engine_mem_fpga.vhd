
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE work.gsnn_package.ALL;

ENTITY wrapper_engine_mem_fpga IS
  GENERIC(
    id                  : bit2_type
  );
  PORT (
    clk                : IN std_logic;

    ena                : IN bit_type;
    wea                : IN bit_type;
    addra              : IN bit12_type;
    worda_in           : IN bit32_type;
    worda_out          : OUT bit32_type;

    enb                : IN bit_type;
    web                : IN bit_type;
    addrb              : IN bit12_type;
    wordb_in           : IN bit32_type;
    wordb_out          : OUT bit32_type
    );
END ENTITY wrapper_engine_mem_fpga;

ARCHITECTURE behavior OF wrapper_engine_mem_fpga IS
  COMPONENT memory_fpga IS
    GENERIC(
      lg2rows       : integer;
      bits          : integer;
      trace_write   : boolean := false;
      trace_read    : boolean := false;
      name          : string := "";
      load          : string := "";
      init          : std_logic := 'X'
    );
    PORT(
      clock         : IN std_logic;
      CEA_in        : IN std_logic;
      WEA_in        : IN std_logic;
      AA_in         : IN std_logic_vector( lg2rows-1 DOWNTO 0 );
      DA_in         : IN std_logic_vector( bits-1 DOWNTO 0 );

      CEB_in        : IN std_logic;
      WEB_in        : IN std_logic;
      AB_in         : IN std_logic_vector( lg2rows-1 DOWNTO 0 );
      DB_in         : IN std_logic_vector( bits-1 DOWNTO 0 );

      QA_out        : OUT std_logic_vector( bits-1 DOWNTO 0 );
      QB_out        : OUT std_logic_vector( bits-1 DOWNTO 0 )
    );
  END COMPONENT memory_fpga;

BEGIN
  a_mem: memory_fpga GENERIC MAP( lg2rows => 12, bits => 32, trace_read => false, trace_write => false, name => "insn#"&std_logic'image(id(1))(2)&std_logic'image(id(0))(2) )
    PORT MAP (
      clock         => clk,

      cea_in        => ena(0),
      wea_in        => wea(0),
      aa_in         => addra,
      da_in         => worda_in,

      ceb_in        => enb(0),
      web_in        => web(0),
      ab_in         => addrb,
      db_in         => wordb_in,

      qa_out        => worda_out,
      qb_out        => wordb_out
    );
END ARCHITECTURE behavior;
