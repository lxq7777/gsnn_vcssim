
---------------------------------------------------------------------------
-- input unit reads from external memory via AXI bus to GSNN memory
---------------------------------------------------------------------------

-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;
USE work.gsnn_interface_package.ALL;

ENTITY input_unit IS
  PORT(
  clock                           : IN std_logic;
  reset                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- engine signals
  ---------------------------------------------------------------------------
  en_cmd_in                       : IN bit_type;
  insn_cmd_in                     : IN insn_type;
  ctl_cmd_in                      : IN bit16_type;
  ctr_cmd_in                      : IN bit32_type;
  base0_cmd_in                    : IN bit32_type;  -- target address
  base1_cmd_in                    : IN bit32_type;  -- source external memory address, lower 32
  spec_cmd_in                     : IN bit32_type;  -- upper 8 bits

  block_busout                    : OUT bit_type;   -- block if queue full or if last not processed

  ---------------------------------------------------------------------------
  -- memory signals
  ---------------------------------------------------------------------------
  en_mem_busout                   : OUT bit_type;
  slice_mem_busout                : OUT slice_mask_type;
  addr_mem_busout                 : OUT slice_addr_type;
  word_mem_busout                 : OUT bit128_type;

  ---------------------------------------------------------------------------
  -- AXI bus signals
  ---------------------------------------------------------------------------
  axi_clock                       : IN std_logic;
  -- AR Channel
  arid_axi_busout                 : OUT arid_axi_type;  -- currently defined as 1 bit
  araddr_axi_busout               : OUT addr_axi_type;
  arlen_axi_busout                : OUT bit4_type;      -- number of bursts
  arsize_axi_busout               : OUT bit3_type;      -- size of each burst
  arburst_axi_busout              : OUT bit2_type;      -- type of burst,  will be INCR=01
  arlock_axi_busout               : OUT alock_axi_type; -- locking, will be NORMAL=00
  arvalid_axi_busout              : OUT std_logic;
  arready_axi_in                  : IN  std_logic;

  -- R Channel
  rid_axi_in                      : IN  arid_axi_type;
  rdata_axi_in                    : IN  data_axi_type;
  rresp_axi_in                    : IN  resp_axi_type;
  rlast_axi_in                    : IN  std_logic;
  rvalid_axi_in                   : IN  std_logic;
  rready_axi_busout               : OUT std_logic

  );
END ENTITY input_unit;

ARCHITECTURE behavior OF input_unit IS
-- pragma translate_off
  CONSTANT disable_print : boolean := true;
  CONSTANT start_print   : time := 0 ns;
-- pragma translate_on

  CONSTANT  SIZE_INPUT  : integer :=  8;
  SUBTYPE mask_entry_type IS std_logic_vector( SIZE_INPUT-1 DOWNTO 0);
  SUBTYPE num_entry_type IS std_logic_vector(2 DOWNTO 0);

  TYPE entry_input_type IS RECORD
    valid           : bit_type;
    last            : bit_type;
    a_from          : addr_axi_type;
    a_to            : global_addr_type;
    in_size         : size_input_type;
    in_type         : type_input_type;
    in_mode         : mode_input_type;
  END RECORD entry_input_type;
  TYPE array_entry_input_type IS ARRAY( natural RANGE<> ) OF entry_input_type;

  SIGNAL  entries_q     : array_entry_input_type( 0 TO SIZE_INPUT-1);
  SIGNAL  hd_d          : num_entry_type;
  SIGNAL  hd_q          : num_entry_type;
  SIGNAL  tl_d          : num_entry_type;
  SIGNAL  tl_q          : num_entry_type;
  SIGNAL  tl2_d         : num_entry_type;
  SIGNAL  tl2_q         : num_entry_type;
  SIGNAL  at_d          : num_entry_type;
  SIGNAL  at_q          : num_entry_type;

  SIGNAL  en_axi_q      : bit_type;
  SIGNAL  last_axi_q    : bit_type;
  SIGNAL  word_axi_q    : bit128_type;
  SIGNAL  en2_axi_q     : bit_type;
  SIGNAL  to_axi_q      : global_addr_type;
  SIGNAL  mode_axi_q    : mode_input_type;
  SIGNAL  type_axi_q    : type_input_type;

  SIGNAL  last_i        : mask_entry_type;
  SIGNAL  at_entry_i    : entry_input_type;
  SIGNAL  hd_entry_i    : entry_input_type;
  SIGNAL  en_hd_to_i    : bit_type;
  SIGNAL  hd_to_i       : global_addr_type;
  SIGNAL  hd_to_d       : global_addr_type;
  SIGNAL  hd_to_q       : global_addr_type;

  SIGNAL  push_i        : bit_type;
  SIGNAL  pop_i         : bit_type;
  SIGNAL  sent_i        : bit_type;
  SIGNAL  ctr0_i        : bit_type;

  SIGNAL  araddr_axi_q  : addr_axi_type;
  SIGNAL  arlen_axi_q   : bit4_type;
  SIGNAL  arvalid_axi_i : bit_type;
  SIGNAL  arvalid_axi_q : bit_type;

BEGIN

  -- blocking code: either when queue is full or when last has been encountered
  last_gen: FOR i IN entries_q'range GENERATE
    last_i(i) <= '1' WHEN entries_q(i).valid = "1" AND entries_q(i).last = "1" ELSE '0';
  END GENERATE last_gen;
  block_busout <= "1" WHEN entries_q(to_integer(unsigned(tl2_q))).valid = "1" ELSE
                  "1" WHEN last_i /= (last_i'range => '0') ELSE
                  "0";

  -- sent new request:
  arid_axi_busout     <= (arid_axi_busout'range => '0');
  arsize_axi_busout   <= "100";
  arburst_axi_busout  <= INCR_ARBURST_AXI;
  arlock_axi_busout   <= NORMAL_ALOCK_AXI;

  araddr_axi_busout   <= araddr_axi_q;
  arlen_axi_busout    <= arlen_axi_q;
  arvalid_axi_busout  <= arvalid_axi_q(0);

  at_entry_i          <= entries_q(to_integer(unsigned(at_q)));
  arlen_axi_q         <= at_entry_i.in_size AFTER 10 ps WHEN rising_edge(clock) AND sent_i = "1";
  araddr_axi_q        <= at_entry_i.a_from AFTER 10 ps WHEN rising_edge(clock) AND sent_i = "1";
  arvalid_axi_i       <= "0"            WHEN reset = "1" ELSE
                         arvalid_axi_q  WHEN axi_clock = '1' ELSE
                         "1"            WHEN sent_i = "1" ELSE
                         "0"            WHEN arready_axi_in = '1' ELSE
                         arvalid_axi_q;
  arvalid_axi_q       <= arvalid_axi_i AFTER 10 ps WHEN rising_edge(clock);

  -- various queue actions
  pop_i               <=  "1" WHEN en_axi_q = "1" AND last_axi_q = "1" ELSE "0";
  push_i              <=  "1" WHEN en_cmd_in = "1" ELSE "0";
  sent_i              <=  "1" WHEN ( arready_axi_in = '1' OR arvalid_axi_q = "0" ) AND at_entry_i.valid = "1" AND axi_clock = '0' ELSE "0";

  ctr0_i              <=  "1" WHEN ctr_cmd_in = (ctr_cmd_in'range => '0') ELSE "0";

  -- recieve new response:
  axi_rd_block: IF true GENERATE
    SIGNAL  en_axi_d      : bit_type;
    SIGNAL  last_axi_d    : bit_type;
    SIGNAL  en2_axi_d     : bit_type;
    SIGNAL  push_hd_bi    : bit_type;
    SIGNAL  to_axi_d      : global_addr_type;
  BEGIN
    rready_axi_busout   <=  '1';  -- we are always ready to accept requests
  
    -- grab a new request 
    en_axi_d            <=  "0" WHEN reset = "1" ELSE
                            "1" WHEN rvalid_axi_in = '1' AND axi_clock = '0' ELSE  -- to deal with aclk = 1/2 regular clock
                            "0";
    en_axi_q   <=  en_axi_d AFTER 10 ps WHEN rising_edge(clock);
    last_axi_d(0)       <=  '1'                 WHEN reset = "1" ELSE
                            rlast_axi_in        WHEN en_axi_d = "1"AND en_axi_q = "0" ELSE
                            last_axi_q(0);
    last_axi_q <=  last_axi_d AFTER 10 ps WHEN rising_edge(clock);
    word_axi_q <=  rdata_axi_in AFTER 10 ps WHEN rising_edge(clock) AND en_axi_d = "1";

    -- deal with 2 cycle writes because of B->FP expansion
    en2_axi_d           <=  "1" WHEN en_axi_q = "1"
                                AND ( type_axi_q = U8_TYPE_INPUT OR type_axi_q = S8_TYPE_INPUT ) ELSE
                            "0";
    en2_axi_q <=  en2_axi_d AFTER 10 ps WHEN rising_edge(clock);

    -- if this is the first transfer of a burst [i.e. the first transfer after last of previous transfer]
    hd_entry_i          <=  entries_q(to_integer(unsigned(hd_q)));
    push_hd_bi          <=  "1" WHEN en_axi_d = "1" AND last_axi_q = "1" ELSE "0";
    mode_axi_q          <=  hd_entry_i.in_mode AFTER 10 ps WHEN rising_edge(clock) AND push_hd_bi = "1";
    type_axi_q          <=  hd_entry_i.in_type AFTER 10 ps WHEN rising_edge(clock) AND push_hd_bi = "1";
    to_axi_d            <=  hd_entry_i.a_to WHEN push_hd_bi = "1" ELSE
                            std_logic_vector(unsigned(to_axi_q)+16) WHEN  en_axi_q = "1" OR en2_axi_q = "1" ELSE
                            to_axi_q;
    to_axi_q <= to_axi_d AFTER 10 ps WHEN rising_edge(clock);
  END GENERATE axi_rd_block;

  mem_out_block: IF true GENERATE
    SIGNAL  slice1_bi     : slice_mask_type;
    SIGNAL  slice_bi      : slice_mask_type;
    SIGNAL  word_bi       : bit128_type;
  BEGIN
    out_gen: FOR i IN 0 TO 7 GENERATE
      SIGNAL  byte_gbi   : bit8_type;
      SIGNAL  sbyte_gbi  : bit8_type;
      SIGNAL  sign_gbi   : bit_type;
      SIGNAL  exp_gbi    : bit5_type;
      SIGNAL  mant_gbi   : bit7_type;
    BEGIN
      byte_gbi          <=  word_axi_q(8*i+7 DOWNTO 8*i)         WHEN en_axi_q = "1" ELSE
                            word_axi_q(8*i+7+64 DOWNTO 8*i+64)   WHEN en2_axi_q = "1" ELSE
                            (OTHERS => 'X');
      sbyte_gbi         <=  byte_gbi            WHEN type_axi_q = U8_TYPE_INPUT ELSE
                            byte_gbi            WHEN type_axi_q = S8_TYPE_INPUT and byte_gbi(7)= '0' ELSE
                            std_logic_vector(-signed(byte_gbi))
                                                WHEN type_axi_q = S8_TYPE_INPUT and byte_gbi(7)= '1' ELSE
                            (OTHERS => 'X');
      sign_gbi          <= "1"  WHEN type_axi_q = S8_TYPE_INPUT and byte_gbi(7)= '1' ELSE "0";
      exp_gbi           <= 
                           "10110"  WHEN sbyte_gbi(7) = '1' ELSE
                           "10101"  WHEN sbyte_gbi(6) = '1' ELSE
                           "10100"  WHEN sbyte_gbi(5) = '1' ELSE
                           "10011"  WHEN sbyte_gbi(4) = '1' ELSE
                           "10010"  WHEN sbyte_gbi(3) = '1' ELSE
                           "10001"  WHEN sbyte_gbi(2) = '1' ELSE
                           "10000"  WHEN sbyte_gbi(1) = '1' ELSE
                           "01111"  WHEN sbyte_gbi(0) = '1' ELSE
                           "00000";
      mant_gbi          <=
                            sbyte_gbi(6 DOWNTO 0)           WHEN sbyte_gbi(7) = '1' ELSE
                            sbyte_gbi(5 DOWNTO 0)&"0"       WHEN sbyte_gbi(6) = '1' ELSE
                            sbyte_gbi(4 DOWNTO 0)&"00"      WHEN sbyte_gbi(5) = '1' ELSE
                            sbyte_gbi(3 DOWNTO 0)&"000"     WHEN sbyte_gbi(4) = '1' ELSE
                            sbyte_gbi(2 DOWNTO 0)&"0000"    WHEN sbyte_gbi(3) = '1' ELSE
                            sbyte_gbi(1 DOWNTO 0)&"00000"   WHEN sbyte_gbi(2) = '1' ELSE
                            sbyte_gbi(0 DOWNTO 0)&"000000"  WHEN sbyte_gbi(1) = '1' ELSE
                            "0000000";
      word_bi(16*i+15 DOWNTO 16*i) <= 
                            word_axi_q(16*i+15 DOWNTO 16*i) WHEN type_axi_q = FP16_TYPE_INPUT ELSE sign_gbi&exp_gbi&mant_gbi&"000";
    END GENERATE out_gen;
    slice1_bi <= slice_sel_to_mask( to_axi_q(SLICE_SEL_ADDR_MEM) );
    WITH mode_axi_q SELECT
      slice_bi          <=  (OTHERS=>'1')    WHEN BROAD_MODE_INPUT,
                            slice1_bi        WHEN NORMAL_MODE_INPUT,
                            (OTHERS=>'X')    WHEN OTHERS;
                            
    en_mem_busout       <=  en_axi_q OR en2_axi_q;
    slice_mem_busout    <=  slice_bi;
    addr_mem_busout     <=  to_axi_q(slice_addr_type'range);
    word_mem_busout     <=  word_bi;
  END GENERATE mem_out_block;
  
  entry_gen: FOR i IN entries_q'range GENERATE
    CONSTANT  entry       : num_entry_type  := std_logic_vector(to_unsigned(i, 3));
    SIGNAL    hd_gi       : bit_type;
    SIGNAL    tl_gi       : bit_type;
    SIGNAL    at_gi       : bit_type;
    SIGNAL    push_en_gi  : bit_type;
    SIGNAL    pop_en_gi   : bit_type;
    SIGNAL    valid_gi    : bit_type;
  BEGIN
    hd_gi                 <=  "1" WHEN hd_q = entry ELSE "0";
    tl_gi                 <=  "1" WHEN tl_q = entry ELSE "0";
    at_gi                 <=  "1" WHEN at_q = entry ELSE "0";

    push_en_gi            <=  tl_gi AND push_i;
    pop_en_gi             <=  hd_gi AND pop_i;

    valid_gi              <=  "0" WHEN reset = "1" ELSE
                              "1" WHEN push_en_gi = "1" ELSE
                              "0" WHEN pop_i = "1" AND hd_gi = "1" ELSE
                              entries_q(i).valid;
    entries_q(i).valid    <=  valid_gi AFTER 10 ps WHEN rising_edge(clock);


    entries_q(i).last     <=  ctr0_i                                AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";
    entries_q(i).in_type  <=  ctl_cmd_in(TYPE_INPUT_INSN_FIELD)    AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";
    entries_q(i).in_mode  <=  ctl_cmd_in(MODE_INPUT_INSN_FIELD)    AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";
    entries_q(i).in_size  <=  ctl_cmd_in(SIZE_INPUT_INSN_FIELD)    AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";
    entries_q(i).a_from   <=  spec_cmd_in(7 DOWNTO 0)&base1_cmd_in  AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";
    entries_q(i).a_to     <=  base0_cmd_in(global_addr_type'range)  AFTER 10 ps WHEN rising_edge(clock) AND push_en_gi = "1";
  END GENERATE entry_gen;

  -- update the various pointers
  hd_d      <= "000"                              WHEN reset = "1" ELSE
               std_logic_vector(unsigned(hd_q)+1) WHEN pop_i = "1" ELSE
               hd_q;
  hd_q      <= hd_d AFTER 10 ps WHEN rising_edge(clock);

  at_d      <= "000"                              WHEN reset = "1" ELSE
               std_logic_vector(unsigned(at_q)+1) WHEN sent_i = "1" ELSE
               at_q;
  at_q      <= at_d AFTER 10 ps WHEN rising_edge(clock);

  tl_d      <= "000"                              WHEN reset = "1" ELSE
               std_logic_vector(unsigned(tl_q)+1) WHEN push_i = "1" ELSE
               tl_q;
  tl_q      <= tl_d AFTER 10 ps WHEN rising_edge(clock);

  tl2_d     <= "010"                              WHEN reset = "1" ELSE
               std_logic_vector(unsigned(tl2_q)+1) WHEN push_i = "1" ELSE
               tl2_q;
  tl2_q     <= tl2_d AFTER 10 ps WHEN rising_edge(clock);

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
    VARIABLE  tick      : integer;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 102 ps;

    IF now >= start_print THEN
      tick := ( now / 1 ns ) - 2;

      IF false THEN
      dump_cycle( buf, "INP>" );
      dump_hex( buf, hd_q,       " hd_q " );
      dump_hex( buf, at_q,       " at_q " );
      dump_hex( buf, tl_q,       " tl_q " );
      dump_hex( buf, tl2_q,       " tl2_q " );
      dump_hex( buf, last_i,      " last_i " );
      dump_bit( buf, arready_axi_in,  " arready_axi_in ");
      dump_bit( buf, rvalid_axi_in,  " rvalid_axi_in ");
      writeline(output, buf );
      END IF;

      IF en_cmd_in = "1" THEN
        dump_cycle( buf, "INP> ");
        dump_insn( buf, insn_cmd_in, " " );
        dump_hex( buf, ctl_cmd_in, " ctl " );
        dump_hex( buf, ctr_cmd_in, " $ctr " );
        dump_hex( buf, base0_cmd_in, " $base0 " );
        dump_hex( buf, base1_cmd_in, " $base1 " );
        dump_hex( buf, spec_cmd_in, " $spec1 " );
        writeline(output, buf );
      END IF;

      IF  en_axi_q = "1" OR en2_axi_q = "1" THEN
        dump_cycle( buf, "INP> ");
        dump_bit( buf, en_axi_q, " en_axi_q " );
        dump_bit( buf, en2_axi_q, " en2_axi_q " );
        dump_bit( buf, last_axi_q, " last_axi_q " );
        dump_hex( buf, to_axi_q, " to_axi_q " );
        dump_hex( buf, word_axi_q, " word_axi_q " );
        dump_hex( buf, mode_axi_q, " mode_axi_q " );
        dump_hex( buf, type_axi_q, " type_axi_q " );
        writeline(output, buf );
      END IF;

      FOR i IN entries_q'range LOOP
        IF entries_q(i).valid = "1" THEN
          dump_cycle( buf, "INP>" );
          dump_integer( buf, i, " Q#" );
          dump_bit( buf, axi_clock, " axi_clock " );
          dump_hex( buf, entries_q(i).a_from,     " a_from ");
          dump_hex( buf, entries_q(i).in_size,    " in_size ");
          dump_hex( buf, entries_q(i).in_type,    " in_type ");
          dump_hex( buf, entries_q(i).in_mode,    " in_mode ");
          dump_bit( buf, entries_q(i).last,       " last ");
          dump_hex( buf, entries_q(i).a_to,       " a_to ");
          writeline(output, buf );
        END IF;
      END LOOP;

    END IF;

  END PROCESS dump;
-- pragma translate_on

END ARCHITECTURE behavior;
