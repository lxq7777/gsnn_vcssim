------------------------------------------------------------------------
--
-- Copyright (c) 2015 General Processor Technologies, Inc.
-- All rights reserved.
--
-- Dual Port memory model
-- All inputs are latched internal to the model.
-- Generic
--  lg2rows:      log-base2 of the number of rows. E.g., for 16 row
--                memory specify lg2rows => 4
--  bits:         number of bits in memory
--  trace_write:  set to true to trace every write to the memory
--  trace_read:   set to true to trace every read from the memory
--  name:         name of memory, used in trace messages
--  load:         name of file used to initialize memory. The file
--                should consist of pairs of address and data.
--                If the file does not exist, simulation will NOT halt.
--                If trace_write is true, the values loaded will be
--                traced.
-- Ports
--  clock:    clock
--
--  CEA_in:    chip enable (port A)
--  WEA_in:    write enable
--  AA_in:     address to be read/written
--  DA_in:     data to be written
--  QA_out:    data value that is read; set to U if WEA='1'
--
--  CEB_N_in:  chip enable (port B)
--  WEB_N_in:  write enable
--  AB_in:     address to be read/written
--  DB_in:     data to be written
--  QB_out:    data value that is read; set to U if WEB='1'
------------------------------------------------------------------------



LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

ENTITY memory_fpga IS
  GENERIC(
    lg2rows       : integer;
    bits          : integer;
    trace_write   : boolean := false;
    trace_read    : boolean := false;
    name          : string := "";
    load          : string := "";
    init          : std_logic := 'X'
  );
  PORT(
    clock         : IN std_logic;
    CEA_in        : IN std_logic;
    WEA_in        : IN std_logic;
    AA_in         : IN std_logic_vector( lg2rows-1 DOWNTO 0 );
    DA_in         : IN std_logic_vector( bits-1 DOWNTO 0 );

    CEB_in        : IN std_logic;
    WEB_in        : IN std_logic;
    AB_in         : IN std_logic_vector( lg2rows-1 DOWNTO 0 );
    DB_in         : IN std_logic_vector( bits-1 DOWNTO 0 );

    QA_out        : OUT std_logic_vector( bits-1 DOWNTO 0 );
    QB_out        : OUT std_logic_vector( bits-1 DOWNTO 0 )
  );
END ENTITY memory_fpga;



ARCHITECTURE behavior OF memory_fpga IS
  SIGNAL  CEA  : std_logic;
  SIGNAL  WEA  : std_logic;
--  SIGNAL  AA   : std_logic_vector( AA_in'range );
--  SIGNAL  DA   : std_logic_vector( DA_in'range );

  SIGNAL  CEB  : std_logic;
  SIGNAL  WEB  : std_logic;
--  SIGNAL  AB   : std_logic_vector( AB_in'range );
--  SIGNAL  DB   : std_logic_vector( DB_in'range );
  
  SIGNAL ena_n : STD_LOGIC;
SIGNAL wea_n :  STD_LOGIC;
signal aa, ab : std_logic_vector(22 downto 0);
signal bwe_a, bwe_b : std_logic_vector (8 downto 0);
signal DIN_Bx : std_logic_vector (71 downto 0);
signal qa,qb : std_logic_vector (71 downto 0);
signal da,db : std_logic_vector (71 downto 0);
signal sd : std_logic;

component blk_mem_gen_0 IS
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
end component blk_mem_gen_0;


  BEGIN
  

--aa(11 downto 0) <= AA_IN;
--aa(22 downto 12) <= (OTHERS => '0');
--ab(11 downto 0) <= AB_IN;
--ab(22 downto 12) <= (OTHERS => '0');
--da(31 downto 0) <= DA_IN;
--da(71 downto 32) <= (OTHERS => '0');
--db(31 downto 0) <= DA_IN;
--db(71 downto 32) <= (OTHERS => '0');

--URAM288_BASE_inst : URAM288_BASE
--   generic map (
--      AUTO_SLEEP_LATENCY => 8,            -- Latency requirement to enter sleep mode
--      AVG_CONS_INACTIVE_CYCLES => 10,     -- Average concecutive inactive cycles when is SLEEP mode for power
--                                          -- estimation
--      BWE_MODE_A => "PARITY_INTERLEAVED", -- Port A Byte write control
--      BWE_MODE_B => "PARITY_INTERLEAVED", -- Port B Byte write control
--      EN_AUTO_SLEEP_MODE => "FALSE",      -- Enable to automatically enter sleep mode
--      EN_ECC_RD_A => "FALSE",             -- Port A ECC encoder
--      EN_ECC_RD_B => "FALSE",             -- Port B ECC encoder
--      EN_ECC_WR_A => "FALSE",             -- Port A ECC decoder
--      EN_ECC_WR_B => "FALSE",             -- Port B ECC decoder
--      IREG_PRE_A => "FALSE",              -- Optional Port A input pipeline registers
--      IREG_PRE_B => "FALSE",              -- Optional Port B input pipeline registers
--      IS_CLK_INVERTED => '0',             -- Optional inverter for CLK
--      IS_EN_A_INVERTED => '0',            -- Optional inverter for Port A enable
--      IS_EN_B_INVERTED => '0',            -- Optional inverter for Port B enable
--      IS_RDB_WR_A_INVERTED => '0',        -- Optional inverter for Port A read/write select
--      IS_RDB_WR_B_INVERTED => '0',        -- Optional inverter for Port B read/write select
--      IS_RST_A_INVERTED => '0',           -- Optional inverter for Port A reset
--      IS_RST_B_INVERTED => '0',           -- Optional inverter for Port B reset
--      OREG_A => "FALSE",                  -- Optional Port A output pipeline registers
--      OREG_B => "FALSE",                  -- Optional Port B output pipeline registers
--      OREG_ECC_A => "FALSE",              -- Port A ECC decoder output
--      OREG_ECC_B => "FALSE",              -- Port B output ECC decoder
--      RST_MODE_A => "SYNC",               -- Port A reset mode
--      RST_MODE_B => "SYNC",               -- Port B reset mode
--      USE_EXT_CE_A => "FALSE",            -- Enable Port A external CE inputs for output registers
--      USE_EXT_CE_B => "FALSE"             -- Enable Port B external CE inputs for output registers
--   )
--   port map (
--     -- DBITERR_A => '0',               -- 1-bit output: Port A double-bit error flag status
--     -- DBITERR_B => '0',               -- 1-bit output: Port B double-bit error flag status
--      DOUT_A => qa,                     -- 72-bit output: Port A read data output
--      DOUT_B => qb ,                     -- 72-bit output: Port B read data output
--  --    SBITERR_A => '0',               -- 1-bit output: Port A single-bit error flag status
--  --    SBITERR_B => '0',               -- 1-bit output: Port B single-bit error flag status
--      ADDR_A => aa,                     -- 23-bit input: Port A address
--      ADDR_B => ab,                     -- 23-bit input: Port B address
--      BWE_A => BWE_A,                       -- 9-bit input: Port A Byte-write enable
--      BWE_B => BWE_B,                       -- 9-bit input: Port B Byte-write enable
--      CLK => clock,                           -- 1-bit input: Clock source
--      DIN_A => da,                       -- 72-bit input: Port A write data input
--      DIN_B => db,                       -- 72-bit input: Port B write data input
--      EN_A => CEA_IN,                         -- 1-bit input: Port A enable
--      EN_B => CEB_IN,                         -- 1-bit input: Port B enable
--      INJECT_DBITERR_A => '0',--INJECT_DBITERR_A, -- 1-bit input: Port A double-bit error injection
--      INJECT_DBITERR_B => '0',-- INJECT_DBITERR_B, -- 1-bit input: Port B double-bit error injection
--      INJECT_SBITERR_A => '0',-- INJECT_SBITERR_A, -- 1-bit input: Port A single-bit error injection
--      INJECT_SBITERR_B => '0',-- INJECT_SBITERR_B, -- 1-bit input: Port B single-bit error injection
--       OREG_CE_A => '0',               -- 1-bit input: Port A output register clock enable
--      OREG_CE_B => '0',               -- 1-bit input: Port B output register clock enable
--      OREG_ECC_CE_A => '0', --OREG_ECC_CE_A,       -- 1-bit input: Port A ECC decoder output register clock enable
--      OREG_ECC_CE_B => '0', --OREG_ECC_CE_B,       -- 1-bit input: Port B ECC decoder output register clock enable
--      RDB_WR_A => WEA_IN,                 -- 1-bit input: Port A read/write select
--      RDB_WR_B => WEB_IN,                 -- 1-bit input: Port B read/write select
--      RST_A => sd,                       -- 1-bit input: Port A asynchronous or synchronous reset for output
--                                            -- registers

--      RST_B => sd,                       -- 1-bit input: Port B asynchronous or synchronous reset for output
--                                            -- registers

--      SLEEP => sd                        -- 1-bit input: Dynamic power gating control
--   );


--QA_OUT(31 downto 0) <= qa(31 downto 0);
--QB_OUT(31 downto 0) <= qb(31 downto 0);



  a_mem: blk_mem_gen_0 
            PORT MAP(
              clka       =>  clock,
              ena        =>  CEA_IN,
              wea(0)        =>  WEA_IN,
              addra      =>  AA_IN,
              dina       =>  DA_IN,
              douta      =>  QA_OUT,
              clkb      => clock,
             
              enb => CEB_IN,
              web(0) => WEB_IN,
              addrb =>  AB_IN,
              dinb  =>   DB_IN,
              doutb =>  QB_OUT
   
            );
END ARCHITECTURE behavior;



