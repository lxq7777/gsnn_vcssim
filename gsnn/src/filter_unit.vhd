LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY clz31 IS 
  PORT(
      word_in                     : IN bit31_type;
      clz_busout                  : OUT bit5_type
    );
END ENTITY clz31;

ARCHITECTURE behavior OF clz31 IS
BEGIN
  clz_busout   <= 
              "11111" WHEN word_in(30) = '1' ELSE
              "11110" WHEN word_in(29) = '1' ELSE
              "11101" WHEN word_in(28) = '1' ELSE
              "11100" WHEN word_in(27) = '1' ELSE
              "11011" WHEN word_in(26) = '1' ELSE
              "11010" WHEN word_in(25) = '1' ELSE
              "11001" WHEN word_in(24) = '1' ELSE
              "11000" WHEN word_in(23) = '1' ELSE
              "10111" WHEN word_in(22) = '1' ELSE
              "10110" WHEN word_in(21) = '1' ELSE
              "10101" WHEN word_in(20) = '1' ELSE
              "10100" WHEN word_in(19) = '1' ELSE
              "10011" WHEN word_in(18) = '1' ELSE
              "10010" WHEN word_in(17) = '1' ELSE
              "10001" WHEN word_in(16) = '1' ELSE
              "10000" WHEN word_in(15) = '1' ELSE
              "01111" WHEN word_in(14) = '1' ELSE
              "01110" WHEN word_in(13) = '1' ELSE
              "01101" WHEN word_in(12) = '1' ELSE
              "01100" WHEN word_in(11) = '1' ELSE
              "01011" WHEN word_in(10) = '1' ELSE
              "01010" WHEN word_in(9) = '1' ELSE
              "01001" WHEN word_in(8) = '1' ELSE
              "01000" WHEN word_in(7) = '1' ELSE
              "00111" WHEN word_in(6) = '1' ELSE
              "00110" WHEN word_in(5) = '1' ELSE
              "00101" WHEN word_in(4) = '1' ELSE
              "00100" WHEN word_in(3) = '1' ELSE
              "00011" WHEN word_in(2) = '1' ELSE
              "00010" WHEN word_in(1) = '1' ELSE
              "00001" WHEN word_in(0) = '1' ELSE
              "00000";

END ARCHITECTURE behavior;
    
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

-- takes a fixed-point number with implict decimal point at bit 25 (i.e. QN.25 format)
ENTITY cvt_fp16 IS 
  PORT(
      word_in                     : IN std_logic_vector;
      fp16_busout                 : OUT fp16_type
    );
END ENTITY cvt_fp16;

ARCHITECTURE behavior OF cvt_fp16 IS

  COMPONENT clz31 IS 
    PORT(
        word_in                     : IN bit31_type;
        clz_busout                  : OUT bit5_type
      );
  END COMPONENT clz31;

  SIGNAL  sign_i    : bit_type;
  SIGNAL  high_i    : std_logic_vector(word_in'high-43 DOWNTO 0);
  SIGNAL  isinf_i   : bit_type;
  SIGNAL  low_i     : bit44_type;
  SIGNAL  abs_i     : bit44_type;
  SIGNAL  sig_i     : bit31_type;
  SIGNAL  exp_i     : bit5_type;
  SIGNAL  sh_i      : bit44_type;
  SIGNAL  fp16_i    : fp16_type;
BEGIN
  sign_i(0)       <= word_in(word_in'high);
  high_i          <= word_in(word_in'high DOWNTO 43);
  isinf_i         <= "0" WHEN high_i = (high_i'range => '0') OR high_i = (high_i'range => '1') ELSE "1";
  low_i           <= word_in(43 DOWNTO 0);
  abs_i           <= std_logic_vector(0-unsigned(low_i)) WHEN sign_i = "1" ELSE low_i;
  sig_i           <= abs_i(43 DOWNTO 13);
  a_clz31:  clz31 PORT MAP( sig_i, exp_i);
  sh_i             <= std_logic_vector( shift_left(unsigned(abs_i), 31-to_integer(unsigned(exp_i))) );

  fp16_i          <= sign_i & "11111" & "0000000000" WHEN isinf_i = "1" ELSE 
                     sign_i & "00000" & "0000000000" WHEN exp_i = "00000" ELSE
                     sign_i & exp_i & sh_i(42 DOWNTO 33);
  fp16_busout     <= fp16_i;
END ARCHITECTURE behavior;

-- pragma translate_off
USE std.textio.ALL;
USE work.dump_package.ALL;
-- pragma translate_on

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.gsnn_package.ALL;

ENTITY filter_unit IS
  PORT(
  clock                           : IN std_logic;
  reset                           : IN bit_type;

  ---------------------------------------------------------------------------
  -- engine signals
  ---------------------------------------------------------------------------
  en_cmd_in                       : IN bit_type;
  insn_cmd_in                     : IN insn_type;
  ctl_cmd_in                      : IN bit16_type;
  ctr_cmd_in                      : IN bit32_type;
  base0_cmd_in                    : IN bit32_type;
  base1_cmd_in                    : IN bit32_type;
  base2_cmd_in                    : IN bit32_type;
  spec_cmd_in                     : IN bit32_type;

  ---------------------------------------------------------------------------
  -- memory signals
  ---------------------------------------------------------------------------
  en_mem_data_busout              : OUT bit_type;
  width_mem_data_busout           : OUT bit2_type;    -- 10 = 6B, 11= 12B, 00 = 18B
  slice_mem_data_busout           : OUT slice_mask_type;
  addr_mem_data_busout            : OUT slice_addr_type;
  word0_data_in                   : IN bit144_type;
  word1_data_in                   : IN bit144_type;
  word2_data_in                   : IN bit144_type;
  word3_data_in                   : IN bit144_type;
  word4_data_in                   : IN bit144_type;
  word5_data_in                   : IN bit144_type;
  word6_data_in                   : IN bit144_type;
  word7_data_in                   : IN bit144_type;

  en_mem_psum_busout              : OUT bit_type;
  width_mem_psum_busout           : OUT bit_type;     -- 1 = 8B, 0 = 2B
  slice_mem_psum_busout           : OUT slice_mask_type;
  addr_mem_psum_busout            : OUT slice_addr_type;
  word0_psum_in                   : IN bit64_type;
  word1_psum_in                   : IN bit64_type;
  word2_psum_in                   : IN bit64_type;
  word3_psum_in                   : IN bit64_type;
  word4_psum_in                   : IN bit64_type;
  word5_psum_in                   : IN bit64_type;
  word6_psum_in                   : IN bit64_type;
  word7_psum_in                   : IN bit64_type;

  en_mem_res_busout               : OUT bit_type;
  width_mem_res_busout            : OUT bit_type;     -- 1 = 8B, 0 = 2B
  slice_mem_res_busout            : OUT slice_mask_type;
  addr_mem_res_busout             : OUT slice_addr_type;
  word0_mem_res_busout            : OUT bit64_type;
  word1_mem_res_busout            : OUT bit64_type;
  word2_mem_res_busout            : OUT bit64_type;
  word3_mem_res_busout            : OUT bit64_type;
  word4_mem_res_busout            : OUT bit64_type;
  word5_mem_res_busout            : OUT bit64_type;
  word6_mem_res_busout            : OUT bit64_type;
  word7_mem_res_busout            : OUT bit64_type
  );
END ENTITY filter_unit;

ARCHITECTURE behavior OF filter_unit IS

-- pragma translate_off
  CONSTANT disable_print : boolean := true;
  CONSTANT start_print   : time := 0 ns;
-- pragma translate_on

  CONSTANT ECO_fix        : boolean := true; -- false restores bug for debugging purposes

  COMPONENT mult_reg_en_fpga is
    GENERIC(
      WIDTHA : integer := 11;
      WIDTHB : integer := 11
    );
    PORT(
      reset : in std_logic;
      clk : in std_logic;
      enA : in std_logic;
      A   : in std_logic_vector(WIDTHA - 1 downto 0);
      enB : in std_logic;
      B   : in std_logic_vector(WIDTHB - 1 downto 0);
      RES : out std_logic_vector(WIDTHA + WIDTHB - 1 downto 0)
    );
  END COMPONENT mult_reg_en_fpga;

  COMPONENT cvt_fp16 IS 
    PORT(
        word_in                     : IN std_logic_vector;
        fp16_busout                 : OUT fp16_type
      );
  END COMPONENT cvt_fp16;


  SIGNAL  en_psum_i0      : bit_type;
  SIGNAL  slice_en_i0     : slice_mask_type;
  SIGNAL  slice_psum1_i0  : slice_mask_type;
  SIGNAL  slice_psum_i0   : slice_mask_type;
  SIGNAL  nzero_psum_i0   : slice_mask_type;
  SIGNAL  base0_cmd_i0    : global_addr_type;

  SIGNAL  insn_cmd_i0     : insn_type;
  SIGNAL  ctl_cmd_i0      : bit16_type;
  SIGNAL  ctr_cmd_i0      : bit32_type;

  SIGNAL  en_cmd_i0       : bit_type;
  SIGNAL  en_cmd_q1       : bit_type;
  SIGNAL  insn_cmd_q1     : insn_type;
  SIGNAL  ctl_cmd_q1      : bit16_type;
  SIGNAL  ctr_cmd_q1      : bit32_type;
  SIGNAL  base0_cmd_q1    : global_addr_type;
  SIGNAL  slice_en_q1     : slice_mask_type;
  SIGNAL  nzero_psum_q1   : slice_mask_type;

  SIGNAL  en_cmd_i1       : bit_type;
  SIGNAL  en_cmd_q2       : bit_type;
  SIGNAL  insn_cmd_q2     : insn_type;
  SIGNAL  ctl_cmd_q2      : bit16_type;
  SIGNAL  ctr_cmd_q2      : bit32_type;
  SIGNAL  base0_cmd_q2    : global_addr_type;
  SIGNAL  slice_en_q2     : slice_mask_type;
  SIGNAL  nzero_psum_q2   : slice_mask_type;

  SIGNAL  word_data_i2    : array_bit144_type(SLICE_RANGE);
  SIGNAL  word_psum_i2    : array_bit64_type(SLICE_RANGE);

  SIGNAL  en_cmd_i2       : bit_type;
  SIGNAL  en_cmd_q3       : bit_type;
  SIGNAL  insn_cmd_q3     : insn_type;
  SIGNAL  ctl_cmd_q3      : bit16_type;
  SIGNAL  slice_en_q3     : slice_mask_type;
  SIGNAL  base0_cmd_q3    : global_addr_type;

  SIGNAL  en_cmd_i3       : bit_type;
  SIGNAL  en_cmd_q4       : bit_type;
  SIGNAL  insn_cmd_q4     : insn_type;
  SIGNAL  ctl_cmd_q4      : bit16_type;
  SIGNAL  slice_en_q4     : slice_mask_type;
  SIGNAL  base0_cmd_q4    : global_addr_type;

  SIGNAL  en_cmd_i4       : bit_type;
  SIGNAL  en_cmd_q5       : bit_type;
  SIGNAL  insn_cmd_q5     : insn_type;
  SIGNAL  ctl_cmd_q5      : bit16_type;
  SIGNAL  slice_en_q5     : slice_mask_type;
  SIGNAL  base0_cmd_q5    : global_addr_type;

  SIGNAL  en_cmd_i5       : bit_type;
  SIGNAL  en_cmd_q6       : bit_type;
  SIGNAL  insn_cmd_q6     : insn_type;
  SIGNAL  ctl_cmd_q6      : bit16_type;
  SIGNAL  slice_en_q6     : slice_mask_type;
  SIGNAL  base0_cmd_q6    : global_addr_type;

  SIGNAL  sum_i5          : array2_bit64_type( SLICE_RANGE, FILTER_RANGE);
  SIGNAL  sum_q6          : array2_bit64_type( SLICE_RANGE, FILTER_RANGE);

  SIGNAL  res_slice_i6    : array2_fp16_type( SLICE_RANGE, FILTER_RANGE);
  SIGNAL  res_global_i7   : array_fp16_type(FILTER_RANGE);

  SIGNAL  width_i0        : bit2_type;

  -- We want to reuse busouts for ECO; these just capture the outputs of the flops
  SIGNAL ECO_en_mem_psum_i    : bit_type;
  SIGNAL ECO_slice_mem_psum_i : slice_mask_type;

BEGIN

  WITH ctl_cmd_in(DATA_FILTER_INSN_FIELD) SELECT
    width_i0 <=
                            "10" WHEN WT3_DATA_FILTER,
                            "11" WHEN IN6_DATA_FILTER,
                            "00" WHEN WT9_DATA_FILTER|IN9_DATA_FILTER|IN9X2_DATA_FILTER,
                            "XX" WHEN OTHERS;
  slice_en_i0           <=  NOT spec_cmd_in(slice_mask_type'range);
  en_psum_i0            <=  "0" WHEN reset = "1" ELSE
                            "1" WHEN en_cmd_in = "1" AND ctl_cmd_in(SUM_FILTER_INSN_FIELD) = PARTIAL_SUM_FILTER ELSE
                            "0";
  slice_psum1_i0        <=  slice_sel_to_mask( base2_cmd_in(SLICE_SEL_ADDR_MEM) );
  slice_psum_i0         <=  slice_en_i0 WHEN ctl_cmd_in(MODE_FILTER_INSN_FIELD) = SLICE_MODE_FILTER ELSE slice_psum1_i0;

  nzero_psum_i0         <= slice_psum1_i0 WHEN en_psum_i0 = "1" ELSE (OTHERS=>'0');
  -- ECO This is what it should be
  -- nzero_psum_i0         <= slice_psum_i0 WHEN en_psum_i0 = "1" ELSE (OTHERS=>'0');
  base0_cmd_i0          <= base0_cmd_in(global_addr_type'range);
  insn_cmd_i0           <= insn_cmd_in;
  ctl_cmd_i0            <= ctl_cmd_in;
  ctr_cmd_i0            <= ctr_cmd_in;

  en_mem_data_busout    <= en_cmd_in AFTER 10 ps WHEN rising_edge(clock);
  width_mem_data_busout <= width_i0 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in = "1";
  slice_mem_data_busout <= slice_en_i0 AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in = "1";
  addr_mem_data_busout  <= base1_cmd_in(slice_addr_type'range) AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_in = "1";

  ECO_en_mem_psum_i     <=  en_psum_i0 AFTER 10 ps WHEN rising_edge(clock);
  en_mem_psum_busout    <= ECO_en_mem_psum_i;
  width_mem_psum_busout <= "1";
  ECO_slice_mem_psum_i  <= slice_psum_i0 AFTER 10 ps WHEN rising_edge(clock) AND en_psum_i0 = "1";
  slice_mem_psum_busout <= ECO_slice_mem_psum_i;
  addr_mem_psum_busout  <= base2_cmd_in(slice_addr_type'range) AFTER 10 ps WHEN rising_edge(clock) AND en_psum_i0 = "1";

  en_cmd_i0             <= "0" WHEN reset = "1" ELSE en_cmd_in;
  en_cmd_q1             <= en_cmd_i0      AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q1           <= insn_cmd_i0    AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1"; 
  ctl_cmd_q1            <= ctl_cmd_i0     AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1"; 
  ctr_cmd_q1            <= ctr_cmd_i0     AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1"; 
  base0_cmd_q1          <= base0_cmd_i0   AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1"; 
  slice_en_q1           <= slice_en_i0    AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i0 = "1"; 
  ECO_before: IF NOT ECO_fix GENERATE
    nzero_psum_q1         <= nzero_psum_i0  AFTER 10 ps WHEN rising_edge(clock);
  END GENERATE ECO_before;
  ECO_after: IF ECO_fix GENERATE
  nzero_psum_q1         <= ECO_slice_mem_psum_i AND (7 DOWNTO 0 => ECO_en_mem_psum_i(0));
  END GENERATE ECO_after;

  en_cmd_i1             <= "0" WHEN reset = "1" ELSE en_cmd_q1;
  en_cmd_q2             <= en_cmd_i1      AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q2           <= insn_cmd_q1    AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1"; 
  ctl_cmd_q2            <= ctl_cmd_q1     AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1"; 
  ctr_cmd_q2            <= ctr_cmd_q1     AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1"; 
  base0_cmd_q2          <= base0_cmd_q1   AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1"; 
  slice_en_q2           <= slice_en_q1    AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_i1 = "1"; 
  nzero_psum_q2         <= nzero_psum_q1  AFTER 10 ps WHEN rising_edge(clock);

  word_data_i2(0)       <= word0_data_in;
  word_data_i2(1)       <= word1_data_in;
  word_data_i2(2)       <= word2_data_in;
  word_data_i2(3)       <= word3_data_in;
  word_data_i2(4)       <= word4_data_in;
  word_data_i2(5)       <= word5_data_in;
  word_data_i2(6)       <= word6_data_in;
  word_data_i2(7)       <= word7_data_in;

  word_psum_i2(0)       <= word0_psum_in;
  word_psum_i2(1)       <= word1_psum_in;
  word_psum_i2(2)       <= word2_psum_in;
  word_psum_i2(3)       <= word3_psum_in;
  word_psum_i2(4)       <= word4_psum_in;
  word_psum_i2(5)       <= word5_psum_in;
  word_psum_i2(6)       <= word6_psum_in;
  word_psum_i2(7)       <= word7_psum_in;

  en_cmd_i2             <= "0" WHEN reset = "1" ELSE
                           "0" WHEN en_cmd_q2 = "0" ELSE
                           "0" WHEN ctl_cmd_q2(KIND_FILTER_INSN_FIELD) = INACTIVE_KIND_FILTER ELSE
                           "0" WHEN ctl_cmd_q2(KIND_FILTER_INSN_FIELD) = X2_KIND_FILTER AND ctr_cmd_q2(0) /= '0' ELSE
                           "0" WHEN ctl_cmd_q2(KIND_FILTER_INSN_FIELD) = X4_KIND_FILTER AND ctr_cmd_q2(1 DOWNTO 0) /= "00" ELSE
                           "1";

  en_cmd_q3     <= en_cmd_i2 AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q3   <= insn_cmd_q2 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q2 = "1";
  ctl_cmd_q3    <= ctl_cmd_q2 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q2 = "1";
  base0_cmd_q3  <= base0_cmd_q2 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q2 = "1";
  slice_en_q3   <= slice_en_q2 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q2 = "1";

  en_cmd_i3     <= "0" WHEN reset = "1" ELSE en_cmd_q3;
  en_cmd_q4     <= en_cmd_i3 AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q4   <= insn_cmd_q3 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q3 = "1";
  ctl_cmd_q4    <= ctl_cmd_q3 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q3 = "1";
  base0_cmd_q4  <= base0_cmd_q3 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q3 = "1";
  slice_en_q4   <= slice_en_q3 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q3 = "1";

  en_cmd_i4     <= "0" WHEN reset = "1" ELSE en_cmd_q4;
  en_cmd_q5     <= en_cmd_i4 AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q5   <= insn_cmd_q4 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q4 = "1";
  ctl_cmd_q5    <= ctl_cmd_q4 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q4 = "1";
  base0_cmd_q5  <= base0_cmd_q4 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q4 = "1";
  slice_en_q5   <= slice_en_q4 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q4 = "1";

  en_cmd_i5     <= "0" WHEN reset = "1" ELSE en_cmd_q5;
  en_cmd_q6     <= en_cmd_i5 AFTER 10 ps WHEN rising_edge(clock);
  insn_cmd_q6   <= insn_cmd_q5 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q5 = "1";
  ctl_cmd_q6    <= ctl_cmd_q5 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q5 = "1";
  base0_cmd_q6  <= base0_cmd_q5 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q5 = "1";
  slice_en_q6   <= slice_en_q5 AFTER 10 ps WHEN  rising_edge(clock) AND en_cmd_q5 = "1";

  slice_gen: FOR sl IN SLICE_RANGE GENERATE
    SIGNAL  wt_q            : array2_fp16_type(FILTER_DIM, FILTER_DIM);
    SIGNAL  mant1_wt_i2     : array2_bit11_type(FILTER_RANGE, FILTER_DIM2);
    SIGNAL  in_q            : array3_fp16_type(FILTER_RANGE, FILTER_DIM, FILTER_DIM);
    SIGNAL  mant1_in_i2     : array2_bit11_type(FILTER_RANGE, FILTER_DIM2);
    SIGNAL  words_i2        : array_fp16_type(FILTER_DIM2);
    SIGNAL  en_wt_i2        : bit_type;
    SIGNAL  en_wt9_i2       : bit_type;
    SIGNAL  en_in_i2        : bit_type;
    SIGNAL  en_in9_i2       : bit_type;
    SIGNAL  en_in6_i2       : bit_type;
    SIGNAL  zero_i2         : bit9_type;
    SIGNAL  is_d1_i2        : bit_type;
    SIGNAL  sum_i4          : array_fp16_type(FILTER_RANGE);
    SIGNAL  en_cmd_i2       : bit_type;
    SIGNAL  word0_psum_i2   : bit64_type;
    SIGNAL  word0_psum_q3   : bit64_type;
    SIGNAL  word0_psum_q4   : bit64_type;
    SIGNAL  slen_i2         : bit_type;
    SIGNAL  slen_c_i2       : bit_type;
    SIGNAL  slen_q3         : bit_type;
    SIGNAL  slen_i3         : bit_type;
    SIGNAL  slen_q4         : bit_type;
    SIGNAL  slen_i4         : bit_type;
    SIGNAL  slen_q5         : bit_type;
    SIGNAL  slen_i5         : bit_type;
    SIGNAL  slen_q6         : bit_type;
  BEGIN
    slen_i2             <= "1" WHEN en_cmd_q2 = "1" AND slice_en_q2(sl) = '1' ELSE "0";
    zero_i2             <= ctl_cmd_q2( ZERO_FILTER_INSN_FIELD );

    words_gen: FOR i IN words_i2'range GENERATE
      words_i2(i) <= (15 DOWNTO 0 => '0') WHEN zero_i2(i) = '1' ELSE word_data_i2(sl)(16*i+15 DOWNTO 16*i);
    END GENERATE words_gen;

    WITH ctl_cmd_q2(DATA_FILTER_INSN_FIELD ) SELECT
      en_wt_i2          <=  slen_i2   WHEN WT3_DATA_FILTER|WT9_DATA_FILTER,
                            "0"       WHEN IN6_DATA_FILTER|IN9x2_DATA_FILTER|IN9_DATA_FILTER,
                            "X"       WHEN OTHERS;
    WITH ctl_cmd_q2(DATA_FILTER_INSN_FIELD ) SELECT
      en_wt9_i2         <=  "0"       WHEN WT3_DATA_FILTER,
                            "1"       WHEN WT9_DATA_FILTER,
                            "X"       WHEN OTHERS;
    wt_gen: FOR i IN FILTER_DIM GENERATE
      SIGNAL  wt0_i2  : fp16_type;
      SIGNAL  wt1_i2  : fp16_type;
      SIGNAL  wt2_i2  : fp16_type;
    BEGIN
      wt0_i2      <= words_i2(0*3+i) WHEN en_wt9_i2 = "1" ELSE wt_q(1, i);
      wt1_i2      <= words_i2(1*3+i) WHEN en_wt9_i2 = "1" ELSE wt_q(2, i);
      wt2_i2      <= words_i2(2*3+i) WHEN en_wt9_i2 = "1" ELSE words_i2(i);
      wt_q(0, i)  <= wt0_i2 AFTER 10 ps WHEN rising_edge(clock) AND en_wt_i2 = "1";
      wt_q(1, i)  <= wt1_i2 AFTER 10 ps WHEN rising_edge(clock) AND en_wt_i2 = "1";
      wt_q(2, i)  <= wt2_i2 AFTER 10 ps WHEN rising_edge(clock) AND en_wt_i2 = "1";

      mant1_gen: FOR fil  IN FILTER_RANGE GENERATE
        mant1_wt_i2(fil, 0*3 + i)    <= "1"&wt0_i2(MANT_FP16); -- AFTER 10 ps WHEN rising_edge(clock) AND en_wt_i2 = "1";
        mant1_wt_i2(fil, 1*3 + i)    <= "1"&wt1_i2(MANT_FP16); -- AFTER 10 ps WHEN rising_edge(clock) AND en_wt_i2 = "1";
        mant1_wt_i2(fil, 2*3 + i)    <= "1"&wt2_i2(MANT_FP16); -- AFTER 10 ps WHEN rising_edge(clock) AND en_wt_i2 = "1";
      END GENERATE mant1_gen;
  END GENERATE wt_gen;


    WITH ctl_cmd_q2(DATA_FILTER_INSN_FIELD ) SELECT
      en_in_i2          <=  "0"       WHEN WT3_DATA_FILTER|WT9_DATA_FILTER,
                            slen_i2   WHEN IN6_DATA_FILTER|IN9x2_DATA_FILTER|IN9_DATA_FILTER,
                            "X"       WHEN OTHERS;
    WITH ctl_cmd_q2(DATA_FILTER_INSN_FIELD ) SELECT
      en_in9_i2         <=  "0"       WHEN IN6_DATA_FILTER|IN9x2_DATA_FILTER,
                            "1"       WHEN IN9_DATA_FILTER,
                            "X"       WHEN OTHERS;
    WITH ctl_cmd_q2(DATA_FILTER_INSN_FIELD ) SELECT
      en_in6_i2         <=  "1"       WHEN IN6_DATA_FILTER,
                            "0"       WHEN IN9_DATA_FILTER|IN9x2_DATA_FILTER,
                            "X"       WHEN OTHERS;
    in_gen: FOR fil IN FILTER_RANGE GENERATE
    BEGIN
      filter_gen: FOR i IN FILTER_DIM GENERATE
        SIGNAL  in0_i2  : fp16_type;
        SIGNAL  in1_i2  : fp16_type;
        SIGNAL  in2_i2  : fp16_type;
      BEGIN

        rest_gen: IF fil /= NUM_FILTERS-1 GENERATE
          in0_i2      <=  in_q(fil+1, 0, i ) WHEN en_in9_i2 = "1" ELSE in_q(fil, 1, i);
          in1_i2      <=  in_q(fil+1, 1, i ) WHEN en_in9_i2 = "1" ELSE in_q(fil, 2, i);
          in2_i2      <=  in_q(fil+1, 2, i ) WHEN en_in9_i2 = "1" ELSE 
                          words_i2(fil+i)   WHEN en_in6_i2 = "1" ELSE
                          words_i2(2*fil+i);
        END GENERATE rest_gen;

        last_gen: IF fil = NUM_FILTERS-1 GENERATE
          in0_i2      <=  words_i2(0*3+i) WHEN en_in9_i2 = "1" ELSE in_q(fil, 1, i);
          in1_i2      <=  words_i2(1*3+i) WHEN en_in9_i2 = "1" ELSE in_q(fil, 2, i);
          in2_i2      <=  words_i2(2*3+i) WHEN en_in9_i2 = "1" ELSE 
                          words_i2(fil+i)   WHEN en_in6_i2 = "1" ELSE
                          words_i2(2*fil+i);
        END GENERATE last_gen;

        in_q(fil, 0, i)  <= in0_i2 AFTER 10 ps WHEN rising_edge(clock) AND en_in_i2 = "1";
        in_q(fil, 1, i)  <= in1_i2 AFTER 10 ps WHEN rising_edge(clock) AND en_in_i2 = "1";
        in_q(fil, 2, i)  <= in2_i2 AFTER 10 ps WHEN rising_edge(clock) AND en_in_i2 = "1";

        mant1_in_i2(fil, 0*3 + i)    <= "1"&in0_i2(MANT_FP16); -- AFTER 10 ps WHEN rising_edge(clock) AND en_in_i2 = "1";
        mant1_in_i2(fil, 1*3 + i)    <= "1"&in1_i2(MANT_FP16); -- AFTER 10 ps WHEN rising_edge(clock) AND en_in_i2 = "1";
        mant1_in_i2(fil, 2*3 + i)    <= "1"&in2_i2(MANT_FP16); -- AFTER 10 ps WHEN rising_edge(clock) AND en_in_i2 = "1";

      END GENERATE filter_gen;
    END GENERATE in_gen;

    -- partial sum is handled by either using the value read from memory or zeroing
    -- if no partial sum (i.e. DIRECT), then all psum are zeroed
    -- if global partial sum (i.e. PARTIAL,GLOBAL) then the psum for one slice is kept, all others are zeroed
    -- if slice partial sum (i.e. PARTIAL,SLICE) then all psums are kept
    word0_psum_i2       <= word_psum_i2(sl)   WHEN nzero_psum_q2(sl) = '1' ELSE (OTHERS=>'0');
    word0_psum_q3       <= word0_psum_i2 AFTER 10 ps WHEN rising_edge(clock) AND slen_i2 = "1";
    word0_psum_q4       <= word0_psum_q3 AFTER 10 ps WHEN rising_edge(clock) AND slen_q3 = "1";

    slen_c_i2           <= "0" WHEN reset = "1" ELSE
                           "0" WHEN slen_i2 = "0"  ELSE
                           "0" WHEN ctl_cmd_q2(KIND_FILTER_INSN_FIELD) = INACTIVE_KIND_FILTER ELSE
                           "0" WHEN ctl_cmd_q2(KIND_FILTER_INSN_FIELD) = X2_KIND_FILTER AND ctr_cmd_q2(0) /= '0' ELSE
                           "0" WHEN ctl_cmd_q2(KIND_FILTER_INSN_FIELD) = X4_KIND_FILTER AND ctr_cmd_q2(1 DOWNTO 0) /= "00" ELSE
                           "1";
    slen_q3             <= slen_c_i2 AFTER 10 ps WHEN rising_edge(clock);
    slen_i3             <= "0" WHEN reset = "1" ELSE slen_q3;
    slen_q4             <= slen_i3 AFTER 10 ps WHEN rising_edge(clock);
    slen_i4             <= "0" WHEN reset = "1" ELSE slen_q4;
    slen_q5             <= slen_i4 AFTER 10 ps WHEN rising_edge(clock);
    slen_i5             <= "0" WHEN reset = "1" ELSE slen_q5;
    slen_q6             <= slen_i5 AFTER 10 ps WHEN rising_edge(clock);

    filter_gen: FOR fil IN FILTER_RANGE GENERATE
      SIGNAL  in_fi2          : array_fp16_type(FILTER_DIM2);
      SIGNAL  wt_fi2          : array_fp16_type(FILTER_DIM2);
      SIGNAL  sign_fq3        : array_bit_type(FILTER_DIM2);
      SIGNAL  prod_fq3        : array_bit59_type(FILTER_DIM2);
      SIGNAL  psum_fq3        : bit42_type;
      SIGNAL  psum_fq4        : bit42_type;
      SIGNAL  sign_psum_fq4   : bit_type;
      SIGNAL  sign_fq4        : array_bit_type(FILTER_DIM2);
      SIGNAL  prod_fq4        : array_bit59_type(FILTER_DIM2);
    BEGIN


      in_gen: FOR j IN FILTER_DIM GENERATE
        wt_fi2(0*3 + j) <= wt_q(0, j);
        wt_fi2(1*3 + j) <= wt_q(1, j);
        wt_fi2(2*3 + j) <= wt_q(2, j);

        in_fi2(0*3 + j) <= in_q(fil, 0, j);
        in_fi2(1*3 + j) <= in_q(fil, 1, j);
        in_fi2(2*3 + j) <= in_q(fil, 2, j);
      END GENERATE in_gen;

      -- do the 9 fp16 mantissa and shift to align the binary point.
      -- the full shifted 82 bit product is NOT kept; the bottom 23 bits are discarded
      prod_gen: FOR j IN FILTER_DIM2 GENERATE
        SIGNAL  sign_wt_gfi2    : bit_type;
        SIGNAL  exp_wt_gfi2     : bit5_type;
        SIGNAL  sign_in_gfi2    : bit_type;
        SIGNAL  exp_in_gfi2     : bit5_type;

        SIGNAL  mant1_wt_gfi2   : bit11_type;
        SIGNAL  mant1_in_gfi2   : bit11_type;
        SIGNAL  sign_fi2        : bit_type;

        SIGNAL  isinf_gfi2      : bit_type;
        SIGNAL  iszero_gfi2     : bit_type;
        SIGNAL  prod_gfi2       : bit22_type;

        SIGNAL  by_gfi2         : bit6_type;
        SIGNAL  prod1_gfi3      : bit53_type;
        SIGNAL  sh1_gfi3        : bit53_type;
        SIGNAL  sh_gfi3         : bit59_type;

        SIGNAL  isinf_gfq3      : bit_type;
        SIGNAL  iszero_gfq3     : bit_type;
        SIGNAL  prod_gfq3       : bit22_type;
        SIGNAL  by_gfq3         : bit6_type;
      BEGIN

        sign_wt_gfi2    <= wt_fi2(j)(SIGN_FP16);
        exp_wt_gfi2     <= wt_fi2(j)(EXP_FP16);

        sign_in_gfi2    <= in_fi2(j)(SIGN_FP16);
        exp_in_gfi2     <= in_fi2(j)(EXP_FP16);

        isinf_gfi2      <= "1" WHEN exp_wt_gfi2 = "11111" OR exp_in_gfi2 = "11111" ELSE "0";
        iszero_gfi2     <= "1" WHEN exp_wt_gfi2 = "00000" OR exp_in_gfi2 = "00000" ELSE "0";
        by_gfi2         <= std_logic_vector(unsigned(bit6_type'("0"&exp_wt_gfi2)) + unsigned(exp_in_gfi2));

        sign_fi2        <=  "0" WHEN iszero_gfi2 = "1" OR sign_wt_gfi2 = sign_in_gfi2 ELSE "1";

        a_mult: mult_reg_en_fpga PORT MAP( reset(0), clock, en_in_i2(0), mant1_in_i2(fil, j), en_wt_i2(0), mant1_wt_i2(fil, j),  prod_gfq3 );

        --prod_gfq3   <= prod_gfi2 AFTER 10 ps WHEN rising_edge(clock) AND slen_q3 = "1";
        by_gfq3     <= by_gfi2 AFTER 10 ps WHEN rising_edge(clock) AND slen_q3 = "1";
        iszero_gfq3 <= iszero_gfi2 AFTER 10 ps WHEN rising_edge(clock) AND slen_q3 = "1";
        isinf_gfq3  <= isinf_gfi2 AFTER 10 ps WHEN rising_edge(clock) AND slen_q3 = "1";

        -- mant1_wt_gfi2   <= mant1_wt_i2(fil, j) AFTER 10 ps WHEN rising_edge(clock) AND en_wt_i2 = "1";
        -- mant1_in_gfi2   <= mant1_in_i2(fil, j) AFTER 10 ps WHEN rising_edge(clock) AND en_in_i2 = "1";
        -- prod_gfi2       <= std_logic_vector( unsigned(mant1_in_gfi2)*unsigned(mant1_wt_gfi2) );

        -- we want prod >> (by-23); by is between 0 and 60 (ignoring inf), so by-23 is between -23 and 37
        -- we split this into two cases: by < 32 and by >= 32. and implement them as
        -- (prod << (by&0x1f))>>23  for by <= 31
        -- (prod << (by&0x1f))<<9   for by >= 32
        -- the product of 11 bit mantissas is 22 bits, and after shifting by 37 it is 59 bits.
        prod1_gfi3      <=   (30 DOWNTO 0 => '0') & prod_gfq3;
        sh1_gfi3        <=  std_logic_vector( shift_left( unsigned(prod1_gfi3), to_integer(unsigned(by_gfq3(4 DOWNTO 0))) ));
        
        sh_gfi3         <=  (OTHERS => '0')                                           WHEN iszero_gfq3 = "1" ELSE
                            (OTHERS => '1')                                           WHEN isinf_gfq3 = "1" ELSE
                            (28 DOWNTO 0 => '0') & sh1_gfi3(sh1_gfi3'high DOWNTO 23)  WHEN by_gfq3(5) = '0' ELSE
                            sh1_gfi3(49 DOWNTO 0 ) & (8 DOWNTO 0 => '0');

        sign_fq3(j) <= sign_fi2 AFTER 10 ps WHEN rising_edge(clock) AND slen_q3 = "1";
        -- prod_fq3(j) <= sh_gfi3 AFTER 10 ps WHEN rising_edge(clock) AND slen_q3 = "1";
        sign_fq4(j) <= sign_fq3(j) AFTER 10 ps WHEN rising_edge(clock) AND slen_q4 = "1";
        prod_fq4(j) <= sh_gfi3 AFTER 10 ps WHEN rising_edge(clock) AND slen_q4 = "1";
      END GENERATE prod_gen;

      -- in parallel deal with aligning the partial sum inputs
      psum_block: IF true GENERATE
        SIGNAL psum_bi4       : fp16_type;
        SIGNAL sign_psum_bi4  : bit_type;
        SIGNAL exp_bi4        : bit5_type;
        SIGNAL mant_bi4       : bit10_type;
        SIGNAL mant1_bi4      : bit42_type;
        SIGNAL sh1_bi4        : bit42_type;
        SIGNAL sh_bi4         : bit42_type;
      BEGIN
        psum_bi4  <= word0_psum_q4(16*fil+15 DOWNTO 16*fil);
        sign_psum_fq4  <= psum_bi4(SIGN_FP16) AFTER 10 ps WHEN rising_edge(clock);

        exp_bi4   <= psum_bi4(EXP_FP16);
        mant_bi4  <= psum_bi4(MANT_FP16);
        mant1_bi4 <= (30 DOWNTO 0=>'0')&"1"&mant_bi4;
        sh1_bi4   <= std_logic_vector(shift_left( unsigned(mant1_bi4), to_integer(unsigned(exp_bi4))));
        sh_bi4    <= (OTHERS => '0') WHEN exp_bi4 = "00000" ELSE sh1_bi4;
        psum_fq4  <= sh_bi4 AFTER 10 ps WHEN rising_edge(clock) AND slen_q4 = "1";
      END GENERATE psum_block;

      -- now add the 9 products and the partial sum.
      -- if a fp value is -ve then we add the complement of its product
      -- the 1s for all -ve value are separately added together (?necessary?)
      sum_block: IF true GENERATE
        SIGNAL  prod_bfi4       : array_bit64_type(FILTER_DIM2);
        SIGNAL  ones_bfi4       : bit4_type;
        SIGNAL  psum_bfi4       : bit64_type;
      BEGIN
        prod_gen: FOR j IN FILTER_DIM2 GENERATE
          prod_bfi4(j)   <= "00000" & prod_fq4(j) WHEN sign_fq4(j) = "0" ELSE
                            "11111" & (NOT prod_fq4(j));
        END GENERATE prod_gen;

        ones_bfi4 <=    std_logic_vector(
                            unsigned( bit4_type'("000"&sign_fq4(0)))+
                            unsigned( bit4_type'("000"&sign_fq4(1)))+
                            unsigned( bit4_type'("000"&sign_fq4(2)))+
                            unsigned( bit4_type'("000"&sign_fq4(3)))+
                            unsigned( bit4_type'("000"&sign_fq4(4)))+
                            unsigned( bit4_type'("000"&sign_fq4(5)))+
                            unsigned( bit4_type'("000"&sign_fq4(6)))+
                            unsigned( bit4_type'("000"&sign_fq4(7)))+
                            unsigned( bit4_type'("000"&sign_fq4(8)))+
                            unsigned( bit4_type'("000"&sign_psum_fq4))
                            );

        psum_bfi4  <= (63 DOWNTO 44 => '0') & psum_fq4 & "00" WHEN sign_psum_fq4 = "0" ELSE
                      (63 DOWNTO 44 => '1' ) & (not psum_fq4) & "11";

        sum_i5(sl, fil) <= std_logic_vector(
                      unsigned(prod_bfi4(0)) +
                      unsigned(prod_bfi4(1)) +
                      unsigned(prod_bfi4(2)) +
                      unsigned(prod_bfi4(3)) +
                      unsigned(prod_bfi4(4)) +
                      unsigned(prod_bfi4(5)) +
                      unsigned(prod_bfi4(6)) +
                      unsigned(prod_bfi4(7)) +
                      unsigned(prod_bfi4(8)) +
                      unsigned(psum_bfi4) +
                      unsigned(ones_bfi4));
        sum_q6(sl, fil) <= sum_i5(sl, fil) AFTER 10 ps WHEN rising_edge(clock) AND slen_q5 = "1";
        a_slice_cvt_fp16: cvt_fp16 PORT MAP( sum_q6(sl, fil), res_slice_i6(sl, fil ));
      END GENERATE sum_block;

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
    VARIABLE  sum       : bit64_type;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 102 ps;


    IF now >= start_print THEN
      IF slen_i4 = "1" THEN
        dump_cycle( buf, "FILT>" );
        dump_integer( buf, sl, " SL#" );
        dump_integer( buf, fil, " FIL#", " WT#" );
        FOR i IN FILTER_DIM2 LOOP
          dump_fp16( buf, wt_fi2(i), " " );
        END LOOP;
        writeline( output, buf );

        dump_cycle( buf, "FILT>" );
        dump_integer( buf, sl, " SL#" );
        dump_integer( buf, fil, " FIL#", " IN#" );
        FOR i IN FILTER_DIM2 LOOP
          dump_fp16( buf, in_fi2(i), " " );
        END LOOP;
        writeline( output, buf );
      END IF;

      IF slen_i5 = "1" THEN

        dump_cycle( buf, "FILT>" );
        dump_integer( buf, sl, " SL#" );
        dump_integer( buf, fil, " FIL#" );
        sum := sum_i5(sl, fil);
        dump_hex( buf, sum, " SUM " );
        dump_string( buf, " PROD " );
        FOR i IN FILTER_DIM2 LOOP
          dump_bit( buf, sign_fq4( i), " (" );
          dump_hex( buf, prod_fq4( i), ",", ") " );
        END LOOP;
        writeline( output, buf );

      END IF;


    END IF;

  END PROCESS dump;
-- pragma translate_on

    END GENERATE filter_gen;

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
    VARIABLE  wt_l      : array2_fp16_type( FILTER_DIM, FILTER_DIM );
    VARIABLE  in_l      : array3_fp16_type( FILTER_RANGE, FILTER_DIM, FILTER_DIM );
    VARIABLE  diff      : boolean;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 102 ps;

    IF now >= start_print THEN

      IF slen_i2 = "1" THEN
        dump_cycle( buf, "FILT>" );
        dump_integer( buf, sl, " SL#" );
        dump_bit( buf, slen_i2, " slen_i2 " );
        dump_bit( buf, en_wt_i2, " en_wt_i2 [" );

        FOR i IN words_i2'range LOOP
          dump_fp16( buf, words_i2(i), " " );
        END LOOP;
        dump_string( buf, " ]");
        writeline( output, buf );
      END IF;

      diff := false;
      FOR i IN FILTER_DIM LOOP
        FOR j IN FILTER_DIM LOOP
          IF wt_l(i, j) /= wt_q(i, j ) THEN
            diff := true;
          END IF;
        END LOOP;
      END LOOP;

      IF diff THEN
        dump_cycle( buf, "FILT>" );
        dump_integer( buf, sl, " WT#", " [" );
        FOR i IN FILTER_DIM LOOP
          dump_string( buf, "[" );
          FOR j IN FILTER_DIM LOOP
            dump_fp16( buf, wt_q(i, j), " " );
            wt_l(i, j ) := wt_q(i, j);
          END LOOP;
          dump_string( buf, "]" );
        END LOOP;
        writeline( output, buf );
      END IF;


      FOR n IN FILTER_RANGE LOOP
        diff := false;
        FOR i IN FILTER_DIM LOOP
          FOR j IN FILTER_DIM LOOP
            IF in_l(n, i, j) /= in_q(n, i, j ) THEN
              diff := true;
            END IF;
          END LOOP;
        END LOOP;
        IF diff THEN
          dump_cycle( buf, "FILT>" );
          dump_integer( buf, sl, " IN#" );
          dump_integer( buf, n, ".", " [" );
          FOR i IN FILTER_DIM LOOP
            dump_string( buf, "[" );
            FOR j IN FILTER_DIM LOOP
              dump_fp16( buf, in_q(n, i, j), " " );
              in_l(n, i, j ) := in_q(n, i, j);
            END LOOP;
            dump_string( buf, "]" );
          END LOOP;
          writeline( output, buf );
        END IF;
      END LOOP;

      IF slen_q6 = "1" THEN
        dump_cycle( buf, "FILT>" );
        dump_integer( buf, sl, " SUM#", " [" );
        FOR fil IN FILTER_RANGE LOOP
          dump_hex( buf,  res_slice_i6(sl, fil ), " ");
          dump_fp16( buf,  res_slice_i6(sl, fil ), " ");
        END LOOP;
        dump_string( buf, " ]" );
        writeline(output, buf);
      END IF;
    END IF;
  END PROCESS dump;
-- pragma translate_on
  END GENERATE slice_gen;

  -- if in GLOBAL mode, the outputs of all slices must be added together
  across_gen: FOR fil IN FILTER_RANGE GENERATE
    SIGNAL  xsum_gi6    : bit66_type;
    SIGNAL  xsum_gq7    : bit66_type;
    SIGNAL  sum0_i6     : array_bit64_type(SLICE_RANGE);
  BEGIN
    sum0_gen: FOR sl IN SLICE_RANGE GENERATE
      sum0_i6(sl) <= sum_q6(sl,fil) WHEN slice_en_q6(sl) = '1' ELSE (OTHERS => '0');
    END GENERATE sum0_gen;

    xsum_gi6        <=  std_logic_vector(
                          resize(signed(sum0_i6(0)),66)+
                          resize(signed(sum0_i6(1)),66)+
                          resize(signed(sum0_i6(2)),66)+
                          resize(signed(sum0_i6(3)),66)+
                          resize(signed(sum0_i6(4)),66)+
                          resize(signed(sum0_i6(5)),66)+
                          resize(signed(sum0_i6(6)),66)+
                          resize(signed(sum0_i6(7)),66)
                        );
    xsum_gq7      <= xsum_gi6  AFTER 10 ps WHEN rising_edge(clock);
    a_slice_cvt_fp16: cvt_fp16 PORT MAP( xsum_gq7, res_global_i7(fil));
  END GENERATE across_gen;

  res_block: IF true GENERATE
    SIGNAL  en_cmd_bi6       : bit_type;
    SIGNAL  slice1_bi6       : slice_mask_type;
    SIGNAL  is_slice_bi6     : bit_type;
    SIGNAL  width_bi6        : bit_type;
    SIGNAL  slice_bi6        : slice_mask_type;
    SIGNAL  word_s_bi7       : array_bit64_type(SLICE_RANGE);
    SIGNAL  word_g_bi7       : bit64_type;

    SIGNAL  en_cmd_bq8       : bit_type;
    SIGNAL  width_bq8        : bit_type;
    SIGNAL  slice_bq8        : slice_mask_type;
    SIGNAL  addr_bq8         : slice_addr_type;
    SIGNAL  word_bq8         : array_bit64_type(SLICE_RANGE);

    SIGNAL  en_cmd_bi8       : bit_type;

    SIGNAL  en_cmd_bq7      : bit_type;
    SIGNAL  en_cmd_bi7      : bit_type;
    SIGNAL  is_slice_bi7    : bit_type;
    SIGNAL  width_bq7       : bit_type;
    SIGNAL  slice_bq7       : slice_mask_type;
    SIGNAL  addr_bq7        : slice_addr_type;

  BEGIN
    slice1_bi6             <= slice_sel_to_mask( base0_cmd_q6(SLICE_SEL_ADDR_MEM) );
    is_slice_bi6           <= "1" WHEN ctl_cmd_q6(MODE_FILTER_INSN_FIELD) = SLICE_MODE_FILTER ELSE "0";
    is_slice_bi7           <= is_slice_bi6  AFTER 10 ps WHEN rising_edge(clock);
    slice_bi6              <= slice_en_q6 WHEN is_slice_bi6 = "1" ELSE slice1_bi6;

    en_cmd_bi6             <= "0" WHEN reset = "1" ELSE
                             "0" WHEN en_cmd_q6 = "0" ELSE
                             "0" WHEN slice_bi6 = "0" ELSE
                             "1";
    en_cmd_bq7            <=  en_cmd_bi6  AFTER 10 ps WHEN rising_edge(clock);
    en_cmd_bi7            <= "0" WHEN reset = "1" ELSE en_cmd_bq7;

    en_cmd_bq8             <= en_cmd_bi7 ; -- MOVE TO GLOBAL AFTER 10 ps WHEN rising_edge(clock);
    en_cmd_bi8             <= "0" WHEN reset = "1" ELSE en_cmd_bq8;
    en_mem_res_busout     <= en_cmd_bi8;

    width_bi6              <= "1";
    width_bq7              <= width_bi6 AFTER 10 ps WHEN rising_edge(clock) ; -- DISPABLE enable AND en_cmd_bi6 = "1";
    width_bq8              <= width_bq7 ; -- MOVE TO GLOBAL  AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_bi7 = "1";
    width_mem_res_busout  <= width_bq8;

    slice_bq7              <= slice_bi6 AFTER 10 ps WHEN rising_edge(clock) ; -- DISPABLE enable AND en_cmd_bi6 = "1";
    slice_bq8              <= slice_bq7   ; -- MOVE TO GLOBAL AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_bi7 = "1";
    slice_mem_res_busout  <= slice_bq8;

    addr_bq7               <= base0_cmd_q6(slice_addr_type'range) AFTER 10 ps WHEN rising_edge(clock) ; -- DISPABLE enable AND en_cmd_bi6 = "1";
    addr_bq8              <= addr_bq7   ; -- MOVE TO GLOBAL AFTER 10 ps WHEN rising_edge(clock) AND en_cmd_bi7 = "1";
    addr_mem_res_busout   <= addr_bq8;

    filter_gen: FOR fil IN FILTER_RANGE GENERATE
    BEGIN
      slice1_gen: FOR sl IN SLICE_RANGE GENERATE
      BEGIN
        word_s_bi7(sl)(16*fil+15 DOWNTO 16*fil) <= res_slice_i6(sl, fil)  AFTER 10 ps WHEN rising_edge(clock);
      END GENERATE slice1_gen;
      word_g_bi7(16*fil+15 DOWNTO 16*fil) <= res_global_i7(fil);
    END GENERATE filter_gen;

    slice_gen: FOR sl IN SLICE_RANGE GENERATE
    SIGNAL  word_bi7         : bit64_type;
    BEGIN
      word_bi7       <= word_s_bi7(sl) WHEN is_slice_bi7 = "1" ELSE word_g_bi7;
      word_bq8(sl)   <= word_bi7 ; -- MOVE TO GLOBAL     AFTER 10 ps WHEN rising_edge(clock) and en_cmd_bi7 = "1";
    END GENERATE slice_gen;

    word0_mem_res_busout <= word_bq8(0);
    word1_mem_res_busout <= word_bq8(1);
    word2_mem_res_busout <= word_bq8(2);
    word3_mem_res_busout <= word_bq8(3);
    word4_mem_res_busout <= word_bq8(4);
    word5_mem_res_busout <= word_bq8(5);
    word6_mem_res_busout <= word_bq8(6);
    word7_mem_res_busout <= word_bq8(7);
  END GENERATE res_block;

-- pragma translate_off
  dump: PROCESS
    VARIABLE  buf       : line;
    VARIABLE  wt_l      : array2_fp16_type( FILTER_DIM, FILTER_DIM );
    VARIABLE  diff      : boolean;
  BEGIN
    IF disable_print THEN
      WAIT;
    END IF;
    WAIT UNTIL falling_edge( clock );
    WAIT FOR 102 ps;

    IF now >= start_print THEN

      IF en_cmd_in = "1" THEN
        dump_cycle( buf, "FILT>" );
        dump_bit( buf, en_cmd_in, " en_cmd_in " );
        dump_insn( buf, insn_cmd_in, " " );
        dump_hex( buf, ctl_cmd_in, " ctl " );
        dump_hex( buf, base0_cmd_in, " base0 " );
        dump_hex( buf, base1_cmd_in, " base1 " );
        dump_hex( buf, base2_cmd_in, " base2 " );
        dump_hex( buf, spec_cmd_in, " spec " );
        dump_hex( buf, slice_en_i0, " slice_en_i0 " );
        dump_bit( buf, en_psum_i0, " en_psum_i0 " );
        dump_bit( buf, slice_psum1_i0, " slice_psum1_i0 " );
        dump_bit( buf, nzero_psum_i0, " nzero_psum_i0 " );
        dump_bit( buf, slice_psum_i0, " slice_psum_i0 " );
        writeline( output, buf );
      END IF;

      IF en_cmd_q2 = "1" THEN
        dump_cycle( buf, "FILT>" );
        dump_bit( buf, en_cmd_q2, " en_cmd_q2 " );
        dump_insn( buf, insn_cmd_q2, " " );
        dump_hex( buf, ctl_cmd_q2, " ctl " );
        dump_hex( buf, ctr_cmd_q2, " ctr " );
        dump_hex( buf, base0_cmd_q2, " base0 " );
        dump_hex( buf, slice_en_q2, " slice_en " );
        dump_hex( buf, nzero_psum_q2, " nzero_psum " );

        dump_string( buf, " data [" );
        FOR i IN SLICE_RANGE LOOP
          IF slice_en_q2(i) = '1' THEN
            dump_hex( buf, word_data_i2(i), " " );
          ELSE
            dump_integer( buf, i, " __", "__" );
          END IF;
        END LOOP;
        dump_string( buf, "]" );

        IF nzero_psum_q2 /= (slice_mask_type'range => '0') THEN
          dump_string( buf, " psum [" );
          FOR i IN SLICE_RANGE LOOP
            IF nzero_psum_q2(i) = '1' THEN
              dump_hex( buf, word_psum_i2(i), " " );
            ELSE
              dump_integer( buf, i, " __", "__" );
            END IF;
          END LOOP;
          dump_string( buf, "]" );
        END IF;

        writeline( output, buf );
      END IF;

    END IF;

  END PROCESS dump;
-- pragma translate_on

END ARCHITECTURE behavior;
