`define vcs

`include "e203_defines.v"

module tb_top();

reg  gsnn_in_clk;
reg  gsnn_in_rst_n;

reg en_pxi_req_in,we_pxi_req_in;
reg [39:0] addr_pxi_req_in;
reg [31:0] word_pxi_req_in;


wire pull_pxi_req_busout;
wire pxi_clk;
wire pxi_reset;

// set clock freq
always begin 
     #2 gsnn_in_clk <= ~gsnn_in_clk;
end

// main process
initial begin
    gsnn_in_clk <= 0;
    gsnn_in_rst_n <= 0;

    en_pxi_req_in <= 1'b0;
    we_pxi_req_in <= 1'b0;
    addr_pxi_req_in <= 40'd0;
    word_pxi_req_in <= 32'd0;

    #120
    gsnn_in_rst_n <= 1;

    #20
    wait(pxi_reset == 1'b0);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h00},32'hcc5f0000);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h04},32'h07e00001);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h08},32'h04200000);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h0c},32'h04001000);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h10},32'h980107ff);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h14},32'h04400005);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h18},32'hd420000a);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h1c},32'h57c2000f);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h20},32'h0fe04000);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h24},32'hc4000000);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h28},32'hc462000d);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h2c},32'h04400000);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h30},32'hc0000800);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h34},32'h980107ff);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h38},32'h980207ff);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h3c},32'h1c420001);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h40},32'hd420000a);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h44},32'h80600000);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h48},32'h10421800);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h4c},32'h80200001);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h50},32'h14000002);
    pxi_master_write({22'd0,1'b0,2'b00,1'b0,14'h54},32'hc0000800);

    #20
    @(posedge pxi_clk);
    pxi_master_write({22'd0,1'b0,2'b01,1'b0,14'h0},32'hccb10000);

    #20
    @(posedge pxi_clk);
    pxi_master_write({22'd0,1'b0,2'b10,1'b0,14'h0},32'hccb20000);

    #20
    @(posedge pxi_clk);
    pxi_master_write({22'd0,1'b0,2'b11,1'b0,14'h0},32'hccb30000);
    
    #20
    @(posedge pxi_clk);
    pxi_master_write({22'd0,18'h200c4},32'h00000001);

    // #1000
    // $display("Finish");
    // $finish;
end



task automatic pxi_master_write;
    input [39:0] dw_addr;
    input [31:0] dw_data;
begin


    @(posedge pxi_clk);
    en_pxi_req_in <= 1'b1;
    we_pxi_req_in <= 1'b1;
    addr_pxi_req_in <= dw_addr;
    word_pxi_req_in <= dw_data;

    @(posedge pxi_clk) begin
      while(pull_pxi_req_busout == 0) begin
            @(posedge pxi_clk);
      end
        
      #1
      en_pxi_req_in <= 1'b0;
      we_pxi_req_in <= 1'b0;
      addr_pxi_req_in <= 40'd0;
      word_pxi_req_in <= 32'd0;   
    end

    $display("!!! task done for address :%h !!!",dw_addr);

end
endtask




integer dumpwave;
// vcs simulation support
initial begin
    if($value$plusargs("DUMPWAVE=%d",dumpwave)) begin
        if(dumpwave != 0) begin

	    `ifdef vcs
            $display("VCS used");
            $fsdbDumpfile("tb_top.fsdb");
            $fsdbDumpvars(0, tb_top, "+mda");
        `endif
        end
    end
end

gsnn_unit_top gsnn_unit_top_inst (
  // 输入端口
  .clock (gsnn_in_clk),
  .reset (!gsnn_in_rst_n),
  .slp_in ('d0),
  .sd_in ('d0),

  .en_pxi_req_in (en_pxi_req_in),
  .we_pxi_req_in (we_pxi_req_in),
  .addr_pxi_req_in (addr_pxi_req_in),
  .word_pxi_req_in (word_pxi_req_in),
  .avail_pxi_resp_in ('d0),
  .en_pxi_resp_regout (),
  .addr_pxi_resp_regout (),
  .word_pxi_resp_regout (),
  .interrupt_pxi_regout (),
  .arready_axi_in ('d0),
  .rid_axi_in ('d0),
  .rdata_axi_in ('d0),
  .rresp_axi_in ('d0),
  .rlast_axi_in ('d0),
  .rvalid_axi_in ('d0),
  .awready_axi_in ('d0),
  .wready_axi_in ('d0),
  .bid_axi_in ('d0),
  .bresp_axi_in ('d0),
  .bvalid_axi_in ('d0),

  // 输出端口
  .pxi_clock (pxi_clk),
  .pxi_reset (pxi_reset),
  .pull_pxi_req_busout (pull_pxi_req_busout),
  .axi_clock (),
  .axi_reset (),
  .arqos_axi_busout (),
  .arid_axi_busout (),
  .araddr_axi_busout (),
  .arlen_axi_busout (),
  .arsize_axi_busout (),
  .arburst_axi_busout (),
  .arlock_axi_busout (),
  .arvalid_axi_busout (),
  .rready_axi_busout (),
  .awid_axi_regout (),
  .awaddr_axi_regout (),
  .awlen_axi_regout (),
  .awsize_axi_regout (),
  .awburst_axi_regout (),
  .awlock_axi_regout (),
  .awvalid_axi_regout (),
  .wid_axi_regout (),
  .wdata_axi_regout (),
  .wstrb_axi_regout (),
  .wlast_axi_regout (),
  .wvalid_axi_regout (),
  .bready_axi_regout ()
);



  reg  clk;
  reg  lfextclk;
  reg  rst_n;

  wire hfclk = clk;

  `define CPU_TOP u_e203_soc_top.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top
  `define EXU `CPU_TOP.u_e203_cpu.u_e203_core.u_e203_exu
  `define ITCM `CPU_TOP.u_e203_srams.u_e203_itcm_ram.u_e203_itcm_gnrl_ram.u_sirv_sim_ram

  `define PC_WRITE_TOHOST       `E203_PC_SIZE'h800000b8
  `define PC_EXT_IRQ_BEFOR_MRET `E203_PC_SIZE'h800000a6
  `define PC_SFT_IRQ_BEFOR_MRET `E203_PC_SIZE'h800000be
  `define PC_TMR_IRQ_BEFOR_MRET `E203_PC_SIZE'h800000d6
  `define PC_AFTER_SETMTVEC     `E203_PC_SIZE'h8000015C

  wire [`E203_XLEN-1:0] x3 = `EXU.u_e203_exu_regfile.rf_r[3];
  wire [`E203_PC_SIZE-1:0] pc = `EXU.u_e203_exu_commit.alu_cmt_i_pc;
  wire [`E203_PC_SIZE-1:0] pc_vld = `EXU.u_e203_exu_commit.alu_cmt_i_valid;

  reg [31:0] pc_write_to_host_cnt;
  reg [31:0] pc_write_to_host_cycle;
  reg [31:0] valid_ir_cycle;
  reg [31:0] cycle_count;
  reg pc_write_to_host_flag;

  always @(posedge hfclk or negedge rst_n)
  begin 
    if(rst_n == 1'b0) begin
        pc_write_to_host_cnt <= 32'b0;
        pc_write_to_host_flag <= 1'b0;
        pc_write_to_host_cycle <= 32'b0;
    end
    else if (pc_vld & (pc == `PC_WRITE_TOHOST)) begin
        pc_write_to_host_cnt <= pc_write_to_host_cnt + 1'b1;
        pc_write_to_host_flag <= 1'b1;
        if (pc_write_to_host_flag == 1'b0) begin
            pc_write_to_host_cycle <= cycle_count;
        end
    end
  end

  always @(posedge hfclk or negedge rst_n)
  begin 
    if(rst_n == 1'b0) begin
        cycle_count <= 32'b0;
    end
    else begin
        cycle_count <= cycle_count + 1'b1;
    end
  end

  wire i_valid = `EXU.i_valid;
  wire i_ready = `EXU.i_ready;

  always @(posedge hfclk or negedge rst_n)
  begin 
    if(rst_n == 1'b0) begin
        valid_ir_cycle <= 32'b0;
    end
    else if(i_valid & i_ready & (pc_write_to_host_flag == 1'b0)) begin
        valid_ir_cycle <= valid_ir_cycle + 1'b1;
    end
  end


  // Randomly force the external interrupt
  `define EXT_IRQ u_e203_soc_top.u_e203_subsys_top.u_e203_subsys_main.plic_ext_irq
  `define SFT_IRQ u_e203_soc_top.u_e203_subsys_top.u_e203_subsys_main.clint_sft_irq
  `define TMR_IRQ u_e203_soc_top.u_e203_subsys_top.u_e203_subsys_main.clint_tmr_irq

  `define U_CPU u_e203_soc_top.u_e203_subsys_top.u_e203_subsys_main.u_e203_cpu_top.u_e203_cpu
  `define ITCM_BUS_ERR `U_CPU.u_e203_itcm_ctrl.sram_icb_rsp_err
  `define ITCM_BUS_READ `U_CPU.u_e203_itcm_ctrl.sram_icb_rsp_read
  `define STATUS_MIE   `U_CPU.u_e203_core.u_e203_exu.u_e203_exu_commit.u_e203_exu_excp.status_mie_r

  wire stop_assert_irq = (pc_write_to_host_cnt > 32);

  reg tb_itcm_bus_err;

  reg tb_ext_irq;
  reg tb_tmr_irq;
  reg tb_sft_irq;
  initial begin
    tb_ext_irq = 1'b0;
    tb_tmr_irq = 1'b0;
    tb_sft_irq = 1'b0;
  end

`ifdef ENABLE_TB_FORCE
  initial begin
    tb_itcm_bus_err = 1'b0;
    #100
    @(pc == `PC_AFTER_SETMTVEC ) // Wait the program goes out the reset_vector program
    forever begin
      repeat ($urandom_range(1, 20)) @(posedge clk) tb_itcm_bus_err = 1'b0; // Wait random times
      repeat ($urandom_range(1, 200)) @(posedge clk) tb_itcm_bus_err = 1'b1; // Wait random times
      if(stop_assert_irq) begin
          break;
      end
    end
  end


  initial begin
    force `EXT_IRQ = tb_ext_irq;
    force `SFT_IRQ = tb_sft_irq;
    force `TMR_IRQ = tb_tmr_irq;
       // We force the bus-error only when:
       //   It is in common code, not in exception code, by checking MIE bit
       //   It is in read operation, not write, otherwise the test cannot recover
    force `ITCM_BUS_ERR = tb_itcm_bus_err
                        & `STATUS_MIE 
                        & `ITCM_BUS_READ
                        ;
  end


  initial begin
    #100
    @(pc == `PC_AFTER_SETMTVEC ) // Wait the program goes out the reset_vector program
    forever begin
      repeat ($urandom_range(1, 1000)) @(posedge clk) tb_ext_irq = 1'b0; // Wait random times
      tb_ext_irq = 1'b1; // assert the irq
      @((pc == `PC_EXT_IRQ_BEFOR_MRET)) // Wait the program run into the IRQ handler by check PC values
      tb_ext_irq = 1'b0;
      if(stop_assert_irq) begin
          break;
      end
    end
  end

  initial begin
    #100
    @(pc == `PC_AFTER_SETMTVEC ) // Wait the program goes out the reset_vector program
    forever begin
      repeat ($urandom_range(1, 1000)) @(posedge clk) tb_sft_irq = 1'b0; // Wait random times
      tb_sft_irq = 1'b1; // assert the irq
      @((pc == `PC_SFT_IRQ_BEFOR_MRET)) // Wait the program run into the IRQ handler by check PC values
      tb_sft_irq = 1'b0;
      if(stop_assert_irq) begin
          break;
      end
    end
  end

  initial begin
    #100
    @(pc == `PC_AFTER_SETMTVEC ) // Wait the program goes out the reset_vector program
    forever begin
      repeat ($urandom_range(1, 1000)) @(posedge clk) tb_tmr_irq = 1'b0; // Wait random times
      tb_tmr_irq = 1'b1; // assert the irq
      @((pc == `PC_TMR_IRQ_BEFOR_MRET)) // Wait the program run into the IRQ handler by check PC values
      tb_tmr_irq = 1'b0;
      if(stop_assert_irq) begin
          break;
      end
    end
  end
`endif

  reg[8*300:1] testcase;
  integer dumpwave;

  initial begin
    $display("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");  
    if($value$plusargs("TESTCASE=%s",testcase))begin
      $display("TESTCASE=%s",testcase);
    end

    pc_write_to_host_flag <=0;
    clk   <=0;
    lfextclk   <=0;
    rst_n <=0;
    #120 rst_n <=1;

    @(pc_write_to_host_cnt == 32'd8) #10 rst_n <=1;
`ifdef ENABLE_TB_FORCE
    @((~tb_tmr_irq) & (~tb_sft_irq) & (~tb_ext_irq)) #10 rst_n <=1;// Wait the interrupt to complete
`endif

        $display("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~~~~~ Test Result Summary ~~~~~~~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        $display("~TESTCASE: %s ~~~~~~~~~~~~~", testcase);
        $display("~~~~~~~~~~~~~~Total cycle_count value: %d ~~~~~~~~~~~~~", cycle_count);
        $display("~~~~~~~~~~The valid Instruction Count: %d ~~~~~~~~~~~~~", valid_ir_cycle);
        $display("~~~~~The test ending reached at cycle: %d ~~~~~~~~~~~~~", pc_write_to_host_cycle);
        $display("~~~~~~~~~~~~~~~The final x3 Reg value: %d ~~~~~~~~~~~~~", x3);
        $display("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
  //  if (x3 == 1) begin
        $display("~~~~~~~~~~~~~~~~ TEST_PASS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~ #####     ##     ####    #### ~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~ #    #   #  #   #       #     ~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~ #    #  #    #   ####    #### ~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~ #####   ######       #       #~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~ #       #    #  #    #  #    #~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~ #       #    #   ####    #### ~~~~~~~~~~~~~~~~");
        $display("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
  //   end
  //   else begin
  //      $display("~~~~~~~~~~~~~~~~ TEST_FAIL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
  //      $display("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
  //      $display("~~~~~~~~~~######    ##       #    #     ~~~~~~~~~~~~~~~~");
  //      $display("~~~~~~~~~~#        #  #      #    #     ~~~~~~~~~~~~~~~~");
  //      $display("~~~~~~~~~~#####   #    #     #    #     ~~~~~~~~~~~~~~~~");
  //      $display("~~~~~~~~~~#       ######     #    #     ~~~~~~~~~~~~~~~~");
  //      $display("~~~~~~~~~~#       #    #     #    #     ~~~~~~~~~~~~~~~~");
  //       $display("~~~~~~~~~~#       #    #     #    ######~~~~~~~~~~~~~~~~");
  //       $display("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
  //  end
    #10
     $finish;
  end

  initial begin
    #40000000
        $display("Time Out !!!");
     $finish;
  end

  always
  begin 
     #2 clk <= ~clk;
  end

  always
  begin 
     #33 lfextclk <= ~lfextclk;
  end

  integer i;

    reg [7:0] itcm_mem [0:(`E203_ITCM_RAM_DP*8)-1];
    initial begin
      $readmemh({testcase, ".verilog"}, itcm_mem);

      for (i=0;i<(`E203_ITCM_RAM_DP);i=i+1) begin
          `ITCM.mem_r[i][00+7:00] = itcm_mem[i*8+0];
          `ITCM.mem_r[i][08+7:08] = itcm_mem[i*8+1];
          `ITCM.mem_r[i][16+7:16] = itcm_mem[i*8+2];
          `ITCM.mem_r[i][24+7:24] = itcm_mem[i*8+3];
          `ITCM.mem_r[i][32+7:32] = itcm_mem[i*8+4];
          `ITCM.mem_r[i][40+7:40] = itcm_mem[i*8+5];
          `ITCM.mem_r[i][48+7:48] = itcm_mem[i*8+6];
          `ITCM.mem_r[i][56+7:56] = itcm_mem[i*8+7];
      end

        $display("ITCM 0x00: %h", `ITCM.mem_r[8'h00]);
        $display("ITCM 0x01: %h", `ITCM.mem_r[8'h01]);
        $display("ITCM 0x02: %h", `ITCM.mem_r[8'h02]);
        $display("ITCM 0x03: %h", `ITCM.mem_r[8'h03]);
        $display("ITCM 0x04: %h", `ITCM.mem_r[8'h04]);
        $display("ITCM 0x05: %h", `ITCM.mem_r[8'h05]);
        $display("ITCM 0x06: %h", `ITCM.mem_r[8'h06]);
        $display("ITCM 0x07: %h", `ITCM.mem_r[8'h07]);
        $display("ITCM 0x16: %h", `ITCM.mem_r[8'h16]);
        $display("ITCM 0x20: %h", `ITCM.mem_r[8'h20]);

    end 



  wire jtag_TDI = 1'b0;
  wire jtag_TDO;
  wire jtag_TCK = 1'b0;
  wire jtag_TMS = 1'b0;
  wire jtag_TRST = 1'b0;

  wire jtag_DRV_TDO = 1'b0;


e203_soc_top u_e203_soc_top(
   
   .hfextclk(hfclk),
   .hfxoscen(),

   .lfextclk(lfextclk),
   .lfxoscen(),

   .io_pads_jtag_TCK_i_ival (jtag_TCK),
   .io_pads_jtag_TMS_i_ival (jtag_TMS),
   .io_pads_jtag_TDI_i_ival (jtag_TDI),
   .io_pads_jtag_TDO_o_oval (jtag_TDO),
   .io_pads_jtag_TDO_o_oe (),

   .io_pads_gpioA_i_ival(32'b0),
   .io_pads_gpioA_o_oval(),
   .io_pads_gpioA_o_oe  (),

   .io_pads_gpioB_i_ival(32'b0),
   .io_pads_gpioB_o_oval(),
   .io_pads_gpioB_o_oe  (),

   .io_pads_qspi0_sck_o_oval (),
   .io_pads_qspi0_cs_0_o_oval(),
   .io_pads_qspi0_dq_0_i_ival(1'b1),
   .io_pads_qspi0_dq_0_o_oval(),
   .io_pads_qspi0_dq_0_o_oe  (),
   .io_pads_qspi0_dq_1_i_ival(1'b1),
   .io_pads_qspi0_dq_1_o_oval(),
   .io_pads_qspi0_dq_1_o_oe  (),
   .io_pads_qspi0_dq_2_i_ival(1'b1),
   .io_pads_qspi0_dq_2_o_oval(),
   .io_pads_qspi0_dq_2_o_oe  (),
   .io_pads_qspi0_dq_3_i_ival(1'b1),
   .io_pads_qspi0_dq_3_o_oval(),
   .io_pads_qspi0_dq_3_o_oe  (),

   .io_pads_aon_erst_n_i_ival (rst_n),//This is the real reset, active low
   .io_pads_aon_pmu_dwakeup_n_i_ival (1'b1),

   .io_pads_aon_pmu_vddpaden_o_oval (),
    .io_pads_aon_pmu_padrst_o_oval    (),

    .io_pads_bootrom_n_i_ival       (1'b0),// In Simulation we boot from ROM
    .io_pads_dbgmode0_n_i_ival       (1'b1),
    .io_pads_dbgmode1_n_i_ival       (1'b1),
    .io_pads_dbgmode2_n_i_ival       (1'b1) 
);


endmodule


