PRESENT_DIR := ${PWD}
RUN_DIR := ${PRESENT_DIR}/run
LOG_DIR := ${RUN_DIR}/log

SCRIPT_DIR := ${PRESENT_DIR}/script
FILELIST_DIR := ${SCRIPT_DIR}/filelists

UNISIM_OUT_DIR := ${RUN_DIR}/o_unisim
WORK_OUT_DIR  := ${RUN_DIR}/o_work

E203_SRC_DIR := ${PWD}/e203/rtl

TESTCASE := tb_top
TESTTB_NAME := ${TESTCASE}.v
TEST_RUNDIR  := ${TESTCASE}_dir

TEST_TB_REALPATH := ${PRESENT_DIR}/tb/${TESTTB_NAME}

XILINX_UNISIM_FILELIST := ${FILELIST_DIR}/xilinx_unisim_filelist.f
GSNN_SRC_FILELIST := ${FILELIST_DIR}/gsnn_src_filelist.f
GSNN_IP_FILELIST := ${FILELIST_DIR}/gsnn_ip_filelist.f
E203_SRC_FILELIST := ${FILELIST_DIR}/e203_src_filelist.f

E203_INCLUDE_OPT := +incdir+"${E203_SRC_DIR}/include/"+"${E203_SRC_DIR}/perips/"+"${E203_SRC_DIR}/perips/apb_i2c/"

SETUP_FILE := ${RUN_DIR}/synopsys_sim.setup

E203_INSTR_TESTCASE := ${PWD}/e203/instr_verilog/helloworld

all: compile elaborate simulate wave

${SETUP_FILE}:
	rm -rf ${RUN_DIR};
	mkdir -p ${RUN_DIR} && touch ${SETUP_FILE}

create_setup: ${SETUP_FILE}
	mkdir -p ${UNISIM_OUT_DIR} && mkdir -p ${WORK_OUT_DIR} &&  mkdir -p ${LOG_DIR};
	echo "UNISIM : ${UNISIM_OUT_DIR}" >> ${SETUP_FILE};
	echo "WORK : ${WORK_OUT_DIR}" >> ${SETUP_FILE};

compile_unisim: create_setup
	cd ${RUN_DIR} && vhdlan -kdb -full64 -nc -work UNISIM -f ${XILINX_UNISIM_FILELIST} > ${LOG_DIR}/unisim_compile.log;
	cd ${UNISIM_OUT_DIR} && llib -full64
	@echo "!!!!!!!!!!!!!!!!compile_unisim done!!!!!!!!!!!!!!!!"


compile_gsnn_ip: compile_unisim
	cd ${RUN_DIR} && vlogan -kdb -nc -full64 -work WORK -f ${GSNN_IP_FILELIST} > ${LOG_DIR}/gsnnip_compile.log;
	cd ${WORK_OUT_DIR} && llib -full64
	@echo "!!!!!!!!!!!!!!!!compile_gsnnip done!!!!!!!!!!!!!!!!"

compile_gsnn_src: compile_gsnn_ip
	cd ${RUN_DIR} && \
	vhdlan -kdb -nc -full64 -work WORK -f ${GSNN_SRC_FILELIST} > ${LOG_DIR}/gsnnsrc_compile.log && \
	vhdlan -kdb -nc -full64 -work WORK -f ${GSNN_SRC_FILELIST} > ${LOG_DIR}/gsnnsrc_compile.log && \
	cd ${WORK_OUT_DIR} && llib -full64 gsnn_unit_top
	@echo "!!!!!!!!!!!!!!!!compile_gsnnsrc done!!!!!!!!!!!!!!!!"

compile_e203_src: 
	cd ${RUN_DIR} && vlogan -kdb -nc -full64 -work WORK +v2k -sverilog ${E203_INCLUDE_OPT} -f ${E203_SRC_FILELIST} > ${LOG_DIR}/e203src_compile.log;
	cd ${WORK_OUT_DIR} && llib -full64 e203_soc_top
	@echo "!!!!!!!!!!!!!!!!compile_e203src done!!!!!!!!!!!!!!!!"

compile_tb: compile_gsnn_src compile_e203_src
	cd ${RUN_DIR} && \
	vlogan -kdb -full64 -nc -work WORK ${E203_INCLUDE_OPT} ${TEST_TB_REALPATH};
	cd ${WORK_OUT_DIR} && llib -full64 tb_top
	@echo "!!!!!!!!!!!!!!!!compile_gsnnsrc done!!!!!!!!!!!!!!!!"

compile: compile_tb

elaborate:
	cd ${RUN_DIR} && \
	vcs -full64  -timescale=1ns/10ps +lint=all,noSVA-NSVU,noVCDE,noUI,noSVA-CE,noSVA-DIU -q -debug_access+all  ${TESTCASE} > ${LOG_DIR}/elaboration.log;
	@if grep "Error" ${LOG_DIR}/elaboration.log; then \
        true && exit; \
    else \
        echo "!!!!!!!!!!! No error ^-^ For Advance check warning to logfile:${LOG_DIR}/elaboration.log"; \
    fi
	@echo "!!!!!!!!!!!!!!!!elaboration done!!!!!!!!!!!!!!!!"


simulate:
	cd ${RUN_DIR} && \
	${RUN_DIR}/simv +ntb_random_seed_automatic +DUMPWAVE=1 +TESTCASE=${E203_INSTR_TESTCASE} +SIM_TOOL=vcs
	@echo "!!!!!!!!!!!!!!!!simulation done!!!!!!!!!!!!!!!!"

wave:
	cd ${RUN_DIR} && \
	verdi -liblist ${UNISIM_OUT_DIR}/UNISIM ${WORK_OUT_DIR}/WORK -ssf ./${TESTCASE}.fsdb -sswr ${SCRIPT_DIR}/signal.rc

clean: 
	rm -rf ${RUN_DIR}

.PHONY: run clean all 